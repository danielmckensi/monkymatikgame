﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void ActivateOnKeypress::Start()
extern void ActivateOnKeypress_Start_mC9C71BD760F97DBC0F686569A13EA91AA8F643C6 ();
// 0x00000002 System.Void ActivateOnKeypress::Update()
extern void ActivateOnKeypress_Update_m7C06B9EF28D5420A29C17EC1D8D6C8239D489312 ();
// 0x00000003 System.Void ActivateOnKeypress::.ctor()
extern void ActivateOnKeypress__ctor_m75534AB7F9820B5C53A22B8244CE4BD87F1A5538 ();
// 0x00000004 System.Void PlayerMove::Update()
extern void PlayerMove_Update_m816082939684D509B053B2848EE5BB2A4B655F45 ();
// 0x00000005 System.Void PlayerMove::.ctor()
extern void PlayerMove__ctor_m95039BC415B767C2F57784CC458BB65C1188BFD7 ();
// 0x00000006 System.Void PlayerMoveOnSphere::Update()
extern void PlayerMoveOnSphere_Update_m8E045C01FD8CCCCFC85B46DDD68978730E97C9B5 ();
// 0x00000007 System.Void PlayerMoveOnSphere::.ctor()
extern void PlayerMoveOnSphere__ctor_m04885E872445EB4B0ADAB9967C4A3F88D2F8A26D ();
// 0x00000008 System.Void PlayerMovePhysics::Start()
extern void PlayerMovePhysics_Start_m68C6BC74B80E0258B34E06A348E6C32F8FB48154 ();
// 0x00000009 System.Void PlayerMovePhysics::OnEnable()
extern void PlayerMovePhysics_OnEnable_m43E1851523FAECBAEDD18FF7072145D99CE6CEDF ();
// 0x0000000A System.Void PlayerMovePhysics::FixedUpdate()
extern void PlayerMovePhysics_FixedUpdate_m41FE39EEE213101002AA90DB208FF4611B71B42E ();
// 0x0000000B System.Void PlayerMovePhysics::.ctor()
extern void PlayerMovePhysics__ctor_m4018D7D2888F136029F447C52A9DB1538F5D5064 ();
// 0x0000000C System.Void BejucoController::Awake()
extern void BejucoController_Awake_m795D66747ED8AB6D70E60E609312039CFDF739F9 ();
// 0x0000000D System.Void BejucoController::Start()
extern void BejucoController_Start_m18761ACCAC20C2B734018F62F7D81D05EB760330 ();
// 0x0000000E System.Void BejucoController::Update()
extern void BejucoController_Update_m2785342A43F73D03858EB28BCC340AA0CFBABD74 ();
// 0x0000000F System.Void BejucoController::FixedUpdate()
extern void BejucoController_FixedUpdate_m748A21EE1003312AC103B07902A1412984896853 ();
// 0x00000010 System.Void BejucoController::LateUpdate()
extern void BejucoController_LateUpdate_m27F7BD50A67CFBA6B218859321D78044D1D9D939 ();
// 0x00000011 System.Void BejucoController::Flip()
extern void BejucoController_Flip_m8C71F2D07618D312308C169CF4F4D3D59DEDA419 ();
// 0x00000012 System.Void BejucoController::.ctor()
extern void BejucoController__ctor_m8363B965BACF48CFDCB7C01427DB40E694A5482D ();
// 0x00000013 System.Void Bullet::Awake()
extern void Bullet_Awake_m37FD11C4734664815AF4EEF3C3814AEBCF88B6CD ();
// 0x00000014 System.Void Bullet::Start()
extern void Bullet_Start_m0C6AD7FCC791ADF08593B040294D8B7329617D04 ();
// 0x00000015 System.Void Bullet::Update()
extern void Bullet_Update_mEDD99D9C6CBBEBCD7C211792A42CBF1D965EB80E ();
// 0x00000016 System.Void Bullet::.ctor()
extern void Bullet__ctor_mFBA1E7297C133AE06ADA2EF2BA04567AD213A9D4 ();
// 0x00000017 System.Void ChangeColor::Awake()
extern void ChangeColor_Awake_m4A2850FB998ED3DB1E294F7107D4C72787F81977 ();
// 0x00000018 System.Void ChangeColor::Start()
extern void ChangeColor_Start_m6D47E8F30AEAB75D8E034168972BEC8C4D0A6869 ();
// 0x00000019 System.Void ChangeColor::Update()
extern void ChangeColor_Update_m0F7BC07040DEF2BFDAFF5F448E1A8D2F92534890 ();
// 0x0000001A System.Void ChangeColor::.ctor()
extern void ChangeColor__ctor_m6611480670D0461C94C966D87A0AB2BF4F992A55 ();
// 0x0000001B System.Void EnemyPatrol::Awake()
extern void EnemyPatrol_Awake_mB4793D74956B3910848A4B4DF141FE6A7F826A25 ();
// 0x0000001C System.Void EnemyPatrol::Start()
extern void EnemyPatrol_Start_mF69D7343C30458904BF576DC87C4EE0D94D5E913 ();
// 0x0000001D System.Void EnemyPatrol::Update()
extern void EnemyPatrol_Update_m9C9A815AFD2925201FC71D4D080ACA2A77307182 ();
// 0x0000001E System.Void EnemyPatrol::UpdateTarget()
extern void EnemyPatrol_UpdateTarget_mBF1D57F2C57B1B19634CFBEE098102D0F2C58819 ();
// 0x0000001F System.Collections.IEnumerator EnemyPatrol::PatrolToTarget()
extern void EnemyPatrol_PatrolToTarget_m675B9C77F20C755079DFE6B4AFFA4E1798C092ED ();
// 0x00000020 System.Void EnemyPatrol::CanShoot()
extern void EnemyPatrol_CanShoot_mFBD932DF4BC7E42F3CD1ED71F77C9646FB51C7CE ();
// 0x00000021 System.Void EnemyPatrol::.ctor()
extern void EnemyPatrol__ctor_mA0C5ED3222E96136C9E8930F5548D7CD32FE2AE2 ();
// 0x00000022 System.Void Weapon::Awake()
extern void Weapon_Awake_m2AC14C75327F3526C722CACC9F4CCD7D95B21F98 ();
// 0x00000023 System.Void Weapon::Start()
extern void Weapon_Start_m4FAB905F9926EEADEEF49D8B77218D1A14F459AC ();
// 0x00000024 System.Void Weapon::Update()
extern void Weapon_Update_mD65E9FD544FEE0974CED54FA39BB05A89FB6D99E ();
// 0x00000025 System.Void Weapon::Shoot()
extern void Weapon_Shoot_mF5B36E6C4428C1E8858F42526F5EEC7C7AF9D4E3 ();
// 0x00000026 System.Void Weapon::.ctor()
extern void Weapon__ctor_mF7C215ECC1D7032E6DE76DE606A9159F840B62FB ();
// 0x00000027 System.Void InstantiatePrefab::Instantiate()
extern void InstantiatePrefab_Instantiate_m31E43D0401E2D3691190E3780516A2EE9A881B23 ();
// 0x00000028 System.Void InstantiatePrefab::.ctor()
extern void InstantiatePrefab__ctor_mE32F9508E28E5A9AA2D84CA8CB9CB2E0FC7868E6 ();
// 0x00000029 System.Void LoadScene::OnMouseDown()
extern void LoadScene_OnMouseDown_mCD182BA2B1429157BDCC1BCC4E31AB9AA0B47EA5 ();
// 0x0000002A System.Void LoadScene::.ctor()
extern void LoadScene__ctor_m52B221B2B1C02A68F125B164DDE573DF0353A336 ();
// 0x0000002B System.Void PlayerController::Awake()
extern void PlayerController_Awake_m118C1D205A374B627E1EC963E5837E23CF554573 ();
// 0x0000002C System.Void PlayerController::Start()
extern void PlayerController_Start_mC0C9B9461D0BDAC48EC43715818A4BA63C4F45EF ();
// 0x0000002D System.Void PlayerController::Update()
extern void PlayerController_Update_m38903EF1C8F12B9388303741F8040EE26C33DC33 ();
// 0x0000002E System.Void PlayerController::FixedUpdate()
extern void PlayerController_FixedUpdate_m914EA3E3CCE4DF6AEB2E78317FFC1D507DACEBDE ();
// 0x0000002F System.Void PlayerController::LateUpdate()
extern void PlayerController_LateUpdate_mAF83D0B2354FDB3E9EEC98CB72E9D77EC004B881 ();
// 0x00000030 System.Void PlayerController::Flip()
extern void PlayerController_Flip_m829B4A4842ABD034CB56BCB1966D3C462A9D6AD4 ();
// 0x00000031 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_m648E40092E37395F4307411E855445886113CD60 ();
// 0x00000032 System.Void ZuroStreet::Awake()
extern void ZuroStreet_Awake_mF39B194AFCADDAB34B04D3D49D262BC9EE9B7336 ();
// 0x00000033 System.Void ZuroStreet::Start()
extern void ZuroStreet_Start_m36B79E7C1DA92EFFEDD8920CC13F24C189FBB39C ();
// 0x00000034 System.Void ZuroStreet::Update()
extern void ZuroStreet_Update_mF55F06C2957CFD22027E9DC239991E181ADB7228 ();
// 0x00000035 System.Void ZuroStreet::LateUpdate()
extern void ZuroStreet_LateUpdate_m0866F695A7D62507AF67715D8439F810D8155A73 ();
// 0x00000036 System.Void ZuroStreet::OnTriggerStay2D(UnityEngine.Collider2D)
extern void ZuroStreet_OnTriggerStay2D_m09FD5E717BF2CBB97B122544254BB7521866D51D ();
// 0x00000037 System.Void ZuroStreet::AttackMovement()
extern void ZuroStreet_AttackMovement_mFC008314CD71D1B0D3D15FD3ED1C5F3F778F78AA ();
// 0x00000038 System.Collections.IEnumerator ZuroStreet::Attack()
extern void ZuroStreet_Attack_m586D7DE76436AE4116587A2A0A68809FF4D20347 ();
// 0x00000039 System.Void ZuroStreet::OnDisable()
extern void ZuroStreet_OnDisable_mB0DACFD3640047DD56EF34506911FED3EB1F0583 ();
// 0x0000003A System.Void ZuroStreet::.ctor()
extern void ZuroStreet__ctor_mD0E3CE2D0B16F0EDB9D56CDAFCFA45236C0B79B9 ();
// 0x0000003B System.Void Cinemachine.Examples.ScriptingExample::Start()
extern void ScriptingExample_Start_m79B8DBE9F2711279277D13BF1308C3516C19F90A ();
// 0x0000003C System.Void Cinemachine.Examples.ScriptingExample::Update()
extern void ScriptingExample_Update_mA1A4F8E6EC542DD873BD3DC42A24FAED1499AB8C ();
// 0x0000003D System.Void Cinemachine.Examples.ScriptingExample::.ctor()
extern void ScriptingExample__ctor_mD5B229275ACF8576FA6EB85B97EAA7591F56DC01 ();
// 0x0000003E System.Void Cinemachine.Examples.GenericTrigger::Start()
extern void GenericTrigger_Start_m9795906FC841CC4B2236CF8888A8306E1C94BBA4 ();
// 0x0000003F System.Void Cinemachine.Examples.GenericTrigger::OnTriggerExit(UnityEngine.Collider)
extern void GenericTrigger_OnTriggerExit_m9B5A0ECBA53C54E0C12DDFA32144509226B0E54B ();
// 0x00000040 System.Void Cinemachine.Examples.GenericTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void GenericTrigger_OnTriggerEnter_mFD4689405DEA14F65E2F8554BF3D571CD73C8E2C ();
// 0x00000041 System.Void Cinemachine.Examples.GenericTrigger::.ctor()
extern void GenericTrigger__ctor_mC9F645021E384E3112CEA2992E787D4CE5F12E88 ();
// 0x00000042 System.Void Cinemachine.Examples.CharacterMovement::Start()
extern void CharacterMovement_Start_mEA069E540139D2289966584FC5067053E21BD85D ();
// 0x00000043 System.Void Cinemachine.Examples.CharacterMovement::FixedUpdate()
extern void CharacterMovement_FixedUpdate_m8615F2B05324419DBB1CEB9506DBDFD2F0F66A0F ();
// 0x00000044 System.Void Cinemachine.Examples.CharacterMovement::UpdateTargetDirection()
extern void CharacterMovement_UpdateTargetDirection_m8571B65E5FCA3579F722687F2D53F0FCFF8AC214 ();
// 0x00000045 System.Void Cinemachine.Examples.CharacterMovement::.ctor()
extern void CharacterMovement__ctor_m6BF3D71AAF2FB1160BD26A58549F946488BDFE7B ();
// 0x00000046 System.Void Cinemachine.Examples.CharacterMovement2D::Start()
extern void CharacterMovement2D_Start_m6C3CC414568A67E6FC1EF370C9E702A997416272 ();
// 0x00000047 System.Void Cinemachine.Examples.CharacterMovement2D::FixedUpdate()
extern void CharacterMovement2D_FixedUpdate_m0706C4622DDDFB6F424C3DC6B4F1DC144A0605C8 ();
// 0x00000048 System.Boolean Cinemachine.Examples.CharacterMovement2D::isGrounded()
extern void CharacterMovement2D_isGrounded_m9181E8749ED948212A8D0A8E4631D625944BDD70 ();
// 0x00000049 System.Void Cinemachine.Examples.CharacterMovement2D::.ctor()
extern void CharacterMovement2D__ctor_m405EFCAAAAE1436EA68509865F6A69B1FAAFBC9F ();
// 0x0000004A System.Void Cinemachine.Examples.ActivateCamOnPlay::Start()
extern void ActivateCamOnPlay_Start_m7CC36D9EF8A6D1AE712C67BF62FD15EF7392E65A ();
// 0x0000004B System.Void Cinemachine.Examples.ActivateCamOnPlay::.ctor()
extern void ActivateCamOnPlay__ctor_mE0248D3F391298D30C06500EAF092416093327B8 ();
// 0x0000004C System.Void Cinemachine.Examples.ActivateCameraWithDistance::Start()
extern void ActivateCameraWithDistance_Start_m435B7DEF0A2173F42274135E588903E9127C9995 ();
// 0x0000004D System.Void Cinemachine.Examples.ActivateCameraWithDistance::Update()
extern void ActivateCameraWithDistance_Update_m61B1F1909206E419F849BE447BC51F5197118976 ();
// 0x0000004E System.Void Cinemachine.Examples.ActivateCameraWithDistance::SwitchCam(Cinemachine.CinemachineVirtualCameraBase)
extern void ActivateCameraWithDistance_SwitchCam_m5736882F44A433FF71B32E13605739D7B7B13A85 ();
// 0x0000004F System.Void Cinemachine.Examples.ActivateCameraWithDistance::.ctor()
extern void ActivateCameraWithDistance__ctor_m0AFD25A3578C0BF0E63A2A36215B71F2792006BF ();
// 0x00000050 System.Void Cinemachine.Examples.ExampleHelpWindow::OnGUI()
extern void ExampleHelpWindow_OnGUI_mDE54F4CCAA35ABCC00159E6C883AEC6DC3BE4A66 ();
// 0x00000051 System.Void Cinemachine.Examples.ExampleHelpWindow::DrawWindow(System.Int32,System.Single)
extern void ExampleHelpWindow_DrawWindow_m42568AC283611EA80B03CAAEE7FDD2C7025ED016 ();
// 0x00000052 System.Void Cinemachine.Examples.ExampleHelpWindow::.ctor()
extern void ExampleHelpWindow__ctor_mC9FD7C32341606F660C2046D479AC4DB3A030857 ();
// 0x00000053 System.Void Cinemachine.Examples.MixingCameraBlend::Start()
extern void MixingCameraBlend_Start_mE6154055F56E65873E236AB81E1C7A53780F90D6 ();
// 0x00000054 System.Void Cinemachine.Examples.MixingCameraBlend::Update()
extern void MixingCameraBlend_Update_m42804BA540A1712167B1EE7E8AEE3AEF359FAF90 ();
// 0x00000055 System.Void Cinemachine.Examples.MixingCameraBlend::.ctor()
extern void MixingCameraBlend__ctor_m5E506BC4C5275485E50A222DF896993983D3440F ();
// 0x00000056 System.Void EnemyPatrol_<PatrolToTarget>d__11::.ctor(System.Int32)
extern void U3CPatrolToTargetU3Ed__11__ctor_m55E022613EF7239E767DC35B5D591AFF3361E960 ();
// 0x00000057 System.Void EnemyPatrol_<PatrolToTarget>d__11::System.IDisposable.Dispose()
extern void U3CPatrolToTargetU3Ed__11_System_IDisposable_Dispose_m0EE6A302193BE3E6AFB381DED643FAC78FC8DFD5 ();
// 0x00000058 System.Boolean EnemyPatrol_<PatrolToTarget>d__11::MoveNext()
extern void U3CPatrolToTargetU3Ed__11_MoveNext_m03BFEC4A71DDD0D46580989D40E65F070AD55DD3 ();
// 0x00000059 System.Object EnemyPatrol_<PatrolToTarget>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPatrolToTargetU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E023F09D9AF079421AD9CBC82AD92E13A25C3B0 ();
// 0x0000005A System.Void EnemyPatrol_<PatrolToTarget>d__11::System.Collections.IEnumerator.Reset()
extern void U3CPatrolToTargetU3Ed__11_System_Collections_IEnumerator_Reset_m4A790E2666FDFBE6B63FCC94B70F2522443C9B9B ();
// 0x0000005B System.Object EnemyPatrol_<PatrolToTarget>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CPatrolToTargetU3Ed__11_System_Collections_IEnumerator_get_Current_m1DCC9C5C0BF5244C7AB62C8E4CC715BDC076CBFE ();
// 0x0000005C System.Void ZuroStreet_<Attack>d__17::.ctor(System.Int32)
extern void U3CAttackU3Ed__17__ctor_mBEBE2F6358D4DA86E68B52BB5F59AB1E5CFC7318 ();
// 0x0000005D System.Void ZuroStreet_<Attack>d__17::System.IDisposable.Dispose()
extern void U3CAttackU3Ed__17_System_IDisposable_Dispose_m3660D96D37E3C0E1C0426C19F1091F73A456B932 ();
// 0x0000005E System.Boolean ZuroStreet_<Attack>d__17::MoveNext()
extern void U3CAttackU3Ed__17_MoveNext_mD7EA6147D3EE57A0195060599A0A3A6217BB1B49 ();
// 0x0000005F System.Object ZuroStreet_<Attack>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18001455E2553F8F415D869737739B8F954AE866 ();
// 0x00000060 System.Void ZuroStreet_<Attack>d__17::System.Collections.IEnumerator.Reset()
extern void U3CAttackU3Ed__17_System_Collections_IEnumerator_Reset_m44994CB80C794CD5F3C627870E3B2AC5AC6EAE5F ();
// 0x00000061 System.Object ZuroStreet_<Attack>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CAttackU3Ed__17_System_Collections_IEnumerator_get_Current_m9ECB9C3719CF235DD11691172A1E83EAE151F299 ();
// 0x00000062 System.Void Cinemachine.Examples.ExampleHelpWindow_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m0B4D84D4DCE8F38EF3771D4A27665027FB4BC4EC ();
// 0x00000063 System.Void Cinemachine.Examples.ExampleHelpWindow_<>c__DisplayClass4_0::<OnGUI>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass4_0_U3COnGUIU3Eb__0_m84E30E3962AC9F5BA3EB82DEF5ABDA2DDE85D6EE ();
static Il2CppMethodPointer s_methodPointers[99] = 
{
	ActivateOnKeypress_Start_mC9C71BD760F97DBC0F686569A13EA91AA8F643C6,
	ActivateOnKeypress_Update_m7C06B9EF28D5420A29C17EC1D8D6C8239D489312,
	ActivateOnKeypress__ctor_m75534AB7F9820B5C53A22B8244CE4BD87F1A5538,
	PlayerMove_Update_m816082939684D509B053B2848EE5BB2A4B655F45,
	PlayerMove__ctor_m95039BC415B767C2F57784CC458BB65C1188BFD7,
	PlayerMoveOnSphere_Update_m8E045C01FD8CCCCFC85B46DDD68978730E97C9B5,
	PlayerMoveOnSphere__ctor_m04885E872445EB4B0ADAB9967C4A3F88D2F8A26D,
	PlayerMovePhysics_Start_m68C6BC74B80E0258B34E06A348E6C32F8FB48154,
	PlayerMovePhysics_OnEnable_m43E1851523FAECBAEDD18FF7072145D99CE6CEDF,
	PlayerMovePhysics_FixedUpdate_m41FE39EEE213101002AA90DB208FF4611B71B42E,
	PlayerMovePhysics__ctor_m4018D7D2888F136029F447C52A9DB1538F5D5064,
	BejucoController_Awake_m795D66747ED8AB6D70E60E609312039CFDF739F9,
	BejucoController_Start_m18761ACCAC20C2B734018F62F7D81D05EB760330,
	BejucoController_Update_m2785342A43F73D03858EB28BCC340AA0CFBABD74,
	BejucoController_FixedUpdate_m748A21EE1003312AC103B07902A1412984896853,
	BejucoController_LateUpdate_m27F7BD50A67CFBA6B218859321D78044D1D9D939,
	BejucoController_Flip_m8C71F2D07618D312308C169CF4F4D3D59DEDA419,
	BejucoController__ctor_m8363B965BACF48CFDCB7C01427DB40E694A5482D,
	Bullet_Awake_m37FD11C4734664815AF4EEF3C3814AEBCF88B6CD,
	Bullet_Start_m0C6AD7FCC791ADF08593B040294D8B7329617D04,
	Bullet_Update_mEDD99D9C6CBBEBCD7C211792A42CBF1D965EB80E,
	Bullet__ctor_mFBA1E7297C133AE06ADA2EF2BA04567AD213A9D4,
	ChangeColor_Awake_m4A2850FB998ED3DB1E294F7107D4C72787F81977,
	ChangeColor_Start_m6D47E8F30AEAB75D8E034168972BEC8C4D0A6869,
	ChangeColor_Update_m0F7BC07040DEF2BFDAFF5F448E1A8D2F92534890,
	ChangeColor__ctor_m6611480670D0461C94C966D87A0AB2BF4F992A55,
	EnemyPatrol_Awake_mB4793D74956B3910848A4B4DF141FE6A7F826A25,
	EnemyPatrol_Start_mF69D7343C30458904BF576DC87C4EE0D94D5E913,
	EnemyPatrol_Update_m9C9A815AFD2925201FC71D4D080ACA2A77307182,
	EnemyPatrol_UpdateTarget_mBF1D57F2C57B1B19634CFBEE098102D0F2C58819,
	EnemyPatrol_PatrolToTarget_m675B9C77F20C755079DFE6B4AFFA4E1798C092ED,
	EnemyPatrol_CanShoot_mFBD932DF4BC7E42F3CD1ED71F77C9646FB51C7CE,
	EnemyPatrol__ctor_mA0C5ED3222E96136C9E8930F5548D7CD32FE2AE2,
	Weapon_Awake_m2AC14C75327F3526C722CACC9F4CCD7D95B21F98,
	Weapon_Start_m4FAB905F9926EEADEEF49D8B77218D1A14F459AC,
	Weapon_Update_mD65E9FD544FEE0974CED54FA39BB05A89FB6D99E,
	Weapon_Shoot_mF5B36E6C4428C1E8858F42526F5EEC7C7AF9D4E3,
	Weapon__ctor_mF7C215ECC1D7032E6DE76DE606A9159F840B62FB,
	InstantiatePrefab_Instantiate_m31E43D0401E2D3691190E3780516A2EE9A881B23,
	InstantiatePrefab__ctor_mE32F9508E28E5A9AA2D84CA8CB9CB2E0FC7868E6,
	LoadScene_OnMouseDown_mCD182BA2B1429157BDCC1BCC4E31AB9AA0B47EA5,
	LoadScene__ctor_m52B221B2B1C02A68F125B164DDE573DF0353A336,
	PlayerController_Awake_m118C1D205A374B627E1EC963E5837E23CF554573,
	PlayerController_Start_mC0C9B9461D0BDAC48EC43715818A4BA63C4F45EF,
	PlayerController_Update_m38903EF1C8F12B9388303741F8040EE26C33DC33,
	PlayerController_FixedUpdate_m914EA3E3CCE4DF6AEB2E78317FFC1D507DACEBDE,
	PlayerController_LateUpdate_mAF83D0B2354FDB3E9EEC98CB72E9D77EC004B881,
	PlayerController_Flip_m829B4A4842ABD034CB56BCB1966D3C462A9D6AD4,
	PlayerController__ctor_m648E40092E37395F4307411E855445886113CD60,
	ZuroStreet_Awake_mF39B194AFCADDAB34B04D3D49D262BC9EE9B7336,
	ZuroStreet_Start_m36B79E7C1DA92EFFEDD8920CC13F24C189FBB39C,
	ZuroStreet_Update_mF55F06C2957CFD22027E9DC239991E181ADB7228,
	ZuroStreet_LateUpdate_m0866F695A7D62507AF67715D8439F810D8155A73,
	ZuroStreet_OnTriggerStay2D_m09FD5E717BF2CBB97B122544254BB7521866D51D,
	ZuroStreet_AttackMovement_mFC008314CD71D1B0D3D15FD3ED1C5F3F778F78AA,
	ZuroStreet_Attack_m586D7DE76436AE4116587A2A0A68809FF4D20347,
	ZuroStreet_OnDisable_mB0DACFD3640047DD56EF34506911FED3EB1F0583,
	ZuroStreet__ctor_mD0E3CE2D0B16F0EDB9D56CDAFCFA45236C0B79B9,
	ScriptingExample_Start_m79B8DBE9F2711279277D13BF1308C3516C19F90A,
	ScriptingExample_Update_mA1A4F8E6EC542DD873BD3DC42A24FAED1499AB8C,
	ScriptingExample__ctor_mD5B229275ACF8576FA6EB85B97EAA7591F56DC01,
	GenericTrigger_Start_m9795906FC841CC4B2236CF8888A8306E1C94BBA4,
	GenericTrigger_OnTriggerExit_m9B5A0ECBA53C54E0C12DDFA32144509226B0E54B,
	GenericTrigger_OnTriggerEnter_mFD4689405DEA14F65E2F8554BF3D571CD73C8E2C,
	GenericTrigger__ctor_mC9F645021E384E3112CEA2992E787D4CE5F12E88,
	CharacterMovement_Start_mEA069E540139D2289966584FC5067053E21BD85D,
	CharacterMovement_FixedUpdate_m8615F2B05324419DBB1CEB9506DBDFD2F0F66A0F,
	CharacterMovement_UpdateTargetDirection_m8571B65E5FCA3579F722687F2D53F0FCFF8AC214,
	CharacterMovement__ctor_m6BF3D71AAF2FB1160BD26A58549F946488BDFE7B,
	CharacterMovement2D_Start_m6C3CC414568A67E6FC1EF370C9E702A997416272,
	CharacterMovement2D_FixedUpdate_m0706C4622DDDFB6F424C3DC6B4F1DC144A0605C8,
	CharacterMovement2D_isGrounded_m9181E8749ED948212A8D0A8E4631D625944BDD70,
	CharacterMovement2D__ctor_m405EFCAAAAE1436EA68509865F6A69B1FAAFBC9F,
	ActivateCamOnPlay_Start_m7CC36D9EF8A6D1AE712C67BF62FD15EF7392E65A,
	ActivateCamOnPlay__ctor_mE0248D3F391298D30C06500EAF092416093327B8,
	ActivateCameraWithDistance_Start_m435B7DEF0A2173F42274135E588903E9127C9995,
	ActivateCameraWithDistance_Update_m61B1F1909206E419F849BE447BC51F5197118976,
	ActivateCameraWithDistance_SwitchCam_m5736882F44A433FF71B32E13605739D7B7B13A85,
	ActivateCameraWithDistance__ctor_m0AFD25A3578C0BF0E63A2A36215B71F2792006BF,
	ExampleHelpWindow_OnGUI_mDE54F4CCAA35ABCC00159E6C883AEC6DC3BE4A66,
	ExampleHelpWindow_DrawWindow_m42568AC283611EA80B03CAAEE7FDD2C7025ED016,
	ExampleHelpWindow__ctor_mC9FD7C32341606F660C2046D479AC4DB3A030857,
	MixingCameraBlend_Start_mE6154055F56E65873E236AB81E1C7A53780F90D6,
	MixingCameraBlend_Update_m42804BA540A1712167B1EE7E8AEE3AEF359FAF90,
	MixingCameraBlend__ctor_m5E506BC4C5275485E50A222DF896993983D3440F,
	U3CPatrolToTargetU3Ed__11__ctor_m55E022613EF7239E767DC35B5D591AFF3361E960,
	U3CPatrolToTargetU3Ed__11_System_IDisposable_Dispose_m0EE6A302193BE3E6AFB381DED643FAC78FC8DFD5,
	U3CPatrolToTargetU3Ed__11_MoveNext_m03BFEC4A71DDD0D46580989D40E65F070AD55DD3,
	U3CPatrolToTargetU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E023F09D9AF079421AD9CBC82AD92E13A25C3B0,
	U3CPatrolToTargetU3Ed__11_System_Collections_IEnumerator_Reset_m4A790E2666FDFBE6B63FCC94B70F2522443C9B9B,
	U3CPatrolToTargetU3Ed__11_System_Collections_IEnumerator_get_Current_m1DCC9C5C0BF5244C7AB62C8E4CC715BDC076CBFE,
	U3CAttackU3Ed__17__ctor_mBEBE2F6358D4DA86E68B52BB5F59AB1E5CFC7318,
	U3CAttackU3Ed__17_System_IDisposable_Dispose_m3660D96D37E3C0E1C0426C19F1091F73A456B932,
	U3CAttackU3Ed__17_MoveNext_mD7EA6147D3EE57A0195060599A0A3A6217BB1B49,
	U3CAttackU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18001455E2553F8F415D869737739B8F954AE866,
	U3CAttackU3Ed__17_System_Collections_IEnumerator_Reset_m44994CB80C794CD5F3C627870E3B2AC5AC6EAE5F,
	U3CAttackU3Ed__17_System_Collections_IEnumerator_get_Current_m9ECB9C3719CF235DD11691172A1E83EAE151F299,
	U3CU3Ec__DisplayClass4_0__ctor_m0B4D84D4DCE8F38EF3771D4A27665027FB4BC4EC,
	U3CU3Ec__DisplayClass4_0_U3COnGUIU3Eb__0_m84E30E3962AC9F5BA3EB82DEF5ABDA2DDE85D6EE,
};
static const int32_t s_InvokerIndices[99] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	114,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	926,
	23,
	23,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	32,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	99,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
