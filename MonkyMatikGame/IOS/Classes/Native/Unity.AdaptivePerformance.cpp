﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>
struct List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AdaptivePerformance.AdaptivePerformanceManager
struct AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA;
// UnityEngine.AdaptivePerformance.AdaptivePerformanceManager/<InvokeEndOfFrame>d__72
struct U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB;
// UnityEngine.AdaptivePerformance.AdaptivePerformanceManagerSpawner
struct AdaptivePerformanceManagerSpawner_tB0C73AFB099B61AC0890B6FB092EE08A7C8794A4;
// UnityEngine.AdaptivePerformance.AutoPerformanceLevelController
struct AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A;
// UnityEngine.AdaptivePerformance.CpuTimeProvider
struct CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994;
// UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl
struct DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326;
// UnityEngine.AdaptivePerformance.GpuTimeProvider
struct GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B;
// UnityEngine.AdaptivePerformance.IAdaptivePerformance
struct IAdaptivePerformance_tEF031ACC81DD5B802D8C9ED0A86B75F3FC213E06;
// UnityEngine.AdaptivePerformance.IDevelopmentSettings
struct IDevelopmentSettings_t4750500646CFEDDBCC8A941148814DC181EC0CDD;
// UnityEngine.AdaptivePerformance.IDevicePerformanceControl
struct IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6;
// UnityEngine.AdaptivePerformance.IPerformanceStatus
struct IPerformanceStatus_t314D47B0E81EFA6C95DDF7B3C42B9DB1AA2C666A;
// UnityEngine.AdaptivePerformance.IThermalStatus
struct IThermalStatus_tDAEFF23197198F2659A13DB40699E469597F66D8;
// UnityEngine.AdaptivePerformance.MainThreadCpuTime
struct MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887;
// UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler
struct PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995;
// UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs
struct PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB;
// UnityEngine.AdaptivePerformance.PerformanceLevelChangeHandler
struct PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301;
// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem
struct AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528;
// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemBase
struct AdaptivePerformanceSubsystemBase_tFE4445DE6A7F0520A1707ECCC71C516E3E4FB12E;
// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor
struct AdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55;
// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor[]
struct AdaptivePerformanceSubsystemDescriptorU5BU5D_t54422C488D4508DF0DF999EE17CFC560BD237FF9;
// UnityEngine.AdaptivePerformance.Provider.IApplicationLifecycle
struct IApplicationLifecycle_t62CD41874D9B90D9161909F588957018E97173A9;
// UnityEngine.AdaptivePerformance.Provider.IDevicePerformanceLevelControl
struct IDevicePerformanceLevelControl_t69BEA7256A9F193E2D588EADD393A04E5F201436;
// UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem
struct TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F;
// UnityEngine.AdaptivePerformance.RenderThreadCpuTime
struct RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710;
// UnityEngine.AdaptivePerformance.RunningAverage
struct RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1;
// UnityEngine.AdaptivePerformance.TemperatureTrend
struct TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B;
// UnityEngine.AdaptivePerformance.ThermalEventHandler
struct ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.FrameTiming[]
struct FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.ISubsystemDescriptor
struct ISubsystemDescriptor_t5BCD578E4BAD3A0C1DF6C5654720FE7D4420605B;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734;
// UnityEngine.Subsystem
struct Subsystem_t17E4AEB5537DC8AECC37EC3F6FCB46CC7D2C73F6;
// UnityEngine.SubsystemDescriptor`1<System.Object>
struct SubsystemDescriptor_1_tE2E4F2A029DA1A307F018FCE747BA53FB9E344C2;
// UnityEngine.SubsystemDescriptor`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem>
struct SubsystemDescriptor_1_t260E615AF73615B1D344C323E0F4A6CBB7360C20;
// UnityEngine.Subsystem`1<System.Object>
struct Subsystem_1_t6048F47F8C2EBFDAC541AA593928233978B85EA9;
// UnityEngine.Subsystem`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>
struct Subsystem_1_t5C16E764132B6384B05BBEE4DA18A26F3A78C9FD;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA;

IL2CPP_EXTERN_C RuntimeClass* APLog_t31BBAEC90A9C4586244623826D33D2C445830E8E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Holder_t3A26EE4375105172B35CF838E774E2DB72D06DF9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IApplicationLifecycle_t62CD41874D9B90D9161909F588957018E97173A9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDevicePerformanceLevelControl_t69BEA7256A9F193E2D588EADD393A04E5F201436_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IPerformanceStatus_t314D47B0E81EFA6C95DDF7B3C42B9DB1AA2C666A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IThermalStatus_tDAEFF23197198F2659A13DB40699E469597F66D8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OnDemandRendering_t89F49EDBAFA9B757B97F0277A419BBC394B2FA19_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SubsystemManager_tFEDEC70DC4119830C96B42915123C27FEDDB0F58_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral5C3DEF53E256C1E5614CF65282B9EEE41FCA53B3;
IL2CPP_EXTERN_C String_t* _stringLiteral89BB893F80FF7991CD99CDFCD9E218924D734AF4;
IL2CPP_EXTERN_C String_t* _stringLiteralFDB5AC7F1BDBCF4BF99E8E359CC36FEE9ED29213;
IL2CPP_EXTERN_C const RuntimeMethod* AdaptivePerformanceManager_U3CStartU3Eb__65_0_m579D006E30704474CA7241DB6E90890689C52CA5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AdaptivePerformanceManager_U3CStartU3Eb__65_1_mA40F9B614EF4AD67A403592F6F063D268B6BA459_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AdaptivePerformanceManager_U3CStartU3Eb__65_2_mA06C26D79D6B5C59EE680771CFD0AC843B8E40DD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AutoPerformanceLevelController_U3C_ctorU3Eb__43_0_m489E89565AB88A5E21B4C332CC758827E2CAC4F8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m1E1887EFB1E29C201DA849DA80C01B6CF756F506_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m61C82A0B80A3224E2C00EC055806FE26F81964A6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mC39D2C568D4151FA33784EB27005767BCC518FF5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisAdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA_m7C0A87C297E85B0C6CD8653EAEFE085697819809_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m8E054D1AFBD9129D72D963784EE1E501B02B6260_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m09E20FB8F2F5E707BB0FCDB5B21388DDDA9A7471_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m4F79244F68273A7F2AFE63F5443754B4A2BE62CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ScriptableObject_CreateInstance_TisAdaptivePerformanceManagerSpawner_tB0C73AFB099B61AC0890B6FB092EE08A7C8794A4_m6D26827E87B21608D2903C52C7DC967BE83532DF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SubsystemDescriptor_1_Create_mB303B00807AACC42F9449C37EF70DF21EF952878_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SubsystemManager_GetSubsystemDescriptors_TisAdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55_m32243E1D766B4AEEF8E2E0EF3A161E83A7AC1B15_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Subsystem_1__ctor_mFC93A7FA2C0A92CDF2FBC703D15FC3E890A71D8C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CInvokeEndOfFrameU3Ed__72_System_Collections_IEnumerator_Reset_m39F4D3E658549D03A3DA0CF1EF253B1F9AEBA8F1_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceInitializer_Initialize_mCAD909920560889DA329579B503C1CA4633853EC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManagerSpawner_OnEnable_m7F213133592CA282EBE4B5583653878E168B9302_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_EffectiveTargetFrameRate_m05458EB876A5E7810268A2DF19E175F2A3D7B9E4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_InvokeEndOfFrame_m1F36AB54DF0C93F0ACFD57FE37ACFA1B4A155E30_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_OnApplicationPause_m1766D5E255DC5DF85116AEF56803228F182DD3F9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_RenderFrameInterval_mD1230D5B327F15435CC27D0C0D5BA62FB53811F9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_Start_m32763F3786711CBF6BEC7CF4DB8674E5CDC59020_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_ToStringWithSign_m87DBE28D6E5B93354B282B463C544723A579DC22_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_UpdateSubsystem_m5D0DFA14D25B45D715DDF4274C46E0DE82D78DBA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_Update_mE11F4B9516F9DCBFB19979CABF54751AAAD32034_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_WillCurrentFrameRender_m9662ECAA12EB05BE964E89A1E29AC0AA37F82DDC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager__ctor_mB68642A5CBB6A1880F09D7971FF24C670CCC9CD5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_add_PerformanceBottleneckChangeEvent_m13961F9A0E9DEA59C0FA135FA1BFA0D77024087D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_add_PerformanceLevelChangeEvent_mCA3922EBF78BB3B8C28955EC352246C3E8D59FB8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_add_ThermalEvent_m882A1B95EDCFA6A87913F08818FE63065C43AE5F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_get_Logging_m12520E205DAA0BD2BCA252EDAF646B3D952DD931_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_remove_PerformanceBottleneckChangeEvent_mC4F532774EE5584F17F04BD4027BF2CF095F7D1A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_remove_PerformanceLevelChangeEvent_m844136D2DF22DD40F967CCCB19A43550364E3DBE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_remove_ThermalEvent_m2408F6AF848DB55EF987D71FD599EFB6E7034726_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceManager_set_Logging_m210CEDE52B16E37F9D1AC0B7D5AD6760F7E8CEEF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceSubsystemBase__ctor_m28396E884C7A5F5ACD2BBF34FE6039698AB2B106_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptivePerformanceSubsystemRegistry_GetRegisteredDescriptors_mA1D2C28ABC171D8FB88C9EEC96BD63A303456604_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AutoPerformanceLevelController_AllowLowerCpuLevel_mB670A9E210BCD84FA7419C79D5679303714447D4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AutoPerformanceLevelController_AllowLowerGpuLevel_m5AD3EB818563DFC12A1538A3011108A1FAD57D52_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AutoPerformanceLevelController_AllowRaiseCpuLevel_m37030CB2A20F89FEBB0CB2228CE2602991D86613_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AutoPerformanceLevelController_AllowRaiseGpuLevel_m3270D2A8F91AE8F4FC4C19CF3E50FD6B50A03816_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AutoPerformanceLevelController_AllowRaiseLevels_m9EEE01DF654F72120AAED804C30DF98C6CA5C2E5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AutoPerformanceLevelController_LowerCpuLevel_m6DF2EDB012FD228606FD09BF4F9C4D1BB9B0E504_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AutoPerformanceLevelController_LowerGpuLevel_mD616F65705E36157413404296E82FC53C5650B74_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AutoPerformanceLevelController_Override_mE3A9350573679DEBE02F35C2F7B80694973A157E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AutoPerformanceLevelController_RaiseCpuLevel_mB6B9F81D1AEE8BF96B2F14295E25C5D82F199250_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AutoPerformanceLevelController_RaiseGpuLevel_mED360B4ACBE7A2787074DB6F75E8D1DCEF28F804_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AutoPerformanceLevelController_UpdateImpl_mE656C13A7FF6E6CD5667D585EC5CAB561A285C1E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AutoPerformanceLevelController__ctor_m252C3934AA192D4135C2A9CE41B7D4B1216BC88C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CpuTimeProvider__ctor_mE73ABBAB48B1D65051956A2C32BE69E65A7B87F4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CpuTimeProvider_get_CpuFrameTime_mFDBF3DF1106A47DA135FF7B49389CC355108BA25_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t DevicePerformanceControlImpl_Update_m62E8744125498EDACB2150F2DEF0DF6D6AA58635_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t DevicePerformanceControlImpl_get_MaxCpuPerformanceLevel_mC8D89A2CC5E0135E12B089A50D6289FC2DC97B9A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t DevicePerformanceControlImpl_get_MaxGpuPerformanceLevel_mF87A40C6B5D9EB47C980229BE85925CF3CEA39CE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GpuTimeProvider__ctor_m53082BB9CAF2611637533E336F6E406A306DCA9A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Holder_set_Instance_m2786C3B74384A62BD2997F76802455BEAD572CAEUnity_AdaptivePerformance_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Holder_set_Instance_m2786C3B74384A62BD2997F76802455BEAD572CAE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PerformanceBottleneckChangeHandler_BeginInvoke_mB184FBFDB47EC724127A67CBDF829AA32A6829AE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PerformanceLevelChangeHandler_BeginInvoke_m15EF7D19D4BC0C7C0CB90A5AC4AF594FDF1277F4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t RunningAverage_AddValue_m5C454C088FA345480368C01D995C56D618CD4733_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t RunningAverage__ctor_m1D5CCDBE8A19B9CCE2F0644773094D4157AEC9AF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_get_AutomaticPerformanceControl_mD3FD90C9A974872C6E1F4C8F0F857CE658654E83Unity_AdaptivePerformance_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_get_AutomaticPerformanceControl_mD3FD90C9A974872C6E1F4C8F0F857CE658654E83_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_get_Enable_m8343935449F13DAF78A3EE247DA316A91695A9D2Unity_AdaptivePerformance_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_get_Enable_m8343935449F13DAF78A3EE247DA316A91695A9D2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_get_Logging_m7A40AE632795747F6E17C5ABABEF45D83E01EAA3Unity_AdaptivePerformance_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_get_Logging_m7A40AE632795747F6E17C5ABABEF45D83E01EAA3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_get_PreferredSubsystem_m288E01FA3B91225A014D305D97163961E988C5B3Unity_AdaptivePerformance_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_get_PreferredSubsystem_m288E01FA3B91225A014D305D97163961E988C5B3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_get_StatsLoggingFrequencyInFrames_m5F108EC9014719068FF27AD8D23E21E1FD1DF380Unity_AdaptivePerformance_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_get_StatsLoggingFrequencyInFrames_m5F108EC9014719068FF27AD8D23E21E1FD1DF380_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_set_AutomaticPerformanceControl_m9C2983846A71477818E1E33CBE4412E9D621A272Unity_AdaptivePerformance_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_set_AutomaticPerformanceControl_m9C2983846A71477818E1E33CBE4412E9D621A272_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_set_Enable_mA1F811FA9465F51D5742B28BFCAA450D9298B6C1Unity_AdaptivePerformance_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_set_Enable_mA1F811FA9465F51D5742B28BFCAA450D9298B6C1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_set_Logging_m6CD2E66D91308712825AECF2A1AF80A86F7C4A30Unity_AdaptivePerformance_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_set_Logging_m6CD2E66D91308712825AECF2A1AF80A86F7C4A30_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_set_PreferredSubsystem_m84A3D06E2638662EE488242C3A5A8764ED31D8DDUnity_AdaptivePerformance_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_set_PreferredSubsystem_m84A3D06E2638662EE488242C3A5A8764ED31D8DD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_set_StatsLoggingFrequencyInFrames_m4AA8C016FE0DAD38D37117A32154D4B542D1D115Unity_AdaptivePerformance_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StartupSettings_set_StatsLoggingFrequencyInFrames_m4AA8C016FE0DAD38D37117A32154D4B542D1D115_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TemperatureTrend_UpdateTrend_mBA99D62A42FAC09AF17975F27A4BEC2C5E05C015_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TemperatureTrend__ctor_mC16C5848B70A106139BFAD5068FEE2332A93D8DB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ThermalEventHandler_BeginInvoke_m1D8E5A8112745ACB6B9B9D3DF35A1ECAE4AE6409_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CInvokeEndOfFrameU3Ed__72_System_Collections_IEnumerator_Reset_m39F4D3E658549D03A3DA0CF1EF253B1F9AEBA8F1_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB;;
struct PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_pinvoke;
struct PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_pinvoke;;

struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
struct FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t46A6F03C2512769BCD65D24A027556CFE9BA536A 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>
struct  List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AdaptivePerformanceSubsystemDescriptorU5BU5D_t54422C488D4508DF0DF999EE17CFC560BD237FF9* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC, ____items_1)); }
	inline AdaptivePerformanceSubsystemDescriptorU5BU5D_t54422C488D4508DF0DF999EE17CFC560BD237FF9* get__items_1() const { return ____items_1; }
	inline AdaptivePerformanceSubsystemDescriptorU5BU5D_t54422C488D4508DF0DF999EE17CFC560BD237FF9** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AdaptivePerformanceSubsystemDescriptorU5BU5D_t54422C488D4508DF0DF999EE17CFC560BD237FF9* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	AdaptivePerformanceSubsystemDescriptorU5BU5D_t54422C488D4508DF0DF999EE17CFC560BD237FF9* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC_StaticFields, ____emptyArray_5)); }
	inline AdaptivePerformanceSubsystemDescriptorU5BU5D_t54422C488D4508DF0DF999EE17CFC560BD237FF9* get__emptyArray_5() const { return ____emptyArray_5; }
	inline AdaptivePerformanceSubsystemDescriptorU5BU5D_t54422C488D4508DF0DF999EE17CFC560BD237FF9** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(AdaptivePerformanceSubsystemDescriptorU5BU5D_t54422C488D4508DF0DF999EE17CFC560BD237FF9* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.AdaptivePerformance.APLog
struct  APLog_t31BBAEC90A9C4586244623826D33D2C445830E8E  : public RuntimeObject
{
public:

public:
};

struct APLog_t31BBAEC90A9C4586244623826D33D2C445830E8E_StaticFields
{
public:
	// System.Boolean UnityEngine.AdaptivePerformance.APLog::enabled
	bool ___enabled_0;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(APLog_t31BBAEC90A9C4586244623826D33D2C445830E8E_StaticFields, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}
};


// UnityEngine.AdaptivePerformance.AdaptivePerformanceInitializer
struct  AdaptivePerformanceInitializer_t3CA673A7DCD267878BB1D376E8BBF36D5BE18AA6  : public RuntimeObject
{
public:

public:
};


// UnityEngine.AdaptivePerformance.AdaptivePerformanceManager_<InvokeEndOfFrame>d__72
struct  U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.AdaptivePerformance.AdaptivePerformanceManager_<InvokeEndOfFrame>d__72::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.AdaptivePerformance.AdaptivePerformanceManager_<InvokeEndOfFrame>d__72::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.AdaptivePerformance.AdaptivePerformanceManager UnityEngine.AdaptivePerformance.AdaptivePerformanceManager_<InvokeEndOfFrame>d__72::<>4__this
	AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB, ___U3CU3E4__this_2)); }
	inline AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// UnityEngine.AdaptivePerformance.AutoPerformanceLevelController
struct  AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A  : public RuntimeObject
{
public:
	// UnityEngine.AdaptivePerformance.IDevicePerformanceControl UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::m_PerfControl
	RuntimeObject* ___m_PerfControl_0;
	// UnityEngine.AdaptivePerformance.IPerformanceStatus UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::m_PerfStats
	RuntimeObject* ___m_PerfStats_1;
	// UnityEngine.AdaptivePerformance.IThermalStatus UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::m_ThermalStats
	RuntimeObject* ___m_ThermalStats_2;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::m_LastChangeTimeStamp
	float ___m_LastChangeTimeStamp_3;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::m_LastGpuLevelRaiseTimeStamp
	float ___m_LastGpuLevelRaiseTimeStamp_4;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::m_LastCpuLevelRaiseTimeStamp
	float ___m_LastCpuLevelRaiseTimeStamp_5;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::m_TargetFrameRateHitTimestamp
	float ___m_TargetFrameRateHitTimestamp_6;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::m_BottleneckUnknownTimestamp
	float ___m_BottleneckUnknownTimestamp_7;
	// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::m_TriedToResolveUnknownBottleneck
	bool ___m_TriedToResolveUnknownBottleneck_8;
	// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::m_Enabled
	bool ___m_Enabled_9;
	// System.String UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::m_featureName
	String_t* ___m_featureName_10;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::<TargetFrameTime>k__BackingField
	float ___U3CTargetFrameTimeU3Ek__BackingField_11;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::<AllowedCpuActiveTimeRatio>k__BackingField
	float ___U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::<AllowedGpuActiveTimeRatio>k__BackingField
	float ___U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::<GpuLevelBounceAvoidanceThreshold>k__BackingField
	float ___U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::<CpuLevelBounceAvoidanceThreshold>k__BackingField
	float ___U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::<UpdateInterval>k__BackingField
	float ___U3CUpdateIntervalU3Ek__BackingField_16;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::<MinTargetFrameRateHitTime>k__BackingField
	float ___U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17;
	// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::<MaxTemperatureLevel>k__BackingField
	float ___U3CMaxTemperatureLevelU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_PerfControl_0() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___m_PerfControl_0)); }
	inline RuntimeObject* get_m_PerfControl_0() const { return ___m_PerfControl_0; }
	inline RuntimeObject** get_address_of_m_PerfControl_0() { return &___m_PerfControl_0; }
	inline void set_m_PerfControl_0(RuntimeObject* value)
	{
		___m_PerfControl_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PerfControl_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PerfStats_1() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___m_PerfStats_1)); }
	inline RuntimeObject* get_m_PerfStats_1() const { return ___m_PerfStats_1; }
	inline RuntimeObject** get_address_of_m_PerfStats_1() { return &___m_PerfStats_1; }
	inline void set_m_PerfStats_1(RuntimeObject* value)
	{
		___m_PerfStats_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PerfStats_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ThermalStats_2() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___m_ThermalStats_2)); }
	inline RuntimeObject* get_m_ThermalStats_2() const { return ___m_ThermalStats_2; }
	inline RuntimeObject** get_address_of_m_ThermalStats_2() { return &___m_ThermalStats_2; }
	inline void set_m_ThermalStats_2(RuntimeObject* value)
	{
		___m_ThermalStats_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ThermalStats_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_LastChangeTimeStamp_3() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___m_LastChangeTimeStamp_3)); }
	inline float get_m_LastChangeTimeStamp_3() const { return ___m_LastChangeTimeStamp_3; }
	inline float* get_address_of_m_LastChangeTimeStamp_3() { return &___m_LastChangeTimeStamp_3; }
	inline void set_m_LastChangeTimeStamp_3(float value)
	{
		___m_LastChangeTimeStamp_3 = value;
	}

	inline static int32_t get_offset_of_m_LastGpuLevelRaiseTimeStamp_4() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___m_LastGpuLevelRaiseTimeStamp_4)); }
	inline float get_m_LastGpuLevelRaiseTimeStamp_4() const { return ___m_LastGpuLevelRaiseTimeStamp_4; }
	inline float* get_address_of_m_LastGpuLevelRaiseTimeStamp_4() { return &___m_LastGpuLevelRaiseTimeStamp_4; }
	inline void set_m_LastGpuLevelRaiseTimeStamp_4(float value)
	{
		___m_LastGpuLevelRaiseTimeStamp_4 = value;
	}

	inline static int32_t get_offset_of_m_LastCpuLevelRaiseTimeStamp_5() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___m_LastCpuLevelRaiseTimeStamp_5)); }
	inline float get_m_LastCpuLevelRaiseTimeStamp_5() const { return ___m_LastCpuLevelRaiseTimeStamp_5; }
	inline float* get_address_of_m_LastCpuLevelRaiseTimeStamp_5() { return &___m_LastCpuLevelRaiseTimeStamp_5; }
	inline void set_m_LastCpuLevelRaiseTimeStamp_5(float value)
	{
		___m_LastCpuLevelRaiseTimeStamp_5 = value;
	}

	inline static int32_t get_offset_of_m_TargetFrameRateHitTimestamp_6() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___m_TargetFrameRateHitTimestamp_6)); }
	inline float get_m_TargetFrameRateHitTimestamp_6() const { return ___m_TargetFrameRateHitTimestamp_6; }
	inline float* get_address_of_m_TargetFrameRateHitTimestamp_6() { return &___m_TargetFrameRateHitTimestamp_6; }
	inline void set_m_TargetFrameRateHitTimestamp_6(float value)
	{
		___m_TargetFrameRateHitTimestamp_6 = value;
	}

	inline static int32_t get_offset_of_m_BottleneckUnknownTimestamp_7() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___m_BottleneckUnknownTimestamp_7)); }
	inline float get_m_BottleneckUnknownTimestamp_7() const { return ___m_BottleneckUnknownTimestamp_7; }
	inline float* get_address_of_m_BottleneckUnknownTimestamp_7() { return &___m_BottleneckUnknownTimestamp_7; }
	inline void set_m_BottleneckUnknownTimestamp_7(float value)
	{
		___m_BottleneckUnknownTimestamp_7 = value;
	}

	inline static int32_t get_offset_of_m_TriedToResolveUnknownBottleneck_8() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___m_TriedToResolveUnknownBottleneck_8)); }
	inline bool get_m_TriedToResolveUnknownBottleneck_8() const { return ___m_TriedToResolveUnknownBottleneck_8; }
	inline bool* get_address_of_m_TriedToResolveUnknownBottleneck_8() { return &___m_TriedToResolveUnknownBottleneck_8; }
	inline void set_m_TriedToResolveUnknownBottleneck_8(bool value)
	{
		___m_TriedToResolveUnknownBottleneck_8 = value;
	}

	inline static int32_t get_offset_of_m_Enabled_9() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___m_Enabled_9)); }
	inline bool get_m_Enabled_9() const { return ___m_Enabled_9; }
	inline bool* get_address_of_m_Enabled_9() { return &___m_Enabled_9; }
	inline void set_m_Enabled_9(bool value)
	{
		___m_Enabled_9 = value;
	}

	inline static int32_t get_offset_of_m_featureName_10() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___m_featureName_10)); }
	inline String_t* get_m_featureName_10() const { return ___m_featureName_10; }
	inline String_t** get_address_of_m_featureName_10() { return &___m_featureName_10; }
	inline void set_m_featureName_10(String_t* value)
	{
		___m_featureName_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_featureName_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTargetFrameTimeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___U3CTargetFrameTimeU3Ek__BackingField_11)); }
	inline float get_U3CTargetFrameTimeU3Ek__BackingField_11() const { return ___U3CTargetFrameTimeU3Ek__BackingField_11; }
	inline float* get_address_of_U3CTargetFrameTimeU3Ek__BackingField_11() { return &___U3CTargetFrameTimeU3Ek__BackingField_11; }
	inline void set_U3CTargetFrameTimeU3Ek__BackingField_11(float value)
	{
		___U3CTargetFrameTimeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12)); }
	inline float get_U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12() const { return ___U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12; }
	inline float* get_address_of_U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12() { return &___U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12; }
	inline void set_U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12(float value)
	{
		___U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13)); }
	inline float get_U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13() const { return ___U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13; }
	inline float* get_address_of_U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13() { return &___U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13; }
	inline void set_U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13(float value)
	{
		___U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14)); }
	inline float get_U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14() const { return ___U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14; }
	inline float* get_address_of_U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14() { return &___U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14; }
	inline void set_U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14(float value)
	{
		___U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15)); }
	inline float get_U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15() const { return ___U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15; }
	inline float* get_address_of_U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15() { return &___U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15; }
	inline void set_U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15(float value)
	{
		___U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CUpdateIntervalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___U3CUpdateIntervalU3Ek__BackingField_16)); }
	inline float get_U3CUpdateIntervalU3Ek__BackingField_16() const { return ___U3CUpdateIntervalU3Ek__BackingField_16; }
	inline float* get_address_of_U3CUpdateIntervalU3Ek__BackingField_16() { return &___U3CUpdateIntervalU3Ek__BackingField_16; }
	inline void set_U3CUpdateIntervalU3Ek__BackingField_16(float value)
	{
		___U3CUpdateIntervalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17)); }
	inline float get_U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17() const { return ___U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17() { return &___U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17; }
	inline void set_U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17(float value)
	{
		___U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CMaxTemperatureLevelU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A, ___U3CMaxTemperatureLevelU3Ek__BackingField_18)); }
	inline float get_U3CMaxTemperatureLevelU3Ek__BackingField_18() const { return ___U3CMaxTemperatureLevelU3Ek__BackingField_18; }
	inline float* get_address_of_U3CMaxTemperatureLevelU3Ek__BackingField_18() { return &___U3CMaxTemperatureLevelU3Ek__BackingField_18; }
	inline void set_U3CMaxTemperatureLevelU3Ek__BackingField_18(float value)
	{
		___U3CMaxTemperatureLevelU3Ek__BackingField_18 = value;
	}
};


// UnityEngine.AdaptivePerformance.BottleneckUtil
struct  BottleneckUtil_tCA8AB200FDC31E60902EAD6E130EBA06153AD9FD  : public RuntimeObject
{
public:

public:
};


// UnityEngine.AdaptivePerformance.CpuTimeProvider
struct  CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994  : public RuntimeObject
{
public:
	// UnityEngine.AdaptivePerformance.RenderThreadCpuTime UnityEngine.AdaptivePerformance.CpuTimeProvider::m_RenderThreadCpuTime
	RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * ___m_RenderThreadCpuTime_0;
	// UnityEngine.AdaptivePerformance.MainThreadCpuTime UnityEngine.AdaptivePerformance.CpuTimeProvider::m_MainThreadCpuTime
	MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * ___m_MainThreadCpuTime_1;

public:
	inline static int32_t get_offset_of_m_RenderThreadCpuTime_0() { return static_cast<int32_t>(offsetof(CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994, ___m_RenderThreadCpuTime_0)); }
	inline RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * get_m_RenderThreadCpuTime_0() const { return ___m_RenderThreadCpuTime_0; }
	inline RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 ** get_address_of_m_RenderThreadCpuTime_0() { return &___m_RenderThreadCpuTime_0; }
	inline void set_m_RenderThreadCpuTime_0(RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * value)
	{
		___m_RenderThreadCpuTime_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RenderThreadCpuTime_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_MainThreadCpuTime_1() { return static_cast<int32_t>(offsetof(CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994, ___m_MainThreadCpuTime_1)); }
	inline MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * get_m_MainThreadCpuTime_1() const { return ___m_MainThreadCpuTime_1; }
	inline MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 ** get_address_of_m_MainThreadCpuTime_1() { return &___m_MainThreadCpuTime_1; }
	inline void set_m_MainThreadCpuTime_1(MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * value)
	{
		___m_MainThreadCpuTime_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MainThreadCpuTime_1), (void*)value);
	}
};


// UnityEngine.AdaptivePerformance.GpuTimeProvider
struct  GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B  : public RuntimeObject
{
public:
	// UnityEngine.FrameTiming[] UnityEngine.AdaptivePerformance.GpuTimeProvider::m_FrameTiming
	FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3* ___m_FrameTiming_0;

public:
	inline static int32_t get_offset_of_m_FrameTiming_0() { return static_cast<int32_t>(offsetof(GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B, ___m_FrameTiming_0)); }
	inline FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3* get_m_FrameTiming_0() const { return ___m_FrameTiming_0; }
	inline FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3** get_address_of_m_FrameTiming_0() { return &___m_FrameTiming_0; }
	inline void set_m_FrameTiming_0(FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3* value)
	{
		___m_FrameTiming_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FrameTiming_0), (void*)value);
	}
};


// UnityEngine.AdaptivePerformance.Holder
struct  Holder_t3A26EE4375105172B35CF838E774E2DB72D06DF9  : public RuntimeObject
{
public:

public:
};

struct Holder_t3A26EE4375105172B35CF838E774E2DB72D06DF9_StaticFields
{
public:
	// UnityEngine.AdaptivePerformance.IAdaptivePerformance UnityEngine.AdaptivePerformance.Holder::<Instance>k__BackingField
	RuntimeObject* ___U3CInstanceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Holder_t3A26EE4375105172B35CF838E774E2DB72D06DF9_StaticFields, ___U3CInstanceU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CInstanceU3Ek__BackingField_0() const { return ___U3CInstanceU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CInstanceU3Ek__BackingField_0() { return &___U3CInstanceU3Ek__BackingField_0; }
	inline void set_U3CInstanceU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CInstanceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_0), (void*)value);
	}
};


// UnityEngine.AdaptivePerformance.MainThreadCpuTime
struct  MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887  : public RuntimeObject
{
public:
	// System.Double UnityEngine.AdaptivePerformance.MainThreadCpuTime::m_LastAbsoluteMainThreadCpuTime
	double ___m_LastAbsoluteMainThreadCpuTime_0;
	// System.Single UnityEngine.AdaptivePerformance.MainThreadCpuTime::m_LatestMainthreadCpuTime
	float ___m_LatestMainthreadCpuTime_1;

public:
	inline static int32_t get_offset_of_m_LastAbsoluteMainThreadCpuTime_0() { return static_cast<int32_t>(offsetof(MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887, ___m_LastAbsoluteMainThreadCpuTime_0)); }
	inline double get_m_LastAbsoluteMainThreadCpuTime_0() const { return ___m_LastAbsoluteMainThreadCpuTime_0; }
	inline double* get_address_of_m_LastAbsoluteMainThreadCpuTime_0() { return &___m_LastAbsoluteMainThreadCpuTime_0; }
	inline void set_m_LastAbsoluteMainThreadCpuTime_0(double value)
	{
		___m_LastAbsoluteMainThreadCpuTime_0 = value;
	}

	inline static int32_t get_offset_of_m_LatestMainthreadCpuTime_1() { return static_cast<int32_t>(offsetof(MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887, ___m_LatestMainthreadCpuTime_1)); }
	inline float get_m_LatestMainthreadCpuTime_1() const { return ___m_LatestMainthreadCpuTime_1; }
	inline float* get_address_of_m_LatestMainthreadCpuTime_1() { return &___m_LatestMainthreadCpuTime_1; }
	inline void set_m_LatestMainthreadCpuTime_1(float value)
	{
		___m_LatestMainthreadCpuTime_1 = value;
	}
};


// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemRegistry
struct  AdaptivePerformanceSubsystemRegistry_t856824FC32076823DA31F47F3666E8D5211F3554  : public RuntimeObject
{
public:

public:
};


// UnityEngine.AdaptivePerformance.RenderThreadCpuTime
struct  RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710  : public RuntimeObject
{
public:

public:
};


// UnityEngine.AdaptivePerformance.RunningAverage
struct  RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1  : public RuntimeObject
{
public:
	// System.Single[] UnityEngine.AdaptivePerformance.RunningAverage::m_Values
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_Values_0;
	// System.Int32 UnityEngine.AdaptivePerformance.RunningAverage::m_NumValues
	int32_t ___m_NumValues_1;
	// System.Int32 UnityEngine.AdaptivePerformance.RunningAverage::m_LastIndex
	int32_t ___m_LastIndex_2;
	// System.Single UnityEngine.AdaptivePerformance.RunningAverage::m_AverageValue
	float ___m_AverageValue_3;

public:
	inline static int32_t get_offset_of_m_Values_0() { return static_cast<int32_t>(offsetof(RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1, ___m_Values_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_Values_0() const { return ___m_Values_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_Values_0() { return &___m_Values_0; }
	inline void set_m_Values_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_Values_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Values_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_NumValues_1() { return static_cast<int32_t>(offsetof(RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1, ___m_NumValues_1)); }
	inline int32_t get_m_NumValues_1() const { return ___m_NumValues_1; }
	inline int32_t* get_address_of_m_NumValues_1() { return &___m_NumValues_1; }
	inline void set_m_NumValues_1(int32_t value)
	{
		___m_NumValues_1 = value;
	}

	inline static int32_t get_offset_of_m_LastIndex_2() { return static_cast<int32_t>(offsetof(RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1, ___m_LastIndex_2)); }
	inline int32_t get_m_LastIndex_2() const { return ___m_LastIndex_2; }
	inline int32_t* get_address_of_m_LastIndex_2() { return &___m_LastIndex_2; }
	inline void set_m_LastIndex_2(int32_t value)
	{
		___m_LastIndex_2 = value;
	}

	inline static int32_t get_offset_of_m_AverageValue_3() { return static_cast<int32_t>(offsetof(RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1, ___m_AverageValue_3)); }
	inline float get_m_AverageValue_3() const { return ___m_AverageValue_3; }
	inline float* get_address_of_m_AverageValue_3() { return &___m_AverageValue_3; }
	inline void set_m_AverageValue_3(float value)
	{
		___m_AverageValue_3 = value;
	}
};


// UnityEngine.AdaptivePerformance.StartupSettings
struct  StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187  : public RuntimeObject
{
public:

public:
};

struct StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields
{
public:
	// System.Boolean UnityEngine.AdaptivePerformance.StartupSettings::<Logging>k__BackingField
	bool ___U3CLoggingU3Ek__BackingField_0;
	// System.Int32 UnityEngine.AdaptivePerformance.StartupSettings::<StatsLoggingFrequencyInFrames>k__BackingField
	int32_t ___U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1;
	// System.Boolean UnityEngine.AdaptivePerformance.StartupSettings::<Enable>k__BackingField
	bool ___U3CEnableU3Ek__BackingField_2;
	// System.Boolean UnityEngine.AdaptivePerformance.StartupSettings::<AutomaticPerformanceControl>k__BackingField
	bool ___U3CAutomaticPerformanceControlU3Ek__BackingField_3;
	// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem UnityEngine.AdaptivePerformance.StartupSettings::<PreferredSubsystem>k__BackingField
	AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * ___U3CPreferredSubsystemU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CLoggingU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields, ___U3CLoggingU3Ek__BackingField_0)); }
	inline bool get_U3CLoggingU3Ek__BackingField_0() const { return ___U3CLoggingU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CLoggingU3Ek__BackingField_0() { return &___U3CLoggingU3Ek__BackingField_0; }
	inline void set_U3CLoggingU3Ek__BackingField_0(bool value)
	{
		___U3CLoggingU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields, ___U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1)); }
	inline int32_t get_U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1() const { return ___U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1() { return &___U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1; }
	inline void set_U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1(int32_t value)
	{
		___U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CEnableU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields, ___U3CEnableU3Ek__BackingField_2)); }
	inline bool get_U3CEnableU3Ek__BackingField_2() const { return ___U3CEnableU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CEnableU3Ek__BackingField_2() { return &___U3CEnableU3Ek__BackingField_2; }
	inline void set_U3CEnableU3Ek__BackingField_2(bool value)
	{
		___U3CEnableU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CAutomaticPerformanceControlU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields, ___U3CAutomaticPerformanceControlU3Ek__BackingField_3)); }
	inline bool get_U3CAutomaticPerformanceControlU3Ek__BackingField_3() const { return ___U3CAutomaticPerformanceControlU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CAutomaticPerformanceControlU3Ek__BackingField_3() { return &___U3CAutomaticPerformanceControlU3Ek__BackingField_3; }
	inline void set_U3CAutomaticPerformanceControlU3Ek__BackingField_3(bool value)
	{
		___U3CAutomaticPerformanceControlU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CPreferredSubsystemU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields, ___U3CPreferredSubsystemU3Ek__BackingField_4)); }
	inline AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * get_U3CPreferredSubsystemU3Ek__BackingField_4() const { return ___U3CPreferredSubsystemU3Ek__BackingField_4; }
	inline AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 ** get_address_of_U3CPreferredSubsystemU3Ek__BackingField_4() { return &___U3CPreferredSubsystemU3Ek__BackingField_4; }
	inline void set_U3CPreferredSubsystemU3Ek__BackingField_4(AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * value)
	{
		___U3CPreferredSubsystemU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPreferredSubsystemU3Ek__BackingField_4), (void*)value);
	}
};


// UnityEngine.AdaptivePerformance.TemperatureTrend
struct  TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.AdaptivePerformance.TemperatureTrend::m_UseProviderTrend
	bool ___m_UseProviderTrend_0;
	// System.Double UnityEngine.AdaptivePerformance.TemperatureTrend::m_SumX
	double ___m_SumX_1;
	// System.Double UnityEngine.AdaptivePerformance.TemperatureTrend::m_SumY
	double ___m_SumY_2;
	// System.Double UnityEngine.AdaptivePerformance.TemperatureTrend::m_SumXY
	double ___m_SumXY_3;
	// System.Double UnityEngine.AdaptivePerformance.TemperatureTrend::m_SumXX
	double ___m_SumXX_4;
	// System.Single[] UnityEngine.AdaptivePerformance.TemperatureTrend::m_TimeStamps
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_TimeStamps_10;
	// System.Single[] UnityEngine.AdaptivePerformance.TemperatureTrend::m_Temperature
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_Temperature_11;
	// System.Int32 UnityEngine.AdaptivePerformance.TemperatureTrend::m_NumValues
	int32_t ___m_NumValues_12;
	// System.Int32 UnityEngine.AdaptivePerformance.TemperatureTrend::m_NextValueIndex
	int32_t ___m_NextValueIndex_13;
	// System.Int32 UnityEngine.AdaptivePerformance.TemperatureTrend::m_OldestValueIndex
	int32_t ___m_OldestValueIndex_14;
	// System.Single UnityEngine.AdaptivePerformance.TemperatureTrend::<ThermalTrend>k__BackingField
	float ___U3CThermalTrendU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_m_UseProviderTrend_0() { return static_cast<int32_t>(offsetof(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B, ___m_UseProviderTrend_0)); }
	inline bool get_m_UseProviderTrend_0() const { return ___m_UseProviderTrend_0; }
	inline bool* get_address_of_m_UseProviderTrend_0() { return &___m_UseProviderTrend_0; }
	inline void set_m_UseProviderTrend_0(bool value)
	{
		___m_UseProviderTrend_0 = value;
	}

	inline static int32_t get_offset_of_m_SumX_1() { return static_cast<int32_t>(offsetof(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B, ___m_SumX_1)); }
	inline double get_m_SumX_1() const { return ___m_SumX_1; }
	inline double* get_address_of_m_SumX_1() { return &___m_SumX_1; }
	inline void set_m_SumX_1(double value)
	{
		___m_SumX_1 = value;
	}

	inline static int32_t get_offset_of_m_SumY_2() { return static_cast<int32_t>(offsetof(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B, ___m_SumY_2)); }
	inline double get_m_SumY_2() const { return ___m_SumY_2; }
	inline double* get_address_of_m_SumY_2() { return &___m_SumY_2; }
	inline void set_m_SumY_2(double value)
	{
		___m_SumY_2 = value;
	}

	inline static int32_t get_offset_of_m_SumXY_3() { return static_cast<int32_t>(offsetof(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B, ___m_SumXY_3)); }
	inline double get_m_SumXY_3() const { return ___m_SumXY_3; }
	inline double* get_address_of_m_SumXY_3() { return &___m_SumXY_3; }
	inline void set_m_SumXY_3(double value)
	{
		___m_SumXY_3 = value;
	}

	inline static int32_t get_offset_of_m_SumXX_4() { return static_cast<int32_t>(offsetof(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B, ___m_SumXX_4)); }
	inline double get_m_SumXX_4() const { return ___m_SumXX_4; }
	inline double* get_address_of_m_SumXX_4() { return &___m_SumXX_4; }
	inline void set_m_SumXX_4(double value)
	{
		___m_SumXX_4 = value;
	}

	inline static int32_t get_offset_of_m_TimeStamps_10() { return static_cast<int32_t>(offsetof(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B, ___m_TimeStamps_10)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_TimeStamps_10() const { return ___m_TimeStamps_10; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_TimeStamps_10() { return &___m_TimeStamps_10; }
	inline void set_m_TimeStamps_10(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_TimeStamps_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TimeStamps_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_Temperature_11() { return static_cast<int32_t>(offsetof(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B, ___m_Temperature_11)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_Temperature_11() const { return ___m_Temperature_11; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_Temperature_11() { return &___m_Temperature_11; }
	inline void set_m_Temperature_11(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_Temperature_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Temperature_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_NumValues_12() { return static_cast<int32_t>(offsetof(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B, ___m_NumValues_12)); }
	inline int32_t get_m_NumValues_12() const { return ___m_NumValues_12; }
	inline int32_t* get_address_of_m_NumValues_12() { return &___m_NumValues_12; }
	inline void set_m_NumValues_12(int32_t value)
	{
		___m_NumValues_12 = value;
	}

	inline static int32_t get_offset_of_m_NextValueIndex_13() { return static_cast<int32_t>(offsetof(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B, ___m_NextValueIndex_13)); }
	inline int32_t get_m_NextValueIndex_13() const { return ___m_NextValueIndex_13; }
	inline int32_t* get_address_of_m_NextValueIndex_13() { return &___m_NextValueIndex_13; }
	inline void set_m_NextValueIndex_13(int32_t value)
	{
		___m_NextValueIndex_13 = value;
	}

	inline static int32_t get_offset_of_m_OldestValueIndex_14() { return static_cast<int32_t>(offsetof(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B, ___m_OldestValueIndex_14)); }
	inline int32_t get_m_OldestValueIndex_14() const { return ___m_OldestValueIndex_14; }
	inline int32_t* get_address_of_m_OldestValueIndex_14() { return &___m_OldestValueIndex_14; }
	inline void set_m_OldestValueIndex_14(int32_t value)
	{
		___m_OldestValueIndex_14 = value;
	}

	inline static int32_t get_offset_of_U3CThermalTrendU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B, ___U3CThermalTrendU3Ek__BackingField_15)); }
	inline float get_U3CThermalTrendU3Ek__BackingField_15() const { return ___U3CThermalTrendU3Ek__BackingField_15; }
	inline float* get_address_of_U3CThermalTrendU3Ek__BackingField_15() { return &___U3CThermalTrendU3Ek__BackingField_15; }
	inline void set_U3CThermalTrendU3Ek__BackingField_15(float value)
	{
		___U3CThermalTrendU3Ek__BackingField_15 = value;
	}
};


// UnityEngine.Subsystem
struct  Subsystem_t17E4AEB5537DC8AECC37EC3F6FCB46CC7D2C73F6  : public RuntimeObject
{
public:
	// UnityEngine.ISubsystemDescriptor UnityEngine.Subsystem::m_subsystemDescriptor
	RuntimeObject* ___m_subsystemDescriptor_0;

public:
	inline static int32_t get_offset_of_m_subsystemDescriptor_0() { return static_cast<int32_t>(offsetof(Subsystem_t17E4AEB5537DC8AECC37EC3F6FCB46CC7D2C73F6, ___m_subsystemDescriptor_0)); }
	inline RuntimeObject* get_m_subsystemDescriptor_0() const { return ___m_subsystemDescriptor_0; }
	inline RuntimeObject** get_address_of_m_subsystemDescriptor_0() { return &___m_subsystemDescriptor_0; }
	inline void set_m_subsystemDescriptor_0(RuntimeObject* value)
	{
		___m_subsystemDescriptor_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_subsystemDescriptor_0), (void*)value);
	}
};


// UnityEngine.SubsystemDescriptor
struct  SubsystemDescriptor_tDE3D888281281BBD122D6D9E7B49F626E69340CA  : public RuntimeObject
{
public:
	// System.String UnityEngine.SubsystemDescriptor::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.Type UnityEngine.SubsystemDescriptor::<subsystemImplementationType>k__BackingField
	Type_t * ___U3CsubsystemImplementationTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SubsystemDescriptor_tDE3D888281281BBD122D6D9E7B49F626E69340CA, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CidU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsubsystemImplementationTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SubsystemDescriptor_tDE3D888281281BBD122D6D9E7B49F626E69340CA, ___U3CsubsystemImplementationTypeU3Ek__BackingField_1)); }
	inline Type_t * get_U3CsubsystemImplementationTypeU3Ek__BackingField_1() const { return ___U3CsubsystemImplementationTypeU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CsubsystemImplementationTypeU3Ek__BackingField_1() { return &___U3CsubsystemImplementationTypeU3Ek__BackingField_1; }
	inline void set_U3CsubsystemImplementationTypeU3Ek__BackingField_1(Type_t * value)
	{
		___U3CsubsystemImplementationTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemImplementationTypeU3Ek__BackingField_1), (void*)value);
	}
};


// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Object>
struct  Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___list_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_list_0() const { return ___list_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>
struct  Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	AdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325, ___list_0)); }
	inline List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * get_list_0() const { return ___list_0; }
	inline List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325, ___current_3)); }
	inline AdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55 * get_current_3() const { return ___current_3; }
	inline AdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(AdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.AdaptivePerformance.FrameTiming
struct  FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 
{
public:
	// System.Single UnityEngine.AdaptivePerformance.FrameTiming::<CurrentFrameTime>k__BackingField
	float ___U3CCurrentFrameTimeU3Ek__BackingField_0;
	// System.Single UnityEngine.AdaptivePerformance.FrameTiming::<AverageFrameTime>k__BackingField
	float ___U3CAverageFrameTimeU3Ek__BackingField_1;
	// System.Single UnityEngine.AdaptivePerformance.FrameTiming::<CurrentGpuFrameTime>k__BackingField
	float ___U3CCurrentGpuFrameTimeU3Ek__BackingField_2;
	// System.Single UnityEngine.AdaptivePerformance.FrameTiming::<AverageGpuFrameTime>k__BackingField
	float ___U3CAverageGpuFrameTimeU3Ek__BackingField_3;
	// System.Single UnityEngine.AdaptivePerformance.FrameTiming::<CurrentCpuFrameTime>k__BackingField
	float ___U3CCurrentCpuFrameTimeU3Ek__BackingField_4;
	// System.Single UnityEngine.AdaptivePerformance.FrameTiming::<AverageCpuFrameTime>k__BackingField
	float ___U3CAverageCpuFrameTimeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CCurrentFrameTimeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1, ___U3CCurrentFrameTimeU3Ek__BackingField_0)); }
	inline float get_U3CCurrentFrameTimeU3Ek__BackingField_0() const { return ___U3CCurrentFrameTimeU3Ek__BackingField_0; }
	inline float* get_address_of_U3CCurrentFrameTimeU3Ek__BackingField_0() { return &___U3CCurrentFrameTimeU3Ek__BackingField_0; }
	inline void set_U3CCurrentFrameTimeU3Ek__BackingField_0(float value)
	{
		___U3CCurrentFrameTimeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CAverageFrameTimeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1, ___U3CAverageFrameTimeU3Ek__BackingField_1)); }
	inline float get_U3CAverageFrameTimeU3Ek__BackingField_1() const { return ___U3CAverageFrameTimeU3Ek__BackingField_1; }
	inline float* get_address_of_U3CAverageFrameTimeU3Ek__BackingField_1() { return &___U3CAverageFrameTimeU3Ek__BackingField_1; }
	inline void set_U3CAverageFrameTimeU3Ek__BackingField_1(float value)
	{
		___U3CAverageFrameTimeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentGpuFrameTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1, ___U3CCurrentGpuFrameTimeU3Ek__BackingField_2)); }
	inline float get_U3CCurrentGpuFrameTimeU3Ek__BackingField_2() const { return ___U3CCurrentGpuFrameTimeU3Ek__BackingField_2; }
	inline float* get_address_of_U3CCurrentGpuFrameTimeU3Ek__BackingField_2() { return &___U3CCurrentGpuFrameTimeU3Ek__BackingField_2; }
	inline void set_U3CCurrentGpuFrameTimeU3Ek__BackingField_2(float value)
	{
		___U3CCurrentGpuFrameTimeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CAverageGpuFrameTimeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1, ___U3CAverageGpuFrameTimeU3Ek__BackingField_3)); }
	inline float get_U3CAverageGpuFrameTimeU3Ek__BackingField_3() const { return ___U3CAverageGpuFrameTimeU3Ek__BackingField_3; }
	inline float* get_address_of_U3CAverageGpuFrameTimeU3Ek__BackingField_3() { return &___U3CAverageGpuFrameTimeU3Ek__BackingField_3; }
	inline void set_U3CAverageGpuFrameTimeU3Ek__BackingField_3(float value)
	{
		___U3CAverageGpuFrameTimeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentCpuFrameTimeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1, ___U3CCurrentCpuFrameTimeU3Ek__BackingField_4)); }
	inline float get_U3CCurrentCpuFrameTimeU3Ek__BackingField_4() const { return ___U3CCurrentCpuFrameTimeU3Ek__BackingField_4; }
	inline float* get_address_of_U3CCurrentCpuFrameTimeU3Ek__BackingField_4() { return &___U3CCurrentCpuFrameTimeU3Ek__BackingField_4; }
	inline void set_U3CCurrentCpuFrameTimeU3Ek__BackingField_4(float value)
	{
		___U3CCurrentCpuFrameTimeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CAverageCpuFrameTimeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1, ___U3CAverageCpuFrameTimeU3Ek__BackingField_5)); }
	inline float get_U3CAverageCpuFrameTimeU3Ek__BackingField_5() const { return ___U3CAverageCpuFrameTimeU3Ek__BackingField_5; }
	inline float* get_address_of_U3CAverageCpuFrameTimeU3Ek__BackingField_5() { return &___U3CAverageCpuFrameTimeU3Ek__BackingField_5; }
	inline void set_U3CAverageCpuFrameTimeU3Ek__BackingField_5(float value)
	{
		___U3CAverageCpuFrameTimeU3Ek__BackingField_5 = value;
	}
};


// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor_Cinfo
struct  Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112 
{
public:
	// System.String UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor_Cinfo::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.Type UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor_Cinfo::<subsystemImplementationType>k__BackingField
	Type_t * ___U3CsubsystemImplementationTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CidU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsubsystemImplementationTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112, ___U3CsubsystemImplementationTypeU3Ek__BackingField_1)); }
	inline Type_t * get_U3CsubsystemImplementationTypeU3Ek__BackingField_1() const { return ___U3CsubsystemImplementationTypeU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CsubsystemImplementationTypeU3Ek__BackingField_1() { return &___U3CsubsystemImplementationTypeU3Ek__BackingField_1; }
	inline void set_U3CsubsystemImplementationTypeU3Ek__BackingField_1(Type_t * value)
	{
		___U3CsubsystemImplementationTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemImplementationTypeU3Ek__BackingField_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor/Cinfo
struct Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshaled_pinvoke
{
	char* ___U3CidU3Ek__BackingField_0;
	Type_t * ___U3CsubsystemImplementationTypeU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor/Cinfo
struct Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshaled_com
{
	Il2CppChar* ___U3CidU3Ek__BackingField_0;
	Type_t * ___U3CsubsystemImplementationTypeU3Ek__BackingField_1;
};

// UnityEngine.FrameTiming
struct  FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC 
{
public:
	// System.UInt64 UnityEngine.FrameTiming::cpuTimePresentCalled
	uint64_t ___cpuTimePresentCalled_0;
	// System.Double UnityEngine.FrameTiming::cpuFrameTime
	double ___cpuFrameTime_1;
	// System.UInt64 UnityEngine.FrameTiming::cpuTimeFrameComplete
	uint64_t ___cpuTimeFrameComplete_2;
	// System.Double UnityEngine.FrameTiming::gpuFrameTime
	double ___gpuFrameTime_3;
	// System.Single UnityEngine.FrameTiming::heightScale
	float ___heightScale_4;
	// System.Single UnityEngine.FrameTiming::widthScale
	float ___widthScale_5;
	// System.UInt32 UnityEngine.FrameTiming::syncInterval
	uint32_t ___syncInterval_6;

public:
	inline static int32_t get_offset_of_cpuTimePresentCalled_0() { return static_cast<int32_t>(offsetof(FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC, ___cpuTimePresentCalled_0)); }
	inline uint64_t get_cpuTimePresentCalled_0() const { return ___cpuTimePresentCalled_0; }
	inline uint64_t* get_address_of_cpuTimePresentCalled_0() { return &___cpuTimePresentCalled_0; }
	inline void set_cpuTimePresentCalled_0(uint64_t value)
	{
		___cpuTimePresentCalled_0 = value;
	}

	inline static int32_t get_offset_of_cpuFrameTime_1() { return static_cast<int32_t>(offsetof(FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC, ___cpuFrameTime_1)); }
	inline double get_cpuFrameTime_1() const { return ___cpuFrameTime_1; }
	inline double* get_address_of_cpuFrameTime_1() { return &___cpuFrameTime_1; }
	inline void set_cpuFrameTime_1(double value)
	{
		___cpuFrameTime_1 = value;
	}

	inline static int32_t get_offset_of_cpuTimeFrameComplete_2() { return static_cast<int32_t>(offsetof(FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC, ___cpuTimeFrameComplete_2)); }
	inline uint64_t get_cpuTimeFrameComplete_2() const { return ___cpuTimeFrameComplete_2; }
	inline uint64_t* get_address_of_cpuTimeFrameComplete_2() { return &___cpuTimeFrameComplete_2; }
	inline void set_cpuTimeFrameComplete_2(uint64_t value)
	{
		___cpuTimeFrameComplete_2 = value;
	}

	inline static int32_t get_offset_of_gpuFrameTime_3() { return static_cast<int32_t>(offsetof(FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC, ___gpuFrameTime_3)); }
	inline double get_gpuFrameTime_3() const { return ___gpuFrameTime_3; }
	inline double* get_address_of_gpuFrameTime_3() { return &___gpuFrameTime_3; }
	inline void set_gpuFrameTime_3(double value)
	{
		___gpuFrameTime_3 = value;
	}

	inline static int32_t get_offset_of_heightScale_4() { return static_cast<int32_t>(offsetof(FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC, ___heightScale_4)); }
	inline float get_heightScale_4() const { return ___heightScale_4; }
	inline float* get_address_of_heightScale_4() { return &___heightScale_4; }
	inline void set_heightScale_4(float value)
	{
		___heightScale_4 = value;
	}

	inline static int32_t get_offset_of_widthScale_5() { return static_cast<int32_t>(offsetof(FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC, ___widthScale_5)); }
	inline float get_widthScale_5() const { return ___widthScale_5; }
	inline float* get_address_of_widthScale_5() { return &___widthScale_5; }
	inline void set_widthScale_5(float value)
	{
		___widthScale_5 = value;
	}

	inline static int32_t get_offset_of_syncInterval_6() { return static_cast<int32_t>(offsetof(FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC, ___syncInterval_6)); }
	inline uint32_t get_syncInterval_6() const { return ___syncInterval_6; }
	inline uint32_t* get_address_of_syncInterval_6() { return &___syncInterval_6; }
	inline void set_syncInterval_6(uint32_t value)
	{
		___syncInterval_6 = value;
	}
};


// UnityEngine.SubsystemDescriptor`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem>
struct  SubsystemDescriptor_1_t260E615AF73615B1D344C323E0F4A6CBB7360C20  : public SubsystemDescriptor_tDE3D888281281BBD122D6D9E7B49F626E69340CA
{
public:

public:
};


// UnityEngine.Subsystem`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>
struct  Subsystem_1_t5C16E764132B6384B05BBEE4DA18A26F3A78C9FD  : public Subsystem_t17E4AEB5537DC8AECC37EC3F6FCB46CC7D2C73F6
{
public:

public:
};


// UnityEngine.WaitForEndOfFrame
struct  WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:

public:
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.AdaptivePerformance.PerformanceBottleneck
struct  PerformanceBottleneck_t31566C822D52D2396B8FA588A7EDCA6F07B40DF6 
{
public:
	// System.Int32 UnityEngine.AdaptivePerformance.PerformanceBottleneck::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PerformanceBottleneck_t31566C822D52D2396B8FA588A7EDCA6F07B40DF6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AdaptivePerformance.PerformanceControlMode
struct  PerformanceControlMode_tD838A6117AA1DD26BD6AD2A677A55CD4B9AF2A60 
{
public:
	// System.Int32 UnityEngine.AdaptivePerformance.PerformanceControlMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PerformanceControlMode_tD838A6117AA1DD26BD6AD2A677A55CD4B9AF2A60, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemBase
struct  AdaptivePerformanceSubsystemBase_tFE4445DE6A7F0520A1707ECCC71C516E3E4FB12E  : public Subsystem_1_t5C16E764132B6384B05BBEE4DA18A26F3A78C9FD
{
public:
	// System.Boolean UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemBase::<initialized>k__BackingField
	bool ___U3CinitializedU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CinitializedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdaptivePerformanceSubsystemBase_tFE4445DE6A7F0520A1707ECCC71C516E3E4FB12E, ___U3CinitializedU3Ek__BackingField_1)); }
	inline bool get_U3CinitializedU3Ek__BackingField_1() const { return ___U3CinitializedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CinitializedU3Ek__BackingField_1() { return &___U3CinitializedU3Ek__BackingField_1; }
	inline void set_U3CinitializedU3Ek__BackingField_1(bool value)
	{
		___U3CinitializedU3Ek__BackingField_1 = value;
	}
};


// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor
struct  AdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55  : public SubsystemDescriptor_1_t260E615AF73615B1D344C323E0F4A6CBB7360C20
{
public:

public:
};


// UnityEngine.AdaptivePerformance.Provider.Feature
struct  Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5 
{
public:
	// System.Int32 UnityEngine.AdaptivePerformance.Provider.Feature::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AdaptivePerformance.WarningLevel
struct  WarningLevel_t9378D044885911164B3D07EC4EC933B6B2249114 
{
public:
	// System.Int32 UnityEngine.AdaptivePerformance.WarningLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WarningLevel_t9378D044885911164B3D07EC4EC933B6B2249114, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Coroutine
struct  Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl
struct  DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326  : public RuntimeObject
{
public:
	// UnityEngine.AdaptivePerformance.Provider.IDevicePerformanceLevelControl UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::m_PerformanceLevelControl
	RuntimeObject* ___m_PerformanceLevelControl_0;
	// UnityEngine.AdaptivePerformance.PerformanceControlMode UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::<PerformanceControlMode>k__BackingField
	int32_t ___U3CPerformanceControlModeU3Ek__BackingField_1;
	// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::<CpuLevel>k__BackingField
	int32_t ___U3CCpuLevelU3Ek__BackingField_2;
	// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::<GpuLevel>k__BackingField
	int32_t ___U3CGpuLevelU3Ek__BackingField_3;
	// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::<CurrentCpuLevel>k__BackingField
	int32_t ___U3CCurrentCpuLevelU3Ek__BackingField_4;
	// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::<CurrentGpuLevel>k__BackingField
	int32_t ___U3CCurrentGpuLevelU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_m_PerformanceLevelControl_0() { return static_cast<int32_t>(offsetof(DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326, ___m_PerformanceLevelControl_0)); }
	inline RuntimeObject* get_m_PerformanceLevelControl_0() const { return ___m_PerformanceLevelControl_0; }
	inline RuntimeObject** get_address_of_m_PerformanceLevelControl_0() { return &___m_PerformanceLevelControl_0; }
	inline void set_m_PerformanceLevelControl_0(RuntimeObject* value)
	{
		___m_PerformanceLevelControl_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PerformanceLevelControl_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPerformanceControlModeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326, ___U3CPerformanceControlModeU3Ek__BackingField_1)); }
	inline int32_t get_U3CPerformanceControlModeU3Ek__BackingField_1() const { return ___U3CPerformanceControlModeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPerformanceControlModeU3Ek__BackingField_1() { return &___U3CPerformanceControlModeU3Ek__BackingField_1; }
	inline void set_U3CPerformanceControlModeU3Ek__BackingField_1(int32_t value)
	{
		___U3CPerformanceControlModeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCpuLevelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326, ___U3CCpuLevelU3Ek__BackingField_2)); }
	inline int32_t get_U3CCpuLevelU3Ek__BackingField_2() const { return ___U3CCpuLevelU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CCpuLevelU3Ek__BackingField_2() { return &___U3CCpuLevelU3Ek__BackingField_2; }
	inline void set_U3CCpuLevelU3Ek__BackingField_2(int32_t value)
	{
		___U3CCpuLevelU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CGpuLevelU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326, ___U3CGpuLevelU3Ek__BackingField_3)); }
	inline int32_t get_U3CGpuLevelU3Ek__BackingField_3() const { return ___U3CGpuLevelU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CGpuLevelU3Ek__BackingField_3() { return &___U3CGpuLevelU3Ek__BackingField_3; }
	inline void set_U3CGpuLevelU3Ek__BackingField_3(int32_t value)
	{
		___U3CGpuLevelU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentCpuLevelU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326, ___U3CCurrentCpuLevelU3Ek__BackingField_4)); }
	inline int32_t get_U3CCurrentCpuLevelU3Ek__BackingField_4() const { return ___U3CCurrentCpuLevelU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CCurrentCpuLevelU3Ek__BackingField_4() { return &___U3CCurrentCpuLevelU3Ek__BackingField_4; }
	inline void set_U3CCurrentCpuLevelU3Ek__BackingField_4(int32_t value)
	{
		___U3CCurrentCpuLevelU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentGpuLevelU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326, ___U3CCurrentGpuLevelU3Ek__BackingField_5)); }
	inline int32_t get_U3CCurrentGpuLevelU3Ek__BackingField_5() const { return ___U3CCurrentGpuLevelU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CCurrentGpuLevelU3Ek__BackingField_5() { return &___U3CCurrentGpuLevelU3Ek__BackingField_5; }
	inline void set_U3CCurrentGpuLevelU3Ek__BackingField_5(int32_t value)
	{
		___U3CCurrentGpuLevelU3Ek__BackingField_5 = value;
	}
};


// UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs
struct  PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 
{
public:
	// UnityEngine.AdaptivePerformance.PerformanceBottleneck UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs::<PerformanceBottleneck>k__BackingField
	int32_t ___U3CPerformanceBottleneckU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CPerformanceBottleneckU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1, ___U3CPerformanceBottleneckU3Ek__BackingField_0)); }
	inline int32_t get_U3CPerformanceBottleneckU3Ek__BackingField_0() const { return ___U3CPerformanceBottleneckU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CPerformanceBottleneckU3Ek__BackingField_0() { return &___U3CPerformanceBottleneckU3Ek__BackingField_0; }
	inline void set_U3CPerformanceBottleneckU3Ek__BackingField_0(int32_t value)
	{
		___U3CPerformanceBottleneckU3Ek__BackingField_0 = value;
	}
};


// UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs
struct  PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB 
{
public:
	// System.Int32 UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::<CpuLevel>k__BackingField
	int32_t ___U3CCpuLevelU3Ek__BackingField_0;
	// System.Int32 UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::<CpuLevelDelta>k__BackingField
	int32_t ___U3CCpuLevelDeltaU3Ek__BackingField_1;
	// System.Int32 UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::<GpuLevel>k__BackingField
	int32_t ___U3CGpuLevelU3Ek__BackingField_2;
	// System.Int32 UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::<GpuLevelDelta>k__BackingField
	int32_t ___U3CGpuLevelDeltaU3Ek__BackingField_3;
	// UnityEngine.AdaptivePerformance.PerformanceControlMode UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::<PerformanceControlMode>k__BackingField
	int32_t ___U3CPerformanceControlModeU3Ek__BackingField_4;
	// System.Boolean UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::<ManualOverride>k__BackingField
	bool ___U3CManualOverrideU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CCpuLevelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB, ___U3CCpuLevelU3Ek__BackingField_0)); }
	inline int32_t get_U3CCpuLevelU3Ek__BackingField_0() const { return ___U3CCpuLevelU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCpuLevelU3Ek__BackingField_0() { return &___U3CCpuLevelU3Ek__BackingField_0; }
	inline void set_U3CCpuLevelU3Ek__BackingField_0(int32_t value)
	{
		___U3CCpuLevelU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCpuLevelDeltaU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB, ___U3CCpuLevelDeltaU3Ek__BackingField_1)); }
	inline int32_t get_U3CCpuLevelDeltaU3Ek__BackingField_1() const { return ___U3CCpuLevelDeltaU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCpuLevelDeltaU3Ek__BackingField_1() { return &___U3CCpuLevelDeltaU3Ek__BackingField_1; }
	inline void set_U3CCpuLevelDeltaU3Ek__BackingField_1(int32_t value)
	{
		___U3CCpuLevelDeltaU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CGpuLevelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB, ___U3CGpuLevelU3Ek__BackingField_2)); }
	inline int32_t get_U3CGpuLevelU3Ek__BackingField_2() const { return ___U3CGpuLevelU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CGpuLevelU3Ek__BackingField_2() { return &___U3CGpuLevelU3Ek__BackingField_2; }
	inline void set_U3CGpuLevelU3Ek__BackingField_2(int32_t value)
	{
		___U3CGpuLevelU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CGpuLevelDeltaU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB, ___U3CGpuLevelDeltaU3Ek__BackingField_3)); }
	inline int32_t get_U3CGpuLevelDeltaU3Ek__BackingField_3() const { return ___U3CGpuLevelDeltaU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CGpuLevelDeltaU3Ek__BackingField_3() { return &___U3CGpuLevelDeltaU3Ek__BackingField_3; }
	inline void set_U3CGpuLevelDeltaU3Ek__BackingField_3(int32_t value)
	{
		___U3CGpuLevelDeltaU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CPerformanceControlModeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB, ___U3CPerformanceControlModeU3Ek__BackingField_4)); }
	inline int32_t get_U3CPerformanceControlModeU3Ek__BackingField_4() const { return ___U3CPerformanceControlModeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CPerformanceControlModeU3Ek__BackingField_4() { return &___U3CPerformanceControlModeU3Ek__BackingField_4; }
	inline void set_U3CPerformanceControlModeU3Ek__BackingField_4(int32_t value)
	{
		___U3CPerformanceControlModeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CManualOverrideU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB, ___U3CManualOverrideU3Ek__BackingField_5)); }
	inline bool get_U3CManualOverrideU3Ek__BackingField_5() const { return ___U3CManualOverrideU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CManualOverrideU3Ek__BackingField_5() { return &___U3CManualOverrideU3Ek__BackingField_5; }
	inline void set_U3CManualOverrideU3Ek__BackingField_5(bool value)
	{
		___U3CManualOverrideU3Ek__BackingField_5 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs
struct PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_pinvoke
{
	int32_t ___U3CCpuLevelU3Ek__BackingField_0;
	int32_t ___U3CCpuLevelDeltaU3Ek__BackingField_1;
	int32_t ___U3CGpuLevelU3Ek__BackingField_2;
	int32_t ___U3CGpuLevelDeltaU3Ek__BackingField_3;
	int32_t ___U3CPerformanceControlModeU3Ek__BackingField_4;
	int32_t ___U3CManualOverrideU3Ek__BackingField_5;
};
// Native definition for COM marshalling of UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs
struct PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_com
{
	int32_t ___U3CCpuLevelU3Ek__BackingField_0;
	int32_t ___U3CCpuLevelDeltaU3Ek__BackingField_1;
	int32_t ___U3CGpuLevelU3Ek__BackingField_2;
	int32_t ___U3CGpuLevelDeltaU3Ek__BackingField_3;
	int32_t ___U3CPerformanceControlModeU3Ek__BackingField_4;
	int32_t ___U3CManualOverrideU3Ek__BackingField_5;
};

// UnityEngine.AdaptivePerformance.PerformanceMetrics
struct  PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F 
{
public:
	// System.Int32 UnityEngine.AdaptivePerformance.PerformanceMetrics::<CurrentCpuLevel>k__BackingField
	int32_t ___U3CCurrentCpuLevelU3Ek__BackingField_0;
	// System.Int32 UnityEngine.AdaptivePerformance.PerformanceMetrics::<CurrentGpuLevel>k__BackingField
	int32_t ___U3CCurrentGpuLevelU3Ek__BackingField_1;
	// UnityEngine.AdaptivePerformance.PerformanceBottleneck UnityEngine.AdaptivePerformance.PerformanceMetrics::<PerformanceBottleneck>k__BackingField
	int32_t ___U3CPerformanceBottleneckU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCurrentCpuLevelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F, ___U3CCurrentCpuLevelU3Ek__BackingField_0)); }
	inline int32_t get_U3CCurrentCpuLevelU3Ek__BackingField_0() const { return ___U3CCurrentCpuLevelU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCurrentCpuLevelU3Ek__BackingField_0() { return &___U3CCurrentCpuLevelU3Ek__BackingField_0; }
	inline void set_U3CCurrentCpuLevelU3Ek__BackingField_0(int32_t value)
	{
		___U3CCurrentCpuLevelU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentGpuLevelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F, ___U3CCurrentGpuLevelU3Ek__BackingField_1)); }
	inline int32_t get_U3CCurrentGpuLevelU3Ek__BackingField_1() const { return ___U3CCurrentGpuLevelU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCurrentGpuLevelU3Ek__BackingField_1() { return &___U3CCurrentGpuLevelU3Ek__BackingField_1; }
	inline void set_U3CCurrentGpuLevelU3Ek__BackingField_1(int32_t value)
	{
		___U3CCurrentGpuLevelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CPerformanceBottleneckU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F, ___U3CPerformanceBottleneckU3Ek__BackingField_2)); }
	inline int32_t get_U3CPerformanceBottleneckU3Ek__BackingField_2() const { return ___U3CPerformanceBottleneckU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CPerformanceBottleneckU3Ek__BackingField_2() { return &___U3CPerformanceBottleneckU3Ek__BackingField_2; }
	inline void set_U3CPerformanceBottleneckU3Ek__BackingField_2(int32_t value)
	{
		___U3CPerformanceBottleneckU3Ek__BackingField_2 = value;
	}
};


// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem
struct  AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528  : public AdaptivePerformanceSubsystemBase_tFE4445DE6A7F0520A1707ECCC71C516E3E4FB12E
{
public:
	// UnityEngine.AdaptivePerformance.Provider.Feature UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem::<Capabilities>k__BackingField
	int32_t ___U3CCapabilitiesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCapabilitiesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528, ___U3CCapabilitiesU3Ek__BackingField_2)); }
	inline int32_t get_U3CCapabilitiesU3Ek__BackingField_2() const { return ___U3CCapabilitiesU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CCapabilitiesU3Ek__BackingField_2() { return &___U3CCapabilitiesU3Ek__BackingField_2; }
	inline void set_U3CCapabilitiesU3Ek__BackingField_2(int32_t value)
	{
		___U3CCapabilitiesU3Ek__BackingField_2 = value;
	}
};


// UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord
struct  PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF 
{
public:
	// UnityEngine.AdaptivePerformance.Provider.Feature UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::<ChangeFlags>k__BackingField
	int32_t ___U3CChangeFlagsU3Ek__BackingField_0;
	// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::<TemperatureLevel>k__BackingField
	float ___U3CTemperatureLevelU3Ek__BackingField_1;
	// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::<TemperatureTrend>k__BackingField
	float ___U3CTemperatureTrendU3Ek__BackingField_2;
	// UnityEngine.AdaptivePerformance.WarningLevel UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::<WarningLevel>k__BackingField
	int32_t ___U3CWarningLevelU3Ek__BackingField_3;
	// System.Int32 UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::<CpuPerformanceLevel>k__BackingField
	int32_t ___U3CCpuPerformanceLevelU3Ek__BackingField_4;
	// System.Int32 UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::<GpuPerformanceLevel>k__BackingField
	int32_t ___U3CGpuPerformanceLevelU3Ek__BackingField_5;
	// System.Boolean UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::<PerformanceLevelControlAvailable>k__BackingField
	bool ___U3CPerformanceLevelControlAvailableU3Ek__BackingField_6;
	// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::<CpuFrameTime>k__BackingField
	float ___U3CCpuFrameTimeU3Ek__BackingField_7;
	// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::<GpuFrameTime>k__BackingField
	float ___U3CGpuFrameTimeU3Ek__BackingField_8;
	// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::<OverallFrameTime>k__BackingField
	float ___U3COverallFrameTimeU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CChangeFlagsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF, ___U3CChangeFlagsU3Ek__BackingField_0)); }
	inline int32_t get_U3CChangeFlagsU3Ek__BackingField_0() const { return ___U3CChangeFlagsU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CChangeFlagsU3Ek__BackingField_0() { return &___U3CChangeFlagsU3Ek__BackingField_0; }
	inline void set_U3CChangeFlagsU3Ek__BackingField_0(int32_t value)
	{
		___U3CChangeFlagsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTemperatureLevelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF, ___U3CTemperatureLevelU3Ek__BackingField_1)); }
	inline float get_U3CTemperatureLevelU3Ek__BackingField_1() const { return ___U3CTemperatureLevelU3Ek__BackingField_1; }
	inline float* get_address_of_U3CTemperatureLevelU3Ek__BackingField_1() { return &___U3CTemperatureLevelU3Ek__BackingField_1; }
	inline void set_U3CTemperatureLevelU3Ek__BackingField_1(float value)
	{
		___U3CTemperatureLevelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTemperatureTrendU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF, ___U3CTemperatureTrendU3Ek__BackingField_2)); }
	inline float get_U3CTemperatureTrendU3Ek__BackingField_2() const { return ___U3CTemperatureTrendU3Ek__BackingField_2; }
	inline float* get_address_of_U3CTemperatureTrendU3Ek__BackingField_2() { return &___U3CTemperatureTrendU3Ek__BackingField_2; }
	inline void set_U3CTemperatureTrendU3Ek__BackingField_2(float value)
	{
		___U3CTemperatureTrendU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CWarningLevelU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF, ___U3CWarningLevelU3Ek__BackingField_3)); }
	inline int32_t get_U3CWarningLevelU3Ek__BackingField_3() const { return ___U3CWarningLevelU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CWarningLevelU3Ek__BackingField_3() { return &___U3CWarningLevelU3Ek__BackingField_3; }
	inline void set_U3CWarningLevelU3Ek__BackingField_3(int32_t value)
	{
		___U3CWarningLevelU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCpuPerformanceLevelU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF, ___U3CCpuPerformanceLevelU3Ek__BackingField_4)); }
	inline int32_t get_U3CCpuPerformanceLevelU3Ek__BackingField_4() const { return ___U3CCpuPerformanceLevelU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CCpuPerformanceLevelU3Ek__BackingField_4() { return &___U3CCpuPerformanceLevelU3Ek__BackingField_4; }
	inline void set_U3CCpuPerformanceLevelU3Ek__BackingField_4(int32_t value)
	{
		___U3CCpuPerformanceLevelU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CGpuPerformanceLevelU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF, ___U3CGpuPerformanceLevelU3Ek__BackingField_5)); }
	inline int32_t get_U3CGpuPerformanceLevelU3Ek__BackingField_5() const { return ___U3CGpuPerformanceLevelU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CGpuPerformanceLevelU3Ek__BackingField_5() { return &___U3CGpuPerformanceLevelU3Ek__BackingField_5; }
	inline void set_U3CGpuPerformanceLevelU3Ek__BackingField_5(int32_t value)
	{
		___U3CGpuPerformanceLevelU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CPerformanceLevelControlAvailableU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF, ___U3CPerformanceLevelControlAvailableU3Ek__BackingField_6)); }
	inline bool get_U3CPerformanceLevelControlAvailableU3Ek__BackingField_6() const { return ___U3CPerformanceLevelControlAvailableU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CPerformanceLevelControlAvailableU3Ek__BackingField_6() { return &___U3CPerformanceLevelControlAvailableU3Ek__BackingField_6; }
	inline void set_U3CPerformanceLevelControlAvailableU3Ek__BackingField_6(bool value)
	{
		___U3CPerformanceLevelControlAvailableU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CCpuFrameTimeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF, ___U3CCpuFrameTimeU3Ek__BackingField_7)); }
	inline float get_U3CCpuFrameTimeU3Ek__BackingField_7() const { return ___U3CCpuFrameTimeU3Ek__BackingField_7; }
	inline float* get_address_of_U3CCpuFrameTimeU3Ek__BackingField_7() { return &___U3CCpuFrameTimeU3Ek__BackingField_7; }
	inline void set_U3CCpuFrameTimeU3Ek__BackingField_7(float value)
	{
		___U3CCpuFrameTimeU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CGpuFrameTimeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF, ___U3CGpuFrameTimeU3Ek__BackingField_8)); }
	inline float get_U3CGpuFrameTimeU3Ek__BackingField_8() const { return ___U3CGpuFrameTimeU3Ek__BackingField_8; }
	inline float* get_address_of_U3CGpuFrameTimeU3Ek__BackingField_8() { return &___U3CGpuFrameTimeU3Ek__BackingField_8; }
	inline void set_U3CGpuFrameTimeU3Ek__BackingField_8(float value)
	{
		___U3CGpuFrameTimeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3COverallFrameTimeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF, ___U3COverallFrameTimeU3Ek__BackingField_9)); }
	inline float get_U3COverallFrameTimeU3Ek__BackingField_9() const { return ___U3COverallFrameTimeU3Ek__BackingField_9; }
	inline float* get_address_of_U3COverallFrameTimeU3Ek__BackingField_9() { return &___U3COverallFrameTimeU3Ek__BackingField_9; }
	inline void set_U3COverallFrameTimeU3Ek__BackingField_9(float value)
	{
		___U3COverallFrameTimeU3Ek__BackingField_9 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord
struct PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshaled_pinvoke
{
	int32_t ___U3CChangeFlagsU3Ek__BackingField_0;
	float ___U3CTemperatureLevelU3Ek__BackingField_1;
	float ___U3CTemperatureTrendU3Ek__BackingField_2;
	int32_t ___U3CWarningLevelU3Ek__BackingField_3;
	int32_t ___U3CCpuPerformanceLevelU3Ek__BackingField_4;
	int32_t ___U3CGpuPerformanceLevelU3Ek__BackingField_5;
	int32_t ___U3CPerformanceLevelControlAvailableU3Ek__BackingField_6;
	float ___U3CCpuFrameTimeU3Ek__BackingField_7;
	float ___U3CGpuFrameTimeU3Ek__BackingField_8;
	float ___U3COverallFrameTimeU3Ek__BackingField_9;
};
// Native definition for COM marshalling of UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord
struct PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshaled_com
{
	int32_t ___U3CChangeFlagsU3Ek__BackingField_0;
	float ___U3CTemperatureLevelU3Ek__BackingField_1;
	float ___U3CTemperatureTrendU3Ek__BackingField_2;
	int32_t ___U3CWarningLevelU3Ek__BackingField_3;
	int32_t ___U3CCpuPerformanceLevelU3Ek__BackingField_4;
	int32_t ___U3CGpuPerformanceLevelU3Ek__BackingField_5;
	int32_t ___U3CPerformanceLevelControlAvailableU3Ek__BackingField_6;
	float ___U3CCpuFrameTimeU3Ek__BackingField_7;
	float ___U3CGpuFrameTimeU3Ek__BackingField_8;
	float ___U3COverallFrameTimeU3Ek__BackingField_9;
};

// UnityEngine.AdaptivePerformance.ThermalMetrics
struct  ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 
{
public:
	// UnityEngine.AdaptivePerformance.WarningLevel UnityEngine.AdaptivePerformance.ThermalMetrics::<WarningLevel>k__BackingField
	int32_t ___U3CWarningLevelU3Ek__BackingField_0;
	// System.Single UnityEngine.AdaptivePerformance.ThermalMetrics::<TemperatureLevel>k__BackingField
	float ___U3CTemperatureLevelU3Ek__BackingField_1;
	// System.Single UnityEngine.AdaptivePerformance.ThermalMetrics::<TemperatureTrend>k__BackingField
	float ___U3CTemperatureTrendU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CWarningLevelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125, ___U3CWarningLevelU3Ek__BackingField_0)); }
	inline int32_t get_U3CWarningLevelU3Ek__BackingField_0() const { return ___U3CWarningLevelU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CWarningLevelU3Ek__BackingField_0() { return &___U3CWarningLevelU3Ek__BackingField_0; }
	inline void set_U3CWarningLevelU3Ek__BackingField_0(int32_t value)
	{
		___U3CWarningLevelU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTemperatureLevelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125, ___U3CTemperatureLevelU3Ek__BackingField_1)); }
	inline float get_U3CTemperatureLevelU3Ek__BackingField_1() const { return ___U3CTemperatureLevelU3Ek__BackingField_1; }
	inline float* get_address_of_U3CTemperatureLevelU3Ek__BackingField_1() { return &___U3CTemperatureLevelU3Ek__BackingField_1; }
	inline void set_U3CTemperatureLevelU3Ek__BackingField_1(float value)
	{
		___U3CTemperatureLevelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTemperatureTrendU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125, ___U3CTemperatureTrendU3Ek__BackingField_2)); }
	inline float get_U3CTemperatureTrendU3Ek__BackingField_2() const { return ___U3CTemperatureTrendU3Ek__BackingField_2; }
	inline float* get_address_of_U3CTemperatureTrendU3Ek__BackingField_2() { return &___U3CTemperatureTrendU3Ek__BackingField_2; }
	inline void set_U3CTemperatureTrendU3Ek__BackingField_2(float value)
	{
		___U3CTemperatureTrendU3Ek__BackingField_2 = value;
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};

// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};


// System.NotSupportedException
struct  NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// UnityEngine.AdaptivePerformance.AdaptivePerformanceManagerSpawner
struct  AdaptivePerformanceManagerSpawner_tB0C73AFB099B61AC0890B6FB092EE08A7C8794A4  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.GameObject UnityEngine.AdaptivePerformance.AdaptivePerformanceManagerSpawner::m_ManagerGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_ManagerGameObject_4;

public:
	inline static int32_t get_offset_of_m_ManagerGameObject_4() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManagerSpawner_tB0C73AFB099B61AC0890B6FB092EE08A7C8794A4, ___m_ManagerGameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_ManagerGameObject_4() const { return ___m_ManagerGameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_ManagerGameObject_4() { return &___m_ManagerGameObject_4; }
	inline void set_m_ManagerGameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_ManagerGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ManagerGameObject_4), (void*)value);
	}
};


// UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler
struct  PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.AdaptivePerformance.PerformanceLevelChangeHandler
struct  PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem
struct  TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F  : public AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528
{
public:
	// UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::updateResult
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF  ___updateResult_3;
	// System.Int32 UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::<LastRequestedCpuLevel>k__BackingField
	int32_t ___U3CLastRequestedCpuLevelU3Ek__BackingField_4;
	// System.Int32 UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::<LastRequestedGpuLevel>k__BackingField
	int32_t ___U3CLastRequestedGpuLevelU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_updateResult_3() { return static_cast<int32_t>(offsetof(TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F, ___updateResult_3)); }
	inline PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF  get_updateResult_3() const { return ___updateResult_3; }
	inline PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * get_address_of_updateResult_3() { return &___updateResult_3; }
	inline void set_updateResult_3(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF  value)
	{
		___updateResult_3 = value;
	}

	inline static int32_t get_offset_of_U3CLastRequestedCpuLevelU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F, ___U3CLastRequestedCpuLevelU3Ek__BackingField_4)); }
	inline int32_t get_U3CLastRequestedCpuLevelU3Ek__BackingField_4() const { return ___U3CLastRequestedCpuLevelU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CLastRequestedCpuLevelU3Ek__BackingField_4() { return &___U3CLastRequestedCpuLevelU3Ek__BackingField_4; }
	inline void set_U3CLastRequestedCpuLevelU3Ek__BackingField_4(int32_t value)
	{
		___U3CLastRequestedCpuLevelU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CLastRequestedGpuLevelU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F, ___U3CLastRequestedGpuLevelU3Ek__BackingField_5)); }
	inline int32_t get_U3CLastRequestedGpuLevelU3Ek__BackingField_5() const { return ___U3CLastRequestedGpuLevelU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CLastRequestedGpuLevelU3Ek__BackingField_5() { return &___U3CLastRequestedGpuLevelU3Ek__BackingField_5; }
	inline void set_U3CLastRequestedGpuLevelU3Ek__BackingField_5(int32_t value)
	{
		___U3CLastRequestedGpuLevelU3Ek__BackingField_5 = value;
	}
};


// UnityEngine.AdaptivePerformance.ThermalEventHandler
struct  ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.AdaptivePerformance.AdaptivePerformanceManager
struct  AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AdaptivePerformance.ThermalEventHandler UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::ThermalEvent
	ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * ___ThermalEvent_4;
	// UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::PerformanceBottleneckChangeEvent
	PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * ___PerformanceBottleneckChangeEvent_5;
	// UnityEngine.AdaptivePerformance.PerformanceLevelChangeHandler UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::PerformanceLevelChangeEvent
	PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * ___PerformanceLevelChangeEvent_6;
	// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_Subsystem
	AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * ___m_Subsystem_7;
	// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_JustResumed
	bool ___m_JustResumed_8;
	// System.Int32 UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_RequestedCpuLevel
	int32_t ___m_RequestedCpuLevel_9;
	// System.Int32 UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_RequestedGpuLevel
	int32_t ___m_RequestedGpuLevel_10;
	// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_NewUserPerformanceLevelRequest
	bool ___m_NewUserPerformanceLevelRequest_11;
	// UnityEngine.AdaptivePerformance.ThermalMetrics UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_ThermalMetrics
	ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  ___m_ThermalMetrics_12;
	// UnityEngine.AdaptivePerformance.PerformanceMetrics UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_PerformanceMetrics
	PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F  ___m_PerformanceMetrics_13;
	// UnityEngine.AdaptivePerformance.FrameTiming UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_FrameTiming
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  ___m_FrameTiming_14;
	// System.Int32 UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::<LoggingFrequencyInFrames>k__BackingField
	int32_t ___U3CLoggingFrequencyInFramesU3Ek__BackingField_15;
	// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::<AutomaticPerformanceControl>k__BackingField
	bool ___U3CAutomaticPerformanceControlU3Ek__BackingField_16;
	// UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_DevicePerfControl
	DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * ___m_DevicePerfControl_17;
	// UnityEngine.AdaptivePerformance.AutoPerformanceLevelController UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_AutoPerformanceLevelController
	AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * ___m_AutoPerformanceLevelController_18;
	// UnityEngine.AdaptivePerformance.CpuTimeProvider UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_CpuFrameTimeProvider
	CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * ___m_CpuFrameTimeProvider_19;
	// UnityEngine.AdaptivePerformance.GpuTimeProvider UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_GpuFrameTimeProvider
	GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * ___m_GpuFrameTimeProvider_20;
	// UnityEngine.AdaptivePerformance.Provider.IApplicationLifecycle UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_AppLifecycle
	RuntimeObject* ___m_AppLifecycle_21;
	// UnityEngine.AdaptivePerformance.TemperatureTrend UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_TemperatureTrend
	TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * ___m_TemperatureTrend_22;
	// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_UseProviderOverallFrameTime
	bool ___m_UseProviderOverallFrameTime_23;
	// UnityEngine.WaitForEndOfFrame UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_WaitForEndOfFrame
	WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * ___m_WaitForEndOfFrame_24;
	// System.Int32 UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_FrameCount
	int32_t ___m_FrameCount_25;
	// UnityEngine.AdaptivePerformance.RunningAverage UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_OverallFrameTime
	RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * ___m_OverallFrameTime_26;
	// System.Single UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_OverallFrameTimeAccu
	float ___m_OverallFrameTimeAccu_27;
	// UnityEngine.AdaptivePerformance.RunningAverage UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_GpuFrameTime
	RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * ___m_GpuFrameTime_28;
	// UnityEngine.AdaptivePerformance.RunningAverage UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::m_CpuFrameTime
	RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * ___m_CpuFrameTime_29;

public:
	inline static int32_t get_offset_of_ThermalEvent_4() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___ThermalEvent_4)); }
	inline ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * get_ThermalEvent_4() const { return ___ThermalEvent_4; }
	inline ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 ** get_address_of_ThermalEvent_4() { return &___ThermalEvent_4; }
	inline void set_ThermalEvent_4(ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * value)
	{
		___ThermalEvent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ThermalEvent_4), (void*)value);
	}

	inline static int32_t get_offset_of_PerformanceBottleneckChangeEvent_5() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___PerformanceBottleneckChangeEvent_5)); }
	inline PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * get_PerformanceBottleneckChangeEvent_5() const { return ___PerformanceBottleneckChangeEvent_5; }
	inline PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 ** get_address_of_PerformanceBottleneckChangeEvent_5() { return &___PerformanceBottleneckChangeEvent_5; }
	inline void set_PerformanceBottleneckChangeEvent_5(PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * value)
	{
		___PerformanceBottleneckChangeEvent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PerformanceBottleneckChangeEvent_5), (void*)value);
	}

	inline static int32_t get_offset_of_PerformanceLevelChangeEvent_6() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___PerformanceLevelChangeEvent_6)); }
	inline PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * get_PerformanceLevelChangeEvent_6() const { return ___PerformanceLevelChangeEvent_6; }
	inline PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 ** get_address_of_PerformanceLevelChangeEvent_6() { return &___PerformanceLevelChangeEvent_6; }
	inline void set_PerformanceLevelChangeEvent_6(PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * value)
	{
		___PerformanceLevelChangeEvent_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PerformanceLevelChangeEvent_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Subsystem_7() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_Subsystem_7)); }
	inline AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * get_m_Subsystem_7() const { return ___m_Subsystem_7; }
	inline AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 ** get_address_of_m_Subsystem_7() { return &___m_Subsystem_7; }
	inline void set_m_Subsystem_7(AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * value)
	{
		___m_Subsystem_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Subsystem_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_JustResumed_8() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_JustResumed_8)); }
	inline bool get_m_JustResumed_8() const { return ___m_JustResumed_8; }
	inline bool* get_address_of_m_JustResumed_8() { return &___m_JustResumed_8; }
	inline void set_m_JustResumed_8(bool value)
	{
		___m_JustResumed_8 = value;
	}

	inline static int32_t get_offset_of_m_RequestedCpuLevel_9() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_RequestedCpuLevel_9)); }
	inline int32_t get_m_RequestedCpuLevel_9() const { return ___m_RequestedCpuLevel_9; }
	inline int32_t* get_address_of_m_RequestedCpuLevel_9() { return &___m_RequestedCpuLevel_9; }
	inline void set_m_RequestedCpuLevel_9(int32_t value)
	{
		___m_RequestedCpuLevel_9 = value;
	}

	inline static int32_t get_offset_of_m_RequestedGpuLevel_10() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_RequestedGpuLevel_10)); }
	inline int32_t get_m_RequestedGpuLevel_10() const { return ___m_RequestedGpuLevel_10; }
	inline int32_t* get_address_of_m_RequestedGpuLevel_10() { return &___m_RequestedGpuLevel_10; }
	inline void set_m_RequestedGpuLevel_10(int32_t value)
	{
		___m_RequestedGpuLevel_10 = value;
	}

	inline static int32_t get_offset_of_m_NewUserPerformanceLevelRequest_11() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_NewUserPerformanceLevelRequest_11)); }
	inline bool get_m_NewUserPerformanceLevelRequest_11() const { return ___m_NewUserPerformanceLevelRequest_11; }
	inline bool* get_address_of_m_NewUserPerformanceLevelRequest_11() { return &___m_NewUserPerformanceLevelRequest_11; }
	inline void set_m_NewUserPerformanceLevelRequest_11(bool value)
	{
		___m_NewUserPerformanceLevelRequest_11 = value;
	}

	inline static int32_t get_offset_of_m_ThermalMetrics_12() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_ThermalMetrics_12)); }
	inline ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  get_m_ThermalMetrics_12() const { return ___m_ThermalMetrics_12; }
	inline ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * get_address_of_m_ThermalMetrics_12() { return &___m_ThermalMetrics_12; }
	inline void set_m_ThermalMetrics_12(ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  value)
	{
		___m_ThermalMetrics_12 = value;
	}

	inline static int32_t get_offset_of_m_PerformanceMetrics_13() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_PerformanceMetrics_13)); }
	inline PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F  get_m_PerformanceMetrics_13() const { return ___m_PerformanceMetrics_13; }
	inline PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * get_address_of_m_PerformanceMetrics_13() { return &___m_PerformanceMetrics_13; }
	inline void set_m_PerformanceMetrics_13(PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F  value)
	{
		___m_PerformanceMetrics_13 = value;
	}

	inline static int32_t get_offset_of_m_FrameTiming_14() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_FrameTiming_14)); }
	inline FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  get_m_FrameTiming_14() const { return ___m_FrameTiming_14; }
	inline FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * get_address_of_m_FrameTiming_14() { return &___m_FrameTiming_14; }
	inline void set_m_FrameTiming_14(FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  value)
	{
		___m_FrameTiming_14 = value;
	}

	inline static int32_t get_offset_of_U3CLoggingFrequencyInFramesU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___U3CLoggingFrequencyInFramesU3Ek__BackingField_15)); }
	inline int32_t get_U3CLoggingFrequencyInFramesU3Ek__BackingField_15() const { return ___U3CLoggingFrequencyInFramesU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CLoggingFrequencyInFramesU3Ek__BackingField_15() { return &___U3CLoggingFrequencyInFramesU3Ek__BackingField_15; }
	inline void set_U3CLoggingFrequencyInFramesU3Ek__BackingField_15(int32_t value)
	{
		___U3CLoggingFrequencyInFramesU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CAutomaticPerformanceControlU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___U3CAutomaticPerformanceControlU3Ek__BackingField_16)); }
	inline bool get_U3CAutomaticPerformanceControlU3Ek__BackingField_16() const { return ___U3CAutomaticPerformanceControlU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CAutomaticPerformanceControlU3Ek__BackingField_16() { return &___U3CAutomaticPerformanceControlU3Ek__BackingField_16; }
	inline void set_U3CAutomaticPerformanceControlU3Ek__BackingField_16(bool value)
	{
		___U3CAutomaticPerformanceControlU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_DevicePerfControl_17() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_DevicePerfControl_17)); }
	inline DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * get_m_DevicePerfControl_17() const { return ___m_DevicePerfControl_17; }
	inline DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 ** get_address_of_m_DevicePerfControl_17() { return &___m_DevicePerfControl_17; }
	inline void set_m_DevicePerfControl_17(DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * value)
	{
		___m_DevicePerfControl_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DevicePerfControl_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_AutoPerformanceLevelController_18() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_AutoPerformanceLevelController_18)); }
	inline AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * get_m_AutoPerformanceLevelController_18() const { return ___m_AutoPerformanceLevelController_18; }
	inline AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A ** get_address_of_m_AutoPerformanceLevelController_18() { return &___m_AutoPerformanceLevelController_18; }
	inline void set_m_AutoPerformanceLevelController_18(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * value)
	{
		___m_AutoPerformanceLevelController_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AutoPerformanceLevelController_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_CpuFrameTimeProvider_19() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_CpuFrameTimeProvider_19)); }
	inline CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * get_m_CpuFrameTimeProvider_19() const { return ___m_CpuFrameTimeProvider_19; }
	inline CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 ** get_address_of_m_CpuFrameTimeProvider_19() { return &___m_CpuFrameTimeProvider_19; }
	inline void set_m_CpuFrameTimeProvider_19(CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * value)
	{
		___m_CpuFrameTimeProvider_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CpuFrameTimeProvider_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_GpuFrameTimeProvider_20() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_GpuFrameTimeProvider_20)); }
	inline GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * get_m_GpuFrameTimeProvider_20() const { return ___m_GpuFrameTimeProvider_20; }
	inline GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B ** get_address_of_m_GpuFrameTimeProvider_20() { return &___m_GpuFrameTimeProvider_20; }
	inline void set_m_GpuFrameTimeProvider_20(GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * value)
	{
		___m_GpuFrameTimeProvider_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GpuFrameTimeProvider_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_AppLifecycle_21() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_AppLifecycle_21)); }
	inline RuntimeObject* get_m_AppLifecycle_21() const { return ___m_AppLifecycle_21; }
	inline RuntimeObject** get_address_of_m_AppLifecycle_21() { return &___m_AppLifecycle_21; }
	inline void set_m_AppLifecycle_21(RuntimeObject* value)
	{
		___m_AppLifecycle_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AppLifecycle_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_TemperatureTrend_22() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_TemperatureTrend_22)); }
	inline TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * get_m_TemperatureTrend_22() const { return ___m_TemperatureTrend_22; }
	inline TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B ** get_address_of_m_TemperatureTrend_22() { return &___m_TemperatureTrend_22; }
	inline void set_m_TemperatureTrend_22(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * value)
	{
		___m_TemperatureTrend_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TemperatureTrend_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_UseProviderOverallFrameTime_23() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_UseProviderOverallFrameTime_23)); }
	inline bool get_m_UseProviderOverallFrameTime_23() const { return ___m_UseProviderOverallFrameTime_23; }
	inline bool* get_address_of_m_UseProviderOverallFrameTime_23() { return &___m_UseProviderOverallFrameTime_23; }
	inline void set_m_UseProviderOverallFrameTime_23(bool value)
	{
		___m_UseProviderOverallFrameTime_23 = value;
	}

	inline static int32_t get_offset_of_m_WaitForEndOfFrame_24() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_WaitForEndOfFrame_24)); }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * get_m_WaitForEndOfFrame_24() const { return ___m_WaitForEndOfFrame_24; }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA ** get_address_of_m_WaitForEndOfFrame_24() { return &___m_WaitForEndOfFrame_24; }
	inline void set_m_WaitForEndOfFrame_24(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * value)
	{
		___m_WaitForEndOfFrame_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_WaitForEndOfFrame_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_FrameCount_25() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_FrameCount_25)); }
	inline int32_t get_m_FrameCount_25() const { return ___m_FrameCount_25; }
	inline int32_t* get_address_of_m_FrameCount_25() { return &___m_FrameCount_25; }
	inline void set_m_FrameCount_25(int32_t value)
	{
		___m_FrameCount_25 = value;
	}

	inline static int32_t get_offset_of_m_OverallFrameTime_26() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_OverallFrameTime_26)); }
	inline RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * get_m_OverallFrameTime_26() const { return ___m_OverallFrameTime_26; }
	inline RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 ** get_address_of_m_OverallFrameTime_26() { return &___m_OverallFrameTime_26; }
	inline void set_m_OverallFrameTime_26(RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * value)
	{
		___m_OverallFrameTime_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverallFrameTime_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverallFrameTimeAccu_27() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_OverallFrameTimeAccu_27)); }
	inline float get_m_OverallFrameTimeAccu_27() const { return ___m_OverallFrameTimeAccu_27; }
	inline float* get_address_of_m_OverallFrameTimeAccu_27() { return &___m_OverallFrameTimeAccu_27; }
	inline void set_m_OverallFrameTimeAccu_27(float value)
	{
		___m_OverallFrameTimeAccu_27 = value;
	}

	inline static int32_t get_offset_of_m_GpuFrameTime_28() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_GpuFrameTime_28)); }
	inline RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * get_m_GpuFrameTime_28() const { return ___m_GpuFrameTime_28; }
	inline RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 ** get_address_of_m_GpuFrameTime_28() { return &___m_GpuFrameTime_28; }
	inline void set_m_GpuFrameTime_28(RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * value)
	{
		___m_GpuFrameTime_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GpuFrameTime_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_CpuFrameTime_29() { return static_cast<int32_t>(offsetof(AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA, ___m_CpuFrameTime_29)); }
	inline RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * get_m_CpuFrameTime_29() const { return ___m_CpuFrameTime_29; }
	inline RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 ** get_address_of_m_CpuFrameTime_29() { return &___m_CpuFrameTime_29; }
	inline void set_m_CpuFrameTime_29(RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * value)
	{
		___m_CpuFrameTime_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CpuFrameTime_29), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.FrameTiming[]
struct FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC  m_Items[1];

public:
	inline FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FrameTiming_tAF2F0C7558BD0631E69FC0D0A5ADCE90EDC166FC  value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};

IL2CPP_EXTERN_C void PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshal_pinvoke(const PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB& unmarshaled, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_pinvoke& marshaled);
IL2CPP_EXTERN_C void PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshal_pinvoke_back(const PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_pinvoke& marshaled, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB& unmarshaled);
IL2CPP_EXTERN_C void PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshal_pinvoke_cleanup(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_pinvoke& marshaled);

// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObject_CreateInstance_TisRuntimeObject_m7A8F75139352BA04C2EEC1D72D430FAC94C753DE_gshared (const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD  List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// !0 UnityEngine.SubsystemDescriptor`1<System.Object>::Create()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * SubsystemDescriptor_1_Create_m64A7E6687E20061C67D007EF2685E13A3CCD8C79_gshared (SubsystemDescriptor_1_tE2E4F2A029DA1A307F018FCE747BA53FB9E344C2 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Subsystem`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Subsystem_1__ctor_m7F707019C5D6E2EDA2EC7BFC45B31681B449CE5C_gshared (Subsystem_1_t6048F47F8C2EBFDAC541AA593928233978B85EA9 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SubsystemManager::GetSubsystemDescriptors<System.Object>(System.Collections.Generic.List`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubsystemManager_GetSubsystemDescriptors_TisRuntimeObject_m4B9078442334D4AB22E569414B069AAD8C0BB960_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___descriptors0, const RuntimeMethod* method);

// !!0 UnityEngine.ScriptableObject::CreateInstance<UnityEngine.AdaptivePerformance.AdaptivePerformanceManagerSpawner>()
inline AdaptivePerformanceManagerSpawner_tB0C73AFB099B61AC0890B6FB092EE08A7C8794A4 * ScriptableObject_CreateInstance_TisAdaptivePerformanceManagerSpawner_tB0C73AFB099B61AC0890B6FB092EE08A7C8794A4_m6D26827E87B21608D2903C52C7DC967BE83532DF (const RuntimeMethod* method)
{
	return ((  AdaptivePerformanceManagerSpawner_tB0C73AFB099B61AC0890B6FB092EE08A7C8794A4 * (*) (const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_m7A8F75139352BA04C2EEC1D72D430FAC94C753DE_gshared)(method);
}
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_MaxCpuPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_MaxCpuPerformanceLevel_mC8D89A2CC5E0135E12B089A50D6289FC2DC97B9A (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_MaxGpuPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_MaxGpuPerformanceLevel_mF87A40C6B5D9EB47C980229BE85925CF3CEA39CE (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method);
// UnityEngine.AdaptivePerformance.PerformanceControlMode UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_PerformanceControlMode()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_PerformanceControlMode_m2E4EE8BB379495FD00D67FCB6675358333F1532D_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.ThermalMetrics::set_WarningLevel(UnityEngine.AdaptivePerformance.WarningLevel)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ThermalMetrics_set_WarningLevel_m2B6C74F0609CAF7AA9FFE97018DB76D03D85CF89_inline (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.ThermalMetrics::set_TemperatureLevel(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ThermalMetrics_set_TemperatureLevel_m55E3869F1C0685354CEBC2FA58FBB009380487FD_inline (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.ThermalMetrics::set_TemperatureTrend(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ThermalMetrics_set_TemperatureTrend_mF48505C1BB0EBE4F530B0405D60D4396A24B9472_inline (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceMetrics::set_CurrentCpuLevel(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceMetrics_set_CurrentCpuLevel_mB0FFC61EEA0C4462338C8E728533FE48D4364D9A_inline (PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceMetrics::set_CurrentGpuLevel(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceMetrics_set_CurrentGpuLevel_m2807D7F1C39CF7C72DCA9CFDC0BB6F90AA2CE326_inline (PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceMetrics::set_PerformanceBottleneck(UnityEngine.AdaptivePerformance.PerformanceBottleneck)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceMetrics_set_PerformanceBottleneck_mFFE12B2A0C07877C363A05EF1F48B4C9BA27D268_inline (PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.FrameTiming::set_CurrentFrameTime(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FrameTiming_set_CurrentFrameTime_m2DD61370159EDB8AE9612CC6303B25175788A0E5_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.FrameTiming::set_AverageFrameTime(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FrameTiming_set_AverageFrameTime_mBDC023C5DDCE8C8069CBEAF28DAC912744D648C5_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.FrameTiming::set_CurrentGpuFrameTime(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FrameTiming_set_CurrentGpuFrameTime_mBAB674B381CA46E589A79890A9BE0A0FDC2EE781_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.FrameTiming::set_AverageGpuFrameTime(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FrameTiming_set_AverageGpuFrameTime_mADCD67452E1D4C92E7562EDD78A5BC411B3BC29B_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.FrameTiming::set_CurrentCpuFrameTime(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FrameTiming_set_CurrentCpuFrameTime_m95A32BC4E4AE8885D1738D309F2C39E96E887810_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.FrameTiming::set_AverageCpuFrameTime(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FrameTiming_set_AverageCpuFrameTime_m87D8B833265478A0EA25153B3D5299AACAE71BDE_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B (WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.RunningAverage::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RunningAverage__ctor_m1D5CCDBE8A19B9CCE2F0644773094D4157AEC9AF (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, int32_t ___sampleWindowSize0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.StartupSettings::get_AutomaticPerformanceControl()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool StartupSettings_get_AutomaticPerformanceControl_mD3FD90C9A974872C6E1F4C8F0F857CE658654E83_inline (const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::set_AutomaticPerformanceControl(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_set_AutomaticPerformanceControl_mE17FB23F321B2C59242F1CB53295DBEA1F7F94FC_inline (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemBase::get_initialized()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool AdaptivePerformanceSubsystemBase_get_initialized_mCF8947352D834034868007E91F84485171F40ACA_inline (AdaptivePerformanceSubsystemBase_tFE4445DE6A7F0520A1707ECCC71C516E3E4FB12E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Subsystem::Destroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Subsystem_Destroy_m1D65C2E3B540A9EC80E14BF0C7A2BE8CDCF887A4 (Subsystem_t17E4AEB5537DC8AECC37EC3F6FCB46CC7D2C73F6 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.StartupSettings::get_Logging()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool StartupSettings_get_Logging_m7A40AE632795747F6E17C5ABABEF45D83E01EAA3_inline (const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.StartupSettings::get_StatsLoggingFrequencyInFrames()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t StartupSettings_get_StatsLoggingFrequencyInFrames_m5F108EC9014719068FF27AD8D23E21E1FD1DF380_inline (const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::set_LoggingFrequencyInFrames(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_set_LoggingFrequencyInFrames_mD228BFEE1C9900E7E8A8063826562B3D7CED38DA_inline (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.StartupSettings::get_Enable()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool StartupSettings_get_Enable_m8343935449F13DAF78A3EE247DA316A91695A9D2_inline (const RuntimeMethod* method);
// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem UnityEngine.AdaptivePerformance.StartupSettings::get_PreferredSubsystem()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * StartupSettings_get_PreferredSubsystem_m288E01FA3B91225A014D305D97163961E988C5B3_inline (const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::InitializeSubsystem(UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AdaptivePerformanceManager_InitializeSubsystem_mBECAD18DEAA314DB5FDB4063D124F29CFA7619FF (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * ___subsystem0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor> UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemRegistry::GetRegisteredDescriptors()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * AdaptivePerformanceSubsystemRegistry_GetRegisteredDescriptors_mA1D2C28ABC171D8FB88C9EEC96BD63A303456604 (const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>::get_Count()
inline int32_t List_1_get_Count_m4F79244F68273A7F2AFE63F5443754B4A2BE62CE_inline (List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>::GetEnumerator()
inline Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325  List_1_GetEnumerator_m8E054D1AFBD9129D72D963784EE1E501B02B6260 (List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325  (*) (List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC *, const RuntimeMethod*))List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>::get_Current()
inline AdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55 * Enumerator_get_Current_mC39D2C568D4151FA33784EB27005767BCC518FF5_inline (Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325 * __this, const RuntimeMethod* method)
{
	return ((  AdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55 * (*) (Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325 *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline)(__this, method);
}
// !0 UnityEngine.SubsystemDescriptor`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem>::Create()
inline AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * SubsystemDescriptor_1_Create_mB303B00807AACC42F9449C37EF70DF21EF952878 (SubsystemDescriptor_1_t260E615AF73615B1D344C323E0F4A6CBB7360C20 * __this, const RuntimeMethod* method)
{
	return ((  AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * (*) (SubsystemDescriptor_1_t260E615AF73615B1D344C323E0F4A6CBB7360C20 *, const RuntimeMethod*))SubsystemDescriptor_1_Create_m64A7E6687E20061C67D007EF2685E13A3CCD8C79_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>::MoveNext()
inline bool Enumerator_MoveNext_m61C82A0B80A3224E2C00EC055806FE26F81964A6 (Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325 *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>::Dispose()
inline void Enumerator_Dispose_m1E1887EFB1E29C201DA849DA80C01B6CF756F506 (Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325 *, const RuntimeMethod*))Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared)(__this, method);
}
// UnityEngine.AdaptivePerformance.Provider.Feature UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem::get_Capabilities()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t AdaptivePerformanceSubsystem_get_Capabilities_m42EEFCDECCBB0DFFEBE3E65BAA42ED01256FB18F_inline (AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * __this, const RuntimeMethod* method);
// System.Boolean System.Enum::HasFlag(System.Enum)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enum_HasFlag_m5D934A541DEEF44DBF3415EE47F8CCED9370C173 (RuntimeObject * __this, Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 * ___flag0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::.ctor(UnityEngine.AdaptivePerformance.Provider.IDevicePerformanceLevelControl)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl__ctor_m41818A4930BAEB5BE2E0903DDBCCE23C043F0A07 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, RuntimeObject* ___performanceLevelControl0, const RuntimeMethod* method);
// UnityEngine.AdaptivePerformance.IPerformanceStatus UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_PerformanceStatus()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AdaptivePerformanceManager_get_PerformanceStatus_m691041B1532A8353043AF4B634AF019C05986C01 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method);
// UnityEngine.AdaptivePerformance.IThermalStatus UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_ThermalStatus()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AdaptivePerformanceManager_get_ThermalStatus_mBA83F333764B91798F714C2F089FF34E7EB7FFAF (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::.ctor(UnityEngine.AdaptivePerformance.IDevicePerformanceControl,UnityEngine.AdaptivePerformance.IPerformanceStatus,UnityEngine.AdaptivePerformance.IThermalStatus)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController__ctor_m252C3934AA192D4135C2A9CE41B7D4B1216BC88C (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, RuntimeObject* ___perfControl0, RuntimeObject* ___perfStat1, RuntimeObject* ___thermalStat2, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.CpuTimeProvider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CpuTimeProvider__ctor_mE73ABBAB48B1D65051956A2C32BE69E65A7B87F4 (CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::InvokeEndOfFrame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AdaptivePerformanceManager_InvokeEndOfFrame_m1F36AB54DF0C93F0ACFD57FE37ACFA1B4A155E30 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.GpuTimeProvider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GpuTimeProvider__ctor_m53082BB9CAF2611637533E336F6E406A306DCA9A (GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend__ctor_mC16C5848B70A106139BFAD5068FEE2332A93D8DB (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, bool ___useProviderTrend0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::set_PerformanceControlMode(UnityEngine.AdaptivePerformance.PerformanceControlMode)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_PerformanceControlMode_m90A342E8781C151B8F940601A615EBE727ED68BD_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_AutomaticPerformanceControl()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool AdaptivePerformanceManager_get_AutomaticPerformanceControl_m7E8E0B158A408D6E61DFEB69C69998792A97A98B_inline (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.ThermalEventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThermalEventHandler__ctor_mAFECFD9819ACDC6C121411AE80642348AD64178E (ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::add_ThermalEvent(UnityEngine.AdaptivePerformance.ThermalEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_add_ThermalEvent_m882A1B95EDCFA6A87913F08818FE63065C43AE5F (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceBottleneckChangeHandler__ctor_mD8934C37B86DA79AB1370FF2DFA1843619EE6527 (PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::add_PerformanceBottleneckChangeEvent(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_add_PerformanceBottleneckChangeEvent_m13961F9A0E9DEA59C0FA135FA1BFA0D77024087D (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceLevelChangeHandler__ctor_mE8D5A7F6C1FBB34E10D5D240F20870D6B8188D17 (PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::add_PerformanceLevelChangeEvent(UnityEngine.AdaptivePerformance.PerformanceLevelChangeHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_add_PerformanceLevelChangeEvent_mCA3922EBF78BB3B8C28955EC352246C3E8D59FB8 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::UpdateSubsystem()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_UpdateSubsystem_m5D0DFA14D25B45D715DDF4274C46E0DE82D78DBA (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method);
// System.String System.Int32::ToString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m5A125A41C41701E41FA0C4CC52CADBC73C1C96D8 (int32_t* __this, String_t* ___format0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.RunningAverage::AddValue(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RunningAverage_AddValue_m5C454C088FA345480368C01D995C56D618CD4733 (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, float ___NewValue0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager/<InvokeEndOfFrame>d__72::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInvokeEndOfFrameU3Ed__72__ctor_m42BA6D9B33294183365E97FCA422CE46CFA7BF88 (U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::WillCurrentFrameRender()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AdaptivePerformanceManager_WillCurrentFrameRender_m9662ECAA12EB05BE964E89A1E29AC0AA37F82DDC (const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.CpuTimeProvider::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CpuTimeProvider_LateUpdate_mABB99A8A913090AFBBBB2C112EE1734BFE1358BC (CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.GpuTimeProvider::Measure()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GpuTimeProvider_Measure_mF6BF86D1707BD5E8700A0A6F2D0D09C39B74E1B8 (GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_LoggingFrequencyInFrames()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t AdaptivePerformanceManager_get_LoggingFrequencyInFrames_mB92896362D49659E0136CADA5FA59D8C052C3B58_inline (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method);
// UnityEngine.AdaptivePerformance.WarningLevel UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_WarningLevel()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PerformanceDataRecord_get_WarningLevel_m8A58D1CDD83B9F86781080C042029E2D4B4795D7_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_TemperatureLevel()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_TemperatureLevel_m8ADD4135E271A910EB33ED6B113E6153A8B54CCE_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2 (const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::AccumulateTimingValue(System.Single&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_AccumulateTimingValue_mD19C087BBD955CBA731D320F37EFCE0013AE7D39 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, float* ___accu0, float ___newValue1, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_OverallFrameTime()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_OverallFrameTime_mD8BE680879118B05C2BFA66EC2F0BF8C69CC8C59_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::AddNonNegativeValue(UnityEngine.AdaptivePerformance.RunningAverage,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_AddNonNegativeValue_m3D374415DF363E5DC0E1B44E04817EA2E2BB6262 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * ___runningAverage0, float ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.GpuTimeProvider::get_GpuFrameTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GpuTimeProvider_get_GpuFrameTime_mA1F553C55D589588F6954C2E87D835640DD43C7B (GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_GpuFrameTime()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_GpuFrameTime_m50329D6AADDB092BE7BA0E9EAD569D4885774BB1_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.CpuTimeProvider::get_CpuFrameTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CpuTimeProvider_get_CpuFrameTime_mFDBF3DF1106A47DA135FF7B49389CC355108BA25 (CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_CpuFrameTime()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_CpuFrameTime_m53BE7B6BA7C387D27C6B789F80487C19E48DD042_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_TemperatureTrend()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_TemperatureTrend_m65CC0E6927BE74931532ACB8DE6BAA1CBBF12231_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method);
// UnityEngine.AdaptivePerformance.Provider.Feature UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_ChangeFlags()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8 (const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::Update(System.Single,System.Single,System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend_Update_m5BA796D968562EF7CEB9D5644CDC7EBC8FA9A1AE (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, float ___temperatureTrendFromProvider0, float ___newTemperatureLevel1, bool ___changed2, float ___newTemperatureTimestamp3, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend_Reset_m71442095ADD319071142E1B14DA2EA27F3F8FEC4 (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.TemperatureTrend::get_ThermalTrend()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float TemperatureTrend_get_ThermalTrend_m971750AEB690F8B4ED9FE84C5BA802B8C5EB4B5F_inline (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.RunningAverage::GetAverageOr(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RunningAverage_GetAverageOr_mEE0503DACB72C6CF46D78B893479E23638E92484 (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, float ___defaultValue0, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.RunningAverage::GetMostRecentValueOr(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RunningAverage_GetMostRecentValueOr_mAC56F749F88BC98DAB20A5070A71B14FCE8E7566 (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, float ___defaultValue0, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::EffectiveTargetFrameRate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AdaptivePerformanceManager_EffectiveTargetFrameRate_m05458EB876A5E7810268A2DF19E175F2A3D7B9E4 (const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.RunningAverage::GetNumValues()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t RunningAverage_GetNumValues_mCAA37709BFE682697178E705A920598DBCD9D395_inline (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.RunningAverage::GetSampleWindowSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RunningAverage_GetSampleWindowSize_mCFD8C3F0168893D1C550FB0C88B62A03EDD35411 (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, const RuntimeMethod* method);
// UnityEngine.AdaptivePerformance.PerformanceBottleneck UnityEngine.AdaptivePerformance.PerformanceMetrics::get_PerformanceBottleneck()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PerformanceMetrics_get_PerformanceBottleneck_m518B78FAA9F20BD9F79F3EAFCE7CC81612F55CC6_inline (PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.FrameTiming::get_AverageCpuFrameTime()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float FrameTiming_get_AverageCpuFrameTime_m1BAD83D813770DB099F29C2ACAB31D4C08A79ED0_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.FrameTiming::get_AverageGpuFrameTime()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float FrameTiming_get_AverageGpuFrameTime_m2AA4C5FB0D07DA307EF04B40B60B211B21700386_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.FrameTiming::get_AverageFrameTime()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float FrameTiming_get_AverageFrameTime_m6C518BA12C5C330F471B31E364A37362AC439618_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, const RuntimeMethod* method);
// UnityEngine.AdaptivePerformance.PerformanceBottleneck UnityEngine.AdaptivePerformance.BottleneckUtil::DetermineBottleneck(UnityEngine.AdaptivePerformance.PerformanceBottleneck,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BottleneckUtil_DetermineBottleneck_m4645F830559B7B5F54151AB2FF354C6419F8E3FD (int32_t ___prevBottleneck0, float ___averageCpuFrameTime1, float ___averageGpuFrametime2, float ___averageOverallFrametime3, float ___targetFrameTime4, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs::set_PerformanceBottleneck(UnityEngine.AdaptivePerformance.PerformanceBottleneck)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceBottleneckChangeEventArgs_set_PerformanceBottleneck_m63560EBBBEA19F0D26251C884A8CEE5FD3C07B03_inline (PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_CpuPerformanceLevel()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PerformanceDataRecord_get_CpuPerformanceLevel_mAADDE6EE97531363BB97CBD7697B9B6D610DEE41_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::set_CurrentCpuLevel(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_CurrentCpuLevel_mA17D47BD068FBA9741C61F26D8869577FDC3B04E_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_GpuPerformanceLevel()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PerformanceDataRecord_get_GpuPerformanceLevel_mB1DD57E4C0ECDC9F74181D5E5CFC8265B52ED4B4_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::set_CurrentGpuLevel(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_CurrentGpuLevel_m58F4826025D590BCC9D4BA9CB9DC76452DD369B2_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_PerformanceLevelControlAvailable()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool PerformanceDataRecord_get_PerformanceLevelControlAvailable_mC2FDF55F921B13DC69CCC033C952A1D293FE4572_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_TargetFrameTime(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_TargetFrameTime_m86953888D9B6699A5BA926B6C0260665B860E7B2_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_Enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_Enabled_mD0D8E5D8A2D467B0DC314AC0C511F4327E2B18E8 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_Enabled()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_get_Enabled_mBA9F1DC76E6FC28B0DB7F4A7319E06EA8CFAA228_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::Override(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_Override_mE3A9350573679DEBE02F35C2F7B80694973A157E (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, int32_t ___requestedCpuLevel0, int32_t ___requestedGpuLevel1, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::set_ManualOverride(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_ManualOverride_mF3CD9BE22E685E48E56D4974302749BA02F2A1FD_inline (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_Update_m7C0E073667FD597C31FE6A817FCC47930D2B6973 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::set_CpuLevel(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_CpuLevel_mE7771043E97430A856CDFAA1162D257801BDD1F4_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::set_GpuLevel(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_GpuLevel_m09C65245F940E90A38B7C18326038C843F1CE005_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::Update(UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DevicePerformanceControlImpl_Update_m62E8744125498EDACB2150F2DEF0DF6D6AA58635 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * ___changeArgs0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeHandler::Invoke(UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceLevelChangeHandler_Invoke_mC0AAD0689A73E5A05C9AA0CFFB6D079D9B3A491E (PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * __this, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  ___levelChangeEventArgs0, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_CurrentCpuLevel()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_CurrentCpuLevel_m5B3842B5593E0ECD6FD2DC40C4A2B33495411C13_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_CurrentGpuLevel()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_CurrentGpuLevel_m8BDBF664287A7FD279C7A264AAA9818102B3B6F4_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.ThermalEventHandler::Invoke(UnityEngine.AdaptivePerformance.ThermalMetrics)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThermalEventHandler_Invoke_m48931BBF9EBE5CFAA39300884D1550F27FBB1370 (ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * __this, ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  ___thermalMetrics0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler::Invoke(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceBottleneckChangeHandler_Invoke_m503AB2767CF3A258FDE85919B7BBC4A91EC1C755 (PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * __this, PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  ___bottleneckEventArgs0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Rendering.OnDemandRendering::get_renderFrameInterval()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OnDemandRendering_get_renderFrameInterval_m5A12FB459D93296FBACCD6FD59EA27CD092A0132 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Rendering.OnDemandRendering::get_willCurrentFrameRender()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OnDemandRendering_get_willCurrentFrameRender_mBD1AF1E7F139D163B7D065EEDD35925BFD8DE42C (const RuntimeMethod* method);
// System.Int32 UnityEngine.Rendering.OnDemandRendering::get_effectiveRenderFrameRate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OnDemandRendering_get_effectiveRenderFrameRate_m84F9BEAB90EDBC64BA2E2885B692841FCF4A7F7C (const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.RunningAverage::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RunningAverage_Reset_m3AEE75202E5AC2F0059A7FEBBEA32879886DDCC9 (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.CpuTimeProvider::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CpuTimeProvider_Reset_m34832EDC0C3FA6E5931758EA733D318847AA3460 (CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::LogThermalEvent(UnityEngine.AdaptivePerformance.ThermalMetrics)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_LogThermalEvent_m6B30D44A5F2437BB7C94D3FE8AA9A2D29FA3475F (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  ___ev0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::LogBottleneckEvent(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_LogBottleneckEvent_m96B69097EF1D84FB750185F8ED6B6B68BDF458B2 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  ___ev0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::LogPerformanceLevelEvent(UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_LogPerformanceLevelEvent_m1E1E2ECE1BB2A0271DB6821ADD166AACE03C8E61 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  ___ev0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.CpuTimeProvider::EndOfFrame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CpuTimeProvider_EndOfFrame_mA508682DFC8C46B87167317E3A947D645BA8EA53 (CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33 (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* ___name0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.AdaptivePerformance.AdaptivePerformanceManager>()
inline AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * GameObject_AddComponent_TisAdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA_m7C0A87C297E85B0C6CD8653EAEFE085697819809 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared)(__this, method);
}
// System.Void UnityEngine.AdaptivePerformance.Holder::set_Instance(UnityEngine.AdaptivePerformance.IAdaptivePerformance)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Holder_set_Instance_m2786C3B74384A62BD2997F76802455BEAD572CAE_inline (RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___target0, const RuntimeMethod* method);
// System.Void UnityEngine.ScriptableObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B (ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_UpdateInterval(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_UpdateInterval_m802A56839C4049389329DBDE94C7DDA9A3C22680_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_AllowedCpuActiveTimeRatio(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_AllowedCpuActiveTimeRatio_m1526444FAD9758DC00AA9DB07425E26A779750C5_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_AllowedGpuActiveTimeRatio(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_AllowedGpuActiveTimeRatio_mD7660071F2B7DEB8381B364116922BE312A4AA0D_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_GpuLevelBounceAvoidanceThreshold(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_GpuLevelBounceAvoidanceThreshold_m2EE42CF824872EDB3CC001F87B44082954C32627_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_CpuLevelBounceAvoidanceThreshold(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_CpuLevelBounceAvoidanceThreshold_m17883BE79795835F004C470CF5AD8B06829FCF6B_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_MinTargetFrameRateHitTime(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_MinTargetFrameRateHitTime_mFED58D9C51678B867173E655AA4A6DF2B7060B48_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_MaxTemperatureLevel(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_MaxTemperatureLevel_m9826A6487749ADC7D1A66096D1A41FB8D8A31E93_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::UpdateImpl(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_UpdateImpl_mE656C13A7FF6E6CD5667D585EC5CAB561A285C1E (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_UpdateInterval()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_UpdateInterval_mFDABD8980DD503CD69B461CFC843D8BD428CF4B9_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::AllowRaiseGpuLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_AllowRaiseGpuLevel_m3270D2A8F91AE8F4FC4C19CF3E50FD6B50A03816 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::RaiseGpuLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_RaiseGpuLevel_mED360B4ACBE7A2787074DB6F75E8D1DCEF28F804 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::AllowRaiseCpuLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_AllowRaiseCpuLevel_m37030CB2A20F89FEBB0CB2228CE2602991D86613 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::RaiseCpuLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_RaiseCpuLevel_mB6B9F81D1AEE8BF96B2F14295E25C5D82F199250 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_MinTargetFrameRateHitTime()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_MinTargetFrameRateHitTime_m988C790CEF196B60A8154D3B5E6655501550D364_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::AllowLowerCpuLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_AllowLowerCpuLevel_mB670A9E210BCD84FA7419C79D5679303714447D4 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::LowerCpuLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_LowerCpuLevel_m6DF2EDB012FD228606FD09BF4F9C4D1BB9B0E504 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::AllowLowerGpuLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_AllowLowerGpuLevel_m5AD3EB818563DFC12A1538A3011108A1FAD57D52 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::LowerGpuLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_LowerGpuLevel_mD616F65705E36157413404296E82FC53C5650B74 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method);
// UnityEngine.AdaptivePerformance.PerformanceBottleneck UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs::get_PerformanceBottleneck()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PerformanceBottleneckChangeEventArgs_get_PerformanceBottleneck_m625E64FC86EF5C7DD4BEECE772F34AF22C07E4BD_inline (PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_CpuLevelBounceAvoidanceThreshold()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_CpuLevelBounceAvoidanceThreshold_mD4671D4F90CB5F09AD26552591A1513A52206606_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_TargetFrameTime()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_TargetFrameTime_m3EF525DF9A1AAFFA8E7A3839A634D6A3413398C3_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_AllowedCpuActiveTimeRatio()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_AllowedCpuActiveTimeRatio_mF966E1BBFDB821FEA660547F0CFFE0338F493782_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_GpuLevelBounceAvoidanceThreshold()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_GpuLevelBounceAvoidanceThreshold_mB46CCEE95E662384E848FA16446C783A622BBB41_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_AllowedGpuActiveTimeRatio()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_AllowedGpuActiveTimeRatio_mED7543A79BF8EDB3F876FCF5E815E58C39EE98A4_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.ThermalMetrics::get_TemperatureLevel()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float ThermalMetrics_get_TemperatureLevel_m1E7E3A67FD29A5ABA322EB933CF7409CD0151212_inline (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_MaxTemperatureLevel()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_MaxTemperatureLevel_m121C40CD08B738C7B5B2646490700F20A3B4AFB7_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::AllowRaiseLevels()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_AllowRaiseLevels_m9EEE01DF654F72120AAED804C30DF98C6CA5C2E5 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::OnBottleneckChange(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_OnBottleneckChange_m86D2D2E9BAC5B9D742DB0CBC99A8853C0D6A62D7 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  ___ev0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.BottleneckUtil::HittingFrameRateLimit(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BottleneckUtil_HittingFrameRateLimit_mAB23E6E95942DFE4AE0E17796D501D07276DB751 (float ___actualFrameTime0, float ___thresholdFactor1, float ___targetFrameTime2, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.MainThreadCpuTime::GetLatestResult()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float MainThreadCpuTime_GetLatestResult_m5E3D72F4A7FBEDD957340CDAB3E19A128269D5B6_inline (MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AdaptivePerformance.RenderThreadCpuTime::GetLatestResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RenderThreadCpuTime_GetLatestResult_mB9798FC4B9FE40428A1C6BF66EB6A37681F1F9C1 (RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Max_m670AE0EC1B09ED1A56FF9606B0F954670319CB65 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.MainThreadCpuTime::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainThreadCpuTime__ctor_m9600A6AD4493C006BBF6A29C876BED4CD42892E2 (MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.SystemInfo::get_graphicsMultiThreaded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SystemInfo_get_graphicsMultiThreaded_mF8F3D1CBC9FC9F487696079DFC56633824A7B6C0 (const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.RenderThreadCpuTime::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderThreadCpuTime__ctor_m4DF8E17D0BD885CC1F829E3A5E06C219AE3830C7 (RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.RenderThreadCpuTime::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderThreadCpuTime_Reset_m3A491AE6D23434703AE0A91D9145592565EB2191 (RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.RenderThreadCpuTime::Measure()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderThreadCpuTime_Measure_m6C2F782E97EFF82696E79BC952500EDADA7D67EB (RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.MainThreadCpuTime::Measure()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainThreadCpuTime_Measure_m7AE49D83360B7818C0FC957250096E4788DF7EA7 (MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::set_PerformanceControlMode(UnityEngine.AdaptivePerformance.PerformanceControlMode)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_PerformanceControlMode_mE4A2CDE249A57590CA4CD493ACA4DEEC745E1093_inline (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::set_CpuLevel(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_CpuLevel_m844481992806A1B152181FA8631B6033003CD640_inline (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::set_GpuLevel(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_GpuLevel_mBF3785C7E3C4FDEBF29E2E8CE640582658A6BE91_inline (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::set_CpuLevelDelta(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_CpuLevelDelta_mDE58CBE256AE306670BE51649DE8895060860A77_inline (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::set_GpuLevelDelta(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_GpuLevelDelta_mBBD8DC5572E5FC9EEA8F67AB5995121010F09532_inline (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_CpuLevel()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_CpuLevel_m3222509C4D795522341F1E93150940DC1966B4B2_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_GpuLevel()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_GpuLevel_m519D52125AE15CDFC3640EBA56B2D8D2B6F5A9B4_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::ComputeDelta(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_ComputeDelta_m31E9E576E9E43E482889111B644554D8841EDE40 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___oldLevel0, int32_t ___newLevel1, const RuntimeMethod* method);
// System.UInt32 UnityEngine.FrameTimingManager::GetLatestTimings(System.UInt32,UnityEngine.FrameTiming[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t FrameTimingManager_GetLatestTimings_m286888EFC8779C9F97D5140EE5D7EE80BEE3DE35 (uint32_t ___numFrames0, FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3* ___timings1, const RuntimeMethod* method);
// System.Void UnityEngine.FrameTimingManager::CaptureFrameTimings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FrameTimingManager_CaptureFrameTimings_m1816EB99EFF92F9394E7000A9CB1F9F9363A90F5 (const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceSubsystemBase__ctor_m28396E884C7A5F5ACD2BBF34FE6039698AB2B106 (AdaptivePerformanceSubsystemBase_tFE4445DE6A7F0520A1707ECCC71C516E3E4FB12E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Subsystem`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>::.ctor()
inline void Subsystem_1__ctor_mFC93A7FA2C0A92CDF2FBC703D15FC3E890A71D8C (Subsystem_1_t5C16E764132B6384B05BBEE4DA18A26F3A78C9FD * __this, const RuntimeMethod* method)
{
	((  void (*) (Subsystem_1_t5C16E764132B6384B05BBEE4DA18A26F3A78C9FD *, const RuntimeMethod*))Subsystem_1__ctor_m7F707019C5D6E2EDA2EC7BFC45B31681B449CE5C_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>::.ctor()
inline void List_1__ctor_m09E20FB8F2F5E707BB0FCDB5B21388DDDA9A7471 (List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void UnityEngine.SubsystemManager::GetSubsystemDescriptors<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor>(System.Collections.Generic.List`1<!!0>)
inline void SubsystemManager_GetSubsystemDescriptors_TisAdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55_m32243E1D766B4AEEF8E2E0EF3A161E83A7AC1B15 (List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * ___descriptors0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC *, const RuntimeMethod*))SubsystemManager_GetSubsystemDescriptors_TisRuntimeObject_m4B9078442334D4AB22E569414B069AAD8C0BB960_gshared)(___descriptors0, method);
}
// System.Void UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::set_ChangeFlags(UnityEngine.AdaptivePerformance.Provider.Feature)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceDataRecord_set_ChangeFlags_m637C86CD7C6A9D4766B6486C0D8CEDD4A0AB63B5_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::set_CpuPerformanceLevel(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceDataRecord_set_CpuPerformanceLevel_m9B1602BD2C7D66ED154B2D2592667EB9A4270C8C_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::set_GpuPerformanceLevel(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceDataRecord_set_GpuPerformanceLevel_mCFD2AA62DA519F272118E5019AE48405BD618E2B_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::set_PerformanceLevelControlAvailable(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceDataRecord_set_PerformanceLevelControlAvailable_m365175DA2C8A6D9EE5D000E389958FDF6F2E23A1_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceSubsystem__ctor_m5A151079573ED0225D9FD5F8DA15204458E0BDC5 (AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem::set_Capabilities(UnityEngine.AdaptivePerformance.Provider.Feature)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AdaptivePerformanceSubsystem_set_Capabilities_m6ACAED2EA7E44E3AF2D8AEB25A42EFA82EFB67F3_inline (AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemBase::set_initialized(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AdaptivePerformanceSubsystemBase_set_initialized_mCF60B3BF86B030D232CAC0EBE07AE58449808B0C_inline (AdaptivePerformanceSubsystemBase_tFE4445DE6A7F0520A1707ECCC71C516E3E4FB12E * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::set_LastRequestedCpuLevel(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem_set_LastRequestedCpuLevel_m35EECCA0681188317D2619A80F6F3B64ABA5D592_inline (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::set_LastRequestedGpuLevel(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem_set_LastRequestedGpuLevel_m5FA5BDBAF556DE1F795CAC408AC077201BD34474_inline (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::get_AcceptsPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TestAdaptivePerformanceSubsystem_get_AcceptsPerformanceLevel_m5611C1AE91C8116552A769005EA126ABAA38D89D (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::set_CpuPerformanceLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem_set_CpuPerformanceLevel_m59E29995AE25848FD051A770DA3D175A7F39098E (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::set_GpuPerformanceLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem_set_GpuPerformanceLevel_mDB2374F91AB158C04869CC2C33F3F483EFE6FFBC (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::get_MaxCpuPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TestAdaptivePerformanceSubsystem_get_MaxCpuPerformanceLevel_m607B05A021EFFA9160E56FCBBE2520526ADBF88A (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::get_MaxGpuPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TestAdaptivePerformanceSubsystem_get_MaxGpuPerformanceLevel_mA0ACEFBE7A1697080A61BC518639D55893E065B5 (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_Min_m1A2CC204E361AE13C329B6535165179798D3313A (int32_t ___a0, int32_t ___b1, const RuntimeMethod* method);
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E (RuntimeArray * ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.StartupSettings::set_Logging(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void StartupSettings_set_Logging_m6CD2E66D91308712825AECF2A1AF80A86F7C4A30_inline (bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.StartupSettings::set_StatsLoggingFrequencyInFrames(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void StartupSettings_set_StatsLoggingFrequencyInFrames_m4AA8C016FE0DAD38D37117A32154D4B542D1D115_inline (int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.StartupSettings::set_Enable(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void StartupSettings_set_Enable_mA1F811FA9465F51D5742B28BFCAA450D9298B6C1_inline (bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.StartupSettings::set_PreferredSubsystem(UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void StartupSettings_set_PreferredSubsystem_m84A3D06E2638662EE488242C3A5A8764ED31D8DD_inline (AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.StartupSettings::set_AutomaticPerformanceControl(System.Boolean)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void StartupSettings_set_AutomaticPerformanceControl_m9C2983846A71477818E1E33CBE4412E9D621A272_inline (bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::set_ThermalTrend(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TemperatureTrend_set_ThermalTrend_mA0E332378E70F80C414402AF81C001DDA7F2740B_inline (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::PushNewValue(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend_PushNewValue_m39C040C880B6B36603F2DF3BA89C47ACB42193E0 (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, float ___tempLevel0, float ___timestamp1, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::UpdateTrend()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend_UpdateTrend_mBA99D62A42FAC09AF17975F27A4BEC2C5E05C015 (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::PopOldestValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend_PopOldestValue_m1EE573E39718EADC81338CBC55C229FFB3D00E53 (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceInitializer::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceInitializer_Initialize_mCAD909920560889DA329579B503C1CA4633853EC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceInitializer_Initialize_mCAD909920560889DA329579B503C1CA4633853EC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ScriptableObject.CreateInstance<AdaptivePerformanceManagerSpawner>();
		ScriptableObject_CreateInstance_TisAdaptivePerformanceManagerSpawner_tB0C73AFB099B61AC0890B6FB092EE08A7C8794A4_m6D26827E87B21608D2903C52C7DC967BE83532DF(/*hidden argument*/ScriptableObject_CreateInstance_TisAdaptivePerformanceManagerSpawner_tB0C73AFB099B61AC0890B6FB092EE08A7C8794A4_m6D26827E87B21608D2903C52C7DC967BE83532DF_RuntimeMethod_var);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::add_ThermalEvent(UnityEngine.AdaptivePerformance.ThermalEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_add_ThermalEvent_m882A1B95EDCFA6A87913F08818FE63065C43AE5F (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_add_ThermalEvent_m882A1B95EDCFA6A87913F08818FE63065C43AE5F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * V_0 = NULL;
	ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * V_1 = NULL;
	ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * V_2 = NULL;
	{
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_0 = __this->get_ThermalEvent_4();
		V_0 = L_0;
	}

IL_0007:
	{
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_1 = V_0;
		V_1 = L_1;
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_2 = V_1;
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 *)CastclassSealed((RuntimeObject*)L_4, ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128_il2cpp_TypeInfo_var));
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 ** L_5 = __this->get_address_of_ThermalEvent_4();
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_6 = V_2;
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_7 = V_1;
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_8 = InterlockedCompareExchangeImpl<ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 *>((ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 **)L_5, L_6, L_7);
		V_0 = L_8;
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_9 = V_0;
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_10 = V_1;
		if ((!(((RuntimeObject*)(ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 *)L_9) == ((RuntimeObject*)(ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::remove_ThermalEvent(UnityEngine.AdaptivePerformance.ThermalEventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_remove_ThermalEvent_m2408F6AF848DB55EF987D71FD599EFB6E7034726 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_remove_ThermalEvent_m2408F6AF848DB55EF987D71FD599EFB6E7034726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * V_0 = NULL;
	ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * V_1 = NULL;
	ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * V_2 = NULL;
	{
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_0 = __this->get_ThermalEvent_4();
		V_0 = L_0;
	}

IL_0007:
	{
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_1 = V_0;
		V_1 = L_1;
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_2 = V_1;
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 *)CastclassSealed((RuntimeObject*)L_4, ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128_il2cpp_TypeInfo_var));
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 ** L_5 = __this->get_address_of_ThermalEvent_4();
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_6 = V_2;
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_7 = V_1;
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_8 = InterlockedCompareExchangeImpl<ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 *>((ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 **)L_5, L_6, L_7);
		V_0 = L_8;
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_9 = V_0;
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_10 = V_1;
		if ((!(((RuntimeObject*)(ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 *)L_9) == ((RuntimeObject*)(ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::add_PerformanceBottleneckChangeEvent(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_add_PerformanceBottleneckChangeEvent_m13961F9A0E9DEA59C0FA135FA1BFA0D77024087D (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_add_PerformanceBottleneckChangeEvent_m13961F9A0E9DEA59C0FA135FA1BFA0D77024087D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * V_0 = NULL;
	PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * V_1 = NULL;
	PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * V_2 = NULL;
	{
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_0 = __this->get_PerformanceBottleneckChangeEvent_5();
		V_0 = L_0;
	}

IL_0007:
	{
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_1 = V_0;
		V_1 = L_1;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_2 = V_1;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 *)CastclassSealed((RuntimeObject*)L_4, PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995_il2cpp_TypeInfo_var));
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 ** L_5 = __this->get_address_of_PerformanceBottleneckChangeEvent_5();
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_6 = V_2;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_7 = V_1;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_8 = InterlockedCompareExchangeImpl<PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 *>((PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 **)L_5, L_6, L_7);
		V_0 = L_8;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_9 = V_0;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_10 = V_1;
		if ((!(((RuntimeObject*)(PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 *)L_9) == ((RuntimeObject*)(PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::remove_PerformanceBottleneckChangeEvent(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_remove_PerformanceBottleneckChangeEvent_mC4F532774EE5584F17F04BD4027BF2CF095F7D1A (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_remove_PerformanceBottleneckChangeEvent_mC4F532774EE5584F17F04BD4027BF2CF095F7D1A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * V_0 = NULL;
	PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * V_1 = NULL;
	PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * V_2 = NULL;
	{
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_0 = __this->get_PerformanceBottleneckChangeEvent_5();
		V_0 = L_0;
	}

IL_0007:
	{
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_1 = V_0;
		V_1 = L_1;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_2 = V_1;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 *)CastclassSealed((RuntimeObject*)L_4, PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995_il2cpp_TypeInfo_var));
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 ** L_5 = __this->get_address_of_PerformanceBottleneckChangeEvent_5();
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_6 = V_2;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_7 = V_1;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_8 = InterlockedCompareExchangeImpl<PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 *>((PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 **)L_5, L_6, L_7);
		V_0 = L_8;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_9 = V_0;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_10 = V_1;
		if ((!(((RuntimeObject*)(PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 *)L_9) == ((RuntimeObject*)(PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::add_PerformanceLevelChangeEvent(UnityEngine.AdaptivePerformance.PerformanceLevelChangeHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_add_PerformanceLevelChangeEvent_mCA3922EBF78BB3B8C28955EC352246C3E8D59FB8 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_add_PerformanceLevelChangeEvent_mCA3922EBF78BB3B8C28955EC352246C3E8D59FB8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * V_0 = NULL;
	PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * V_1 = NULL;
	PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * V_2 = NULL;
	{
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_0 = __this->get_PerformanceLevelChangeEvent_6();
		V_0 = L_0;
	}

IL_0007:
	{
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_1 = V_0;
		V_1 = L_1;
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_2 = V_1;
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 *)CastclassSealed((RuntimeObject*)L_4, PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301_il2cpp_TypeInfo_var));
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 ** L_5 = __this->get_address_of_PerformanceLevelChangeEvent_6();
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_6 = V_2;
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_7 = V_1;
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_8 = InterlockedCompareExchangeImpl<PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 *>((PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 **)L_5, L_6, L_7);
		V_0 = L_8;
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_9 = V_0;
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_10 = V_1;
		if ((!(((RuntimeObject*)(PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 *)L_9) == ((RuntimeObject*)(PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::remove_PerformanceLevelChangeEvent(UnityEngine.AdaptivePerformance.PerformanceLevelChangeHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_remove_PerformanceLevelChangeEvent_m844136D2DF22DD40F967CCCB19A43550364E3DBE (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_remove_PerformanceLevelChangeEvent_m844136D2DF22DD40F967CCCB19A43550364E3DBE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * V_0 = NULL;
	PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * V_1 = NULL;
	PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * V_2 = NULL;
	{
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_0 = __this->get_PerformanceLevelChangeEvent_6();
		V_0 = L_0;
	}

IL_0007:
	{
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_1 = V_0;
		V_1 = L_1;
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_2 = V_1;
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 *)CastclassSealed((RuntimeObject*)L_4, PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301_il2cpp_TypeInfo_var));
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 ** L_5 = __this->get_address_of_PerformanceLevelChangeEvent_6();
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_6 = V_2;
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_7 = V_1;
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_8 = InterlockedCompareExchangeImpl<PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 *>((PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 **)L_5, L_6, L_7);
		V_0 = L_8;
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_9 = V_0;
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_10 = V_1;
		if ((!(((RuntimeObject*)(PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 *)L_9) == ((RuntimeObject*)(PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// UnityEngine.AdaptivePerformance.ThermalMetrics UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_ThermalMetrics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  AdaptivePerformanceManager_get_ThermalMetrics_mF02703C9D1FB356D713CA9A19E5071AB36F78F06 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public ThermalMetrics ThermalMetrics { get { return m_ThermalMetrics; } }
		ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  L_0 = __this->get_m_ThermalMetrics_12();
		return L_0;
	}
}
// UnityEngine.AdaptivePerformance.PerformanceMetrics UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_PerformanceMetrics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F  AdaptivePerformanceManager_get_PerformanceMetrics_m0DF66F63DF6F5AAE52F30BD34436EA2C9C1FC7E8 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public PerformanceMetrics PerformanceMetrics { get { return m_PerformanceMetrics; } }
		PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F  L_0 = __this->get_m_PerformanceMetrics_13();
		return L_0;
	}
}
// UnityEngine.AdaptivePerformance.FrameTiming UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_FrameTiming()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  AdaptivePerformanceManager_get_FrameTiming_mA5B9441D7984CC69D4ADD66F01F83E68D98BFCE4 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public FrameTiming FrameTiming { get { return m_FrameTiming; } }
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  L_0 = __this->get_m_FrameTiming_14();
		return L_0;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_Logging()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AdaptivePerformanceManager_get_Logging_m12520E205DAA0BD2BCA252EDAF646B3D952DD931 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_get_Logging_m12520E205DAA0BD2BCA252EDAF646B3D952DD931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return APLog.enabled; }
		bool L_0 = ((APLog_t31BBAEC90A9C4586244623826D33D2C445830E8E_StaticFields*)il2cpp_codegen_static_fields_for(APLog_t31BBAEC90A9C4586244623826D33D2C445830E8E_il2cpp_TypeInfo_var))->get_enabled_0();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::set_Logging(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_set_Logging_m210CEDE52B16E37F9D1AC0B7D5AD6760F7E8CEEF (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_set_Logging_m210CEDE52B16E37F9D1AC0B7D5AD6760F7E8CEEF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// set { APLog.enabled = value; }
		bool L_0 = ___value0;
		((APLog_t31BBAEC90A9C4586244623826D33D2C445830E8E_StaticFields*)il2cpp_codegen_static_fields_for(APLog_t31BBAEC90A9C4586244623826D33D2C445830E8E_il2cpp_TypeInfo_var))->set_enabled_0(L_0);
		// set { APLog.enabled = value; }
		return;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_LoggingFrequencyInFrames()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AdaptivePerformanceManager_get_LoggingFrequencyInFrames_mB92896362D49659E0136CADA5FA59D8C052C3B58 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public int LoggingFrequencyInFrames { get; set; }
		int32_t L_0 = __this->get_U3CLoggingFrequencyInFramesU3Ek__BackingField_15();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::set_LoggingFrequencyInFrames(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_set_LoggingFrequencyInFrames_mD228BFEE1C9900E7E8A8063826562B3D7CED38DA (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int LoggingFrequencyInFrames { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CLoggingFrequencyInFramesU3Ek__BackingField_15(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_Active()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AdaptivePerformanceManager_get_Active_m87F48C2946C1A9754AF63D7AD436CA1F0A9A68F3 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public bool Active { get { return m_Subsystem != null; } }
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_0 = __this->get_m_Subsystem_7();
		return (bool)((!(((RuntimeObject*)(AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 *)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_MaxCpuPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AdaptivePerformanceManager_get_MaxCpuPerformanceLevel_mE52AB027FB908F9AA27B501E1BA79B91CCA87EA1 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public int MaxCpuPerformanceLevel { get { return m_DevicePerfControl != null ? m_DevicePerfControl.MaxCpuPerformanceLevel : Constants.UnknownPerformanceLevel; } }
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_0 = __this->get_m_DevicePerfControl_17();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (-1);
	}

IL_000a:
	{
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_1 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_1);
		int32_t L_2 = DevicePerformanceControlImpl_get_MaxCpuPerformanceLevel_mC8D89A2CC5E0135E12B089A50D6289FC2DC97B9A(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_MaxGpuPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AdaptivePerformanceManager_get_MaxGpuPerformanceLevel_m599AEBD49058EBCD9CD82962E3B890FDEEC89F50 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public int MaxGpuPerformanceLevel { get { return m_DevicePerfControl != null ? m_DevicePerfControl.MaxGpuPerformanceLevel : Constants.UnknownPerformanceLevel; } }
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_0 = __this->get_m_DevicePerfControl_17();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (-1);
	}

IL_000a:
	{
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_1 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_1);
		int32_t L_2 = DevicePerformanceControlImpl_get_MaxGpuPerformanceLevel_mF87A40C6B5D9EB47C980229BE85925CF3CEA39CE(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_AutomaticPerformanceControl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AdaptivePerformanceManager_get_AutomaticPerformanceControl_m7E8E0B158A408D6E61DFEB69C69998792A97A98B (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public bool AutomaticPerformanceControl { get; set; }
		bool L_0 = __this->get_U3CAutomaticPerformanceControlU3Ek__BackingField_16();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::set_AutomaticPerformanceControl(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_set_AutomaticPerformanceControl_mE17FB23F321B2C59242F1CB53295DBEA1F7F94FC (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool AutomaticPerformanceControl { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CAutomaticPerformanceControlU3Ek__BackingField_16(L_0);
		return;
	}
}
// UnityEngine.AdaptivePerformance.PerformanceControlMode UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_PerformanceControlMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AdaptivePerformanceManager_get_PerformanceControlMode_mA001E75E34EBF6986422457CF74D5E0FC5D8B4F6 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public PerformanceControlMode PerformanceControlMode { get { return m_DevicePerfControl != null ? m_DevicePerfControl.PerformanceControlMode : PerformanceControlMode.System; } }
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_0 = __this->get_m_DevicePerfControl_17();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (int32_t)(2);
	}

IL_000a:
	{
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_1 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_1);
		int32_t L_2 = DevicePerformanceControlImpl_get_PerformanceControlMode_m2E4EE8BB379495FD00D67FCB6675358333F1532D_inline(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_CpuLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AdaptivePerformanceManager_get_CpuLevel_m75CA6089C6D68C655901F942DDFA51C7FF8AA54B (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// get { return m_RequestedCpuLevel; }
		int32_t L_0 = __this->get_m_RequestedCpuLevel_9();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::set_CpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_set_CpuLevel_mDDC08B5FA171DCA3FC7776C040E3B2D81FA4FA70 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// m_RequestedCpuLevel = value;
		int32_t L_0 = ___value0;
		__this->set_m_RequestedCpuLevel_9(L_0);
		// m_NewUserPerformanceLevelRequest = true;
		__this->set_m_NewUserPerformanceLevelRequest_11((bool)1);
		// }
		return;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_GpuLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AdaptivePerformanceManager_get_GpuLevel_mAAB45A225C4394C2434454CBEA7654D783FD09B2 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// get { return m_RequestedGpuLevel; }
		int32_t L_0 = __this->get_m_RequestedGpuLevel_10();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::set_GpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_set_GpuLevel_m4FF0489A63246B1020795010C7526BE1843480C0 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// m_RequestedGpuLevel = value;
		int32_t L_0 = ___value0;
		__this->set_m_RequestedGpuLevel_10(L_0);
		// m_NewUserPerformanceLevelRequest = true;
		__this->set_m_NewUserPerformanceLevelRequest_11((bool)1);
		// }
		return;
	}
}
// UnityEngine.AdaptivePerformance.IDevelopmentSettings UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_DevelopmentSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AdaptivePerformanceManager_get_DevelopmentSettings_m23DEE2FCBD6B6AADFD8CE15841A31DCAFCB0D6FB (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public IDevelopmentSettings DevelopmentSettings { get { return this; } }
		return __this;
	}
}
// UnityEngine.AdaptivePerformance.IThermalStatus UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_ThermalStatus()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AdaptivePerformanceManager_get_ThermalStatus_mBA83F333764B91798F714C2F089FF34E7EB7FFAF (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public IThermalStatus ThermalStatus { get { return this; } }
		return __this;
	}
}
// UnityEngine.AdaptivePerformance.IPerformanceStatus UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_PerformanceStatus()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AdaptivePerformanceManager_get_PerformanceStatus_m691041B1532A8353043AF4B634AF019C05986C01 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public IPerformanceStatus PerformanceStatus { get { return this; } }
		return __this;
	}
}
// UnityEngine.AdaptivePerformance.IDevicePerformanceControl UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::get_DevicePerformanceControl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AdaptivePerformanceManager_get_DevicePerformanceControl_m4AD2199517AE0161801EC2280BEC3624102DBC8F (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public IDevicePerformanceControl DevicePerformanceControl { get { return this; } }
		return __this;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager__ctor_mB68642A5CBB6A1880F09D7971FF24C670CCC9CD5 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager__ctor_mB68642A5CBB6A1880F09D7971FF24C670CCC9CD5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  V_0;
	memset((&V_0), 0, sizeof(V_0));
	PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// private int m_RequestedCpuLevel = Constants.UnknownPerformanceLevel;
		__this->set_m_RequestedCpuLevel_9((-1));
		// private int m_RequestedGpuLevel = Constants.UnknownPerformanceLevel;
		__this->set_m_RequestedGpuLevel_10((-1));
		// private ThermalMetrics m_ThermalMetrics = new ThermalMetrics
		// {
		//     WarningLevel = WarningLevel.NoWarning,
		//     TemperatureLevel = -1.0f,
		//     TemperatureTrend = 0.0f,
		// };
		il2cpp_codegen_initobj((&V_0), sizeof(ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 ));
		ThermalMetrics_set_WarningLevel_m2B6C74F0609CAF7AA9FFE97018DB76D03D85CF89_inline((ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 *)(&V_0), 0, /*hidden argument*/NULL);
		ThermalMetrics_set_TemperatureLevel_m55E3869F1C0685354CEBC2FA58FBB009380487FD_inline((ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 *)(&V_0), (-1.0f), /*hidden argument*/NULL);
		ThermalMetrics_set_TemperatureTrend_mF48505C1BB0EBE4F530B0405D60D4396A24B9472_inline((ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 *)(&V_0), (0.0f), /*hidden argument*/NULL);
		ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  L_0 = V_0;
		__this->set_m_ThermalMetrics_12(L_0);
		// private PerformanceMetrics m_PerformanceMetrics = new PerformanceMetrics
		// {
		//     CurrentCpuLevel = Constants.UnknownPerformanceLevel,
		//     CurrentGpuLevel = Constants.UnknownPerformanceLevel,
		//     PerformanceBottleneck = PerformanceBottleneck.Unknown
		// };
		il2cpp_codegen_initobj((&V_1), sizeof(PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F ));
		PerformanceMetrics_set_CurrentCpuLevel_mB0FFC61EEA0C4462338C8E728533FE48D4364D9A_inline((PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *)(&V_1), (-1), /*hidden argument*/NULL);
		PerformanceMetrics_set_CurrentGpuLevel_m2807D7F1C39CF7C72DCA9CFDC0BB6F90AA2CE326_inline((PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *)(&V_1), (-1), /*hidden argument*/NULL);
		PerformanceMetrics_set_PerformanceBottleneck_mFFE12B2A0C07877C363A05EF1F48B4C9BA27D268_inline((PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *)(&V_1), 0, /*hidden argument*/NULL);
		PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F  L_1 = V_1;
		__this->set_m_PerformanceMetrics_13(L_1);
		// private FrameTiming m_FrameTiming = new FrameTiming
		// {
		//     CurrentFrameTime = -1.0f,
		//     AverageFrameTime = -1.0f,
		//     CurrentGpuFrameTime = -1.0f,
		//     AverageGpuFrameTime = -1.0f,
		//     CurrentCpuFrameTime = -1.0f,
		//     AverageCpuFrameTime = -1.0f
		// };
		il2cpp_codegen_initobj((&V_2), sizeof(FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 ));
		FrameTiming_set_CurrentFrameTime_m2DD61370159EDB8AE9612CC6303B25175788A0E5_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)(&V_2), (-1.0f), /*hidden argument*/NULL);
		FrameTiming_set_AverageFrameTime_mBDC023C5DDCE8C8069CBEAF28DAC912744D648C5_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)(&V_2), (-1.0f), /*hidden argument*/NULL);
		FrameTiming_set_CurrentGpuFrameTime_mBAB674B381CA46E589A79890A9BE0A0FDC2EE781_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)(&V_2), (-1.0f), /*hidden argument*/NULL);
		FrameTiming_set_AverageGpuFrameTime_mADCD67452E1D4C92E7562EDD78A5BC411B3BC29B_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)(&V_2), (-1.0f), /*hidden argument*/NULL);
		FrameTiming_set_CurrentCpuFrameTime_m95A32BC4E4AE8885D1738D309F2C39E96E887810_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)(&V_2), (-1.0f), /*hidden argument*/NULL);
		FrameTiming_set_AverageCpuFrameTime_m87D8B833265478A0EA25153B3D5299AACAE71BDE_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)(&V_2), (-1.0f), /*hidden argument*/NULL);
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  L_2 = V_2;
		__this->set_m_FrameTiming_14(L_2);
		// private WaitForEndOfFrame m_WaitForEndOfFrame = new WaitForEndOfFrame();
		WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * L_3 = (WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA *)il2cpp_codegen_object_new(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m6CDB79476A4A84CEC62947D36ADED96E907BA20B(L_3, /*hidden argument*/NULL);
		__this->set_m_WaitForEndOfFrame_24(L_3);
		// private RunningAverage m_OverallFrameTime = new RunningAverage();   // In seconds
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_4 = (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 *)il2cpp_codegen_object_new(RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1_il2cpp_TypeInfo_var);
		RunningAverage__ctor_m1D5CCDBE8A19B9CCE2F0644773094D4157AEC9AF(L_4, ((int32_t)100), /*hidden argument*/NULL);
		__this->set_m_OverallFrameTime_26(L_4);
		// private RunningAverage m_GpuFrameTime = new RunningAverage();   // In seconds
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_5 = (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 *)il2cpp_codegen_object_new(RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1_il2cpp_TypeInfo_var);
		RunningAverage__ctor_m1D5CCDBE8A19B9CCE2F0644773094D4157AEC9AF(L_5, ((int32_t)100), /*hidden argument*/NULL);
		__this->set_m_GpuFrameTime_28(L_5);
		// private RunningAverage m_CpuFrameTime = new RunningAverage();   // In seconds
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_6 = (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 *)il2cpp_codegen_object_new(RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1_il2cpp_TypeInfo_var);
		RunningAverage__ctor_m1D5CCDBE8A19B9CCE2F0644773094D4157AEC9AF(L_6, ((int32_t)100), /*hidden argument*/NULL);
		__this->set_m_CpuFrameTime_29(L_6);
		// AdaptivePerformanceManager()
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		// AutomaticPerformanceControl = StartupSettings.AutomaticPerformanceControl;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		bool L_7 = StartupSettings_get_AutomaticPerformanceControl_mD3FD90C9A974872C6E1F4C8F0F857CE658654E83_inline(/*hidden argument*/NULL);
		AdaptivePerformanceManager_set_AutomaticPerformanceControl_mE17FB23F321B2C59242F1CB53295DBEA1F7F94FC_inline(__this, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::InitializeSubsystem(UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AdaptivePerformanceManager_InitializeSubsystem_mBECAD18DEAA314DB5FDB4063D124F29CFA7619FF (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * ___subsystem0, const RuntimeMethod* method)
{
	{
		// if (subsystem == null)
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_0 = ___subsystem0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0005:
	{
		// subsystem.Start();
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_1 = ___subsystem0;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(4 /* System.Void UnityEngine.Subsystem::Start() */, L_1);
		// if (subsystem.initialized)
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_2 = ___subsystem0;
		NullCheck(L_2);
		bool L_3 = AdaptivePerformanceSubsystemBase_get_initialized_mCF8947352D834034868007E91F84485171F40ACA_inline(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		// m_Subsystem = subsystem;
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_4 = ___subsystem0;
		__this->set_m_Subsystem_7(L_4);
		// return true;
		return (bool)1;
	}

IL_001c:
	{
		// subsystem.Destroy();
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_5 = ___subsystem0;
		NullCheck(L_5);
		Subsystem_Destroy_m1D65C2E3B540A9EC80E14BF0C7A2BE8CDCF887A4(L_5, /*hidden argument*/NULL);
		// return false;
		return (bool)0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_Start_m32763F3786711CBF6BEC7CF4DB8674E5CDC59020 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_Start_m32763F3786711CBF6BEC7CF4DB8674E5CDC59020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325  V_0;
	memset((&V_0), 0, sizeof(V_0));
	AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// APLog.enabled = StartupSettings.Logging;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		bool L_0 = StartupSettings_get_Logging_m7A40AE632795747F6E17C5ABABEF45D83E01EAA3_inline(/*hidden argument*/NULL);
		((APLog_t31BBAEC90A9C4586244623826D33D2C445830E8E_StaticFields*)il2cpp_codegen_static_fields_for(APLog_t31BBAEC90A9C4586244623826D33D2C445830E8E_il2cpp_TypeInfo_var))->set_enabled_0(L_0);
		// LoggingFrequencyInFrames = StartupSettings.StatsLoggingFrequencyInFrames;
		int32_t L_1 = StartupSettings_get_StatsLoggingFrequencyInFrames_m5F108EC9014719068FF27AD8D23E21E1FD1DF380_inline(/*hidden argument*/NULL);
		AdaptivePerformanceManager_set_LoggingFrequencyInFrames_mD228BFEE1C9900E7E8A8063826562B3D7CED38DA_inline(__this, L_1, /*hidden argument*/NULL);
		// if (!StartupSettings.Enable)
		bool L_2 = StartupSettings_get_Enable_m8343935449F13DAF78A3EE247DA316A91695A9D2_inline(/*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		// return;
		return;
	}

IL_001d:
	{
		// if (InitializeSubsystem(StartupSettings.PreferredSubsystem))
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_3 = StartupSettings_get_PreferredSubsystem_m288E01FA3B91225A014D305D97163961E988C5B3_inline(/*hidden argument*/NULL);
		bool L_4 = AdaptivePerformanceManager_InitializeSubsystem_mBECAD18DEAA314DB5FDB4063D124F29CFA7619FF(__this, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		// m_Subsystem = StartupSettings.PreferredSubsystem;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_5 = StartupSettings_get_PreferredSubsystem_m288E01FA3B91225A014D305D97163961E988C5B3_inline(/*hidden argument*/NULL);
		__this->set_m_Subsystem_7(L_5);
		// }
		goto IL_0083;
	}

IL_0037:
	{
		// System.Collections.Generic.List<Provider.AdaptivePerformanceSubsystemDescriptor> perfDescriptors = Provider.AdaptivePerformanceSubsystemRegistry.GetRegisteredDescriptors();
		List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * L_6 = AdaptivePerformanceSubsystemRegistry_GetRegisteredDescriptors_mA1D2C28ABC171D8FB88C9EEC96BD63A303456604(/*hidden argument*/NULL);
		// if(perfDescriptors.Count == 0)
		List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * L_7 = L_6;
		NullCheck(L_7);
		List_1_get_Count_m4F79244F68273A7F2AFE63F5443754B4A2BE62CE_inline(L_7, /*hidden argument*/List_1_get_Count_m4F79244F68273A7F2AFE63F5443754B4A2BE62CE_RuntimeMethod_var);
		// foreach (var perfDesc in perfDescriptors)
		NullCheck(L_7);
		Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325  L_8 = List_1_GetEnumerator_m8E054D1AFBD9129D72D963784EE1E501B02B6260(L_7, /*hidden argument*/List_1_GetEnumerator_m8E054D1AFBD9129D72D963784EE1E501B02B6260_RuntimeMethod_var);
		V_0 = L_8;
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006a;
		}

IL_004b:
		{
			// foreach (var perfDesc in perfDescriptors)
			AdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55 * L_9 = Enumerator_get_Current_mC39D2C568D4151FA33784EB27005767BCC518FF5_inline((Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325 *)(&V_0), /*hidden argument*/Enumerator_get_Current_mC39D2C568D4151FA33784EB27005767BCC518FF5_RuntimeMethod_var);
			// var subsystem = perfDesc.Create();
			NullCheck(L_9);
			AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_10 = SubsystemDescriptor_1_Create_mB303B00807AACC42F9449C37EF70DF21EF952878(L_9, /*hidden argument*/SubsystemDescriptor_1_Create_mB303B00807AACC42F9449C37EF70DF21EF952878_RuntimeMethod_var);
			V_1 = L_10;
			// if (InitializeSubsystem(subsystem))
			AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_11 = V_1;
			bool L_12 = AdaptivePerformanceManager_InitializeSubsystem_mBECAD18DEAA314DB5FDB4063D124F29CFA7619FF(__this, L_11, /*hidden argument*/NULL);
			if (!L_12)
			{
				goto IL_006a;
			}
		}

IL_0061:
		{
			// m_Subsystem = subsystem;
			AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_13 = V_1;
			__this->set_m_Subsystem_7(L_13);
			// break;
			IL2CPP_LEAVE(0x83, FINALLY_0075);
		}

IL_006a:
		{
			// foreach (var perfDesc in perfDescriptors)
			bool L_14 = Enumerator_MoveNext_m61C82A0B80A3224E2C00EC055806FE26F81964A6((Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m61C82A0B80A3224E2C00EC055806FE26F81964A6_RuntimeMethod_var);
			if (L_14)
			{
				goto IL_004b;
			}
		}

IL_0073:
		{
			IL2CPP_LEAVE(0x83, FINALLY_0075);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0075;
	}

FINALLY_0075:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1E1887EFB1E29C201DA849DA80C01B6CF756F506((Enumerator_t3ACAF532B8333B5ACC1248FB0FA564783AD03325 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m1E1887EFB1E29C201DA849DA80C01B6CF756F506_RuntimeMethod_var);
		IL2CPP_END_FINALLY(117)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(117)
	{
		IL2CPP_JUMP_TBL(0x83, IL_0083)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0083:
	{
		// if (m_Subsystem != null)
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_15 = __this->get_m_Subsystem_7();
		if (!L_15)
		{
			goto IL_0233;
		}
	}
	{
		// m_UseProviderOverallFrameTime = m_Subsystem.Capabilities.HasFlag(Provider.Feature.OverallFrameTime);
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_16 = __this->get_m_Subsystem_7();
		NullCheck(L_16);
		int32_t L_17 = AdaptivePerformanceSubsystem_get_Capabilities_m42EEFCDECCBB0DFFEBE3E65BAA42ED01256FB18F_inline(L_16, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		RuntimeObject * L_19 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_18);
		int32_t L_20 = ((int32_t)((int32_t)256));
		RuntimeObject * L_21 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_20);
		NullCheck((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_19);
		bool L_22 = Enum_HasFlag_m5D934A541DEEF44DBF3415EE47F8CCED9370C173((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_19, (Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_21, /*hidden argument*/NULL);
		__this->set_m_UseProviderOverallFrameTime_23(L_22);
		// m_DevicePerfControl = new DevicePerformanceControlImpl(m_Subsystem.PerformanceLevelControl);
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_23 = __this->get_m_Subsystem_7();
		NullCheck(L_23);
		RuntimeObject* L_24 = VirtFuncInvoker0< RuntimeObject* >::Invoke(9 /* UnityEngine.AdaptivePerformance.Provider.IDevicePerformanceLevelControl UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem::get_PerformanceLevelControl() */, L_23);
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_25 = (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 *)il2cpp_codegen_object_new(DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326_il2cpp_TypeInfo_var);
		DevicePerformanceControlImpl__ctor_m41818A4930BAEB5BE2E0903DDBCCE23C043F0A07(L_25, L_24, /*hidden argument*/NULL);
		__this->set_m_DevicePerfControl_17(L_25);
		// m_AutoPerformanceLevelController = new AutoPerformanceLevelController(m_DevicePerfControl, PerformanceStatus, ThermalStatus);
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_26 = __this->get_m_DevicePerfControl_17();
		RuntimeObject* L_27 = AdaptivePerformanceManager_get_PerformanceStatus_m691041B1532A8353043AF4B634AF019C05986C01(__this, /*hidden argument*/NULL);
		RuntimeObject* L_28 = AdaptivePerformanceManager_get_ThermalStatus_mBA83F333764B91798F714C2F089FF34E7EB7FFAF(__this, /*hidden argument*/NULL);
		AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * L_29 = (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A *)il2cpp_codegen_object_new(AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A_il2cpp_TypeInfo_var);
		AutoPerformanceLevelController__ctor_m252C3934AA192D4135C2A9CE41B7D4B1216BC88C(L_29, L_26, L_27, L_28, /*hidden argument*/NULL);
		__this->set_m_AutoPerformanceLevelController_18(L_29);
		// m_AppLifecycle = m_Subsystem.ApplicationLifecycle;
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_30 = __this->get_m_Subsystem_7();
		NullCheck(L_30);
		RuntimeObject* L_31 = VirtFuncInvoker0< RuntimeObject* >::Invoke(8 /* UnityEngine.AdaptivePerformance.Provider.IApplicationLifecycle UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem::get_ApplicationLifecycle() */, L_30);
		__this->set_m_AppLifecycle_21(L_31);
		// if (!m_Subsystem.Capabilities.HasFlag(Provider.Feature.CpuFrameTime))
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_32 = __this->get_m_Subsystem_7();
		NullCheck(L_32);
		int32_t L_33 = AdaptivePerformanceSubsystem_get_Capabilities_m42EEFCDECCBB0DFFEBE3E65BAA42ED01256FB18F_inline(L_32, /*hidden argument*/NULL);
		int32_t L_34 = L_33;
		RuntimeObject * L_35 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_34);
		int32_t L_36 = ((int32_t)((int32_t)128));
		RuntimeObject * L_37 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_36);
		NullCheck((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_35);
		bool L_38 = Enum_HasFlag_m5D934A541DEEF44DBF3415EE47F8CCED9370C173((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_35, (Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_37, /*hidden argument*/NULL);
		if (L_38)
		{
			goto IL_0130;
		}
	}
	{
		// m_CpuFrameTimeProvider = new CpuTimeProvider();
		CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * L_39 = (CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 *)il2cpp_codegen_object_new(CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994_il2cpp_TypeInfo_var);
		CpuTimeProvider__ctor_mE73ABBAB48B1D65051956A2C32BE69E65A7B87F4(L_39, /*hidden argument*/NULL);
		__this->set_m_CpuFrameTimeProvider_19(L_39);
		// StartCoroutine(InvokeEndOfFrame());
		RuntimeObject* L_40 = AdaptivePerformanceManager_InvokeEndOfFrame_m1F36AB54DF0C93F0ACFD57FE37ACFA1B4A155E30(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7(__this, L_40, /*hidden argument*/NULL);
	}

IL_0130:
	{
		// if (!m_Subsystem.Capabilities.HasFlag(Provider.Feature.GpuFrameTime))
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_41 = __this->get_m_Subsystem_7();
		NullCheck(L_41);
		int32_t L_42 = AdaptivePerformanceSubsystem_get_Capabilities_m42EEFCDECCBB0DFFEBE3E65BAA42ED01256FB18F_inline(L_41, /*hidden argument*/NULL);
		int32_t L_43 = L_42;
		RuntimeObject * L_44 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_43);
		int32_t L_45 = ((int32_t)((int32_t)64));
		RuntimeObject * L_46 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_45);
		NullCheck((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_44);
		bool L_47 = Enum_HasFlag_m5D934A541DEEF44DBF3415EE47F8CCED9370C173((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_44, (Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_46, /*hidden argument*/NULL);
		if (L_47)
		{
			goto IL_0159;
		}
	}
	{
		// m_GpuFrameTimeProvider = new GpuTimeProvider();
		GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * L_48 = (GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B *)il2cpp_codegen_object_new(GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B_il2cpp_TypeInfo_var);
		GpuTimeProvider__ctor_m53082BB9CAF2611637533E336F6E406A306DCA9A(L_48, /*hidden argument*/NULL);
		__this->set_m_GpuFrameTimeProvider_20(L_48);
	}

IL_0159:
	{
		// m_TemperatureTrend = new TemperatureTrend(m_Subsystem.Capabilities.HasFlag(Provider.Feature.TemperatureTrend));
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_49 = __this->get_m_Subsystem_7();
		NullCheck(L_49);
		int32_t L_50 = AdaptivePerformanceSubsystem_get_Capabilities_m42EEFCDECCBB0DFFEBE3E65BAA42ED01256FB18F_inline(L_49, /*hidden argument*/NULL);
		int32_t L_51 = L_50;
		RuntimeObject * L_52 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_51);
		int32_t L_53 = ((int32_t)4);
		RuntimeObject * L_54 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_53);
		NullCheck((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_52);
		bool L_55 = Enum_HasFlag_m5D934A541DEEF44DBF3415EE47F8CCED9370C173((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_52, (Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_54, /*hidden argument*/NULL);
		TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * L_56 = (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B *)il2cpp_codegen_object_new(TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B_il2cpp_TypeInfo_var);
		TemperatureTrend__ctor_mC16C5848B70A106139BFAD5068FEE2332A93D8DB(L_56, L_55, /*hidden argument*/NULL);
		__this->set_m_TemperatureTrend_22(L_56);
		// if (m_RequestedCpuLevel == Constants.UnknownPerformanceLevel)
		int32_t L_57 = __this->get_m_RequestedCpuLevel_9();
		if ((!(((uint32_t)L_57) == ((uint32_t)(-1)))))
		{
			goto IL_0199;
		}
	}
	{
		// m_RequestedCpuLevel = m_DevicePerfControl.MaxCpuPerformanceLevel;
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_58 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_58);
		int32_t L_59 = DevicePerformanceControlImpl_get_MaxCpuPerformanceLevel_mC8D89A2CC5E0135E12B089A50D6289FC2DC97B9A(L_58, /*hidden argument*/NULL);
		__this->set_m_RequestedCpuLevel_9(L_59);
	}

IL_0199:
	{
		// if (m_RequestedGpuLevel == Constants.UnknownPerformanceLevel)
		int32_t L_60 = __this->get_m_RequestedGpuLevel_10();
		if ((!(((uint32_t)L_60) == ((uint32_t)(-1)))))
		{
			goto IL_01b3;
		}
	}
	{
		// m_RequestedGpuLevel = m_DevicePerfControl.MaxGpuPerformanceLevel;
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_61 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_61);
		int32_t L_62 = DevicePerformanceControlImpl_get_MaxGpuPerformanceLevel_mF87A40C6B5D9EB47C980229BE85925CF3CEA39CE(L_61, /*hidden argument*/NULL);
		__this->set_m_RequestedGpuLevel_10(L_62);
	}

IL_01b3:
	{
		// m_NewUserPerformanceLevelRequest = true;
		__this->set_m_NewUserPerformanceLevelRequest_11((bool)1);
		// if (m_Subsystem.PerformanceLevelControl == null)
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_63 = __this->get_m_Subsystem_7();
		NullCheck(L_63);
		RuntimeObject* L_64 = VirtFuncInvoker0< RuntimeObject* >::Invoke(9 /* UnityEngine.AdaptivePerformance.Provider.IDevicePerformanceLevelControl UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem::get_PerformanceLevelControl() */, L_63);
		if (L_64)
		{
			goto IL_01d5;
		}
	}
	{
		// m_DevicePerfControl.PerformanceControlMode = PerformanceControlMode.System;
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_65 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_65);
		DevicePerformanceControlImpl_set_PerformanceControlMode_m90A342E8781C151B8F940601A615EBE727ED68BD_inline(L_65, 2, /*hidden argument*/NULL);
		goto IL_01f7;
	}

IL_01d5:
	{
		// else if (AutomaticPerformanceControl)
		bool L_66 = AdaptivePerformanceManager_get_AutomaticPerformanceControl_m7E8E0B158A408D6E61DFEB69C69998792A97A98B_inline(__this, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_01eb;
		}
	}
	{
		// m_DevicePerfControl.PerformanceControlMode = PerformanceControlMode.Automatic;
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_67 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_67);
		DevicePerformanceControlImpl_set_PerformanceControlMode_m90A342E8781C151B8F940601A615EBE727ED68BD_inline(L_67, 0, /*hidden argument*/NULL);
		goto IL_01f7;
	}

IL_01eb:
	{
		// m_DevicePerfControl.PerformanceControlMode = PerformanceControlMode.Manual;
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_68 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_68);
		DevicePerformanceControlImpl_set_PerformanceControlMode_m90A342E8781C151B8F940601A615EBE727ED68BD_inline(L_68, 1, /*hidden argument*/NULL);
	}

IL_01f7:
	{
		// ThermalEvent += (ThermalMetrics thermalMetrics) => LogThermalEvent(thermalMetrics);
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_69 = (ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 *)il2cpp_codegen_object_new(ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128_il2cpp_TypeInfo_var);
		ThermalEventHandler__ctor_mAFECFD9819ACDC6C121411AE80642348AD64178E(L_69, __this, (intptr_t)((intptr_t)AdaptivePerformanceManager_U3CStartU3Eb__65_0_m579D006E30704474CA7241DB6E90890689C52CA5_RuntimeMethod_var), /*hidden argument*/NULL);
		AdaptivePerformanceManager_add_ThermalEvent_m882A1B95EDCFA6A87913F08818FE63065C43AE5F(__this, L_69, /*hidden argument*/NULL);
		// PerformanceBottleneckChangeEvent += (PerformanceBottleneckChangeEventArgs ev) => LogBottleneckEvent(ev);
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_70 = (PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 *)il2cpp_codegen_object_new(PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995_il2cpp_TypeInfo_var);
		PerformanceBottleneckChangeHandler__ctor_mD8934C37B86DA79AB1370FF2DFA1843619EE6527(L_70, __this, (intptr_t)((intptr_t)AdaptivePerformanceManager_U3CStartU3Eb__65_1_mA40F9B614EF4AD67A403592F6F063D268B6BA459_RuntimeMethod_var), /*hidden argument*/NULL);
		AdaptivePerformanceManager_add_PerformanceBottleneckChangeEvent_m13961F9A0E9DEA59C0FA135FA1BFA0D77024087D(__this, L_70, /*hidden argument*/NULL);
		// PerformanceLevelChangeEvent += (PerformanceLevelChangeEventArgs ev) => LogPerformanceLevelEvent(ev);
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_71 = (PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 *)il2cpp_codegen_object_new(PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301_il2cpp_TypeInfo_var);
		PerformanceLevelChangeHandler__ctor_mE8D5A7F6C1FBB34E10D5D240F20870D6B8188D17(L_71, __this, (intptr_t)((intptr_t)AdaptivePerformanceManager_U3CStartU3Eb__65_2_mA06C26D79D6B5C59EE680771CFD0AC843B8E40DD_RuntimeMethod_var), /*hidden argument*/NULL);
		AdaptivePerformanceManager_add_PerformanceLevelChangeEvent_mCA3922EBF78BB3B8C28955EC352246C3E8D59FB8(__this, L_71, /*hidden argument*/NULL);
		// UpdateSubsystem();
		AdaptivePerformanceManager_UpdateSubsystem_m5D0DFA14D25B45D715DDF4274C46E0DE82D78DBA(__this, /*hidden argument*/NULL);
	}

IL_0233:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::LogThermalEvent(UnityEngine.AdaptivePerformance.ThermalMetrics)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_LogThermalEvent_m6B30D44A5F2437BB7C94D3FE8AA9A2D29FA3475F (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  ___ev0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::LogBottleneckEvent(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_LogBottleneckEvent_m96B69097EF1D84FB750185F8ED6B6B68BDF458B2 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  ___ev0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.String UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::ToStringWithSign(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AdaptivePerformanceManager_ToStringWithSign_m87DBE28D6E5B93354B282B463C544723A579DC22 (int32_t ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_ToStringWithSign_m87DBE28D6E5B93354B282B463C544723A579DC22_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return x.ToString("+#;-#;0");
		String_t* L_0 = Int32_ToString_m5A125A41C41701E41FA0C4CC52CADBC73C1C96D8((int32_t*)(&___x0), _stringLiteralFDB5AC7F1BDBCF4BF99E8E359CC36FEE9ED29213, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::LogPerformanceLevelEvent(UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_LogPerformanceLevelEvent_m1E1E2ECE1BB2A0271DB6821ADD166AACE03C8E61 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  ___ev0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::AddNonNegativeValue(UnityEngine.AdaptivePerformance.RunningAverage,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_AddNonNegativeValue_m3D374415DF363E5DC0E1B44E04817EA2E2BB6262 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * ___runningAverage0, float ___value1, const RuntimeMethod* method)
{
	{
		// if (value >= 0.0f && value < 1.0f) // don't add frames that took longer than 1s
		float L_0 = ___value1;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		float L_1 = ___value1;
		if ((!(((float)L_1) < ((float)(1.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		// runningAverage.AddValue(value);
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_2 = ___runningAverage0;
		float L_3 = ___value1;
		NullCheck(L_2);
		RunningAverage_AddValue_m5C454C088FA345480368C01D995C56D618CD4733(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::InvokeEndOfFrame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AdaptivePerformanceManager_InvokeEndOfFrame_m1F36AB54DF0C93F0ACFD57FE37ACFA1B4A155E30 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_InvokeEndOfFrame_m1F36AB54DF0C93F0ACFD57FE37ACFA1B4A155E30_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB * L_0 = (U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB *)il2cpp_codegen_object_new(U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB_il2cpp_TypeInfo_var);
		U3CInvokeEndOfFrameU3Ed__72__ctor_m42BA6D9B33294183365E97FCA422CE46CFA7BF88(L_0, 0, /*hidden argument*/NULL);
		U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_LateUpdate_m9454EE6D6B7A6061BCF30282B2B0BA048975CBA4 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// if (m_CpuFrameTimeProvider != null || m_GpuFrameTimeProvider != null)
		CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * L_0 = __this->get_m_CpuFrameTimeProvider_19();
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * L_1 = __this->get_m_GpuFrameTimeProvider_20();
		if (!L_1)
		{
			goto IL_003d;
		}
	}

IL_0010:
	{
		// if (WillCurrentFrameRender())
		bool L_2 = AdaptivePerformanceManager_WillCurrentFrameRender_m9662ECAA12EB05BE964E89A1E29AC0AA37F82DDC(/*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003d;
		}
	}
	{
		// if (m_CpuFrameTimeProvider != null)
		CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * L_3 = __this->get_m_CpuFrameTimeProvider_19();
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		// m_CpuFrameTimeProvider.LateUpdate();
		CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * L_4 = __this->get_m_CpuFrameTimeProvider_19();
		NullCheck(L_4);
		CpuTimeProvider_LateUpdate_mABB99A8A913090AFBBBB2C112EE1734BFE1358BC(L_4, /*hidden argument*/NULL);
	}

IL_002a:
	{
		// if (m_GpuFrameTimeProvider != null)
		GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * L_5 = __this->get_m_GpuFrameTimeProvider_20();
		if (!L_5)
		{
			goto IL_003d;
		}
	}
	{
		// m_GpuFrameTimeProvider.Measure();
		GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * L_6 = __this->get_m_GpuFrameTimeProvider_20();
		NullCheck(L_6);
		GpuTimeProvider_Measure_mF6BF86D1707BD5E8700A0A6F2D0D09C39B74E1B8(L_6, /*hidden argument*/NULL);
	}

IL_003d:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_Update_mE11F4B9516F9DCBFB19979CABF54751AAAD32034 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_Update_mE11F4B9516F9DCBFB19979CABF54751AAAD32034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_Subsystem == null)
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_0 = __this->get_m_Subsystem_7();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// UpdateSubsystem();
		AdaptivePerformanceManager_UpdateSubsystem_m5D0DFA14D25B45D715DDF4274C46E0DE82D78DBA(__this, /*hidden argument*/NULL);
		// if (APLog.enabled && LoggingFrequencyInFrames > 0)
		bool L_1 = ((APLog_t31BBAEC90A9C4586244623826D33D2C445830E8E_StaticFields*)il2cpp_codegen_static_fields_for(APLog_t31BBAEC90A9C4586244623826D33D2C445830E8E_il2cpp_TypeInfo_var))->get_enabled_0();
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_2 = AdaptivePerformanceManager_get_LoggingFrequencyInFrames_mB92896362D49659E0136CADA5FA59D8C052C3B58_inline(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		// m_FrameCount++;
		int32_t L_3 = __this->get_m_FrameCount_25();
		__this->set_m_FrameCount_25(((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)));
		// if (m_FrameCount % LoggingFrequencyInFrames == 0)
		int32_t L_4 = __this->get_m_FrameCount_25();
		int32_t L_5 = AdaptivePerformanceManager_get_LoggingFrequencyInFrames_mB92896362D49659E0136CADA5FA59D8C052C3B58_inline(__this, /*hidden argument*/NULL);
	}

IL_003b:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::AccumulateTimingValue(System.Single&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_AccumulateTimingValue_mD19C087BBD955CBA731D320F37EFCE0013AE7D39 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, float* ___accu0, float ___newValue1, const RuntimeMethod* method)
{
	{
		// if (accu < 0.0f)
		float* L_0 = ___accu0;
		float L_1 = *((float*)L_0);
		if ((!(((float)L_1) < ((float)(0.0f)))))
		{
			goto IL_000a;
		}
	}
	{
		// return; // already invalid
		return;
	}

IL_000a:
	{
		// if (newValue >= 0.0f)
		float L_2 = ___newValue1;
		if ((!(((float)L_2) >= ((float)(0.0f)))))
		{
			goto IL_0019;
		}
	}
	{
		// accu += newValue;
		float* L_3 = ___accu0;
		float* L_4 = ___accu0;
		float L_5 = *((float*)L_4);
		float L_6 = ___newValue1;
		*((float*)L_3) = (float)((float)il2cpp_codegen_add((float)L_5, (float)L_6));
		return;
	}

IL_0019:
	{
		// accu = -1.0f; // set invalid
		float* L_7 = ___accu0;
		*((float*)L_7) = (float)(-1.0f);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::UpdateSubsystem()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_UpdateSubsystem_m5D0DFA14D25B45D715DDF4274C46E0DE82D78DBA (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_UpdateSubsystem_m5D0DFA14D25B45D715DDF4274C46E0DE82D78DBA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	bool V_3 = false;
	PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  V_4;
	memset((&V_4), 0, sizeof(V_4));
	PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  V_5;
	memset((&V_5), 0, sizeof(V_5));
	int32_t V_6 = 0;
	RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * G_B6_0 = NULL;
	AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * G_B6_1 = NULL;
	RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * G_B5_0 = NULL;
	AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * G_B5_1 = NULL;
	float G_B7_0 = 0.0f;
	RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * G_B7_1 = NULL;
	AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * G_B7_2 = NULL;
	RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * G_B9_0 = NULL;
	AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * G_B9_1 = NULL;
	RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * G_B8_0 = NULL;
	AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * G_B8_1 = NULL;
	float G_B10_0 = 0.0f;
	RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * G_B10_1 = NULL;
	AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * G_B10_2 = NULL;
	RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * G_B12_0 = NULL;
	AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * G_B12_1 = NULL;
	RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * G_B11_0 = NULL;
	AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * G_B13_1 = NULL;
	AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * G_B13_2 = NULL;
	int32_t G_B29_0 = 0;
	int32_t G_B31_0 = 0;
	int32_t G_B30_0 = 0;
	int32_t G_B33_0 = 0;
	int32_t G_B32_0 = 0;
	int32_t G_B39_0 = 0;
	int32_t G_B34_0 = 0;
	int32_t G_B38_0 = 0;
	int32_t G_B35_0 = 0;
	int32_t G_B37_0 = 0;
	int32_t G_B36_0 = 0;
	int32_t G_B45_0 = 0;
	int32_t G_B40_0 = 0;
	int32_t G_B44_0 = 0;
	int32_t G_B41_0 = 0;
	int32_t G_B43_0 = 0;
	int32_t G_B42_0 = 0;
	int32_t G_B48_0 = 0;
	int32_t G_B46_0 = 0;
	int32_t G_B47_0 = 0;
	{
		// Provider.PerformanceDataRecord updateResult = m_Subsystem.Update();
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_0 = __this->get_m_Subsystem_7();
		NullCheck(L_0);
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF  L_1 = VirtFuncInvoker0< PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF  >::Invoke(7 /* UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem::Update() */, L_0);
		V_0 = L_1;
		// m_ThermalMetrics.WarningLevel = updateResult.WarningLevel;
		ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * L_2 = __this->get_address_of_m_ThermalMetrics_12();
		int32_t L_3 = PerformanceDataRecord_get_WarningLevel_m8A58D1CDD83B9F86781080C042029E2D4B4795D7_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		ThermalMetrics_set_WarningLevel_m2B6C74F0609CAF7AA9FFE97018DB76D03D85CF89_inline((ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 *)L_2, L_3, /*hidden argument*/NULL);
		// m_ThermalMetrics.TemperatureLevel = updateResult.TemperatureLevel;
		ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * L_4 = __this->get_address_of_m_ThermalMetrics_12();
		float L_5 = PerformanceDataRecord_get_TemperatureLevel_m8ADD4135E271A910EB33ED6B113E6153A8B54CCE_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		ThermalMetrics_set_TemperatureLevel_m55E3869F1C0685354CEBC2FA58FBB009380487FD_inline((ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 *)L_4, L_5, /*hidden argument*/NULL);
		// if (!m_JustResumed)
		bool L_6 = __this->get_m_JustResumed_8();
		if (L_6)
		{
			goto IL_0110;
		}
	}
	{
		// if (!m_UseProviderOverallFrameTime)
		bool L_7 = __this->get_m_UseProviderOverallFrameTime_23();
		if (L_7)
		{
			goto IL_0054;
		}
	}
	{
		// AccumulateTimingValue(ref m_OverallFrameTimeAccu, Time.unscaledDeltaTime);
		float* L_8 = __this->get_address_of_m_OverallFrameTimeAccu_27();
		float L_9 = Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2(/*hidden argument*/NULL);
		AdaptivePerformanceManager_AccumulateTimingValue_mD19C087BBD955CBA731D320F37EFCE0013AE7D39(__this, (float*)L_8, L_9, /*hidden argument*/NULL);
	}

IL_0054:
	{
		// if (WillCurrentFrameRender())
		bool L_10 = AdaptivePerformanceManager_WillCurrentFrameRender_m9662ECAA12EB05BE964E89A1E29AC0AA37F82DDC(/*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00d9;
		}
	}
	{
		// AddNonNegativeValue(m_OverallFrameTime, m_UseProviderOverallFrameTime ? updateResult.OverallFrameTime : m_OverallFrameTimeAccu);
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_11 = __this->get_m_OverallFrameTime_26();
		bool L_12 = __this->get_m_UseProviderOverallFrameTime_23();
		G_B5_0 = L_11;
		G_B5_1 = __this;
		if (L_12)
		{
			G_B6_0 = L_11;
			G_B6_1 = __this;
			goto IL_0072;
		}
	}
	{
		float L_13 = __this->get_m_OverallFrameTimeAccu_27();
		G_B7_0 = L_13;
		G_B7_1 = G_B5_0;
		G_B7_2 = G_B5_1;
		goto IL_0079;
	}

IL_0072:
	{
		float L_14 = PerformanceDataRecord_get_OverallFrameTime_mD8BE680879118B05C2BFA66EC2F0BF8C69CC8C59_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		G_B7_0 = L_14;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
	}

IL_0079:
	{
		NullCheck(G_B7_2);
		AdaptivePerformanceManager_AddNonNegativeValue_m3D374415DF363E5DC0E1B44E04817EA2E2BB6262(G_B7_2, G_B7_1, G_B7_0, /*hidden argument*/NULL);
		// AddNonNegativeValue(m_GpuFrameTime, m_GpuFrameTimeProvider == null ? updateResult.GpuFrameTime : m_GpuFrameTimeProvider.GpuFrameTime);
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_15 = __this->get_m_GpuFrameTime_28();
		GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * L_16 = __this->get_m_GpuFrameTimeProvider_20();
		G_B8_0 = L_15;
		G_B8_1 = __this;
		if (!L_16)
		{
			G_B9_0 = L_15;
			G_B9_1 = __this;
			goto IL_009a;
		}
	}
	{
		GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * L_17 = __this->get_m_GpuFrameTimeProvider_20();
		NullCheck(L_17);
		float L_18 = GpuTimeProvider_get_GpuFrameTime_mA1F553C55D589588F6954C2E87D835640DD43C7B(L_17, /*hidden argument*/NULL);
		G_B10_0 = L_18;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_00a1;
	}

IL_009a:
	{
		float L_19 = PerformanceDataRecord_get_GpuFrameTime_m50329D6AADDB092BE7BA0E9EAD569D4885774BB1_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		G_B10_0 = L_19;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_00a1:
	{
		NullCheck(G_B10_2);
		AdaptivePerformanceManager_AddNonNegativeValue_m3D374415DF363E5DC0E1B44E04817EA2E2BB6262(G_B10_2, G_B10_1, G_B10_0, /*hidden argument*/NULL);
		// AddNonNegativeValue(m_CpuFrameTime, m_CpuFrameTimeProvider == null ? updateResult.CpuFrameTime : m_CpuFrameTimeProvider.CpuFrameTime);
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_20 = __this->get_m_CpuFrameTime_29();
		CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * L_21 = __this->get_m_CpuFrameTimeProvider_19();
		G_B11_0 = L_20;
		G_B11_1 = __this;
		if (!L_21)
		{
			G_B12_0 = L_20;
			G_B12_1 = __this;
			goto IL_00c2;
		}
	}
	{
		CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * L_22 = __this->get_m_CpuFrameTimeProvider_19();
		NullCheck(L_22);
		float L_23 = CpuTimeProvider_get_CpuFrameTime_mFDBF3DF1106A47DA135FF7B49389CC355108BA25(L_22, /*hidden argument*/NULL);
		G_B13_0 = L_23;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c9;
	}

IL_00c2:
	{
		float L_24 = PerformanceDataRecord_get_CpuFrameTime_m53BE7B6BA7C387D27C6B789F80487C19E48DD042_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		G_B13_0 = L_24;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c9:
	{
		NullCheck(G_B13_2);
		AdaptivePerformanceManager_AddNonNegativeValue_m3D374415DF363E5DC0E1B44E04817EA2E2BB6262(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		// m_OverallFrameTimeAccu = 0.0f;
		__this->set_m_OverallFrameTimeAccu_27((0.0f));
	}

IL_00d9:
	{
		// m_TemperatureTrend.Update(updateResult.TemperatureTrend, updateResult.TemperatureLevel, updateResult.ChangeFlags.HasFlag(Provider.Feature.TemperatureLevel), Time.time);
		TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * L_25 = __this->get_m_TemperatureTrend_22();
		float L_26 = PerformanceDataRecord_get_TemperatureTrend_m65CC0E6927BE74931532ACB8DE6BAA1CBBF12231_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		float L_27 = PerformanceDataRecord_get_TemperatureLevel_m8ADD4135E271A910EB33ED6B113E6153A8B54CCE_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		int32_t L_28 = PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		int32_t L_29 = L_28;
		RuntimeObject * L_30 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_29);
		int32_t L_31 = ((int32_t)2);
		RuntimeObject * L_32 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_31);
		NullCheck((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_30);
		bool L_33 = Enum_HasFlag_m5D934A541DEEF44DBF3415EE47F8CCED9370C173((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_30, (Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_32, /*hidden argument*/NULL);
		float L_34 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		NullCheck(L_25);
		TemperatureTrend_Update_m5BA796D968562EF7CEB9D5644CDC7EBC8FA9A1AE(L_25, L_26, L_27, L_33, L_34, /*hidden argument*/NULL);
		// }
		goto IL_0122;
	}

IL_0110:
	{
		// m_TemperatureTrend.Reset();
		TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * L_35 = __this->get_m_TemperatureTrend_22();
		NullCheck(L_35);
		TemperatureTrend_Reset_m71442095ADD319071142E1B14DA2EA27F3F8FEC4(L_35, /*hidden argument*/NULL);
		// m_JustResumed = false;
		__this->set_m_JustResumed_8((bool)0);
	}

IL_0122:
	{
		// m_ThermalMetrics.TemperatureTrend = m_TemperatureTrend.ThermalTrend;
		ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * L_36 = __this->get_address_of_m_ThermalMetrics_12();
		TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * L_37 = __this->get_m_TemperatureTrend_22();
		NullCheck(L_37);
		float L_38 = TemperatureTrend_get_ThermalTrend_m971750AEB690F8B4ED9FE84C5BA802B8C5EB4B5F_inline(L_37, /*hidden argument*/NULL);
		ThermalMetrics_set_TemperatureTrend_mF48505C1BB0EBE4F530B0405D60D4396A24B9472_inline((ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 *)L_36, L_38, /*hidden argument*/NULL);
		// m_FrameTiming.AverageFrameTime = m_OverallFrameTime.GetAverageOr(invalidTimingValue);
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * L_39 = __this->get_address_of_m_FrameTiming_14();
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_40 = __this->get_m_OverallFrameTime_26();
		NullCheck(L_40);
		float L_41 = RunningAverage_GetAverageOr_mEE0503DACB72C6CF46D78B893479E23638E92484(L_40, (-1.0f), /*hidden argument*/NULL);
		FrameTiming_set_AverageFrameTime_mBDC023C5DDCE8C8069CBEAF28DAC912744D648C5_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)L_39, L_41, /*hidden argument*/NULL);
		// m_FrameTiming.CurrentFrameTime = m_OverallFrameTime.GetMostRecentValueOr(invalidTimingValue);
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * L_42 = __this->get_address_of_m_FrameTiming_14();
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_43 = __this->get_m_OverallFrameTime_26();
		NullCheck(L_43);
		float L_44 = RunningAverage_GetMostRecentValueOr_mAC56F749F88BC98DAB20A5070A71B14FCE8E7566(L_43, (-1.0f), /*hidden argument*/NULL);
		FrameTiming_set_CurrentFrameTime_m2DD61370159EDB8AE9612CC6303B25175788A0E5_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)L_42, L_44, /*hidden argument*/NULL);
		// m_FrameTiming.AverageGpuFrameTime = m_GpuFrameTime.GetAverageOr(invalidTimingValue);
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * L_45 = __this->get_address_of_m_FrameTiming_14();
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_46 = __this->get_m_GpuFrameTime_28();
		NullCheck(L_46);
		float L_47 = RunningAverage_GetAverageOr_mEE0503DACB72C6CF46D78B893479E23638E92484(L_46, (-1.0f), /*hidden argument*/NULL);
		FrameTiming_set_AverageGpuFrameTime_mADCD67452E1D4C92E7562EDD78A5BC411B3BC29B_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)L_45, L_47, /*hidden argument*/NULL);
		// m_FrameTiming.CurrentGpuFrameTime = m_GpuFrameTime.GetMostRecentValueOr(invalidTimingValue);
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * L_48 = __this->get_address_of_m_FrameTiming_14();
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_49 = __this->get_m_GpuFrameTime_28();
		NullCheck(L_49);
		float L_50 = RunningAverage_GetMostRecentValueOr_mAC56F749F88BC98DAB20A5070A71B14FCE8E7566(L_49, (-1.0f), /*hidden argument*/NULL);
		FrameTiming_set_CurrentGpuFrameTime_mBAB674B381CA46E589A79890A9BE0A0FDC2EE781_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)L_48, L_50, /*hidden argument*/NULL);
		// m_FrameTiming.AverageCpuFrameTime = m_CpuFrameTime.GetAverageOr(invalidTimingValue);
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * L_51 = __this->get_address_of_m_FrameTiming_14();
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_52 = __this->get_m_CpuFrameTime_29();
		NullCheck(L_52);
		float L_53 = RunningAverage_GetAverageOr_mEE0503DACB72C6CF46D78B893479E23638E92484(L_52, (-1.0f), /*hidden argument*/NULL);
		FrameTiming_set_AverageCpuFrameTime_m87D8B833265478A0EA25153B3D5299AACAE71BDE_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)L_51, L_53, /*hidden argument*/NULL);
		// m_FrameTiming.CurrentCpuFrameTime = m_CpuFrameTime.GetMostRecentValueOr(invalidTimingValue);
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * L_54 = __this->get_address_of_m_FrameTiming_14();
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_55 = __this->get_m_CpuFrameTime_29();
		NullCheck(L_55);
		float L_56 = RunningAverage_GetMostRecentValueOr_mAC56F749F88BC98DAB20A5070A71B14FCE8E7566(L_55, (-1.0f), /*hidden argument*/NULL);
		FrameTiming_set_CurrentCpuFrameTime_m95A32BC4E4AE8885D1738D309F2C39E96E887810_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)L_54, L_56, /*hidden argument*/NULL);
		// float targerFrameRate = EffectiveTargetFrameRate();
		float L_57 = AdaptivePerformanceManager_EffectiveTargetFrameRate_m05458EB876A5E7810268A2DF19E175F2A3D7B9E4(/*hidden argument*/NULL);
		V_1 = L_57;
		// float targetFrameTime = -1.0f;
		V_2 = (-1.0f);
		// if (targerFrameRate > 0)
		float L_58 = V_1;
		if ((!(((float)L_58) > ((float)(0.0f)))))
		{
			goto IL_01f6;
		}
	}
	{
		// targetFrameTime = 1.0f / targerFrameRate;
		float L_59 = V_1;
		V_2 = ((float)((float)(1.0f)/(float)L_59));
	}

IL_01f6:
	{
		// bool triggerPerformanceBottleneckChangeEvent = false;
		V_3 = (bool)0;
		// var performanceBottleneckChangeEventArgs = new PerformanceBottleneckChangeEventArgs();
		il2cpp_codegen_initobj((&V_4), sizeof(PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 ));
		// if (m_OverallFrameTime.GetNumValues() == m_OverallFrameTime.GetSampleWindowSize() &&
		//     m_GpuFrameTime.GetNumValues() == m_GpuFrameTime.GetSampleWindowSize() &&
		//     m_CpuFrameTime.GetNumValues() == m_CpuFrameTime.GetSampleWindowSize())
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_60 = __this->get_m_OverallFrameTime_26();
		NullCheck(L_60);
		int32_t L_61 = RunningAverage_GetNumValues_mCAA37709BFE682697178E705A920598DBCD9D395_inline(L_60, /*hidden argument*/NULL);
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_62 = __this->get_m_OverallFrameTime_26();
		NullCheck(L_62);
		int32_t L_63 = RunningAverage_GetSampleWindowSize_mCFD8C3F0168893D1C550FB0C88B62A03EDD35411(L_62, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_61) == ((uint32_t)L_63))))
		{
			goto IL_02ae;
		}
	}
	{
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_64 = __this->get_m_GpuFrameTime_28();
		NullCheck(L_64);
		int32_t L_65 = RunningAverage_GetNumValues_mCAA37709BFE682697178E705A920598DBCD9D395_inline(L_64, /*hidden argument*/NULL);
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_66 = __this->get_m_GpuFrameTime_28();
		NullCheck(L_66);
		int32_t L_67 = RunningAverage_GetSampleWindowSize_mCFD8C3F0168893D1C550FB0C88B62A03EDD35411(L_66, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_65) == ((uint32_t)L_67))))
		{
			goto IL_02ae;
		}
	}
	{
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_68 = __this->get_m_CpuFrameTime_29();
		NullCheck(L_68);
		int32_t L_69 = RunningAverage_GetNumValues_mCAA37709BFE682697178E705A920598DBCD9D395_inline(L_68, /*hidden argument*/NULL);
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_70 = __this->get_m_CpuFrameTime_29();
		NullCheck(L_70);
		int32_t L_71 = RunningAverage_GetSampleWindowSize_mCFD8C3F0168893D1C550FB0C88B62A03EDD35411(L_70, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_69) == ((uint32_t)L_71))))
		{
			goto IL_02ae;
		}
	}
	{
		// PerformanceBottleneck bottleneck = BottleneckUtil.DetermineBottleneck(m_PerformanceMetrics.PerformanceBottleneck, m_FrameTiming.AverageCpuFrameTime,
		//     m_FrameTiming.AverageGpuFrameTime, m_FrameTiming.AverageFrameTime, targetFrameTime);
		PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * L_72 = __this->get_address_of_m_PerformanceMetrics_13();
		int32_t L_73 = PerformanceMetrics_get_PerformanceBottleneck_m518B78FAA9F20BD9F79F3EAFCE7CC81612F55CC6_inline((PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *)L_72, /*hidden argument*/NULL);
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * L_74 = __this->get_address_of_m_FrameTiming_14();
		float L_75 = FrameTiming_get_AverageCpuFrameTime_m1BAD83D813770DB099F29C2ACAB31D4C08A79ED0_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)L_74, /*hidden argument*/NULL);
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * L_76 = __this->get_address_of_m_FrameTiming_14();
		float L_77 = FrameTiming_get_AverageGpuFrameTime_m2AA4C5FB0D07DA307EF04B40B60B211B21700386_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)L_76, /*hidden argument*/NULL);
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * L_78 = __this->get_address_of_m_FrameTiming_14();
		float L_79 = FrameTiming_get_AverageFrameTime_m6C518BA12C5C330F471B31E364A37362AC439618_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)L_78, /*hidden argument*/NULL);
		float L_80 = V_2;
		int32_t L_81 = BottleneckUtil_DetermineBottleneck_m4645F830559B7B5F54151AB2FF354C6419F8E3FD(L_73, L_75, L_77, L_79, L_80, /*hidden argument*/NULL);
		V_6 = L_81;
		// if (bottleneck != m_PerformanceMetrics.PerformanceBottleneck)
		int32_t L_82 = V_6;
		PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * L_83 = __this->get_address_of_m_PerformanceMetrics_13();
		int32_t L_84 = PerformanceMetrics_get_PerformanceBottleneck_m518B78FAA9F20BD9F79F3EAFCE7CC81612F55CC6_inline((PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *)L_83, /*hidden argument*/NULL);
		if ((((int32_t)L_82) == ((int32_t)L_84)))
		{
			goto IL_02ae;
		}
	}
	{
		// m_PerformanceMetrics.PerformanceBottleneck = bottleneck;
		PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * L_85 = __this->get_address_of_m_PerformanceMetrics_13();
		int32_t L_86 = V_6;
		PerformanceMetrics_set_PerformanceBottleneck_mFFE12B2A0C07877C363A05EF1F48B4C9BA27D268_inline((PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *)L_85, L_86, /*hidden argument*/NULL);
		// performanceBottleneckChangeEventArgs.PerformanceBottleneck = bottleneck;
		int32_t L_87 = V_6;
		PerformanceBottleneckChangeEventArgs_set_PerformanceBottleneck_m63560EBBBEA19F0D26251C884A8CEE5FD3C07B03_inline((PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 *)(&V_4), L_87, /*hidden argument*/NULL);
		// triggerPerformanceBottleneckChangeEvent = (PerformanceBottleneckChangeEvent != null);
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_88 = __this->get_PerformanceBottleneckChangeEvent_5();
		V_3 = (bool)((!(((RuntimeObject*)(PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 *)L_88) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}

IL_02ae:
	{
		// triggerThermalEventEvent = (ThermalEvent != null) &&
		//     (updateResult.ChangeFlags.HasFlag(Provider.Feature.WarningLevel) ||
		//      updateResult.ChangeFlags.HasFlag(Provider.Feature.TemperatureLevel) ||
		//      updateResult.ChangeFlags.HasFlag(Provider.Feature.TemperatureTrend));
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_89 = __this->get_ThermalEvent_4();
		if (!L_89)
		{
			goto IL_0304;
		}
	}
	{
		int32_t L_90 = PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		int32_t L_91 = L_90;
		RuntimeObject * L_92 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_91);
		int32_t L_93 = ((int32_t)1);
		RuntimeObject * L_94 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_93);
		NullCheck((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_92);
		bool L_95 = Enum_HasFlag_m5D934A541DEEF44DBF3415EE47F8CCED9370C173((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_92, (Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_94, /*hidden argument*/NULL);
		if (L_95)
		{
			goto IL_0301;
		}
	}
	{
		int32_t L_96 = PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		int32_t L_97 = L_96;
		RuntimeObject * L_98 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_97);
		int32_t L_99 = ((int32_t)2);
		RuntimeObject * L_100 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_99);
		NullCheck((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_98);
		bool L_101 = Enum_HasFlag_m5D934A541DEEF44DBF3415EE47F8CCED9370C173((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_98, (Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_100, /*hidden argument*/NULL);
		if (L_101)
		{
			goto IL_0301;
		}
	}
	{
		int32_t L_102 = PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		int32_t L_103 = L_102;
		RuntimeObject * L_104 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_103);
		int32_t L_105 = ((int32_t)4);
		RuntimeObject * L_106 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_105);
		NullCheck((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_104);
		bool L_107 = Enum_HasFlag_m5D934A541DEEF44DBF3415EE47F8CCED9370C173((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_104, (Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_106, /*hidden argument*/NULL);
		G_B29_0 = ((int32_t)(L_107));
		goto IL_0305;
	}

IL_0301:
	{
		G_B29_0 = 1;
		goto IL_0305;
	}

IL_0304:
	{
		G_B29_0 = 0;
	}

IL_0305:
	{
		// if (updateResult.ChangeFlags.HasFlag(Provider.Feature.CpuPerformanceLevel))
		int32_t L_108 = PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		int32_t L_109 = L_108;
		RuntimeObject * L_110 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_109);
		int32_t L_111 = ((int32_t)8);
		RuntimeObject * L_112 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_111);
		NullCheck((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_110);
		bool L_113 = Enum_HasFlag_m5D934A541DEEF44DBF3415EE47F8CCED9370C173((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_110, (Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_112, /*hidden argument*/NULL);
		G_B30_0 = G_B29_0;
		if (!L_113)
		{
			G_B31_0 = G_B29_0;
			goto IL_0330;
		}
	}
	{
		// m_DevicePerfControl.CurrentCpuLevel = updateResult.CpuPerformanceLevel;
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_114 = __this->get_m_DevicePerfControl_17();
		int32_t L_115 = PerformanceDataRecord_get_CpuPerformanceLevel_mAADDE6EE97531363BB97CBD7697B9B6D610DEE41_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_114);
		DevicePerformanceControlImpl_set_CurrentCpuLevel_mA17D47BD068FBA9741C61F26D8869577FDC3B04E_inline(L_114, L_115, /*hidden argument*/NULL);
		G_B31_0 = G_B30_0;
	}

IL_0330:
	{
		// if (updateResult.ChangeFlags.HasFlag(Provider.Feature.GpuPerformanceLevel))
		int32_t L_116 = PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		int32_t L_117 = L_116;
		RuntimeObject * L_118 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_117);
		int32_t L_119 = ((int32_t)((int32_t)16));
		RuntimeObject * L_120 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_119);
		NullCheck((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_118);
		bool L_121 = Enum_HasFlag_m5D934A541DEEF44DBF3415EE47F8CCED9370C173((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_118, (Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_120, /*hidden argument*/NULL);
		G_B32_0 = G_B31_0;
		if (!L_121)
		{
			G_B33_0 = G_B31_0;
			goto IL_035c;
		}
	}
	{
		// m_DevicePerfControl.CurrentGpuLevel = updateResult.GpuPerformanceLevel;
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_122 = __this->get_m_DevicePerfControl_17();
		int32_t L_123 = PerformanceDataRecord_get_GpuPerformanceLevel_mB1DD57E4C0ECDC9F74181D5E5CFC8265B52ED4B4_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_122);
		DevicePerformanceControlImpl_set_CurrentGpuLevel_m58F4826025D590BCC9D4BA9CB9DC76452DD369B2_inline(L_122, L_123, /*hidden argument*/NULL);
		G_B33_0 = G_B32_0;
	}

IL_035c:
	{
		// if (updateResult.ChangeFlags.HasFlag(Provider.Feature.PerformanceLevelControl))
		int32_t L_124 = PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		int32_t L_125 = L_124;
		RuntimeObject * L_126 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_125);
		int32_t L_127 = ((int32_t)((int32_t)32));
		RuntimeObject * L_128 = Box(Feature_t58CCDD1ECB63293FE7E26A3BFE961DD5713589F5_il2cpp_TypeInfo_var, &L_127);
		NullCheck((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_126);
		bool L_129 = Enum_HasFlag_m5D934A541DEEF44DBF3415EE47F8CCED9370C173((Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_126, (Enum_t2AF27C02B8653AE29442467390005ABC74D8F521 *)L_128, /*hidden argument*/NULL);
		G_B34_0 = G_B33_0;
		if (!L_129)
		{
			G_B39_0 = G_B33_0;
			goto IL_03af;
		}
	}
	{
		// if (updateResult.PerformanceLevelControlAvailable)
		bool L_130 = PerformanceDataRecord_get_PerformanceLevelControlAvailable_mC2FDF55F921B13DC69CCC033C952A1D293FE4572_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)(&V_0), /*hidden argument*/NULL);
		G_B35_0 = G_B34_0;
		if (!L_130)
		{
			G_B38_0 = G_B34_0;
			goto IL_03a3;
		}
	}
	{
		// if (AutomaticPerformanceControl)
		bool L_131 = AdaptivePerformanceManager_get_AutomaticPerformanceControl_m7E8E0B158A408D6E61DFEB69C69998792A97A98B_inline(__this, /*hidden argument*/NULL);
		G_B36_0 = G_B35_0;
		if (!L_131)
		{
			G_B37_0 = G_B35_0;
			goto IL_0395;
		}
	}
	{
		// m_DevicePerfControl.PerformanceControlMode = PerformanceControlMode.Automatic;
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_132 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_132);
		DevicePerformanceControlImpl_set_PerformanceControlMode_m90A342E8781C151B8F940601A615EBE727ED68BD_inline(L_132, 0, /*hidden argument*/NULL);
		G_B39_0 = G_B36_0;
		goto IL_03af;
	}

IL_0395:
	{
		// m_DevicePerfControl.PerformanceControlMode = PerformanceControlMode.Manual;
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_133 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_133);
		DevicePerformanceControlImpl_set_PerformanceControlMode_m90A342E8781C151B8F940601A615EBE727ED68BD_inline(L_133, 1, /*hidden argument*/NULL);
		// }
		G_B39_0 = G_B37_0;
		goto IL_03af;
	}

IL_03a3:
	{
		// m_DevicePerfControl.PerformanceControlMode = PerformanceControlMode.System;
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_134 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_134);
		DevicePerformanceControlImpl_set_PerformanceControlMode_m90A342E8781C151B8F940601A615EBE727ED68BD_inline(L_134, 2, /*hidden argument*/NULL);
		G_B39_0 = G_B38_0;
	}

IL_03af:
	{
		// m_AutoPerformanceLevelController.TargetFrameTime = targetFrameTime;
		AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * L_135 = __this->get_m_AutoPerformanceLevelController_18();
		float L_136 = V_2;
		NullCheck(L_135);
		AutoPerformanceLevelController_set_TargetFrameTime_m86953888D9B6699A5BA926B6C0260665B860E7B2_inline(L_135, L_136, /*hidden argument*/NULL);
		// m_AutoPerformanceLevelController.Enabled = (m_DevicePerfControl.PerformanceControlMode == PerformanceControlMode.Automatic);
		AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * L_137 = __this->get_m_AutoPerformanceLevelController_18();
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_138 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_138);
		int32_t L_139 = DevicePerformanceControlImpl_get_PerformanceControlMode_m2E4EE8BB379495FD00D67FCB6675358333F1532D_inline(L_138, /*hidden argument*/NULL);
		NullCheck(L_137);
		AutoPerformanceLevelController_set_Enabled_mD0D8E5D8A2D467B0DC314AC0C511F4327E2B18E8(L_137, (bool)((((int32_t)L_139) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// PerformanceLevelChangeEventArgs levelChangeEventArgs = new PerformanceLevelChangeEventArgs();
		il2cpp_codegen_initobj((&V_5), sizeof(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB ));
		// if (m_DevicePerfControl.PerformanceControlMode != PerformanceControlMode.System)
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_140 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_140);
		int32_t L_141 = DevicePerformanceControlImpl_get_PerformanceControlMode_m2E4EE8BB379495FD00D67FCB6675358333F1532D_inline(L_140, /*hidden argument*/NULL);
		G_B40_0 = G_B39_0;
		if ((((int32_t)L_141) == ((int32_t)2)))
		{
			G_B45_0 = G_B39_0;
			goto IL_044d;
		}
	}
	{
		// if (m_AutoPerformanceLevelController.Enabled)
		AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * L_142 = __this->get_m_AutoPerformanceLevelController_18();
		NullCheck(L_142);
		bool L_143 = AutoPerformanceLevelController_get_Enabled_mBA9F1DC76E6FC28B0DB7F4A7319E06EA8CFAA228_inline(L_142, /*hidden argument*/NULL);
		G_B41_0 = G_B40_0;
		if (!L_143)
		{
			G_B44_0 = G_B40_0;
			goto IL_042b;
		}
	}
	{
		// if (m_NewUserPerformanceLevelRequest)
		bool L_144 = __this->get_m_NewUserPerformanceLevelRequest_11();
		G_B42_0 = G_B41_0;
		if (!L_144)
		{
			G_B43_0 = G_B41_0;
			goto IL_041e;
		}
	}
	{
		// m_AutoPerformanceLevelController.Override(m_RequestedCpuLevel, m_RequestedGpuLevel);
		AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * L_145 = __this->get_m_AutoPerformanceLevelController_18();
		int32_t L_146 = __this->get_m_RequestedCpuLevel_9();
		int32_t L_147 = __this->get_m_RequestedGpuLevel_10();
		NullCheck(L_145);
		AutoPerformanceLevelController_Override_mE3A9350573679DEBE02F35C2F7B80694973A157E(L_145, L_146, L_147, /*hidden argument*/NULL);
		// levelChangeEventArgs.ManualOverride = true;
		PerformanceLevelChangeEventArgs_set_ManualOverride_mF3CD9BE22E685E48E56D4974302749BA02F2A1FD_inline((PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)(&V_5), (bool)1, /*hidden argument*/NULL);
		G_B43_0 = G_B42_0;
	}

IL_041e:
	{
		// m_AutoPerformanceLevelController.Update();
		AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * L_148 = __this->get_m_AutoPerformanceLevelController_18();
		NullCheck(L_148);
		AutoPerformanceLevelController_Update_m7C0E073667FD597C31FE6A817FCC47930D2B6973(L_148, /*hidden argument*/NULL);
		// }
		G_B45_0 = G_B43_0;
		goto IL_044d;
	}

IL_042b:
	{
		// m_DevicePerfControl.CpuLevel = m_RequestedCpuLevel;
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_149 = __this->get_m_DevicePerfControl_17();
		int32_t L_150 = __this->get_m_RequestedCpuLevel_9();
		NullCheck(L_149);
		DevicePerformanceControlImpl_set_CpuLevel_mE7771043E97430A856CDFAA1162D257801BDD1F4_inline(L_149, L_150, /*hidden argument*/NULL);
		// m_DevicePerfControl.GpuLevel = m_RequestedGpuLevel;
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_151 = __this->get_m_DevicePerfControl_17();
		int32_t L_152 = __this->get_m_RequestedGpuLevel_10();
		NullCheck(L_151);
		DevicePerformanceControlImpl_set_GpuLevel_m09C65245F940E90A38B7C18326038C843F1CE005_inline(L_151, L_152, /*hidden argument*/NULL);
		G_B45_0 = G_B44_0;
	}

IL_044d:
	{
		// if (m_DevicePerfControl.Update(out levelChangeEventArgs) && PerformanceLevelChangeEvent != null)
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_153 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_153);
		bool L_154 = DevicePerformanceControlImpl_Update_m62E8744125498EDACB2150F2DEF0DF6D6AA58635(L_153, (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)(&V_5), /*hidden argument*/NULL);
		G_B46_0 = G_B45_0;
		if (!L_154)
		{
			G_B48_0 = G_B45_0;
			goto IL_0471;
		}
	}
	{
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_155 = __this->get_PerformanceLevelChangeEvent_6();
		G_B47_0 = G_B46_0;
		if (!L_155)
		{
			G_B48_0 = G_B46_0;
			goto IL_0471;
		}
	}
	{
		// PerformanceLevelChangeEvent.Invoke(levelChangeEventArgs);
		PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * L_156 = __this->get_PerformanceLevelChangeEvent_6();
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  L_157 = V_5;
		NullCheck(L_156);
		PerformanceLevelChangeHandler_Invoke_mC0AAD0689A73E5A05C9AA0CFFB6D079D9B3A491E(L_156, L_157, /*hidden argument*/NULL);
		G_B48_0 = G_B47_0;
	}

IL_0471:
	{
		// m_PerformanceMetrics.CurrentCpuLevel = m_DevicePerfControl.CurrentCpuLevel;
		PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * L_158 = __this->get_address_of_m_PerformanceMetrics_13();
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_159 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_159);
		int32_t L_160 = DevicePerformanceControlImpl_get_CurrentCpuLevel_m5B3842B5593E0ECD6FD2DC40C4A2B33495411C13_inline(L_159, /*hidden argument*/NULL);
		PerformanceMetrics_set_CurrentCpuLevel_mB0FFC61EEA0C4462338C8E728533FE48D4364D9A_inline((PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *)L_158, L_160, /*hidden argument*/NULL);
		// m_PerformanceMetrics.CurrentGpuLevel = m_DevicePerfControl.CurrentGpuLevel;
		PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * L_161 = __this->get_address_of_m_PerformanceMetrics_13();
		DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * L_162 = __this->get_m_DevicePerfControl_17();
		NullCheck(L_162);
		int32_t L_163 = DevicePerformanceControlImpl_get_CurrentGpuLevel_m8BDBF664287A7FD279C7A264AAA9818102B3B6F4_inline(L_162, /*hidden argument*/NULL);
		PerformanceMetrics_set_CurrentGpuLevel_m2807D7F1C39CF7C72DCA9CFDC0BB6F90AA2CE326_inline((PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *)L_161, L_163, /*hidden argument*/NULL);
		// m_NewUserPerformanceLevelRequest = false;
		__this->set_m_NewUserPerformanceLevelRequest_11((bool)0);
		// if (triggerThermalEventEvent)
		if (!G_B48_0)
		{
			goto IL_04b7;
		}
	}
	{
		// ThermalEvent.Invoke(m_ThermalMetrics);
		ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * L_164 = __this->get_ThermalEvent_4();
		ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  L_165 = __this->get_m_ThermalMetrics_12();
		NullCheck(L_164);
		ThermalEventHandler_Invoke_m48931BBF9EBE5CFAA39300884D1550F27FBB1370(L_164, L_165, /*hidden argument*/NULL);
	}

IL_04b7:
	{
		// if (triggerPerformanceBottleneckChangeEvent)
		bool L_166 = V_3;
		if (!L_166)
		{
			goto IL_04c7;
		}
	}
	{
		// PerformanceBottleneckChangeEvent(performanceBottleneckChangeEventArgs);
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_167 = __this->get_PerformanceBottleneckChangeEvent_5();
		PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  L_168 = V_4;
		NullCheck(L_167);
		PerformanceBottleneckChangeHandler_Invoke_m503AB2767CF3A258FDE85919B7BBC4A91EC1C755(L_167, L_168, /*hidden argument*/NULL);
	}

IL_04c7:
	{
		// }
		return;
	}
}
// System.Single UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::AndroidDefaultFrameRate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AdaptivePerformanceManager_AndroidDefaultFrameRate_m0856A7E8405FDB79E8A6F71F3FDD97AC7249DEC3 (const RuntimeMethod* method)
{
	{
		// return 30.0f;
		return (30.0f);
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::RenderFrameInterval()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AdaptivePerformanceManager_RenderFrameInterval_mD1230D5B327F15435CC27D0C0D5BA62FB53811F9 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_RenderFrameInterval_mD1230D5B327F15435CC27D0C0D5BA62FB53811F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return UnityEngine.Rendering.OnDemandRendering.renderFrameInterval;
		IL2CPP_RUNTIME_CLASS_INIT(OnDemandRendering_t89F49EDBAFA9B757B97F0277A419BBC394B2FA19_il2cpp_TypeInfo_var);
		int32_t L_0 = OnDemandRendering_get_renderFrameInterval_m5A12FB459D93296FBACCD6FD59EA27CD092A0132(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::WillCurrentFrameRender()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AdaptivePerformanceManager_WillCurrentFrameRender_m9662ECAA12EB05BE964E89A1E29AC0AA37F82DDC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_WillCurrentFrameRender_m9662ECAA12EB05BE964E89A1E29AC0AA37F82DDC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return UnityEngine.Rendering.OnDemandRendering.willCurrentFrameRender;
		IL2CPP_RUNTIME_CLASS_INIT(OnDemandRendering_t89F49EDBAFA9B757B97F0277A419BBC394B2FA19_il2cpp_TypeInfo_var);
		bool L_0 = OnDemandRendering_get_willCurrentFrameRender_mBD1AF1E7F139D163B7D065EEDD35925BFD8DE42C(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::EffectiveTargetFrameRate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AdaptivePerformanceManager_EffectiveTargetFrameRate_m05458EB876A5E7810268A2DF19E175F2A3D7B9E4 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_EffectiveTargetFrameRate_m05458EB876A5E7810268A2DF19E175F2A3D7B9E4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return UnityEngine.Rendering.OnDemandRendering.effectiveRenderFrameRate;
		IL2CPP_RUNTIME_CLASS_INIT(OnDemandRendering_t89F49EDBAFA9B757B97F0277A419BBC394B2FA19_il2cpp_TypeInfo_var);
		int32_t L_0 = OnDemandRendering_get_effectiveRenderFrameRate_m84F9BEAB90EDBC64BA2E2885B692841FCF4A7F7C(/*hidden argument*/NULL);
		return (((float)((float)L_0)));
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_OnDestroy_mFE5954F66E179428686D17742F262C72325C8EC0 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// if (m_Subsystem != null)
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_0 = __this->get_m_Subsystem_7();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// m_Subsystem.Destroy();
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_1 = __this->get_m_Subsystem_7();
		NullCheck(L_1);
		Subsystem_Destroy_m1D65C2E3B540A9EC80E14BF0C7A2BE8CDCF887A4(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::OnApplicationPause(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_OnApplicationPause_m1766D5E255DC5DF85116AEF56803228F182DD3F9 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, bool ___pause0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManager_OnApplicationPause_m1766D5E255DC5DF85116AEF56803228F182DD3F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_Subsystem != null)
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_0 = __this->get_m_Subsystem_7();
		if (!L_0)
		{
			goto IL_0079;
		}
	}
	{
		// if (pause)
		bool L_1 = ___pause0;
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		// if (m_AppLifecycle != null)
		RuntimeObject* L_2 = __this->get_m_AppLifecycle_21();
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		// m_AppLifecycle.ApplicationPause();
		RuntimeObject* L_3 = __this->get_m_AppLifecycle_21();
		NullCheck(L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.AdaptivePerformance.Provider.IApplicationLifecycle::ApplicationPause() */, IApplicationLifecycle_t62CD41874D9B90D9161909F588957018E97173A9_il2cpp_TypeInfo_var, L_3);
	}

IL_001e:
	{
		// m_OverallFrameTime.Reset();
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_4 = __this->get_m_OverallFrameTime_26();
		NullCheck(L_4);
		RunningAverage_Reset_m3AEE75202E5AC2F0059A7FEBBEA32879886DDCC9(L_4, /*hidden argument*/NULL);
		// m_GpuFrameTime.Reset();
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_5 = __this->get_m_GpuFrameTime_28();
		NullCheck(L_5);
		RunningAverage_Reset_m3AEE75202E5AC2F0059A7FEBBEA32879886DDCC9(L_5, /*hidden argument*/NULL);
		// m_CpuFrameTime.Reset();
		RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * L_6 = __this->get_m_CpuFrameTime_29();
		NullCheck(L_6);
		RunningAverage_Reset_m3AEE75202E5AC2F0059A7FEBBEA32879886DDCC9(L_6, /*hidden argument*/NULL);
		// if (m_CpuFrameTimeProvider != null)
		CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * L_7 = __this->get_m_CpuFrameTimeProvider_19();
		if (!L_7)
		{
			goto IL_0079;
		}
	}
	{
		// m_CpuFrameTimeProvider.Reset();
		CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * L_8 = __this->get_m_CpuFrameTimeProvider_19();
		NullCheck(L_8);
		CpuTimeProvider_Reset_m34832EDC0C3FA6E5931758EA733D318847AA3460(L_8, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0053:
	{
		// m_ThermalMetrics.WarningLevel = WarningLevel.NoWarning;
		ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * L_9 = __this->get_address_of_m_ThermalMetrics_12();
		ThermalMetrics_set_WarningLevel_m2B6C74F0609CAF7AA9FFE97018DB76D03D85CF89_inline((ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 *)L_9, 0, /*hidden argument*/NULL);
		// if (m_AppLifecycle != null)
		RuntimeObject* L_10 = __this->get_m_AppLifecycle_21();
		if (!L_10)
		{
			goto IL_0072;
		}
	}
	{
		// m_AppLifecycle.ApplicationResume();
		RuntimeObject* L_11 = __this->get_m_AppLifecycle_21();
		NullCheck(L_11);
		InterfaceActionInvoker0::Invoke(1 /* System.Void UnityEngine.AdaptivePerformance.Provider.IApplicationLifecycle::ApplicationResume() */, IApplicationLifecycle_t62CD41874D9B90D9161909F588957018E97173A9_il2cpp_TypeInfo_var, L_11);
	}

IL_0072:
	{
		// m_JustResumed = true;
		__this->set_m_JustResumed_8((bool)1);
	}

IL_0079:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::<Start>b__65_0(UnityEngine.AdaptivePerformance.ThermalMetrics)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_U3CStartU3Eb__65_0_m579D006E30704474CA7241DB6E90890689C52CA5 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  ___thermalMetrics0, const RuntimeMethod* method)
{
	{
		// ThermalEvent += (ThermalMetrics thermalMetrics) => LogThermalEvent(thermalMetrics);
		ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  L_0 = ___thermalMetrics0;
		AdaptivePerformanceManager_LogThermalEvent_m6B30D44A5F2437BB7C94D3FE8AA9A2D29FA3475F(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::<Start>b__65_1(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_U3CStartU3Eb__65_1_mA40F9B614EF4AD67A403592F6F063D268B6BA459 (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  ___ev0, const RuntimeMethod* method)
{
	{
		// PerformanceBottleneckChangeEvent += (PerformanceBottleneckChangeEventArgs ev) => LogBottleneckEvent(ev);
		PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  L_0 = ___ev0;
		AdaptivePerformanceManager_LogBottleneckEvent_m96B69097EF1D84FB750185F8ED6B6B68BDF458B2(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager::<Start>b__65_2(UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_U3CStartU3Eb__65_2_mA06C26D79D6B5C59EE680771CFD0AC843B8E40DD (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  ___ev0, const RuntimeMethod* method)
{
	{
		// PerformanceLevelChangeEvent += (PerformanceLevelChangeEventArgs ev) => LogPerformanceLevelEvent(ev);
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  L_0 = ___ev0;
		AdaptivePerformanceManager_LogPerformanceLevelEvent_m1E1E2ECE1BB2A0271DB6821ADD166AACE03C8E61(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager_<InvokeEndOfFrame>d__72::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInvokeEndOfFrameU3Ed__72__ctor_m42BA6D9B33294183365E97FCA422CE46CFA7BF88 (U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager_<InvokeEndOfFrame>d__72::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInvokeEndOfFrameU3Ed__72_System_IDisposable_Dispose_mD310A775D3DB95F2B7A232FDD77514E862FBFE97 (U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.AdaptivePerformanceManager_<InvokeEndOfFrame>d__72::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CInvokeEndOfFrameU3Ed__72_MoveNext_m1CF0AA53C6244BEB216F093B25AF309D51ABB2BD (U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0033;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_001e:
	{
		// yield return m_WaitForEndOfFrame;
		AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * L_4 = V_1;
		NullCheck(L_4);
		WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * L_5 = L_4->get_m_WaitForEndOfFrame_24();
		__this->set_U3CU3E2__current_1(L_5);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0033:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (m_CpuFrameTimeProvider != null)
		AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * L_6 = V_1;
		NullCheck(L_6);
		CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * L_7 = L_6->get_m_CpuFrameTimeProvider_19();
		if (!L_7)
		{
			goto IL_001e;
		}
	}
	{
		// m_CpuFrameTimeProvider.EndOfFrame();
		AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * L_8 = V_1;
		NullCheck(L_8);
		CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * L_9 = L_8->get_m_CpuFrameTimeProvider_19();
		NullCheck(L_9);
		CpuTimeProvider_EndOfFrame_mA508682DFC8C46B87167317E3A947D645BA8EA53(L_9, /*hidden argument*/NULL);
		// while (true)
		goto IL_001e;
	}
}
// System.Object UnityEngine.AdaptivePerformance.AdaptivePerformanceManager_<InvokeEndOfFrame>d__72::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInvokeEndOfFrameU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m214F65655B84651D74D1CF089451A894EBD7594D (U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManager_<InvokeEndOfFrame>d__72::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInvokeEndOfFrameU3Ed__72_System_Collections_IEnumerator_Reset_m39F4D3E658549D03A3DA0CF1EF253B1F9AEBA8F1 (U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CInvokeEndOfFrameU3Ed__72_System_Collections_IEnumerator_Reset_m39F4D3E658549D03A3DA0CF1EF253B1F9AEBA8F1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, U3CInvokeEndOfFrameU3Ed__72_System_Collections_IEnumerator_Reset_m39F4D3E658549D03A3DA0CF1EF253B1F9AEBA8F1_RuntimeMethod_var);
	}
}
// System.Object UnityEngine.AdaptivePerformance.AdaptivePerformanceManager_<InvokeEndOfFrame>d__72::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInvokeEndOfFrameU3Ed__72_System_Collections_IEnumerator_get_Current_m4CF09DBC0989D2D2879E3D3B131CDA3D1D53B44B (U3CInvokeEndOfFrameU3Ed__72_t1895D3985A68F3481D268D00D6A90B28173A02DB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManagerSpawner::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManagerSpawner_OnEnable_m7F213133592CA282EBE4B5583653878E168B9302 (AdaptivePerformanceManagerSpawner_tB0C73AFB099B61AC0890B6FB092EE08A7C8794A4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceManagerSpawner_OnEnable_m7F213133592CA282EBE4B5583653878E168B9302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_ManagerGameObject == null)
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_m_ManagerGameObject_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		// m_ManagerGameObject = new GameObject("AdaptivePerformanceManager");
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_2, _stringLiteral5C3DEF53E256C1E5614CF65282B9EEE41FCA53B3, /*hidden argument*/NULL);
		__this->set_m_ManagerGameObject_4(L_2);
		// var ap = m_ManagerGameObject.AddComponent<AdaptivePerformanceManager>();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = __this->get_m_ManagerGameObject_4();
		NullCheck(L_3);
		AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * L_4 = GameObject_AddComponent_TisAdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA_m7C0A87C297E85B0C6CD8653EAEFE085697819809(L_3, /*hidden argument*/GameObject_AddComponent_TisAdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA_m7C0A87C297E85B0C6CD8653EAEFE085697819809_RuntimeMethod_var);
		// Holder.Instance = ap;
		Holder_set_Instance_m2786C3B74384A62BD2997F76802455BEAD572CAE_inline(L_4, /*hidden argument*/NULL);
		// DontDestroyOnLoad(m_ManagerGameObject);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = __this->get_m_ManagerGameObject_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207(L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AdaptivePerformanceManagerSpawner::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceManagerSpawner__ctor_m0A1DA9DEE0BC973D9B76A4CE2E1DA33BA907D2B8 (AdaptivePerformanceManagerSpawner_tB0C73AFB099B61AC0890B6FB092EE08A7C8794A4 * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_TargetFrameTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_TargetFrameTime_m3EF525DF9A1AAFFA8E7A3839A634D6A3413398C3 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float TargetFrameTime { get; set; }
		float L_0 = __this->get_U3CTargetFrameTimeU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_TargetFrameTime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_TargetFrameTime_m86953888D9B6699A5BA926B6C0260665B860E7B2 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float TargetFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CTargetFrameTimeU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_AllowedCpuActiveTimeRatio()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_AllowedCpuActiveTimeRatio_mF966E1BBFDB821FEA660547F0CFFE0338F493782 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float AllowedCpuActiveTimeRatio { get; set; }
		float L_0 = __this->get_U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_AllowedCpuActiveTimeRatio(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_AllowedCpuActiveTimeRatio_m1526444FAD9758DC00AA9DB07425E26A779750C5 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float AllowedCpuActiveTimeRatio { get; set; }
		float L_0 = ___value0;
		__this->set_U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_AllowedGpuActiveTimeRatio()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_AllowedGpuActiveTimeRatio_mED7543A79BF8EDB3F876FCF5E815E58C39EE98A4 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float AllowedGpuActiveTimeRatio { get; set; }
		float L_0 = __this->get_U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_AllowedGpuActiveTimeRatio(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_AllowedGpuActiveTimeRatio_mD7660071F2B7DEB8381B364116922BE312A4AA0D (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float AllowedGpuActiveTimeRatio { get; set; }
		float L_0 = ___value0;
		__this->set_U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_GpuLevelBounceAvoidanceThreshold()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_GpuLevelBounceAvoidanceThreshold_mB46CCEE95E662384E848FA16446C783A622BBB41 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float GpuLevelBounceAvoidanceThreshold { get; set; }
		float L_0 = __this->get_U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_GpuLevelBounceAvoidanceThreshold(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_GpuLevelBounceAvoidanceThreshold_m2EE42CF824872EDB3CC001F87B44082954C32627 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float GpuLevelBounceAvoidanceThreshold { get; set; }
		float L_0 = ___value0;
		__this->set_U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_CpuLevelBounceAvoidanceThreshold()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_CpuLevelBounceAvoidanceThreshold_mD4671D4F90CB5F09AD26552591A1513A52206606 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float CpuLevelBounceAvoidanceThreshold { get; set; }
		float L_0 = __this->get_U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_CpuLevelBounceAvoidanceThreshold(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_CpuLevelBounceAvoidanceThreshold_m17883BE79795835F004C470CF5AD8B06829FCF6B (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float CpuLevelBounceAvoidanceThreshold { get; set; }
		float L_0 = ___value0;
		__this->set_U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15(L_0);
		return;
	}
}
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_UpdateInterval()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_UpdateInterval_mFDABD8980DD503CD69B461CFC843D8BD428CF4B9 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float UpdateInterval { get; set; }
		float L_0 = __this->get_U3CUpdateIntervalU3Ek__BackingField_16();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_UpdateInterval(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_UpdateInterval_m802A56839C4049389329DBDE94C7DDA9A3C22680 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float UpdateInterval { get; set; }
		float L_0 = ___value0;
		__this->set_U3CUpdateIntervalU3Ek__BackingField_16(L_0);
		return;
	}
}
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_MinTargetFrameRateHitTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_MinTargetFrameRateHitTime_m988C790CEF196B60A8154D3B5E6655501550D364 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float MinTargetFrameRateHitTime { get; set; }
		float L_0 = __this->get_U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_MinTargetFrameRateHitTime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_MinTargetFrameRateHitTime_mFED58D9C51678B867173E655AA4A6DF2B7060B48 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float MinTargetFrameRateHitTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17(L_0);
		return;
	}
}
// System.Single UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_MaxTemperatureLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_MaxTemperatureLevel_m121C40CD08B738C7B5B2646490700F20A3B4AFB7 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float MaxTemperatureLevel { get; set; }
		float L_0 = __this->get_U3CMaxTemperatureLevelU3Ek__BackingField_18();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_MaxTemperatureLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_MaxTemperatureLevel_m9826A6487749ADC7D1A66096D1A41FB8D8A31E93 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float MaxTemperatureLevel { get; set; }
		float L_0 = ___value0;
		__this->set_U3CMaxTemperatureLevelU3Ek__BackingField_18(L_0);
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::.ctor(UnityEngine.AdaptivePerformance.IDevicePerformanceControl,UnityEngine.AdaptivePerformance.IPerformanceStatus,UnityEngine.AdaptivePerformance.IThermalStatus)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController__ctor_m252C3934AA192D4135C2A9CE41B7D4B1216BC88C (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, RuntimeObject* ___perfControl0, RuntimeObject* ___perfStat1, RuntimeObject* ___thermalStat2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoPerformanceLevelController__ctor_m252C3934AA192D4135C2A9CE41B7D4B1216BC88C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// string m_featureName = "Auto Performance Control";
		__this->set_m_featureName_10(_stringLiteral89BB893F80FF7991CD99CDFCD9E218924D734AF4);
		// public AutoPerformanceLevelController(IDevicePerformanceControl perfControl, IPerformanceStatus perfStat, IThermalStatus thermalStat)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// UpdateInterval = 5.0f;
		AutoPerformanceLevelController_set_UpdateInterval_m802A56839C4049389329DBDE94C7DDA9A3C22680_inline(__this, (5.0f), /*hidden argument*/NULL);
		// TargetFrameTime = -1.0f;
		AutoPerformanceLevelController_set_TargetFrameTime_m86953888D9B6699A5BA926B6C0260665B860E7B2_inline(__this, (-1.0f), /*hidden argument*/NULL);
		// AllowedCpuActiveTimeRatio = 0.8f;
		AutoPerformanceLevelController_set_AllowedCpuActiveTimeRatio_m1526444FAD9758DC00AA9DB07425E26A779750C5_inline(__this, (0.8f), /*hidden argument*/NULL);
		// AllowedGpuActiveTimeRatio = 0.8f;
		AutoPerformanceLevelController_set_AllowedGpuActiveTimeRatio_mD7660071F2B7DEB8381B364116922BE312A4AA0D_inline(__this, (0.8f), /*hidden argument*/NULL);
		// GpuLevelBounceAvoidanceThreshold = 10.0f;
		AutoPerformanceLevelController_set_GpuLevelBounceAvoidanceThreshold_m2EE42CF824872EDB3CC001F87B44082954C32627_inline(__this, (10.0f), /*hidden argument*/NULL);
		// CpuLevelBounceAvoidanceThreshold = 10.0f;
		AutoPerformanceLevelController_set_CpuLevelBounceAvoidanceThreshold_m17883BE79795835F004C470CF5AD8B06829FCF6B_inline(__this, (10.0f), /*hidden argument*/NULL);
		// MinTargetFrameRateHitTime = 10.0f;
		AutoPerformanceLevelController_set_MinTargetFrameRateHitTime_mFED58D9C51678B867173E655AA4A6DF2B7060B48_inline(__this, (10.0f), /*hidden argument*/NULL);
		// MaxTemperatureLevel = 0.9f;
		AutoPerformanceLevelController_set_MaxTemperatureLevel_m9826A6487749ADC7D1A66096D1A41FB8D8A31E93_inline(__this, (0.9f), /*hidden argument*/NULL);
		// m_PerfStats = perfStat;
		RuntimeObject* L_0 = ___perfStat1;
		__this->set_m_PerfStats_1(L_0);
		// m_PerfControl = perfControl;
		RuntimeObject* L_1 = ___perfControl0;
		__this->set_m_PerfControl_0(L_1);
		// m_ThermalStats = thermalStat;
		RuntimeObject* L_2 = ___thermalStat2;
		__this->set_m_ThermalStats_2(L_2);
		// perfStat.PerformanceBottleneckChangeEvent += (PerformanceBottleneckChangeEventArgs ev) => OnBottleneckChange(ev);
		RuntimeObject* L_3 = ___perfStat1;
		PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * L_4 = (PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 *)il2cpp_codegen_object_new(PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995_il2cpp_TypeInfo_var);
		PerformanceBottleneckChangeHandler__ctor_mD8934C37B86DA79AB1370FF2DFA1843619EE6527(L_4, __this, (intptr_t)((intptr_t)AutoPerformanceLevelController_U3C_ctorU3Eb__43_0_m489E89565AB88A5E21B4C332CC758827E2CAC4F8_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceActionInvoker1< PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * >::Invoke(2 /* System.Void UnityEngine.AdaptivePerformance.IPerformanceStatus::add_PerformanceBottleneckChangeEvent(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler) */, IPerformanceStatus_t314D47B0E81EFA6C95DDF7B3C42B9DB1AA2C666A_il2cpp_TypeInfo_var, L_3, L_4);
		// }
		return;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::get_Enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_get_Enabled_mBA9F1DC76E6FC28B0DB7F4A7319E06EA8CFAA228 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// return m_Enabled;
		bool L_0 = __this->get_m_Enabled_9();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::set_Enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_Enabled_mD0D8E5D8A2D467B0DC314AC0C511F4327E2B18E8 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (m_Enabled == value)
		bool L_0 = __this->get_m_Enabled_9();
		bool L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// return;
		return;
	}

IL_000a:
	{
		// m_Enabled = value;
		bool L_2 = ___value0;
		__this->set_m_Enabled_9(L_2);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_Update_m7C0E073667FD597C31FE6A817FCC47930D2B6973 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// if (!m_Enabled)
		bool L_0 = __this->get_m_Enabled_9();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// UpdateImpl(Time.time);
		float L_1 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		AutoPerformanceLevelController_UpdateImpl_mE656C13A7FF6E6CD5667D585EC5CAB561A285C1E(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::Override(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_Override_mE3A9350573679DEBE02F35C2F7B80694973A157E (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, int32_t ___requestedCpuLevel0, int32_t ___requestedGpuLevel1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoPerformanceLevelController_Override_mE3A9350573679DEBE02F35C2F7B80694973A157E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_LastChangeTimeStamp = Time.time;
		float L_0 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		__this->set_m_LastChangeTimeStamp_3(L_0);
		// if (requestedCpuLevel > m_PerfControl.CpuLevel)
		int32_t L_1 = ___requestedCpuLevel0;
		RuntimeObject* L_2 = __this->get_m_PerfControl_0();
		NullCheck(L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 UnityEngine.AdaptivePerformance.IDevicePerformanceControl::get_CpuLevel() */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_2);
		if ((((int32_t)L_1) <= ((int32_t)L_3)))
		{
			goto IL_0025;
		}
	}
	{
		// m_LastCpuLevelRaiseTimeStamp = m_LastChangeTimeStamp;
		float L_4 = __this->get_m_LastChangeTimeStamp_3();
		__this->set_m_LastCpuLevelRaiseTimeStamp_5(L_4);
	}

IL_0025:
	{
		// if (requestedGpuLevel > m_PerfControl.GpuLevel)
		int32_t L_5 = ___requestedGpuLevel1;
		RuntimeObject* L_6 = __this->get_m_PerfControl_0();
		NullCheck(L_6);
		int32_t L_7 = InterfaceFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.AdaptivePerformance.IDevicePerformanceControl::get_GpuLevel() */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_6);
		if ((((int32_t)L_5) <= ((int32_t)L_7)))
		{
			goto IL_003f;
		}
	}
	{
		// m_LastCpuLevelRaiseTimeStamp = m_LastChangeTimeStamp;
		float L_8 = __this->get_m_LastChangeTimeStamp_3();
		__this->set_m_LastCpuLevelRaiseTimeStamp_5(L_8);
	}

IL_003f:
	{
		// m_PerfControl.CpuLevel = requestedCpuLevel;
		RuntimeObject* L_9 = __this->get_m_PerfControl_0();
		int32_t L_10 = ___requestedCpuLevel0;
		NullCheck(L_9);
		InterfaceActionInvoker1< int32_t >::Invoke(3 /* System.Void UnityEngine.AdaptivePerformance.IDevicePerformanceControl::set_CpuLevel(System.Int32) */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_9, L_10);
		// m_PerfControl.GpuLevel = requestedGpuLevel;
		RuntimeObject* L_11 = __this->get_m_PerfControl_0();
		int32_t L_12 = ___requestedGpuLevel1;
		NullCheck(L_11);
		InterfaceActionInvoker1< int32_t >::Invoke(5 /* System.Void UnityEngine.AdaptivePerformance.IDevicePerformanceControl::set_GpuLevel(System.Int32) */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_11, L_12);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::UpdateImpl(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_UpdateImpl_mE656C13A7FF6E6CD5667D585EC5CAB561A285C1E (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoPerformanceLevelController_UpdateImpl_mE656C13A7FF6E6CD5667D585EC5CAB561A285C1E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		// if (timestamp - m_LastChangeTimeStamp < UpdateInterval)
		float L_0 = ___timestamp0;
		float L_1 = __this->get_m_LastChangeTimeStamp_3();
		float L_2 = AutoPerformanceLevelController_get_UpdateInterval_mFDABD8980DD503CD69B461CFC843D8BD428CF4B9_inline(__this, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_0, (float)L_1))) < ((float)L_2))))
		{
			goto IL_0011;
		}
	}
	{
		// return;
		return;
	}

IL_0011:
	{
		// switch (m_PerfStats.PerformanceMetrics.PerformanceBottleneck)
		RuntimeObject* L_3 = __this->get_m_PerfStats_1();
		NullCheck(L_3);
		PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F  L_4 = InterfaceFuncInvoker0< PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F  >::Invoke(0 /* UnityEngine.AdaptivePerformance.PerformanceMetrics UnityEngine.AdaptivePerformance.IPerformanceStatus::get_PerformanceMetrics() */, IPerformanceStatus_t314D47B0E81EFA6C95DDF7B3C42B9DB1AA2C666A_il2cpp_TypeInfo_var, L_3);
		V_0 = L_4;
		int32_t L_5 = PerformanceMetrics_get_PerformanceBottleneck_m518B78FAA9F20BD9F79F3EAFCE7CC81612F55CC6_inline((PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		switch (L_6)
		{
			case 0:
			{
				goto IL_0091;
			}
			case 1:
			{
				goto IL_004f;
			}
			case 2:
			{
				goto IL_003c;
			}
			case 3:
			{
				goto IL_005f;
			}
		}
	}
	{
		return;
	}

IL_003c:
	{
		// if (AllowRaiseGpuLevel())
		bool L_7 = AutoPerformanceLevelController_AllowRaiseGpuLevel_m3270D2A8F91AE8F4FC4C19CF3E50FD6B50A03816(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00d5;
		}
	}
	{
		// RaiseGpuLevel(timestamp);
		float L_8 = ___timestamp0;
		AutoPerformanceLevelController_RaiseGpuLevel_mED360B4ACBE7A2787074DB6F75E8D1DCEF28F804(__this, L_8, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_004f:
	{
		// if (AllowRaiseCpuLevel())
		bool L_9 = AutoPerformanceLevelController_AllowRaiseCpuLevel_m37030CB2A20F89FEBB0CB2228CE2602991D86613(__this, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00d5;
		}
	}
	{
		// RaiseCpuLevel(timestamp);
		float L_10 = ___timestamp0;
		AutoPerformanceLevelController_RaiseCpuLevel_mB6B9F81D1AEE8BF96B2F14295E25C5D82F199250(__this, L_10, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_005f:
	{
		// if (timestamp - m_TargetFrameRateHitTimestamp > MinTargetFrameRateHitTime) // Make sure we have been able to hold targetFrameRate for at least 5s before making adjustments that might lower it.
		float L_11 = ___timestamp0;
		float L_12 = __this->get_m_TargetFrameRateHitTimestamp_6();
		float L_13 = AutoPerformanceLevelController_get_MinTargetFrameRateHitTime_m988C790CEF196B60A8154D3B5E6655501550D364_inline(__this, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_11, (float)L_12))) > ((float)L_13))))
		{
			goto IL_00d5;
		}
	}
	{
		// if (AllowLowerCpuLevel(timestamp))
		float L_14 = ___timestamp0;
		bool L_15 = AutoPerformanceLevelController_AllowLowerCpuLevel_mB670A9E210BCD84FA7419C79D5679303714447D4(__this, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0080;
		}
	}
	{
		// LowerCpuLevel(timestamp);
		float L_16 = ___timestamp0;
		AutoPerformanceLevelController_LowerCpuLevel_m6DF2EDB012FD228606FD09BF4F9C4D1BB9B0E504(__this, L_16, /*hidden argument*/NULL);
		return;
	}

IL_0080:
	{
		// else if (AllowLowerGpuLevel(timestamp))
		float L_17 = ___timestamp0;
		bool L_18 = AutoPerformanceLevelController_AllowLowerGpuLevel_m5AD3EB818563DFC12A1538A3011108A1FAD57D52(__this, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00d5;
		}
	}
	{
		// LowerGpuLevel(timestamp);
		float L_19 = ___timestamp0;
		AutoPerformanceLevelController_LowerGpuLevel_mD616F65705E36157413404296E82FC53C5650B74(__this, L_19, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0091:
	{
		// if (!m_TriedToResolveUnknownBottleneck && timestamp - m_BottleneckUnknownTimestamp > 10.0f)
		bool L_20 = __this->get_m_TriedToResolveUnknownBottleneck_8();
		if (L_20)
		{
			goto IL_00d5;
		}
	}
	{
		float L_21 = ___timestamp0;
		float L_22 = __this->get_m_BottleneckUnknownTimestamp_7();
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_21, (float)L_22))) > ((float)(10.0f)))))
		{
			goto IL_00d5;
		}
	}
	{
		// if (AllowRaiseCpuLevel())
		bool L_23 = AutoPerformanceLevelController_AllowRaiseCpuLevel_m37030CB2A20F89FEBB0CB2228CE2602991D86613(__this, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00bf;
		}
	}
	{
		// RaiseCpuLevel(timestamp);
		float L_24 = ___timestamp0;
		AutoPerformanceLevelController_RaiseCpuLevel_mB6B9F81D1AEE8BF96B2F14295E25C5D82F199250(__this, L_24, /*hidden argument*/NULL);
		// m_TriedToResolveUnknownBottleneck = true;
		__this->set_m_TriedToResolveUnknownBottleneck_8((bool)1);
		// }
		return;
	}

IL_00bf:
	{
		// else if (AllowRaiseGpuLevel())
		bool L_25 = AutoPerformanceLevelController_AllowRaiseGpuLevel_m3270D2A8F91AE8F4FC4C19CF3E50FD6B50A03816(__this, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d5;
		}
	}
	{
		// RaiseGpuLevel(timestamp);
		float L_26 = ___timestamp0;
		AutoPerformanceLevelController_RaiseGpuLevel_mED360B4ACBE7A2787074DB6F75E8D1DCEF28F804(__this, L_26, /*hidden argument*/NULL);
		// m_TriedToResolveUnknownBottleneck = true;
		__this->set_m_TriedToResolveUnknownBottleneck_8((bool)1);
	}

IL_00d5:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::OnBottleneckChange(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_OnBottleneckChange_m86D2D2E9BAC5B9D742DB0CBC99A8853C0D6A62D7 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  ___ev0, const RuntimeMethod* method)
{
	{
		// if (ev.PerformanceBottleneck == PerformanceBottleneck.TargetFrameRate)
		int32_t L_0 = PerformanceBottleneckChangeEventArgs_get_PerformanceBottleneck_m625E64FC86EF5C7DD4BEECE772F34AF22C07E4BD_inline((PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 *)(&___ev0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0015;
		}
	}
	{
		// m_TargetFrameRateHitTimestamp = Time.time;
		float L_1 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		__this->set_m_TargetFrameRateHitTimestamp_6(L_1);
	}

IL_0015:
	{
		// if (ev.PerformanceBottleneck == PerformanceBottleneck.Unknown)
		int32_t L_2 = PerformanceBottleneckChangeEventArgs_get_PerformanceBottleneck_m625E64FC86EF5C7DD4BEECE772F34AF22C07E4BD_inline((PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 *)(&___ev0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		// m_BottleneckUnknownTimestamp = Time.time;
		float L_3 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		__this->set_m_BottleneckUnknownTimestamp_7(L_3);
		return;
	}

IL_002a:
	{
		// m_TriedToResolveUnknownBottleneck = false;
		__this->set_m_TriedToResolveUnknownBottleneck_8((bool)0);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::RaiseGpuLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_RaiseGpuLevel_mED360B4ACBE7A2787074DB6F75E8D1DCEF28F804 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoPerformanceLevelController_RaiseGpuLevel_mED360B4ACBE7A2787074DB6F75E8D1DCEF28F804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// ++m_PerfControl.GpuLevel;
		RuntimeObject* L_0 = __this->get_m_PerfControl_0();
		RuntimeObject* L_1 = L_0;
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.AdaptivePerformance.IDevicePerformanceControl::get_GpuLevel() */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_1);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1));
		int32_t L_3 = V_0;
		NullCheck(L_1);
		InterfaceActionInvoker1< int32_t >::Invoke(5 /* System.Void UnityEngine.AdaptivePerformance.IDevicePerformanceControl::set_GpuLevel(System.Int32) */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_1, L_3);
		// m_LastChangeTimeStamp = timestamp;
		float L_4 = ___timestamp0;
		__this->set_m_LastChangeTimeStamp_3(L_4);
		// m_LastGpuLevelRaiseTimeStamp = timestamp;
		float L_5 = ___timestamp0;
		__this->set_m_LastGpuLevelRaiseTimeStamp_4(L_5);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::RaiseCpuLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_RaiseCpuLevel_mB6B9F81D1AEE8BF96B2F14295E25C5D82F199250 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoPerformanceLevelController_RaiseCpuLevel_mB6B9F81D1AEE8BF96B2F14295E25C5D82F199250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// ++m_PerfControl.CpuLevel;
		RuntimeObject* L_0 = __this->get_m_PerfControl_0();
		RuntimeObject* L_1 = L_0;
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 UnityEngine.AdaptivePerformance.IDevicePerformanceControl::get_CpuLevel() */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_1);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1));
		int32_t L_3 = V_0;
		NullCheck(L_1);
		InterfaceActionInvoker1< int32_t >::Invoke(3 /* System.Void UnityEngine.AdaptivePerformance.IDevicePerformanceControl::set_CpuLevel(System.Int32) */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_1, L_3);
		// m_LastChangeTimeStamp = timestamp;
		float L_4 = ___timestamp0;
		__this->set_m_LastChangeTimeStamp_3(L_4);
		// m_LastCpuLevelRaiseTimeStamp = timestamp;
		float L_5 = ___timestamp0;
		__this->set_m_LastCpuLevelRaiseTimeStamp_5(L_5);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::LowerCpuLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_LowerCpuLevel_m6DF2EDB012FD228606FD09BF4F9C4D1BB9B0E504 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoPerformanceLevelController_LowerCpuLevel_m6DF2EDB012FD228606FD09BF4F9C4D1BB9B0E504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// --m_PerfControl.CpuLevel;
		RuntimeObject* L_0 = __this->get_m_PerfControl_0();
		RuntimeObject* L_1 = L_0;
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 UnityEngine.AdaptivePerformance.IDevicePerformanceControl::get_CpuLevel() */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_1);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1));
		int32_t L_3 = V_0;
		NullCheck(L_1);
		InterfaceActionInvoker1< int32_t >::Invoke(3 /* System.Void UnityEngine.AdaptivePerformance.IDevicePerformanceControl::set_CpuLevel(System.Int32) */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_1, L_3);
		// m_LastChangeTimeStamp = timestamp;
		float L_4 = ___timestamp0;
		__this->set_m_LastChangeTimeStamp_3(L_4);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::LowerGpuLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_LowerGpuLevel_mD616F65705E36157413404296E82FC53C5650B74 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoPerformanceLevelController_LowerGpuLevel_mD616F65705E36157413404296E82FC53C5650B74_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// --m_PerfControl.GpuLevel;
		RuntimeObject* L_0 = __this->get_m_PerfControl_0();
		RuntimeObject* L_1 = L_0;
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.AdaptivePerformance.IDevicePerformanceControl::get_GpuLevel() */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_1);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1));
		int32_t L_3 = V_0;
		NullCheck(L_1);
		InterfaceActionInvoker1< int32_t >::Invoke(5 /* System.Void UnityEngine.AdaptivePerformance.IDevicePerformanceControl::set_GpuLevel(System.Int32) */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_1, L_3);
		// m_LastChangeTimeStamp = timestamp;
		float L_4 = ___timestamp0;
		__this->set_m_LastChangeTimeStamp_3(L_4);
		// }
		return;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::AllowLowerCpuLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_AllowLowerCpuLevel_mB670A9E210BCD84FA7419C79D5679303714447D4 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoPerformanceLevelController_AllowLowerCpuLevel_mB670A9E210BCD84FA7419C79D5679303714447D4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (m_PerfControl.CpuLevel > 0 && timestamp - m_LastCpuLevelRaiseTimeStamp > CpuLevelBounceAvoidanceThreshold)
		RuntimeObject* L_0 = __this->get_m_PerfControl_0();
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 UnityEngine.AdaptivePerformance.IDevicePerformanceControl::get_CpuLevel() */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		float L_2 = ___timestamp0;
		float L_3 = __this->get_m_LastCpuLevelRaiseTimeStamp_5();
		float L_4 = AutoPerformanceLevelController_get_CpuLevelBounceAvoidanceThreshold_mD4671D4F90CB5F09AD26552591A1513A52206606_inline(__this, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_2, (float)L_3))) > ((float)L_4))))
		{
			goto IL_0061;
		}
	}
	{
		// if (TargetFrameTime <= 0.0f)
		float L_5 = AutoPerformanceLevelController_get_TargetFrameTime_m3EF525DF9A1AAFFA8E7A3839A634D6A3413398C3_inline(__this, /*hidden argument*/NULL);
		if ((!(((float)L_5) <= ((float)(0.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_002d:
	{
		// var ft = m_PerfStats.FrameTiming;
		RuntimeObject* L_6 = __this->get_m_PerfStats_1();
		NullCheck(L_6);
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  L_7 = InterfaceFuncInvoker0< FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  >::Invoke(1 /* UnityEngine.AdaptivePerformance.FrameTiming UnityEngine.AdaptivePerformance.IPerformanceStatus::get_FrameTiming() */, IPerformanceStatus_t314D47B0E81EFA6C95DDF7B3C42B9DB1AA2C666A_il2cpp_TypeInfo_var, L_6);
		V_0 = L_7;
		// if (ft.AverageCpuFrameTime <= 0.0f)
		float L_8 = FrameTiming_get_AverageCpuFrameTime_m1BAD83D813770DB099F29C2ACAB31D4C08A79ED0_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_8) <= ((float)(0.0f)))))
		{
			goto IL_0049;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0049:
	{
		// if (ft.AverageCpuFrameTime < AllowedCpuActiveTimeRatio * TargetFrameTime)
		float L_9 = FrameTiming_get_AverageCpuFrameTime_m1BAD83D813770DB099F29C2ACAB31D4C08A79ED0_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)(&V_0), /*hidden argument*/NULL);
		float L_10 = AutoPerformanceLevelController_get_AllowedCpuActiveTimeRatio_mF966E1BBFDB821FEA660547F0CFFE0338F493782_inline(__this, /*hidden argument*/NULL);
		float L_11 = AutoPerformanceLevelController_get_TargetFrameTime_m3EF525DF9A1AAFFA8E7A3839A634D6A3413398C3_inline(__this, /*hidden argument*/NULL);
		if ((!(((float)L_9) < ((float)((float)il2cpp_codegen_multiply((float)L_10, (float)L_11))))))
		{
			goto IL_0061;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0061:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::AllowLowerGpuLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_AllowLowerGpuLevel_m5AD3EB818563DFC12A1538A3011108A1FAD57D52 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___timestamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoPerformanceLevelController_AllowLowerGpuLevel_m5AD3EB818563DFC12A1538A3011108A1FAD57D52_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (m_PerfControl.GpuLevel > 0 && timestamp - m_LastGpuLevelRaiseTimeStamp > GpuLevelBounceAvoidanceThreshold)
		RuntimeObject* L_0 = __this->get_m_PerfControl_0();
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.AdaptivePerformance.IDevicePerformanceControl::get_GpuLevel() */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		float L_2 = ___timestamp0;
		float L_3 = __this->get_m_LastGpuLevelRaiseTimeStamp_4();
		float L_4 = AutoPerformanceLevelController_get_GpuLevelBounceAvoidanceThreshold_mB46CCEE95E662384E848FA16446C783A622BBB41_inline(__this, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_2, (float)L_3))) > ((float)L_4))))
		{
			goto IL_0061;
		}
	}
	{
		// if (TargetFrameTime <= 0.0f)
		float L_5 = AutoPerformanceLevelController_get_TargetFrameTime_m3EF525DF9A1AAFFA8E7A3839A634D6A3413398C3_inline(__this, /*hidden argument*/NULL);
		if ((!(((float)L_5) <= ((float)(0.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_002d:
	{
		// var ft = m_PerfStats.FrameTiming;
		RuntimeObject* L_6 = __this->get_m_PerfStats_1();
		NullCheck(L_6);
		FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  L_7 = InterfaceFuncInvoker0< FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1  >::Invoke(1 /* UnityEngine.AdaptivePerformance.FrameTiming UnityEngine.AdaptivePerformance.IPerformanceStatus::get_FrameTiming() */, IPerformanceStatus_t314D47B0E81EFA6C95DDF7B3C42B9DB1AA2C666A_il2cpp_TypeInfo_var, L_6);
		V_0 = L_7;
		// if (ft.AverageGpuFrameTime <= 0.0f)
		float L_8 = FrameTiming_get_AverageGpuFrameTime_m2AA4C5FB0D07DA307EF04B40B60B211B21700386_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_8) <= ((float)(0.0f)))))
		{
			goto IL_0049;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0049:
	{
		// if (ft.AverageGpuFrameTime < AllowedGpuActiveTimeRatio * TargetFrameTime)
		float L_9 = FrameTiming_get_AverageGpuFrameTime_m2AA4C5FB0D07DA307EF04B40B60B211B21700386_inline((FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *)(&V_0), /*hidden argument*/NULL);
		float L_10 = AutoPerformanceLevelController_get_AllowedGpuActiveTimeRatio_mED7543A79BF8EDB3F876FCF5E815E58C39EE98A4_inline(__this, /*hidden argument*/NULL);
		float L_11 = AutoPerformanceLevelController_get_TargetFrameTime_m3EF525DF9A1AAFFA8E7A3839A634D6A3413398C3_inline(__this, /*hidden argument*/NULL);
		if ((!(((float)L_9) < ((float)((float)il2cpp_codegen_multiply((float)L_10, (float)L_11))))))
		{
			goto IL_0061;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0061:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::AllowRaiseLevels()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_AllowRaiseLevels_m9EEE01DF654F72120AAED804C30DF98C6CA5C2E5 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoPerformanceLevelController_AllowRaiseLevels_m9EEE01DF654F72120AAED804C30DF98C6CA5C2E5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// float tempLevel = m_ThermalStats.ThermalMetrics.TemperatureLevel;
		RuntimeObject* L_0 = __this->get_m_ThermalStats_2();
		NullCheck(L_0);
		ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  L_1 = InterfaceFuncInvoker0< ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  >::Invoke(0 /* UnityEngine.AdaptivePerformance.ThermalMetrics UnityEngine.AdaptivePerformance.IThermalStatus::get_ThermalMetrics() */, IThermalStatus_tDAEFF23197198F2659A13DB40699E469597F66D8_il2cpp_TypeInfo_var, L_0);
		V_1 = L_1;
		float L_2 = ThermalMetrics_get_TemperatureLevel_m1E7E3A67FD29A5ABA322EB933CF7409CD0151212_inline((ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 *)(&V_1), /*hidden argument*/NULL);
		V_0 = L_2;
		// if (tempLevel < 0.0f)
		float L_3 = V_0;
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_001e;
		}
	}
	{
		// return true; // temperature level not supported, allow changing levels anyway
		return (bool)1;
	}

IL_001e:
	{
		// if (tempLevel < MaxTemperatureLevel)
		float L_4 = V_0;
		float L_5 = AutoPerformanceLevelController_get_MaxTemperatureLevel_m121C40CD08B738C7B5B2646490700F20A3B4AFB7_inline(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) < ((float)L_5))))
		{
			goto IL_0029;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0029:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::AllowRaiseCpuLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_AllowRaiseCpuLevel_m37030CB2A20F89FEBB0CB2228CE2602991D86613 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoPerformanceLevelController_AllowRaiseCpuLevel_m37030CB2A20F89FEBB0CB2228CE2602991D86613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_PerfControl.CpuLevel >= m_PerfControl.MaxCpuPerformanceLevel)
		RuntimeObject* L_0 = __this->get_m_PerfControl_0();
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 UnityEngine.AdaptivePerformance.IDevicePerformanceControl::get_CpuLevel() */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_0);
		RuntimeObject* L_2 = __this->get_m_PerfControl_0();
		NullCheck(L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 UnityEngine.AdaptivePerformance.IDevicePerformanceControl::get_MaxCpuPerformanceLevel() */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_2);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_001a;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_001a:
	{
		// return AllowRaiseLevels();
		bool L_4 = AutoPerformanceLevelController_AllowRaiseLevels_m9EEE01DF654F72120AAED804C30DF98C6CA5C2E5(__this, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::AllowRaiseGpuLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_AllowRaiseGpuLevel_m3270D2A8F91AE8F4FC4C19CF3E50FD6B50A03816 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoPerformanceLevelController_AllowRaiseGpuLevel_m3270D2A8F91AE8F4FC4C19CF3E50FD6B50A03816_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_PerfControl.GpuLevel >= m_PerfControl.MaxGpuPerformanceLevel)
		RuntimeObject* L_0 = __this->get_m_PerfControl_0();
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.AdaptivePerformance.IDevicePerformanceControl::get_GpuLevel() */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_0);
		RuntimeObject* L_2 = __this->get_m_PerfControl_0();
		NullCheck(L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 UnityEngine.AdaptivePerformance.IDevicePerformanceControl::get_MaxGpuPerformanceLevel() */, IDevicePerformanceControl_t5CE4DC8810E94E96421212FA533BB2FCE81242D6_il2cpp_TypeInfo_var, L_2);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_001a;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_001a:
	{
		// return AllowRaiseLevels();
		bool L_4 = AutoPerformanceLevelController_AllowRaiseLevels_m9EEE01DF654F72120AAED804C30DF98C6CA5C2E5(__this, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.AdaptivePerformance.AutoPerformanceLevelController::<.ctor>b__43_0(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_U3C_ctorU3Eb__43_0_m489E89565AB88A5E21B4C332CC758827E2CAC4F8 (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  ___ev0, const RuntimeMethod* method)
{
	{
		// perfStat.PerformanceBottleneckChangeEvent += (PerformanceBottleneckChangeEventArgs ev) => OnBottleneckChange(ev);
		PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  L_0 = ___ev0;
		AutoPerformanceLevelController_OnBottleneckChange_m86D2D2E9BAC5B9D742DB0CBC99A8853C0D6A62D7(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.AdaptivePerformance.PerformanceBottleneck UnityEngine.AdaptivePerformance.BottleneckUtil::DetermineBottleneck(UnityEngine.AdaptivePerformance.PerformanceBottleneck,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BottleneckUtil_DetermineBottleneck_m4645F830559B7B5F54151AB2FF354C6419F8E3FD (int32_t ___prevBottleneck0, float ___averageCpuFrameTime1, float ___averageGpuFrametime2, float ___averageOverallFrametime3, float ___targetFrameTime4, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float G_B2_0 = 0.0f;
	float G_B1_0 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	float G_B12_0 = 0.0f;
	float G_B17_0 = 0.0f;
	float G_B23_0 = 0.0f;
	float G_B27_0 = 0.0f;
	float G_B32_0 = 0.0f;
	float G_B37_0 = 0.0f;
	{
		// if (HittingFrameRateLimit(averageOverallFrametime, prevBottleneck == PerformanceBottleneck.TargetFrameRate ? 0.03f : 0.02f, targetFrameTime))
		float L_0 = ___averageOverallFrametime3;
		int32_t L_1 = ___prevBottleneck0;
		G_B1_0 = L_0;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			G_B2_0 = L_0;
			goto IL_000c;
		}
	}
	{
		G_B3_0 = (0.02f);
		G_B3_1 = G_B1_0;
		goto IL_0011;
	}

IL_000c:
	{
		G_B3_0 = (0.03f);
		G_B3_1 = G_B2_0;
	}

IL_0011:
	{
		float L_2 = ___targetFrameTime4;
		bool L_3 = BottleneckUtil_HittingFrameRateLimit_mAB23E6E95942DFE4AE0E17796D501D07276DB751(G_B3_1, G_B3_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		// return PerformanceBottleneck.TargetFrameRate;
		return (int32_t)(3);
	}

IL_001c:
	{
		// if (averageGpuFrametime >= averageOverallFrametime)
		float L_4 = ___averageGpuFrametime2;
		float L_5 = ___averageOverallFrametime3;
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0022;
		}
	}
	{
		// return PerformanceBottleneck.GPU;
		return (int32_t)(2);
	}

IL_0022:
	{
		// else if (averageCpuFrameTime >= averageOverallFrametime)
		float L_6 = ___averageCpuFrameTime1;
		float L_7 = ___averageOverallFrametime3;
		if ((!(((float)L_6) >= ((float)L_7))))
		{
			goto IL_0028;
		}
	}
	{
		// return PerformanceBottleneck.CPU;
		return (int32_t)(1);
	}

IL_0028:
	{
		// bool wasGpuBound = prevBottleneck == PerformanceBottleneck.GPU;
		int32_t L_8 = ___prevBottleneck0;
		V_0 = (bool)((((int32_t)L_8) == ((int32_t)2))? 1 : 0);
		// bool wasCpuBound = prevBottleneck == PerformanceBottleneck.CPU;
		int32_t L_9 = ___prevBottleneck0;
		V_1 = (bool)((((int32_t)L_9) == ((int32_t)1))? 1 : 0);
		// float gpuUtilization = averageGpuFrametime / averageOverallFrametime;
		float L_10 = ___averageGpuFrametime2;
		float L_11 = ___averageOverallFrametime3;
		V_2 = ((float)((float)L_10/(float)L_11));
		// float cpuUtilization = averageCpuFrameTime / averageOverallFrametime;
		float L_12 = ___averageCpuFrameTime1;
		float L_13 = ___averageOverallFrametime3;
		V_3 = ((float)((float)L_12/(float)L_13));
		// float highCpuUtilThreshold = wasCpuBound ? 0.87f : 0.90f;
		bool L_14 = V_1;
		if (L_14)
		{
			goto IL_0044;
		}
	}
	{
		G_B12_0 = (0.9f);
		goto IL_0049;
	}

IL_0044:
	{
		G_B12_0 = (0.87f);
	}

IL_0049:
	{
		V_4 = G_B12_0;
		// if (cpuUtilization > highCpuUtilThreshold)
		float L_15 = V_3;
		float L_16 = V_4;
		if ((!(((float)L_15) > ((float)L_16))))
		{
			goto IL_0052;
		}
	}
	{
		// return PerformanceBottleneck.CPU;
		return (int32_t)(1);
	}

IL_0052:
	{
		// float highGpuUtilThreshold = wasGpuBound ? 0.87f : 0.90f;
		bool L_17 = V_0;
		if (L_17)
		{
			goto IL_005c;
		}
	}
	{
		G_B17_0 = (0.9f);
		goto IL_0061;
	}

IL_005c:
	{
		G_B17_0 = (0.87f);
	}

IL_0061:
	{
		V_5 = G_B17_0;
		// if (averageGpuFrametime > highGpuUtilThreshold)
		float L_18 = ___averageGpuFrametime2;
		float L_19 = V_5;
		if ((!(((float)L_18) > ((float)L_19))))
		{
			goto IL_006a;
		}
	}
	{
		// return PerformanceBottleneck.GPU;
		return (int32_t)(2);
	}

IL_006a:
	{
		// if (averageGpuFrametime > averageCpuFrameTime)
		float L_20 = ___averageGpuFrametime2;
		float L_21 = ___averageCpuFrameTime1;
		if ((!(((float)L_20) > ((float)L_21))))
		{
			goto IL_009e;
		}
	}
	{
		// float gpuUtilizationThreshold = wasGpuBound ? 0.7f : 0.72f;
		bool L_22 = V_0;
		if (L_22)
		{
			goto IL_0078;
		}
	}
	{
		G_B23_0 = (0.72f);
		goto IL_007d;
	}

IL_0078:
	{
		G_B23_0 = (0.7f);
	}

IL_007d:
	{
		V_6 = G_B23_0;
		// if (gpuUtilization > gpuUtilizationThreshold)
		float L_23 = V_2;
		float L_24 = V_6;
		if ((!(((float)L_23) > ((float)L_24))))
		{
			goto IL_00d2;
		}
	}
	{
		// float gpuFactor = wasGpuBound ? 0.72f : 0.70f;
		bool L_25 = V_0;
		if (L_25)
		{
			goto IL_008e;
		}
	}
	{
		G_B27_0 = (0.7f);
		goto IL_0093;
	}

IL_008e:
	{
		G_B27_0 = (0.72f);
	}

IL_0093:
	{
		V_7 = G_B27_0;
		// if (averageGpuFrametime * gpuFactor > averageCpuFrameTime)
		float L_26 = ___averageGpuFrametime2;
		float L_27 = V_7;
		float L_28 = ___averageCpuFrameTime1;
		if ((!(((float)((float)il2cpp_codegen_multiply((float)L_26, (float)L_27))) > ((float)L_28))))
		{
			goto IL_00d2;
		}
	}
	{
		// return PerformanceBottleneck.GPU;
		return (int32_t)(2);
	}

IL_009e:
	{
		// float cpuUtilizationThreshold = wasCpuBound ? 0.5f : 0.52f;
		bool L_29 = V_1;
		if (L_29)
		{
			goto IL_00a8;
		}
	}
	{
		G_B32_0 = (0.52f);
		goto IL_00ad;
	}

IL_00a8:
	{
		G_B32_0 = (0.5f);
	}

IL_00ad:
	{
		V_8 = G_B32_0;
		// if (cpuUtilization > cpuUtilizationThreshold && averageGpuFrametime < averageCpuFrameTime)
		float L_30 = V_3;
		float L_31 = V_8;
		if ((!(((float)L_30) > ((float)L_31))))
		{
			goto IL_00d2;
		}
	}
	{
		float L_32 = ___averageGpuFrametime2;
		float L_33 = ___averageCpuFrameTime1;
		if ((!(((float)L_32) < ((float)L_33))))
		{
			goto IL_00d2;
		}
	}
	{
		// float cpuFactor = wasCpuBound ? 0.85f : 0.80f;
		bool L_34 = V_1;
		if (L_34)
		{
			goto IL_00c2;
		}
	}
	{
		G_B37_0 = (0.8f);
		goto IL_00c7;
	}

IL_00c2:
	{
		G_B37_0 = (0.85f);
	}

IL_00c7:
	{
		V_9 = G_B37_0;
		// if (averageCpuFrameTime * cpuFactor > averageGpuFrametime)
		float L_35 = ___averageCpuFrameTime1;
		float L_36 = V_9;
		float L_37 = ___averageGpuFrametime2;
		if ((!(((float)((float)il2cpp_codegen_multiply((float)L_35, (float)L_36))) > ((float)L_37))))
		{
			goto IL_00d2;
		}
	}
	{
		// return PerformanceBottleneck.CPU;
		return (int32_t)(1);
	}

IL_00d2:
	{
		// return PerformanceBottleneck.Unknown;
		return (int32_t)(0);
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.BottleneckUtil::HittingFrameRateLimit(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BottleneckUtil_HittingFrameRateLimit_mAB23E6E95942DFE4AE0E17796D501D07276DB751 (float ___actualFrameTime0, float ___thresholdFactor1, float ___targetFrameTime2, const RuntimeMethod* method)
{
	{
		// if (targetFrameTime <= 0)
		float L_0 = ___targetFrameTime2;
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_000a;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_000a:
	{
		// if (actualFrameTime <= targetFrameTime)
		float L_1 = ___actualFrameTime0;
		float L_2 = ___targetFrameTime2;
		if ((!(((float)L_1) <= ((float)L_2))))
		{
			goto IL_0010;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0010:
	{
		// if (actualFrameTime - targetFrameTime < thresholdFactor * targetFrameTime)
		float L_3 = ___actualFrameTime0;
		float L_4 = ___targetFrameTime2;
		float L_5 = ___thresholdFactor1;
		float L_6 = ___targetFrameTime2;
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_3, (float)L_4))) < ((float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_6))))))
		{
			goto IL_001a;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_001a:
	{
		// return false;
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single UnityEngine.AdaptivePerformance.CpuTimeProvider::get_CpuFrameTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CpuTimeProvider_get_CpuFrameTime_mFDBF3DF1106A47DA135FF7B49389CC355108BA25 (CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CpuTimeProvider_get_CpuFrameTime_mFDBF3DF1106A47DA135FF7B49389CC355108BA25_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_RenderThreadCpuTime != null)
		RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * L_0 = __this->get_m_RenderThreadCpuTime_0();
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		// return Mathf.Max(m_MainThreadCpuTime.GetLatestResult(), m_RenderThreadCpuTime.GetLatestResult());
		MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * L_1 = __this->get_m_MainThreadCpuTime_1();
		NullCheck(L_1);
		float L_2 = MainThreadCpuTime_GetLatestResult_m5E3D72F4A7FBEDD957340CDAB3E19A128269D5B6_inline(L_1, /*hidden argument*/NULL);
		RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * L_3 = __this->get_m_RenderThreadCpuTime_0();
		NullCheck(L_3);
		float L_4 = RenderThreadCpuTime_GetLatestResult_mB9798FC4B9FE40428A1C6BF66EB6A37681F1F9C1(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Max_m670AE0EC1B09ED1A56FF9606B0F954670319CB65(L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0024:
	{
		// return m_MainThreadCpuTime.GetLatestResult();
		MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * L_6 = __this->get_m_MainThreadCpuTime_1();
		NullCheck(L_6);
		float L_7 = MainThreadCpuTime_GetLatestResult_m5E3D72F4A7FBEDD957340CDAB3E19A128269D5B6_inline(L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UnityEngine.AdaptivePerformance.CpuTimeProvider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CpuTimeProvider__ctor_mE73ABBAB48B1D65051956A2C32BE69E65A7B87F4 (CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CpuTimeProvider__ctor_mE73ABBAB48B1D65051956A2C32BE69E65A7B87F4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public CpuTimeProvider()
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// m_MainThreadCpuTime = new MainThreadCpuTime();
		MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * L_0 = (MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 *)il2cpp_codegen_object_new(MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887_il2cpp_TypeInfo_var);
		MainThreadCpuTime__ctor_m9600A6AD4493C006BBF6A29C876BED4CD42892E2(L_0, /*hidden argument*/NULL);
		__this->set_m_MainThreadCpuTime_1(L_0);
		// if (SystemInfo.graphicsMultiThreaded)
		bool L_1 = SystemInfo_get_graphicsMultiThreaded_mF8F3D1CBC9FC9F487696079DFC56633824A7B6C0(/*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		// m_RenderThreadCpuTime = new RenderThreadCpuTime();
		RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * L_2 = (RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 *)il2cpp_codegen_object_new(RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710_il2cpp_TypeInfo_var);
		RenderThreadCpuTime__ctor_m4DF8E17D0BD885CC1F829E3A5E06C219AE3830C7(L_2, /*hidden argument*/NULL);
		__this->set_m_RenderThreadCpuTime_0(L_2);
	}

IL_0023:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.CpuTimeProvider::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CpuTimeProvider_Reset_m34832EDC0C3FA6E5931758EA733D318847AA3460 (CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * __this, const RuntimeMethod* method)
{
	{
		// if (m_RenderThreadCpuTime != null)
		RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * L_0 = __this->get_m_RenderThreadCpuTime_0();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// m_RenderThreadCpuTime.Reset();
		RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * L_1 = __this->get_m_RenderThreadCpuTime_0();
		NullCheck(L_1);
		RenderThreadCpuTime_Reset_m3A491AE6D23434703AE0A91D9145592565EB2191(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.CpuTimeProvider::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CpuTimeProvider_LateUpdate_mABB99A8A913090AFBBBB2C112EE1734BFE1358BC (CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * __this, const RuntimeMethod* method)
{
	{
		// if (m_RenderThreadCpuTime != null)
		RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * L_0 = __this->get_m_RenderThreadCpuTime_0();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// m_RenderThreadCpuTime.Measure();
		RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * L_1 = __this->get_m_RenderThreadCpuTime_0();
		NullCheck(L_1);
		RenderThreadCpuTime_Measure_m6C2F782E97EFF82696E79BC952500EDADA7D67EB(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.CpuTimeProvider::EndOfFrame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CpuTimeProvider_EndOfFrame_mA508682DFC8C46B87167317E3A947D645BA8EA53 (CpuTimeProvider_t20084E05E671D9A1977AF9343146C8499B3A5994 * __this, const RuntimeMethod* method)
{
	{
		// m_MainThreadCpuTime.Measure();
		MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * L_0 = __this->get_m_MainThreadCpuTime_1();
		NullCheck(L_0);
		MainThreadCpuTime_Measure_m7AE49D83360B7818C0FC957250096E4788DF7EA7(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::.ctor(UnityEngine.AdaptivePerformance.Provider.IDevicePerformanceLevelControl)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl__ctor_m41818A4930BAEB5BE2E0903DDBCCE23C043F0A07 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, RuntimeObject* ___performanceLevelControl0, const RuntimeMethod* method)
{
	{
		// public DevicePerformanceControlImpl(Provider.IDevicePerformanceLevelControl performanceLevelControl)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// m_PerformanceLevelControl = performanceLevelControl;
		RuntimeObject* L_0 = ___performanceLevelControl0;
		__this->set_m_PerformanceLevelControl_0(L_0);
		// PerformanceControlMode = PerformanceControlMode.Automatic;
		DevicePerformanceControlImpl_set_PerformanceControlMode_m90A342E8781C151B8F940601A615EBE727ED68BD_inline(__this, 0, /*hidden argument*/NULL);
		// CurrentCpuLevel = Constants.UnknownPerformanceLevel;
		DevicePerformanceControlImpl_set_CurrentCpuLevel_mA17D47BD068FBA9741C61F26D8869577FDC3B04E_inline(__this, (-1), /*hidden argument*/NULL);
		// CurrentGpuLevel = Constants.UnknownPerformanceLevel;
		DevicePerformanceControlImpl_set_CurrentGpuLevel_m58F4826025D590BCC9D4BA9CB9DC76452DD369B2_inline(__this, (-1), /*hidden argument*/NULL);
		// CpuLevel = Constants.UnknownPerformanceLevel;
		DevicePerformanceControlImpl_set_CpuLevel_mE7771043E97430A856CDFAA1162D257801BDD1F4_inline(__this, (-1), /*hidden argument*/NULL);
		// GpuLevel = Constants.UnknownPerformanceLevel;
		DevicePerformanceControlImpl_set_GpuLevel_m09C65245F940E90A38B7C18326038C843F1CE005_inline(__this, (-1), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::Update(UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DevicePerformanceControlImpl_Update_m62E8744125498EDACB2150F2DEF0DF6D6AA58635 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * ___changeArgs0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DevicePerformanceControlImpl_Update_m62E8744125498EDACB2150F2DEF0DF6D6AA58635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B5_0 = 0;
	{
		// changeArgs = new PerformanceLevelChangeEventArgs();
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * L_0 = ___changeArgs0;
		il2cpp_codegen_initobj(L_0, sizeof(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB ));
		// changeArgs.PerformanceControlMode = PerformanceControlMode;
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * L_1 = ___changeArgs0;
		int32_t L_2 = DevicePerformanceControlImpl_get_PerformanceControlMode_m2E4EE8BB379495FD00D67FCB6675358333F1532D_inline(__this, /*hidden argument*/NULL);
		PerformanceLevelChangeEventArgs_set_PerformanceControlMode_mE4A2CDE249A57590CA4CD493ACA4DEEC745E1093_inline((PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)L_1, L_2, /*hidden argument*/NULL);
		// if (PerformanceControlMode == PerformanceControlMode.System)
		int32_t L_3 = DevicePerformanceControlImpl_get_PerformanceControlMode_m2E4EE8BB379495FD00D67FCB6675358333F1532D_inline(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_006c;
		}
	}
	{
		// bool changed = CurrentCpuLevel != Constants.UnknownPerformanceLevel || CurrentGpuLevel != Constants.UnknownPerformanceLevel;
		int32_t L_4 = DevicePerformanceControlImpl_get_CurrentCpuLevel_m5B3842B5593E0ECD6FD2DC40C4A2B33495411C13_inline(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_5 = DevicePerformanceControlImpl_get_CurrentGpuLevel_m8BDBF664287A7FD279C7A264AAA9818102B3B6F4_inline(__this, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)((((int32_t)L_5) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0034;
	}

IL_0033:
	{
		G_B4_0 = 1;
	}

IL_0034:
	{
		// CurrentCpuLevel = Constants.UnknownPerformanceLevel;
		DevicePerformanceControlImpl_set_CurrentCpuLevel_mA17D47BD068FBA9741C61F26D8869577FDC3B04E_inline(__this, (-1), /*hidden argument*/NULL);
		// CurrentGpuLevel = Constants.UnknownPerformanceLevel;
		DevicePerformanceControlImpl_set_CurrentGpuLevel_m58F4826025D590BCC9D4BA9CB9DC76452DD369B2_inline(__this, (-1), /*hidden argument*/NULL);
		// if (changed)
		int32_t L_6 = G_B4_0;
		G_B5_0 = L_6;
		if (!L_6)
		{
			G_B6_0 = L_6;
			goto IL_006b;
		}
	}
	{
		// changeArgs.CpuLevel = CurrentCpuLevel;
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * L_7 = ___changeArgs0;
		int32_t L_8 = DevicePerformanceControlImpl_get_CurrentCpuLevel_m5B3842B5593E0ECD6FD2DC40C4A2B33495411C13_inline(__this, /*hidden argument*/NULL);
		PerformanceLevelChangeEventArgs_set_CpuLevel_m844481992806A1B152181FA8631B6033003CD640_inline((PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)L_7, L_8, /*hidden argument*/NULL);
		// changeArgs.GpuLevel = CurrentGpuLevel;
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * L_9 = ___changeArgs0;
		int32_t L_10 = DevicePerformanceControlImpl_get_CurrentGpuLevel_m8BDBF664287A7FD279C7A264AAA9818102B3B6F4_inline(__this, /*hidden argument*/NULL);
		PerformanceLevelChangeEventArgs_set_GpuLevel_mBF3785C7E3C4FDEBF29E2E8CE640582658A6BE91_inline((PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)L_9, L_10, /*hidden argument*/NULL);
		// changeArgs.CpuLevelDelta = 0;
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * L_11 = ___changeArgs0;
		PerformanceLevelChangeEventArgs_set_CpuLevelDelta_mDE58CBE256AE306670BE51649DE8895060860A77_inline((PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)L_11, 0, /*hidden argument*/NULL);
		// changeArgs.GpuLevelDelta = 0;
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * L_12 = ___changeArgs0;
		PerformanceLevelChangeEventArgs_set_GpuLevelDelta_mBBD8DC5572E5FC9EEA8F67AB5995121010F09532_inline((PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)L_12, 0, /*hidden argument*/NULL);
		G_B6_0 = G_B5_0;
	}

IL_006b:
	{
		// return changed;
		return (bool)G_B6_0;
	}

IL_006c:
	{
		// if (CpuLevel != Constants.UnknownPerformanceLevel || GpuLevel != Constants.UnknownPerformanceLevel)
		int32_t L_13 = DevicePerformanceControlImpl_get_CpuLevel_m3222509C4D795522341F1E93150940DC1966B4B2_inline(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)(-1)))))
		{
			goto IL_0081;
		}
	}
	{
		int32_t L_14 = DevicePerformanceControlImpl_get_GpuLevel_m519D52125AE15CDFC3640EBA56B2D8D2B6F5A9B4_inline(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_14) == ((int32_t)(-1))))
		{
			goto IL_0139;
		}
	}

IL_0081:
	{
		// if (CpuLevel != CurrentCpuLevel || GpuLevel != CurrentGpuLevel)
		int32_t L_15 = DevicePerformanceControlImpl_get_CpuLevel_m3222509C4D795522341F1E93150940DC1966B4B2_inline(__this, /*hidden argument*/NULL);
		int32_t L_16 = DevicePerformanceControlImpl_get_CurrentCpuLevel_m5B3842B5593E0ECD6FD2DC40C4A2B33495411C13_inline(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_17 = DevicePerformanceControlImpl_get_GpuLevel_m519D52125AE15CDFC3640EBA56B2D8D2B6F5A9B4_inline(__this, /*hidden argument*/NULL);
		int32_t L_18 = DevicePerformanceControlImpl_get_CurrentGpuLevel_m8BDBF664287A7FD279C7A264AAA9818102B3B6F4_inline(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_17) == ((int32_t)L_18)))
		{
			goto IL_0139;
		}
	}

IL_00a0:
	{
		// if (m_PerformanceLevelControl.SetPerformanceLevel(CpuLevel, GpuLevel))
		RuntimeObject* L_19 = __this->get_m_PerformanceLevelControl_0();
		int32_t L_20 = DevicePerformanceControlImpl_get_CpuLevel_m3222509C4D795522341F1E93150940DC1966B4B2_inline(__this, /*hidden argument*/NULL);
		int32_t L_21 = DevicePerformanceControlImpl_get_GpuLevel_m519D52125AE15CDFC3640EBA56B2D8D2B6F5A9B4_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		bool L_22 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(2 /* System.Boolean UnityEngine.AdaptivePerformance.Provider.IDevicePerformanceLevelControl::SetPerformanceLevel(System.Int32,System.Int32) */, IDevicePerformanceLevelControl_t69BEA7256A9F193E2D588EADD393A04E5F201436_il2cpp_TypeInfo_var, L_19, L_20, L_21);
		if (!L_22)
		{
			goto IL_0103;
		}
	}
	{
		// changeArgs.CpuLevelDelta = ComputeDelta(CurrentCpuLevel, CpuLevel);
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * L_23 = ___changeArgs0;
		int32_t L_24 = DevicePerformanceControlImpl_get_CurrentCpuLevel_m5B3842B5593E0ECD6FD2DC40C4A2B33495411C13_inline(__this, /*hidden argument*/NULL);
		int32_t L_25 = DevicePerformanceControlImpl_get_CpuLevel_m3222509C4D795522341F1E93150940DC1966B4B2_inline(__this, /*hidden argument*/NULL);
		int32_t L_26 = DevicePerformanceControlImpl_ComputeDelta_m31E9E576E9E43E482889111B644554D8841EDE40(__this, L_24, L_25, /*hidden argument*/NULL);
		PerformanceLevelChangeEventArgs_set_CpuLevelDelta_mDE58CBE256AE306670BE51649DE8895060860A77_inline((PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)L_23, L_26, /*hidden argument*/NULL);
		// changeArgs.GpuLevelDelta = ComputeDelta(CurrentGpuLevel, GpuLevel);
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * L_27 = ___changeArgs0;
		int32_t L_28 = DevicePerformanceControlImpl_get_CurrentGpuLevel_m8BDBF664287A7FD279C7A264AAA9818102B3B6F4_inline(__this, /*hidden argument*/NULL);
		int32_t L_29 = DevicePerformanceControlImpl_get_GpuLevel_m519D52125AE15CDFC3640EBA56B2D8D2B6F5A9B4_inline(__this, /*hidden argument*/NULL);
		int32_t L_30 = DevicePerformanceControlImpl_ComputeDelta_m31E9E576E9E43E482889111B644554D8841EDE40(__this, L_28, L_29, /*hidden argument*/NULL);
		PerformanceLevelChangeEventArgs_set_GpuLevelDelta_mBBD8DC5572E5FC9EEA8F67AB5995121010F09532_inline((PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)L_27, L_30, /*hidden argument*/NULL);
		// CurrentCpuLevel = CpuLevel;
		int32_t L_31 = DevicePerformanceControlImpl_get_CpuLevel_m3222509C4D795522341F1E93150940DC1966B4B2_inline(__this, /*hidden argument*/NULL);
		DevicePerformanceControlImpl_set_CurrentCpuLevel_mA17D47BD068FBA9741C61F26D8869577FDC3B04E_inline(__this, L_31, /*hidden argument*/NULL);
		// CurrentGpuLevel = GpuLevel;
		int32_t L_32 = DevicePerformanceControlImpl_get_GpuLevel_m519D52125AE15CDFC3640EBA56B2D8D2B6F5A9B4_inline(__this, /*hidden argument*/NULL);
		DevicePerformanceControlImpl_set_CurrentGpuLevel_m58F4826025D590BCC9D4BA9CB9DC76452DD369B2_inline(__this, L_32, /*hidden argument*/NULL);
		// }
		goto IL_011f;
	}

IL_0103:
	{
		// changeArgs.CpuLevelDelta = 0;
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * L_33 = ___changeArgs0;
		PerformanceLevelChangeEventArgs_set_CpuLevelDelta_mDE58CBE256AE306670BE51649DE8895060860A77_inline((PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)L_33, 0, /*hidden argument*/NULL);
		// changeArgs.GpuLevelDelta = 0;
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * L_34 = ___changeArgs0;
		PerformanceLevelChangeEventArgs_set_GpuLevelDelta_mBBD8DC5572E5FC9EEA8F67AB5995121010F09532_inline((PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)L_34, 0, /*hidden argument*/NULL);
		// CurrentCpuLevel = Constants.UnknownPerformanceLevel;
		DevicePerformanceControlImpl_set_CurrentCpuLevel_mA17D47BD068FBA9741C61F26D8869577FDC3B04E_inline(__this, (-1), /*hidden argument*/NULL);
		// CurrentGpuLevel = Constants.UnknownPerformanceLevel;
		DevicePerformanceControlImpl_set_CurrentGpuLevel_m58F4826025D590BCC9D4BA9CB9DC76452DD369B2_inline(__this, (-1), /*hidden argument*/NULL);
	}

IL_011f:
	{
		// changeArgs.CpuLevel = CurrentCpuLevel;
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * L_35 = ___changeArgs0;
		int32_t L_36 = DevicePerformanceControlImpl_get_CurrentCpuLevel_m5B3842B5593E0ECD6FD2DC40C4A2B33495411C13_inline(__this, /*hidden argument*/NULL);
		PerformanceLevelChangeEventArgs_set_CpuLevel_m844481992806A1B152181FA8631B6033003CD640_inline((PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)L_35, L_36, /*hidden argument*/NULL);
		// changeArgs.GpuLevel = CurrentGpuLevel;
		PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * L_37 = ___changeArgs0;
		int32_t L_38 = DevicePerformanceControlImpl_get_CurrentGpuLevel_m8BDBF664287A7FD279C7A264AAA9818102B3B6F4_inline(__this, /*hidden argument*/NULL);
		PerformanceLevelChangeEventArgs_set_GpuLevel_mBF3785C7E3C4FDEBF29E2E8CE640582658A6BE91_inline((PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *)L_37, L_38, /*hidden argument*/NULL);
		// return true;
		return (bool)1;
	}

IL_0139:
	{
		// return false;
		return (bool)0;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::ComputeDelta(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_ComputeDelta_m31E9E576E9E43E482889111B644554D8841EDE40 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___oldLevel0, int32_t ___newLevel1, const RuntimeMethod* method)
{
	{
		// if (oldLevel < 0 || newLevel < 0)
		int32_t L_0 = ___oldLevel0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_1 = ___newLevel1;
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_000a;
		}
	}

IL_0008:
	{
		// return 0;
		return 0;
	}

IL_000a:
	{
		// return newLevel - oldLevel;
		int32_t L_2 = ___newLevel1;
		int32_t L_3 = ___oldLevel0;
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)L_3));
	}
}
// UnityEngine.AdaptivePerformance.PerformanceControlMode UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_PerformanceControlMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_PerformanceControlMode_m2E4EE8BB379495FD00D67FCB6675358333F1532D (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method)
{
	{
		// public PerformanceControlMode PerformanceControlMode { get; set; }
		int32_t L_0 = __this->get_U3CPerformanceControlModeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::set_PerformanceControlMode(UnityEngine.AdaptivePerformance.PerformanceControlMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_PerformanceControlMode_m90A342E8781C151B8F940601A615EBE727ED68BD (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public PerformanceControlMode PerformanceControlMode { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CPerformanceControlModeU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_MaxCpuPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_MaxCpuPerformanceLevel_mC8D89A2CC5E0135E12B089A50D6289FC2DC97B9A (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DevicePerformanceControlImpl_get_MaxCpuPerformanceLevel_mC8D89A2CC5E0135E12B089A50D6289FC2DC97B9A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public int MaxCpuPerformanceLevel { get { return m_PerformanceLevelControl != null ? m_PerformanceLevelControl.MaxCpuPerformanceLevel : Constants.UnknownPerformanceLevel; } }
		RuntimeObject* L_0 = __this->get_m_PerformanceLevelControl_0();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (-1);
	}

IL_000a:
	{
		RuntimeObject* L_1 = __this->get_m_PerformanceLevelControl_0();
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 UnityEngine.AdaptivePerformance.Provider.IDevicePerformanceLevelControl::get_MaxCpuPerformanceLevel() */, IDevicePerformanceLevelControl_t69BEA7256A9F193E2D588EADD393A04E5F201436_il2cpp_TypeInfo_var, L_1);
		return L_2;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_MaxGpuPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_MaxGpuPerformanceLevel_mF87A40C6B5D9EB47C980229BE85925CF3CEA39CE (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DevicePerformanceControlImpl_get_MaxGpuPerformanceLevel_mF87A40C6B5D9EB47C980229BE85925CF3CEA39CE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public int MaxGpuPerformanceLevel { get { return m_PerformanceLevelControl != null ? m_PerformanceLevelControl.MaxGpuPerformanceLevel : Constants.UnknownPerformanceLevel; } }
		RuntimeObject* L_0 = __this->get_m_PerformanceLevelControl_0();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (-1);
	}

IL_000a:
	{
		RuntimeObject* L_1 = __this->get_m_PerformanceLevelControl_0();
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 UnityEngine.AdaptivePerformance.Provider.IDevicePerformanceLevelControl::get_MaxGpuPerformanceLevel() */, IDevicePerformanceLevelControl_t69BEA7256A9F193E2D588EADD393A04E5F201436_il2cpp_TypeInfo_var, L_1);
		return L_2;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_CpuLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_CpuLevel_m3222509C4D795522341F1E93150940DC1966B4B2 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method)
{
	{
		// public int CpuLevel { get; set; }
		int32_t L_0 = __this->get_U3CCpuLevelU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::set_CpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_CpuLevel_mE7771043E97430A856CDFAA1162D257801BDD1F4 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCpuLevelU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_GpuLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_GpuLevel_m519D52125AE15CDFC3640EBA56B2D8D2B6F5A9B4 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method)
{
	{
		// public int GpuLevel { get; set; }
		int32_t L_0 = __this->get_U3CGpuLevelU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::set_GpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_GpuLevel_m09C65245F940E90A38B7C18326038C843F1CE005 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int GpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CGpuLevelU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_CurrentCpuLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_CurrentCpuLevel_m5B3842B5593E0ECD6FD2DC40C4A2B33495411C13 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method)
{
	{
		// public int CurrentCpuLevel { get; set; }
		int32_t L_0 = __this->get_U3CCurrentCpuLevelU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::set_CurrentCpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_CurrentCpuLevel_mA17D47BD068FBA9741C61F26D8869577FDC3B04E (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CurrentCpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentCpuLevelU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::get_CurrentGpuLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_CurrentGpuLevel_m8BDBF664287A7FD279C7A264AAA9818102B3B6F4 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method)
{
	{
		// public int CurrentGpuLevel { get; set; }
		int32_t L_0 = __this->get_U3CCurrentGpuLevelU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.DevicePerformanceControlImpl::set_CurrentGpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_CurrentGpuLevel_m58F4826025D590BCC9D4BA9CB9DC76452DD369B2 (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CurrentGpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentGpuLevelU3Ek__BackingField_5(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.FrameTiming::set_CurrentFrameTime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FrameTiming_set_CurrentFrameTime_m2DD61370159EDB8AE9612CC6303B25175788A0E5 (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float CurrentFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CCurrentFrameTimeU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void FrameTiming_set_CurrentFrameTime_m2DD61370159EDB8AE9612CC6303B25175788A0E5_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * _thisAdjusted = reinterpret_cast<FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *>(__this + _offset);
	FrameTiming_set_CurrentFrameTime_m2DD61370159EDB8AE9612CC6303B25175788A0E5_inline(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.AdaptivePerformance.FrameTiming::get_AverageFrameTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float FrameTiming_get_AverageFrameTime_m6C518BA12C5C330F471B31E364A37362AC439618 (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, const RuntimeMethod* method)
{
	{
		// public float AverageFrameTime { get; set; }
		float L_0 = __this->get_U3CAverageFrameTimeU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C  float FrameTiming_get_AverageFrameTime_m6C518BA12C5C330F471B31E364A37362AC439618_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * _thisAdjusted = reinterpret_cast<FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *>(__this + _offset);
	return FrameTiming_get_AverageFrameTime_m6C518BA12C5C330F471B31E364A37362AC439618_inline(_thisAdjusted, method);
}
// System.Void UnityEngine.AdaptivePerformance.FrameTiming::set_AverageFrameTime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FrameTiming_set_AverageFrameTime_mBDC023C5DDCE8C8069CBEAF28DAC912744D648C5 (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float AverageFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CAverageFrameTimeU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void FrameTiming_set_AverageFrameTime_mBDC023C5DDCE8C8069CBEAF28DAC912744D648C5_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * _thisAdjusted = reinterpret_cast<FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *>(__this + _offset);
	FrameTiming_set_AverageFrameTime_mBDC023C5DDCE8C8069CBEAF28DAC912744D648C5_inline(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.AdaptivePerformance.FrameTiming::set_CurrentGpuFrameTime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FrameTiming_set_CurrentGpuFrameTime_mBAB674B381CA46E589A79890A9BE0A0FDC2EE781 (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float CurrentGpuFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CCurrentGpuFrameTimeU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void FrameTiming_set_CurrentGpuFrameTime_mBAB674B381CA46E589A79890A9BE0A0FDC2EE781_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * _thisAdjusted = reinterpret_cast<FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *>(__this + _offset);
	FrameTiming_set_CurrentGpuFrameTime_mBAB674B381CA46E589A79890A9BE0A0FDC2EE781_inline(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.AdaptivePerformance.FrameTiming::get_AverageGpuFrameTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float FrameTiming_get_AverageGpuFrameTime_m2AA4C5FB0D07DA307EF04B40B60B211B21700386 (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, const RuntimeMethod* method)
{
	{
		// public float AverageGpuFrameTime { get; set; }
		float L_0 = __this->get_U3CAverageGpuFrameTimeU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C  float FrameTiming_get_AverageGpuFrameTime_m2AA4C5FB0D07DA307EF04B40B60B211B21700386_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * _thisAdjusted = reinterpret_cast<FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *>(__this + _offset);
	return FrameTiming_get_AverageGpuFrameTime_m2AA4C5FB0D07DA307EF04B40B60B211B21700386_inline(_thisAdjusted, method);
}
// System.Void UnityEngine.AdaptivePerformance.FrameTiming::set_AverageGpuFrameTime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FrameTiming_set_AverageGpuFrameTime_mADCD67452E1D4C92E7562EDD78A5BC411B3BC29B (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float AverageGpuFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CAverageGpuFrameTimeU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void FrameTiming_set_AverageGpuFrameTime_mADCD67452E1D4C92E7562EDD78A5BC411B3BC29B_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * _thisAdjusted = reinterpret_cast<FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *>(__this + _offset);
	FrameTiming_set_AverageGpuFrameTime_mADCD67452E1D4C92E7562EDD78A5BC411B3BC29B_inline(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.AdaptivePerformance.FrameTiming::set_CurrentCpuFrameTime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FrameTiming_set_CurrentCpuFrameTime_m95A32BC4E4AE8885D1738D309F2C39E96E887810 (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float CurrentCpuFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CCurrentCpuFrameTimeU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void FrameTiming_set_CurrentCpuFrameTime_m95A32BC4E4AE8885D1738D309F2C39E96E887810_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * _thisAdjusted = reinterpret_cast<FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *>(__this + _offset);
	FrameTiming_set_CurrentCpuFrameTime_m95A32BC4E4AE8885D1738D309F2C39E96E887810_inline(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.AdaptivePerformance.FrameTiming::get_AverageCpuFrameTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float FrameTiming_get_AverageCpuFrameTime_m1BAD83D813770DB099F29C2ACAB31D4C08A79ED0 (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, const RuntimeMethod* method)
{
	{
		// public float AverageCpuFrameTime { get; set; }
		float L_0 = __this->get_U3CAverageCpuFrameTimeU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C  float FrameTiming_get_AverageCpuFrameTime_m1BAD83D813770DB099F29C2ACAB31D4C08A79ED0_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * _thisAdjusted = reinterpret_cast<FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *>(__this + _offset);
	return FrameTiming_get_AverageCpuFrameTime_m1BAD83D813770DB099F29C2ACAB31D4C08A79ED0_inline(_thisAdjusted, method);
}
// System.Void UnityEngine.AdaptivePerformance.FrameTiming::set_AverageCpuFrameTime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FrameTiming_set_AverageCpuFrameTime_m87D8B833265478A0EA25153B3D5299AACAE71BDE (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float AverageCpuFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CAverageCpuFrameTimeU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void FrameTiming_set_AverageCpuFrameTime_m87D8B833265478A0EA25153B3D5299AACAE71BDE_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * _thisAdjusted = reinterpret_cast<FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 *>(__this + _offset);
	FrameTiming_set_AverageCpuFrameTime_m87D8B833265478A0EA25153B3D5299AACAE71BDE_inline(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.GpuTimeProvider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GpuTimeProvider__ctor_m53082BB9CAF2611637533E336F6E406A306DCA9A (GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GpuTimeProvider__ctor_m53082BB9CAF2611637533E336F6E406A306DCA9A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private UnityEngine.FrameTiming[] m_FrameTiming = new UnityEngine.FrameTiming[1];
		FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3* L_0 = (FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3*)(FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3*)SZArrayNew(FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3_il2cpp_TypeInfo_var, (uint32_t)1);
		__this->set_m_FrameTiming_0(L_0);
		// public GpuTimeProvider()
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Single UnityEngine.AdaptivePerformance.GpuTimeProvider::get_GpuFrameTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GpuTimeProvider_get_GpuFrameTime_mA1F553C55D589588F6954C2E87D835640DD43C7B (GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * __this, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	{
		// if (UnityEngine.FrameTimingManager.GetLatestTimings(1, m_FrameTiming) >= 1)
		FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3* L_0 = __this->get_m_FrameTiming_0();
		uint32_t L_1 = FrameTimingManager_GetLatestTimings_m286888EFC8779C9F97D5140EE5D7EE80BEE3DE35(1, L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) >= ((uint32_t)1))))
		{
			goto IL_003a;
		}
	}
	{
		// double gpuFrameTime = m_FrameTiming[0].gpuFrameTime;
		FrameTimingU5BU5D_t85032E841FA7033B896FB52B489D4CE5E2266FB3* L_2 = __this->get_m_FrameTiming_0();
		NullCheck(L_2);
		double L_3 = ((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_gpuFrameTime_3();
		V_0 = L_3;
		// if (gpuFrameTime > 0.0)
		double L_4 = V_0;
		if ((!(((double)L_4) > ((double)(0.0)))))
		{
			goto IL_003a;
		}
	}
	{
		// return (float)(gpuFrameTime * 0.001);
		double L_5 = V_0;
		return (((float)((float)((double)il2cpp_codegen_multiply((double)L_5, (double)(0.001))))));
	}

IL_003a:
	{
		// return -1.0f;
		return (-1.0f);
	}
}
// System.Void UnityEngine.AdaptivePerformance.GpuTimeProvider::Measure()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GpuTimeProvider_Measure_mF6BF86D1707BD5E8700A0A6F2D0D09C39B74E1B8 (GpuTimeProvider_t3391C7594EF71A215E3EA00D39E894E5FAA45B2B * __this, const RuntimeMethod* method)
{
	{
		// UnityEngine.FrameTimingManager.CaptureFrameTimings();
		FrameTimingManager_CaptureFrameTimings_m1816EB99EFF92F9394E7000A9CB1F9F9363A90F5(/*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.Holder::set_Instance(UnityEngine.AdaptivePerformance.IAdaptivePerformance)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Holder_set_Instance_m2786C3B74384A62BD2997F76802455BEAD572CAE (RuntimeObject* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Holder_set_Instance_m2786C3B74384A62BD2997F76802455BEAD572CAE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public IAdaptivePerformance Instance { get; internal set; }
		RuntimeObject* L_0 = ___value0;
		((Holder_t3A26EE4375105172B35CF838E774E2DB72D06DF9_StaticFields*)il2cpp_codegen_static_fields_for(Holder_t3A26EE4375105172B35CF838E774E2DB72D06DF9_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single UnityEngine.AdaptivePerformance.MainThreadCpuTime::GetLatestResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainThreadCpuTime_GetLatestResult_m5E3D72F4A7FBEDD957340CDAB3E19A128269D5B6 (MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * __this, const RuntimeMethod* method)
{
	{
		// return m_LatestMainthreadCpuTime;
		float L_0 = __this->get_m_LatestMainthreadCpuTime_1();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.MainThreadCpuTime::Measure()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainThreadCpuTime_Measure_m7AE49D83360B7818C0FC957250096E4788DF7EA7 (MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * __this, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	{
		// double cpuTime = 0.0;
		V_0 = (0.0);
		// if (cpuTime > 0.0)
		double L_0 = V_0;
		if ((!(((double)L_0) > ((double)(0.0)))))
		{
			goto IL_002e;
		}
	}
	{
		// double dt = cpuTime - m_LastAbsoluteMainThreadCpuTime;
		double L_1 = V_0;
		double L_2 = __this->get_m_LastAbsoluteMainThreadCpuTime_0();
		V_1 = ((double)il2cpp_codegen_subtract((double)L_1, (double)L_2));
		// m_LastAbsoluteMainThreadCpuTime = cpuTime;
		double L_3 = V_0;
		__this->set_m_LastAbsoluteMainThreadCpuTime_0(L_3);
		// m_LatestMainthreadCpuTime = (float)dt;
		double L_4 = V_1;
		__this->set_m_LatestMainthreadCpuTime_1((((float)((float)L_4))));
	}

IL_002e:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.MainThreadCpuTime::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainThreadCpuTime__ctor_m9600A6AD4493C006BBF6A29C876BED4CD42892E2 (MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * __this, const RuntimeMethod* method)
{
	{
		// private float m_LatestMainthreadCpuTime = -1.0f;
		__this->set_m_LatestMainthreadCpuTime_1((-1.0f));
		// public MainThreadCpuTime()
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.AdaptivePerformance.PerformanceBottleneck UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs::get_PerformanceBottleneck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PerformanceBottleneckChangeEventArgs_get_PerformanceBottleneck_m625E64FC86EF5C7DD4BEECE772F34AF22C07E4BD (PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 * __this, const RuntimeMethod* method)
{
	{
		// public PerformanceBottleneck PerformanceBottleneck { get; set; }
		int32_t L_0 = __this->get_U3CPerformanceBottleneckU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t PerformanceBottleneckChangeEventArgs_get_PerformanceBottleneck_m625E64FC86EF5C7DD4BEECE772F34AF22C07E4BD_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 * _thisAdjusted = reinterpret_cast<PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 *>(__this + _offset);
	return PerformanceBottleneckChangeEventArgs_get_PerformanceBottleneck_m625E64FC86EF5C7DD4BEECE772F34AF22C07E4BD_inline(_thisAdjusted, method);
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs::set_PerformanceBottleneck(UnityEngine.AdaptivePerformance.PerformanceBottleneck)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceBottleneckChangeEventArgs_set_PerformanceBottleneck_m63560EBBBEA19F0D26251C884A8CEE5FD3C07B03 (PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public PerformanceBottleneck PerformanceBottleneck { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CPerformanceBottleneckU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceBottleneckChangeEventArgs_set_PerformanceBottleneck_m63560EBBBEA19F0D26251C884A8CEE5FD3C07B03_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 * _thisAdjusted = reinterpret_cast<PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 *>(__this + _offset);
	PerformanceBottleneckChangeEventArgs_set_PerformanceBottleneck_m63560EBBBEA19F0D26251C884A8CEE5FD3C07B03_inline(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 (PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * __this, PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  ___bottleneckEventArgs0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___bottleneckEventArgs0);

}
// System.Void UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceBottleneckChangeHandler__ctor_mD8934C37B86DA79AB1370FF2DFA1843619EE6527 (PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler::Invoke(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceBottleneckChangeHandler_Invoke_m503AB2767CF3A258FDE85919B7BBC4A91EC1C755 (PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * __this, PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  ___bottleneckEventArgs0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___bottleneckEventArgs0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___bottleneckEventArgs0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___bottleneckEventArgs0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  >::Invoke(targetMethod, targetThis, ___bottleneckEventArgs0);
					else
						GenericVirtActionInvoker1< PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  >::Invoke(targetMethod, targetThis, ___bottleneckEventArgs0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___bottleneckEventArgs0);
					else
						VirtActionInvoker1< PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___bottleneckEventArgs0);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(&___bottleneckEventArgs0) - 1), targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___bottleneckEventArgs0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler::BeginInvoke(UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeEventArgs,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PerformanceBottleneckChangeHandler_BeginInvoke_mB184FBFDB47EC724127A67CBDF829AA32A6829AE (PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * __this, PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1  ___bottleneckEventArgs0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PerformanceBottleneckChangeHandler_BeginInvoke_mB184FBFDB47EC724127A67CBDF829AA32A6829AE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1_il2cpp_TypeInfo_var, &___bottleneckEventArgs0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceBottleneckChangeHandler::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceBottleneckChangeHandler_EndInvoke_m74470A9EE84AACAD66EF2D2F2773F52C03647335 (PerformanceBottleneckChangeHandler_t4A3E3D4B82B506DCCAD952D18E0A73A3107D5995 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs
IL2CPP_EXTERN_C void PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshal_pinvoke(const PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB& unmarshaled, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_pinvoke& marshaled)
{
	marshaled.___U3CCpuLevelU3Ek__BackingField_0 = unmarshaled.get_U3CCpuLevelU3Ek__BackingField_0();
	marshaled.___U3CCpuLevelDeltaU3Ek__BackingField_1 = unmarshaled.get_U3CCpuLevelDeltaU3Ek__BackingField_1();
	marshaled.___U3CGpuLevelU3Ek__BackingField_2 = unmarshaled.get_U3CGpuLevelU3Ek__BackingField_2();
	marshaled.___U3CGpuLevelDeltaU3Ek__BackingField_3 = unmarshaled.get_U3CGpuLevelDeltaU3Ek__BackingField_3();
	marshaled.___U3CPerformanceControlModeU3Ek__BackingField_4 = unmarshaled.get_U3CPerformanceControlModeU3Ek__BackingField_4();
	marshaled.___U3CManualOverrideU3Ek__BackingField_5 = static_cast<int32_t>(unmarshaled.get_U3CManualOverrideU3Ek__BackingField_5());
}
IL2CPP_EXTERN_C void PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshal_pinvoke_back(const PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_pinvoke& marshaled, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB& unmarshaled)
{
	int32_t unmarshaled_U3CCpuLevelU3Ek__BackingField_temp_0 = 0;
	unmarshaled_U3CCpuLevelU3Ek__BackingField_temp_0 = marshaled.___U3CCpuLevelU3Ek__BackingField_0;
	unmarshaled.set_U3CCpuLevelU3Ek__BackingField_0(unmarshaled_U3CCpuLevelU3Ek__BackingField_temp_0);
	int32_t unmarshaled_U3CCpuLevelDeltaU3Ek__BackingField_temp_1 = 0;
	unmarshaled_U3CCpuLevelDeltaU3Ek__BackingField_temp_1 = marshaled.___U3CCpuLevelDeltaU3Ek__BackingField_1;
	unmarshaled.set_U3CCpuLevelDeltaU3Ek__BackingField_1(unmarshaled_U3CCpuLevelDeltaU3Ek__BackingField_temp_1);
	int32_t unmarshaled_U3CGpuLevelU3Ek__BackingField_temp_2 = 0;
	unmarshaled_U3CGpuLevelU3Ek__BackingField_temp_2 = marshaled.___U3CGpuLevelU3Ek__BackingField_2;
	unmarshaled.set_U3CGpuLevelU3Ek__BackingField_2(unmarshaled_U3CGpuLevelU3Ek__BackingField_temp_2);
	int32_t unmarshaled_U3CGpuLevelDeltaU3Ek__BackingField_temp_3 = 0;
	unmarshaled_U3CGpuLevelDeltaU3Ek__BackingField_temp_3 = marshaled.___U3CGpuLevelDeltaU3Ek__BackingField_3;
	unmarshaled.set_U3CGpuLevelDeltaU3Ek__BackingField_3(unmarshaled_U3CGpuLevelDeltaU3Ek__BackingField_temp_3);
	int32_t unmarshaled_U3CPerformanceControlModeU3Ek__BackingField_temp_4 = 0;
	unmarshaled_U3CPerformanceControlModeU3Ek__BackingField_temp_4 = marshaled.___U3CPerformanceControlModeU3Ek__BackingField_4;
	unmarshaled.set_U3CPerformanceControlModeU3Ek__BackingField_4(unmarshaled_U3CPerformanceControlModeU3Ek__BackingField_temp_4);
	bool unmarshaled_U3CManualOverrideU3Ek__BackingField_temp_5 = false;
	unmarshaled_U3CManualOverrideU3Ek__BackingField_temp_5 = static_cast<bool>(marshaled.___U3CManualOverrideU3Ek__BackingField_5);
	unmarshaled.set_U3CManualOverrideU3Ek__BackingField_5(unmarshaled_U3CManualOverrideU3Ek__BackingField_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs
IL2CPP_EXTERN_C void PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshal_pinvoke_cleanup(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs
IL2CPP_EXTERN_C void PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshal_com(const PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB& unmarshaled, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_com& marshaled)
{
	marshaled.___U3CCpuLevelU3Ek__BackingField_0 = unmarshaled.get_U3CCpuLevelU3Ek__BackingField_0();
	marshaled.___U3CCpuLevelDeltaU3Ek__BackingField_1 = unmarshaled.get_U3CCpuLevelDeltaU3Ek__BackingField_1();
	marshaled.___U3CGpuLevelU3Ek__BackingField_2 = unmarshaled.get_U3CGpuLevelU3Ek__BackingField_2();
	marshaled.___U3CGpuLevelDeltaU3Ek__BackingField_3 = unmarshaled.get_U3CGpuLevelDeltaU3Ek__BackingField_3();
	marshaled.___U3CPerformanceControlModeU3Ek__BackingField_4 = unmarshaled.get_U3CPerformanceControlModeU3Ek__BackingField_4();
	marshaled.___U3CManualOverrideU3Ek__BackingField_5 = static_cast<int32_t>(unmarshaled.get_U3CManualOverrideU3Ek__BackingField_5());
}
IL2CPP_EXTERN_C void PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshal_com_back(const PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_com& marshaled, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB& unmarshaled)
{
	int32_t unmarshaled_U3CCpuLevelU3Ek__BackingField_temp_0 = 0;
	unmarshaled_U3CCpuLevelU3Ek__BackingField_temp_0 = marshaled.___U3CCpuLevelU3Ek__BackingField_0;
	unmarshaled.set_U3CCpuLevelU3Ek__BackingField_0(unmarshaled_U3CCpuLevelU3Ek__BackingField_temp_0);
	int32_t unmarshaled_U3CCpuLevelDeltaU3Ek__BackingField_temp_1 = 0;
	unmarshaled_U3CCpuLevelDeltaU3Ek__BackingField_temp_1 = marshaled.___U3CCpuLevelDeltaU3Ek__BackingField_1;
	unmarshaled.set_U3CCpuLevelDeltaU3Ek__BackingField_1(unmarshaled_U3CCpuLevelDeltaU3Ek__BackingField_temp_1);
	int32_t unmarshaled_U3CGpuLevelU3Ek__BackingField_temp_2 = 0;
	unmarshaled_U3CGpuLevelU3Ek__BackingField_temp_2 = marshaled.___U3CGpuLevelU3Ek__BackingField_2;
	unmarshaled.set_U3CGpuLevelU3Ek__BackingField_2(unmarshaled_U3CGpuLevelU3Ek__BackingField_temp_2);
	int32_t unmarshaled_U3CGpuLevelDeltaU3Ek__BackingField_temp_3 = 0;
	unmarshaled_U3CGpuLevelDeltaU3Ek__BackingField_temp_3 = marshaled.___U3CGpuLevelDeltaU3Ek__BackingField_3;
	unmarshaled.set_U3CGpuLevelDeltaU3Ek__BackingField_3(unmarshaled_U3CGpuLevelDeltaU3Ek__BackingField_temp_3);
	int32_t unmarshaled_U3CPerformanceControlModeU3Ek__BackingField_temp_4 = 0;
	unmarshaled_U3CPerformanceControlModeU3Ek__BackingField_temp_4 = marshaled.___U3CPerformanceControlModeU3Ek__BackingField_4;
	unmarshaled.set_U3CPerformanceControlModeU3Ek__BackingField_4(unmarshaled_U3CPerformanceControlModeU3Ek__BackingField_temp_4);
	bool unmarshaled_U3CManualOverrideU3Ek__BackingField_temp_5 = false;
	unmarshaled_U3CManualOverrideU3Ek__BackingField_temp_5 = static_cast<bool>(marshaled.___U3CManualOverrideU3Ek__BackingField_5);
	unmarshaled.set_U3CManualOverrideU3Ek__BackingField_5(unmarshaled_U3CManualOverrideU3Ek__BackingField_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs
IL2CPP_EXTERN_C void PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshal_com_cleanup(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::set_CpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_CpuLevel_m844481992806A1B152181FA8631B6033003CD640 (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCpuLevelU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceLevelChangeEventArgs_set_CpuLevel_m844481992806A1B152181FA8631B6033003CD640_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * _thisAdjusted = reinterpret_cast<PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *>(__this + _offset);
	PerformanceLevelChangeEventArgs_set_CpuLevel_m844481992806A1B152181FA8631B6033003CD640_inline(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::set_CpuLevelDelta(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_CpuLevelDelta_mDE58CBE256AE306670BE51649DE8895060860A77 (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CpuLevelDelta { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCpuLevelDeltaU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceLevelChangeEventArgs_set_CpuLevelDelta_mDE58CBE256AE306670BE51649DE8895060860A77_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * _thisAdjusted = reinterpret_cast<PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *>(__this + _offset);
	PerformanceLevelChangeEventArgs_set_CpuLevelDelta_mDE58CBE256AE306670BE51649DE8895060860A77_inline(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::set_GpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_GpuLevel_mBF3785C7E3C4FDEBF29E2E8CE640582658A6BE91 (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int GpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CGpuLevelU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceLevelChangeEventArgs_set_GpuLevel_mBF3785C7E3C4FDEBF29E2E8CE640582658A6BE91_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * _thisAdjusted = reinterpret_cast<PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *>(__this + _offset);
	PerformanceLevelChangeEventArgs_set_GpuLevel_mBF3785C7E3C4FDEBF29E2E8CE640582658A6BE91_inline(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::set_GpuLevelDelta(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_GpuLevelDelta_mBBD8DC5572E5FC9EEA8F67AB5995121010F09532 (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int GpuLevelDelta { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CGpuLevelDeltaU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceLevelChangeEventArgs_set_GpuLevelDelta_mBBD8DC5572E5FC9EEA8F67AB5995121010F09532_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * _thisAdjusted = reinterpret_cast<PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *>(__this + _offset);
	PerformanceLevelChangeEventArgs_set_GpuLevelDelta_mBBD8DC5572E5FC9EEA8F67AB5995121010F09532_inline(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::set_PerformanceControlMode(UnityEngine.AdaptivePerformance.PerformanceControlMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_PerformanceControlMode_mE4A2CDE249A57590CA4CD493ACA4DEEC745E1093 (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public PerformanceControlMode PerformanceControlMode { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CPerformanceControlModeU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceLevelChangeEventArgs_set_PerformanceControlMode_mE4A2CDE249A57590CA4CD493ACA4DEEC745E1093_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * _thisAdjusted = reinterpret_cast<PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *>(__this + _offset);
	PerformanceLevelChangeEventArgs_set_PerformanceControlMode_mE4A2CDE249A57590CA4CD493ACA4DEEC745E1093_inline(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs::set_ManualOverride(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_ManualOverride_mF3CD9BE22E685E48E56D4974302749BA02F2A1FD (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool ManualOverride { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CManualOverrideU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceLevelChangeEventArgs_set_ManualOverride_mF3CD9BE22E685E48E56D4974302749BA02F2A1FD_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * _thisAdjusted = reinterpret_cast<PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB *>(__this + _offset);
	PerformanceLevelChangeEventArgs_set_ManualOverride_mF3CD9BE22E685E48E56D4974302749BA02F2A1FD_inline(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 (PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * __this, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  ___levelChangeEventArgs0, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc)(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___levelChangeEventArgs0' to native representation
	PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshaled_pinvoke ____levelChangeEventArgs0_marshaled = {};
	PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshal_pinvoke(___levelChangeEventArgs0, ____levelChangeEventArgs0_marshaled);

	// Native function invocation
	il2cppPInvokeFunc(____levelChangeEventArgs0_marshaled);

	// Marshaling cleanup of parameter '___levelChangeEventArgs0' native representation
	PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_marshal_pinvoke_cleanup(____levelChangeEventArgs0_marshaled);

}
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceLevelChangeHandler__ctor_mE8D5A7F6C1FBB34E10D5D240F20870D6B8188D17 (PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeHandler::Invoke(UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceLevelChangeHandler_Invoke_mC0AAD0689A73E5A05C9AA0CFFB6D079D9B3A491E (PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * __this, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  ___levelChangeEventArgs0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___levelChangeEventArgs0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___levelChangeEventArgs0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___levelChangeEventArgs0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  >::Invoke(targetMethod, targetThis, ___levelChangeEventArgs0);
					else
						GenericVirtActionInvoker1< PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  >::Invoke(targetMethod, targetThis, ___levelChangeEventArgs0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___levelChangeEventArgs0);
					else
						VirtActionInvoker1< PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___levelChangeEventArgs0);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(&___levelChangeEventArgs0) - 1), targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___levelChangeEventArgs0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult UnityEngine.AdaptivePerformance.PerformanceLevelChangeHandler::BeginInvoke(UnityEngine.AdaptivePerformance.PerformanceLevelChangeEventArgs,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PerformanceLevelChangeHandler_BeginInvoke_m15EF7D19D4BC0C7C0CB90A5AC4AF594FDF1277F4 (PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * __this, PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB  ___levelChangeEventArgs0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PerformanceLevelChangeHandler_BeginInvoke_m15EF7D19D4BC0C7C0CB90A5AC4AF594FDF1277F4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB_il2cpp_TypeInfo_var, &___levelChangeEventArgs0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceLevelChangeHandler::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceLevelChangeHandler_EndInvoke_m72D7610F75B27710C4848ACC47157A86790B4A01 (PerformanceLevelChangeHandler_t8B845CEF62EC978A869CC5969E303D886C27A301 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.PerformanceMetrics::set_CurrentCpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceMetrics_set_CurrentCpuLevel_mB0FFC61EEA0C4462338C8E728533FE48D4364D9A (PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CurrentCpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentCpuLevelU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceMetrics_set_CurrentCpuLevel_mB0FFC61EEA0C4462338C8E728533FE48D4364D9A_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * _thisAdjusted = reinterpret_cast<PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *>(__this + _offset);
	PerformanceMetrics_set_CurrentCpuLevel_mB0FFC61EEA0C4462338C8E728533FE48D4364D9A_inline(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceMetrics::set_CurrentGpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceMetrics_set_CurrentGpuLevel_m2807D7F1C39CF7C72DCA9CFDC0BB6F90AA2CE326 (PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CurrentGpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentGpuLevelU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceMetrics_set_CurrentGpuLevel_m2807D7F1C39CF7C72DCA9CFDC0BB6F90AA2CE326_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * _thisAdjusted = reinterpret_cast<PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *>(__this + _offset);
	PerformanceMetrics_set_CurrentGpuLevel_m2807D7F1C39CF7C72DCA9CFDC0BB6F90AA2CE326_inline(_thisAdjusted, ___value0, method);
}
// UnityEngine.AdaptivePerformance.PerformanceBottleneck UnityEngine.AdaptivePerformance.PerformanceMetrics::get_PerformanceBottleneck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PerformanceMetrics_get_PerformanceBottleneck_m518B78FAA9F20BD9F79F3EAFCE7CC81612F55CC6 (PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * __this, const RuntimeMethod* method)
{
	{
		// public PerformanceBottleneck PerformanceBottleneck { get; set; }
		int32_t L_0 = __this->get_U3CPerformanceBottleneckU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t PerformanceMetrics_get_PerformanceBottleneck_m518B78FAA9F20BD9F79F3EAFCE7CC81612F55CC6_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * _thisAdjusted = reinterpret_cast<PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *>(__this + _offset);
	return PerformanceMetrics_get_PerformanceBottleneck_m518B78FAA9F20BD9F79F3EAFCE7CC81612F55CC6_inline(_thisAdjusted, method);
}
// System.Void UnityEngine.AdaptivePerformance.PerformanceMetrics::set_PerformanceBottleneck(UnityEngine.AdaptivePerformance.PerformanceBottleneck)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceMetrics_set_PerformanceBottleneck_mFFE12B2A0C07877C363A05EF1F48B4C9BA27D268 (PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public PerformanceBottleneck PerformanceBottleneck { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CPerformanceBottleneckU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceMetrics_set_PerformanceBottleneck_mFFE12B2A0C07877C363A05EF1F48B4C9BA27D268_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * _thisAdjusted = reinterpret_cast<PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F *>(__this + _offset);
	PerformanceMetrics_set_PerformanceBottleneck_mFFE12B2A0C07877C363A05EF1F48B4C9BA27D268_inline(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceSubsystem__ctor_m5A151079573ED0225D9FD5F8DA15204458E0BDC5 (AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * __this, const RuntimeMethod* method)
{
	{
		// protected AdaptivePerformanceSubsystem()
		AdaptivePerformanceSubsystemBase__ctor_m28396E884C7A5F5ACD2BBF34FE6039698AB2B106(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.AdaptivePerformance.Provider.Feature UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem::get_Capabilities()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AdaptivePerformanceSubsystem_get_Capabilities_m42EEFCDECCBB0DFFEBE3E65BAA42ED01256FB18F (AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * __this, const RuntimeMethod* method)
{
	{
		// public Feature Capabilities { get; protected set; }
		int32_t L_0 = __this->get_U3CCapabilitiesU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem::set_Capabilities(UnityEngine.AdaptivePerformance.Provider.Feature)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceSubsystem_set_Capabilities_m6ACAED2EA7E44E3AF2D8AEB25A42EFA82EFB67F3 (AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public Feature Capabilities { get; protected set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCapabilitiesU3Ek__BackingField_2(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemBase::get_initialized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AdaptivePerformanceSubsystemBase_get_initialized_mCF8947352D834034868007E91F84485171F40ACA (AdaptivePerformanceSubsystemBase_tFE4445DE6A7F0520A1707ECCC71C516E3E4FB12E * __this, const RuntimeMethod* method)
{
	{
		// public bool initialized { get; protected set; }
		bool L_0 = __this->get_U3CinitializedU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemBase::set_initialized(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceSubsystemBase_set_initialized_mCF60B3BF86B030D232CAC0EBE07AE58449808B0C (AdaptivePerformanceSubsystemBase_tFE4445DE6A7F0520A1707ECCC71C516E3E4FB12E * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool initialized { get; protected set; }
		bool L_0 = ___value0;
		__this->set_U3CinitializedU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptivePerformanceSubsystemBase__ctor_m28396E884C7A5F5ACD2BBF34FE6039698AB2B106 (AdaptivePerformanceSubsystemBase_tFE4445DE6A7F0520A1707ECCC71C516E3E4FB12E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceSubsystemBase__ctor_m28396E884C7A5F5ACD2BBF34FE6039698AB2B106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Subsystem_1__ctor_mFC93A7FA2C0A92CDF2FBC703D15FC3E890A71D8C(__this, /*hidden argument*/Subsystem_1__ctor_mFC93A7FA2C0A92CDF2FBC703D15FC3E890A71D8C_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor/Cinfo
IL2CPP_EXTERN_C void Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshal_pinvoke(const Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112& unmarshaled, Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshaled_pinvoke& marshaled)
{
	Exception_t* ___U3CsubsystemImplementationTypeU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<subsystemImplementationType>k__BackingField' of type 'Cinfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsubsystemImplementationTypeU3Ek__BackingField_1Exception, NULL);
}
IL2CPP_EXTERN_C void Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshal_pinvoke_back(const Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshaled_pinvoke& marshaled, Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112& unmarshaled)
{
	Exception_t* ___U3CsubsystemImplementationTypeU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<subsystemImplementationType>k__BackingField' of type 'Cinfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsubsystemImplementationTypeU3Ek__BackingField_1Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor/Cinfo
IL2CPP_EXTERN_C void Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshal_pinvoke_cleanup(Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor/Cinfo
IL2CPP_EXTERN_C void Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshal_com(const Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112& unmarshaled, Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshaled_com& marshaled)
{
	Exception_t* ___U3CsubsystemImplementationTypeU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<subsystemImplementationType>k__BackingField' of type 'Cinfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsubsystemImplementationTypeU3Ek__BackingField_1Exception, NULL);
}
IL2CPP_EXTERN_C void Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshal_com_back(const Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshaled_com& marshaled, Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112& unmarshaled)
{
	Exception_t* ___U3CsubsystemImplementationTypeU3Ek__BackingField_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<subsystemImplementationType>k__BackingField' of type 'Cinfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsubsystemImplementationTypeU3Ek__BackingField_1Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor/Cinfo
IL2CPP_EXTERN_C void Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshal_com_cleanup(Cinfo_tE146A4FB93AA68449DE61F6D7D96C6D441A90112_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.List`1<UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemDescriptor> UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystemRegistry::GetRegisteredDescriptors()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * AdaptivePerformanceSubsystemRegistry_GetRegisteredDescriptors_mA1D2C28ABC171D8FB88C9EEC96BD63A303456604 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptivePerformanceSubsystemRegistry_GetRegisteredDescriptors_mA1D2C28ABC171D8FB88C9EEC96BD63A303456604_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var perfDescriptors = new List<AdaptivePerformanceSubsystemDescriptor>();
		List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * L_0 = (List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC *)il2cpp_codegen_object_new(List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC_il2cpp_TypeInfo_var);
		List_1__ctor_m09E20FB8F2F5E707BB0FCDB5B21388DDDA9A7471(L_0, /*hidden argument*/List_1__ctor_m09E20FB8F2F5E707BB0FCDB5B21388DDDA9A7471_RuntimeMethod_var);
		// SubsystemManager.GetSubsystemDescriptors<AdaptivePerformanceSubsystemDescriptor>(perfDescriptors);
		List_1_tCE222DE01D144D1E7F344AE8D783E553D3B436DC * L_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(SubsystemManager_tFEDEC70DC4119830C96B42915123C27FEDDB0F58_il2cpp_TypeInfo_var);
		SubsystemManager_GetSubsystemDescriptors_TisAdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55_m32243E1D766B4AEEF8E2E0EF3A161E83A7AC1B15(L_1, /*hidden argument*/SubsystemManager_GetSubsystemDescriptors_TisAdaptivePerformanceSubsystemDescriptor_tC3EC21FE335D3D94228000D40FFE97E331B76F55_m32243E1D766B4AEEF8E2E0EF3A161E83A7AC1B15_RuntimeMethod_var);
		// return perfDescriptors;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord
IL2CPP_EXTERN_C void PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshal_pinvoke(const PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF& unmarshaled, PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshaled_pinvoke& marshaled)
{
	marshaled.___U3CChangeFlagsU3Ek__BackingField_0 = unmarshaled.get_U3CChangeFlagsU3Ek__BackingField_0();
	marshaled.___U3CTemperatureLevelU3Ek__BackingField_1 = unmarshaled.get_U3CTemperatureLevelU3Ek__BackingField_1();
	marshaled.___U3CTemperatureTrendU3Ek__BackingField_2 = unmarshaled.get_U3CTemperatureTrendU3Ek__BackingField_2();
	marshaled.___U3CWarningLevelU3Ek__BackingField_3 = unmarshaled.get_U3CWarningLevelU3Ek__BackingField_3();
	marshaled.___U3CCpuPerformanceLevelU3Ek__BackingField_4 = unmarshaled.get_U3CCpuPerformanceLevelU3Ek__BackingField_4();
	marshaled.___U3CGpuPerformanceLevelU3Ek__BackingField_5 = unmarshaled.get_U3CGpuPerformanceLevelU3Ek__BackingField_5();
	marshaled.___U3CPerformanceLevelControlAvailableU3Ek__BackingField_6 = static_cast<int32_t>(unmarshaled.get_U3CPerformanceLevelControlAvailableU3Ek__BackingField_6());
	marshaled.___U3CCpuFrameTimeU3Ek__BackingField_7 = unmarshaled.get_U3CCpuFrameTimeU3Ek__BackingField_7();
	marshaled.___U3CGpuFrameTimeU3Ek__BackingField_8 = unmarshaled.get_U3CGpuFrameTimeU3Ek__BackingField_8();
	marshaled.___U3COverallFrameTimeU3Ek__BackingField_9 = unmarshaled.get_U3COverallFrameTimeU3Ek__BackingField_9();
}
IL2CPP_EXTERN_C void PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshal_pinvoke_back(const PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshaled_pinvoke& marshaled, PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF& unmarshaled)
{
	int32_t unmarshaled_U3CChangeFlagsU3Ek__BackingField_temp_0 = 0;
	unmarshaled_U3CChangeFlagsU3Ek__BackingField_temp_0 = marshaled.___U3CChangeFlagsU3Ek__BackingField_0;
	unmarshaled.set_U3CChangeFlagsU3Ek__BackingField_0(unmarshaled_U3CChangeFlagsU3Ek__BackingField_temp_0);
	float unmarshaled_U3CTemperatureLevelU3Ek__BackingField_temp_1 = 0.0f;
	unmarshaled_U3CTemperatureLevelU3Ek__BackingField_temp_1 = marshaled.___U3CTemperatureLevelU3Ek__BackingField_1;
	unmarshaled.set_U3CTemperatureLevelU3Ek__BackingField_1(unmarshaled_U3CTemperatureLevelU3Ek__BackingField_temp_1);
	float unmarshaled_U3CTemperatureTrendU3Ek__BackingField_temp_2 = 0.0f;
	unmarshaled_U3CTemperatureTrendU3Ek__BackingField_temp_2 = marshaled.___U3CTemperatureTrendU3Ek__BackingField_2;
	unmarshaled.set_U3CTemperatureTrendU3Ek__BackingField_2(unmarshaled_U3CTemperatureTrendU3Ek__BackingField_temp_2);
	int32_t unmarshaled_U3CWarningLevelU3Ek__BackingField_temp_3 = 0;
	unmarshaled_U3CWarningLevelU3Ek__BackingField_temp_3 = marshaled.___U3CWarningLevelU3Ek__BackingField_3;
	unmarshaled.set_U3CWarningLevelU3Ek__BackingField_3(unmarshaled_U3CWarningLevelU3Ek__BackingField_temp_3);
	int32_t unmarshaled_U3CCpuPerformanceLevelU3Ek__BackingField_temp_4 = 0;
	unmarshaled_U3CCpuPerformanceLevelU3Ek__BackingField_temp_4 = marshaled.___U3CCpuPerformanceLevelU3Ek__BackingField_4;
	unmarshaled.set_U3CCpuPerformanceLevelU3Ek__BackingField_4(unmarshaled_U3CCpuPerformanceLevelU3Ek__BackingField_temp_4);
	int32_t unmarshaled_U3CGpuPerformanceLevelU3Ek__BackingField_temp_5 = 0;
	unmarshaled_U3CGpuPerformanceLevelU3Ek__BackingField_temp_5 = marshaled.___U3CGpuPerformanceLevelU3Ek__BackingField_5;
	unmarshaled.set_U3CGpuPerformanceLevelU3Ek__BackingField_5(unmarshaled_U3CGpuPerformanceLevelU3Ek__BackingField_temp_5);
	bool unmarshaled_U3CPerformanceLevelControlAvailableU3Ek__BackingField_temp_6 = false;
	unmarshaled_U3CPerformanceLevelControlAvailableU3Ek__BackingField_temp_6 = static_cast<bool>(marshaled.___U3CPerformanceLevelControlAvailableU3Ek__BackingField_6);
	unmarshaled.set_U3CPerformanceLevelControlAvailableU3Ek__BackingField_6(unmarshaled_U3CPerformanceLevelControlAvailableU3Ek__BackingField_temp_6);
	float unmarshaled_U3CCpuFrameTimeU3Ek__BackingField_temp_7 = 0.0f;
	unmarshaled_U3CCpuFrameTimeU3Ek__BackingField_temp_7 = marshaled.___U3CCpuFrameTimeU3Ek__BackingField_7;
	unmarshaled.set_U3CCpuFrameTimeU3Ek__BackingField_7(unmarshaled_U3CCpuFrameTimeU3Ek__BackingField_temp_7);
	float unmarshaled_U3CGpuFrameTimeU3Ek__BackingField_temp_8 = 0.0f;
	unmarshaled_U3CGpuFrameTimeU3Ek__BackingField_temp_8 = marshaled.___U3CGpuFrameTimeU3Ek__BackingField_8;
	unmarshaled.set_U3CGpuFrameTimeU3Ek__BackingField_8(unmarshaled_U3CGpuFrameTimeU3Ek__BackingField_temp_8);
	float unmarshaled_U3COverallFrameTimeU3Ek__BackingField_temp_9 = 0.0f;
	unmarshaled_U3COverallFrameTimeU3Ek__BackingField_temp_9 = marshaled.___U3COverallFrameTimeU3Ek__BackingField_9;
	unmarshaled.set_U3COverallFrameTimeU3Ek__BackingField_9(unmarshaled_U3COverallFrameTimeU3Ek__BackingField_temp_9);
}
// Conversion method for clean up from marshalling of: UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord
IL2CPP_EXTERN_C void PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshal_pinvoke_cleanup(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord
IL2CPP_EXTERN_C void PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshal_com(const PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF& unmarshaled, PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshaled_com& marshaled)
{
	marshaled.___U3CChangeFlagsU3Ek__BackingField_0 = unmarshaled.get_U3CChangeFlagsU3Ek__BackingField_0();
	marshaled.___U3CTemperatureLevelU3Ek__BackingField_1 = unmarshaled.get_U3CTemperatureLevelU3Ek__BackingField_1();
	marshaled.___U3CTemperatureTrendU3Ek__BackingField_2 = unmarshaled.get_U3CTemperatureTrendU3Ek__BackingField_2();
	marshaled.___U3CWarningLevelU3Ek__BackingField_3 = unmarshaled.get_U3CWarningLevelU3Ek__BackingField_3();
	marshaled.___U3CCpuPerformanceLevelU3Ek__BackingField_4 = unmarshaled.get_U3CCpuPerformanceLevelU3Ek__BackingField_4();
	marshaled.___U3CGpuPerformanceLevelU3Ek__BackingField_5 = unmarshaled.get_U3CGpuPerformanceLevelU3Ek__BackingField_5();
	marshaled.___U3CPerformanceLevelControlAvailableU3Ek__BackingField_6 = static_cast<int32_t>(unmarshaled.get_U3CPerformanceLevelControlAvailableU3Ek__BackingField_6());
	marshaled.___U3CCpuFrameTimeU3Ek__BackingField_7 = unmarshaled.get_U3CCpuFrameTimeU3Ek__BackingField_7();
	marshaled.___U3CGpuFrameTimeU3Ek__BackingField_8 = unmarshaled.get_U3CGpuFrameTimeU3Ek__BackingField_8();
	marshaled.___U3COverallFrameTimeU3Ek__BackingField_9 = unmarshaled.get_U3COverallFrameTimeU3Ek__BackingField_9();
}
IL2CPP_EXTERN_C void PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshal_com_back(const PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshaled_com& marshaled, PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF& unmarshaled)
{
	int32_t unmarshaled_U3CChangeFlagsU3Ek__BackingField_temp_0 = 0;
	unmarshaled_U3CChangeFlagsU3Ek__BackingField_temp_0 = marshaled.___U3CChangeFlagsU3Ek__BackingField_0;
	unmarshaled.set_U3CChangeFlagsU3Ek__BackingField_0(unmarshaled_U3CChangeFlagsU3Ek__BackingField_temp_0);
	float unmarshaled_U3CTemperatureLevelU3Ek__BackingField_temp_1 = 0.0f;
	unmarshaled_U3CTemperatureLevelU3Ek__BackingField_temp_1 = marshaled.___U3CTemperatureLevelU3Ek__BackingField_1;
	unmarshaled.set_U3CTemperatureLevelU3Ek__BackingField_1(unmarshaled_U3CTemperatureLevelU3Ek__BackingField_temp_1);
	float unmarshaled_U3CTemperatureTrendU3Ek__BackingField_temp_2 = 0.0f;
	unmarshaled_U3CTemperatureTrendU3Ek__BackingField_temp_2 = marshaled.___U3CTemperatureTrendU3Ek__BackingField_2;
	unmarshaled.set_U3CTemperatureTrendU3Ek__BackingField_2(unmarshaled_U3CTemperatureTrendU3Ek__BackingField_temp_2);
	int32_t unmarshaled_U3CWarningLevelU3Ek__BackingField_temp_3 = 0;
	unmarshaled_U3CWarningLevelU3Ek__BackingField_temp_3 = marshaled.___U3CWarningLevelU3Ek__BackingField_3;
	unmarshaled.set_U3CWarningLevelU3Ek__BackingField_3(unmarshaled_U3CWarningLevelU3Ek__BackingField_temp_3);
	int32_t unmarshaled_U3CCpuPerformanceLevelU3Ek__BackingField_temp_4 = 0;
	unmarshaled_U3CCpuPerformanceLevelU3Ek__BackingField_temp_4 = marshaled.___U3CCpuPerformanceLevelU3Ek__BackingField_4;
	unmarshaled.set_U3CCpuPerformanceLevelU3Ek__BackingField_4(unmarshaled_U3CCpuPerformanceLevelU3Ek__BackingField_temp_4);
	int32_t unmarshaled_U3CGpuPerformanceLevelU3Ek__BackingField_temp_5 = 0;
	unmarshaled_U3CGpuPerformanceLevelU3Ek__BackingField_temp_5 = marshaled.___U3CGpuPerformanceLevelU3Ek__BackingField_5;
	unmarshaled.set_U3CGpuPerformanceLevelU3Ek__BackingField_5(unmarshaled_U3CGpuPerformanceLevelU3Ek__BackingField_temp_5);
	bool unmarshaled_U3CPerformanceLevelControlAvailableU3Ek__BackingField_temp_6 = false;
	unmarshaled_U3CPerformanceLevelControlAvailableU3Ek__BackingField_temp_6 = static_cast<bool>(marshaled.___U3CPerformanceLevelControlAvailableU3Ek__BackingField_6);
	unmarshaled.set_U3CPerformanceLevelControlAvailableU3Ek__BackingField_6(unmarshaled_U3CPerformanceLevelControlAvailableU3Ek__BackingField_temp_6);
	float unmarshaled_U3CCpuFrameTimeU3Ek__BackingField_temp_7 = 0.0f;
	unmarshaled_U3CCpuFrameTimeU3Ek__BackingField_temp_7 = marshaled.___U3CCpuFrameTimeU3Ek__BackingField_7;
	unmarshaled.set_U3CCpuFrameTimeU3Ek__BackingField_7(unmarshaled_U3CCpuFrameTimeU3Ek__BackingField_temp_7);
	float unmarshaled_U3CGpuFrameTimeU3Ek__BackingField_temp_8 = 0.0f;
	unmarshaled_U3CGpuFrameTimeU3Ek__BackingField_temp_8 = marshaled.___U3CGpuFrameTimeU3Ek__BackingField_8;
	unmarshaled.set_U3CGpuFrameTimeU3Ek__BackingField_8(unmarshaled_U3CGpuFrameTimeU3Ek__BackingField_temp_8);
	float unmarshaled_U3COverallFrameTimeU3Ek__BackingField_temp_9 = 0.0f;
	unmarshaled_U3COverallFrameTimeU3Ek__BackingField_temp_9 = marshaled.___U3COverallFrameTimeU3Ek__BackingField_9;
	unmarshaled.set_U3COverallFrameTimeU3Ek__BackingField_9(unmarshaled_U3COverallFrameTimeU3Ek__BackingField_temp_9);
}
// Conversion method for clean up from marshalling of: UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord
IL2CPP_EXTERN_C void PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshal_com_cleanup(PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF_marshaled_com& marshaled)
{
}
// UnityEngine.AdaptivePerformance.Provider.Feature UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_ChangeFlags()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public Feature ChangeFlags { get; set; }
		int32_t L_0 = __this->get_U3CChangeFlagsU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	return PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline(_thisAdjusted, method);
}
// System.Void UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::set_ChangeFlags(UnityEngine.AdaptivePerformance.Provider.Feature)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceDataRecord_set_ChangeFlags_m637C86CD7C6A9D4766B6486C0D8CEDD4A0AB63B5 (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public Feature ChangeFlags { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CChangeFlagsU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceDataRecord_set_ChangeFlags_m637C86CD7C6A9D4766B6486C0D8CEDD4A0AB63B5_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	PerformanceDataRecord_set_ChangeFlags_m637C86CD7C6A9D4766B6486C0D8CEDD4A0AB63B5_inline(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_TemperatureLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_TemperatureLevel_m8ADD4135E271A910EB33ED6B113E6153A8B54CCE (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public float TemperatureLevel { get; set; }
		float L_0 = __this->get_U3CTemperatureLevelU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C  float PerformanceDataRecord_get_TemperatureLevel_m8ADD4135E271A910EB33ED6B113E6153A8B54CCE_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	return PerformanceDataRecord_get_TemperatureLevel_m8ADD4135E271A910EB33ED6B113E6153A8B54CCE_inline(_thisAdjusted, method);
}
// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_TemperatureTrend()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_TemperatureTrend_m65CC0E6927BE74931532ACB8DE6BAA1CBBF12231 (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public float TemperatureTrend { get; set; }
		float L_0 = __this->get_U3CTemperatureTrendU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C  float PerformanceDataRecord_get_TemperatureTrend_m65CC0E6927BE74931532ACB8DE6BAA1CBBF12231_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	return PerformanceDataRecord_get_TemperatureTrend_m65CC0E6927BE74931532ACB8DE6BAA1CBBF12231_inline(_thisAdjusted, method);
}
// UnityEngine.AdaptivePerformance.WarningLevel UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_WarningLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PerformanceDataRecord_get_WarningLevel_m8A58D1CDD83B9F86781080C042029E2D4B4795D7 (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public WarningLevel WarningLevel { get; set; }
		int32_t L_0 = __this->get_U3CWarningLevelU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t PerformanceDataRecord_get_WarningLevel_m8A58D1CDD83B9F86781080C042029E2D4B4795D7_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	return PerformanceDataRecord_get_WarningLevel_m8A58D1CDD83B9F86781080C042029E2D4B4795D7_inline(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_CpuPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PerformanceDataRecord_get_CpuPerformanceLevel_mAADDE6EE97531363BB97CBD7697B9B6D610DEE41 (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public int CpuPerformanceLevel { get; set; }
		int32_t L_0 = __this->get_U3CCpuPerformanceLevelU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t PerformanceDataRecord_get_CpuPerformanceLevel_mAADDE6EE97531363BB97CBD7697B9B6D610DEE41_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	return PerformanceDataRecord_get_CpuPerformanceLevel_mAADDE6EE97531363BB97CBD7697B9B6D610DEE41_inline(_thisAdjusted, method);
}
// System.Void UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::set_CpuPerformanceLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceDataRecord_set_CpuPerformanceLevel_m9B1602BD2C7D66ED154B2D2592667EB9A4270C8C (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CpuPerformanceLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCpuPerformanceLevelU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceDataRecord_set_CpuPerformanceLevel_m9B1602BD2C7D66ED154B2D2592667EB9A4270C8C_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	PerformanceDataRecord_set_CpuPerformanceLevel_m9B1602BD2C7D66ED154B2D2592667EB9A4270C8C_inline(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_GpuPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PerformanceDataRecord_get_GpuPerformanceLevel_mB1DD57E4C0ECDC9F74181D5E5CFC8265B52ED4B4 (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public int GpuPerformanceLevel { get; set; }
		int32_t L_0 = __this->get_U3CGpuPerformanceLevelU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t PerformanceDataRecord_get_GpuPerformanceLevel_mB1DD57E4C0ECDC9F74181D5E5CFC8265B52ED4B4_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	return PerformanceDataRecord_get_GpuPerformanceLevel_mB1DD57E4C0ECDC9F74181D5E5CFC8265B52ED4B4_inline(_thisAdjusted, method);
}
// System.Void UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::set_GpuPerformanceLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceDataRecord_set_GpuPerformanceLevel_mCFD2AA62DA519F272118E5019AE48405BD618E2B (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int GpuPerformanceLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CGpuPerformanceLevelU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceDataRecord_set_GpuPerformanceLevel_mCFD2AA62DA519F272118E5019AE48405BD618E2B_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	PerformanceDataRecord_set_GpuPerformanceLevel_mCFD2AA62DA519F272118E5019AE48405BD618E2B_inline(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_PerformanceLevelControlAvailable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PerformanceDataRecord_get_PerformanceLevelControlAvailable_mC2FDF55F921B13DC69CCC033C952A1D293FE4572 (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public bool PerformanceLevelControlAvailable { get; set; }
		bool L_0 = __this->get_U3CPerformanceLevelControlAvailableU3Ek__BackingField_6();
		return L_0;
	}
}
IL2CPP_EXTERN_C  bool PerformanceDataRecord_get_PerformanceLevelControlAvailable_mC2FDF55F921B13DC69CCC033C952A1D293FE4572_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	return PerformanceDataRecord_get_PerformanceLevelControlAvailable_mC2FDF55F921B13DC69CCC033C952A1D293FE4572_inline(_thisAdjusted, method);
}
// System.Void UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::set_PerformanceLevelControlAvailable(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerformanceDataRecord_set_PerformanceLevelControlAvailable_m365175DA2C8A6D9EE5D000E389958FDF6F2E23A1 (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool PerformanceLevelControlAvailable { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CPerformanceLevelControlAvailableU3Ek__BackingField_6(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void PerformanceDataRecord_set_PerformanceLevelControlAvailable_m365175DA2C8A6D9EE5D000E389958FDF6F2E23A1_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	PerformanceDataRecord_set_PerformanceLevelControlAvailable_m365175DA2C8A6D9EE5D000E389958FDF6F2E23A1_inline(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_CpuFrameTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_CpuFrameTime_m53BE7B6BA7C387D27C6B789F80487C19E48DD042 (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public float CpuFrameTime { get; set; }
		float L_0 = __this->get_U3CCpuFrameTimeU3Ek__BackingField_7();
		return L_0;
	}
}
IL2CPP_EXTERN_C  float PerformanceDataRecord_get_CpuFrameTime_m53BE7B6BA7C387D27C6B789F80487C19E48DD042_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	return PerformanceDataRecord_get_CpuFrameTime_m53BE7B6BA7C387D27C6B789F80487C19E48DD042_inline(_thisAdjusted, method);
}
// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_GpuFrameTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_GpuFrameTime_m50329D6AADDB092BE7BA0E9EAD569D4885774BB1 (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public float GpuFrameTime { get; set; }
		float L_0 = __this->get_U3CGpuFrameTimeU3Ek__BackingField_8();
		return L_0;
	}
}
IL2CPP_EXTERN_C  float PerformanceDataRecord_get_GpuFrameTime_m50329D6AADDB092BE7BA0E9EAD569D4885774BB1_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	return PerformanceDataRecord_get_GpuFrameTime_m50329D6AADDB092BE7BA0E9EAD569D4885774BB1_inline(_thisAdjusted, method);
}
// System.Single UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord::get_OverallFrameTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_OverallFrameTime_mD8BE680879118B05C2BFA66EC2F0BF8C69CC8C59 (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public float OverallFrameTime { get; set; }
		float L_0 = __this->get_U3COverallFrameTimeU3Ek__BackingField_9();
		return L_0;
	}
}
IL2CPP_EXTERN_C  float PerformanceDataRecord_get_OverallFrameTime_mD8BE680879118B05C2BFA66EC2F0BF8C69CC8C59_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * _thisAdjusted = reinterpret_cast<PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *>(__this + _offset);
	return PerformanceDataRecord_get_OverallFrameTime_mD8BE680879118B05C2BFA66EC2F0BF8C69CC8C59_inline(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::set_CpuPerformanceLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem_set_CpuPerformanceLevel_m59E29995AE25848FD051A770DA3D175A7F39098E (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// updateResult.CpuPerformanceLevel = value;
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * L_0 = __this->get_address_of_updateResult_3();
		int32_t L_1 = ___value0;
		PerformanceDataRecord_set_CpuPerformanceLevel_m9B1602BD2C7D66ED154B2D2592667EB9A4270C8C_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)L_0, L_1, /*hidden argument*/NULL);
		// updateResult.ChangeFlags |= Provider.Feature.CpuPerformanceLevel;
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * L_2 = __this->get_address_of_updateResult_3();
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * L_3 = L_2;
		int32_t L_4 = PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)L_3, /*hidden argument*/NULL);
		PerformanceDataRecord_set_ChangeFlags_m637C86CD7C6A9D4766B6486C0D8CEDD4A0AB63B5_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)L_3, ((int32_t)((int32_t)L_4|(int32_t)8)), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::set_GpuPerformanceLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem_set_GpuPerformanceLevel_mDB2374F91AB158C04869CC2C33F3F483EFE6FFBC (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// updateResult.GpuPerformanceLevel = value;
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * L_0 = __this->get_address_of_updateResult_3();
		int32_t L_1 = ___value0;
		PerformanceDataRecord_set_GpuPerformanceLevel_mCFD2AA62DA519F272118E5019AE48405BD618E2B_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)L_0, L_1, /*hidden argument*/NULL);
		// updateResult.ChangeFlags |= Provider.Feature.GpuPerformanceLevel;
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * L_2 = __this->get_address_of_updateResult_3();
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * L_3 = L_2;
		int32_t L_4 = PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)L_3, /*hidden argument*/NULL);
		PerformanceDataRecord_set_ChangeFlags_m637C86CD7C6A9D4766B6486C0D8CEDD4A0AB63B5_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)L_3, ((int32_t)((int32_t)L_4|(int32_t)((int32_t)16))), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::get_AcceptsPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TestAdaptivePerformanceSubsystem_get_AcceptsPerformanceLevel_m5611C1AE91C8116552A769005EA126ABAA38D89D (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, const RuntimeMethod* method)
{
	{
		// get { return updateResult.PerformanceLevelControlAvailable; }
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * L_0 = __this->get_address_of_updateResult_3();
		bool L_1 = PerformanceDataRecord_get_PerformanceLevelControlAvailable_mC2FDF55F921B13DC69CCC033C952A1D293FE4572_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::get_MaxCpuPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TestAdaptivePerformanceSubsystem_get_MaxCpuPerformanceLevel_m607B05A021EFFA9160E56FCBBE2520526ADBF88A (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, const RuntimeMethod* method)
{
	{
		// public int MaxCpuPerformanceLevel { get { return 4; } }
		return 4;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::get_MaxGpuPerformanceLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TestAdaptivePerformanceSubsystem_get_MaxGpuPerformanceLevel_mA0ACEFBE7A1697080A61BC518639D55893E065B5 (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, const RuntimeMethod* method)
{
	{
		// public int MaxGpuPerformanceLevel { get { return 2; } }
		return 2;
	}
}
// System.Void UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem__ctor_m501C19FE3805931F42E798F303E33477AD8AC7E3 (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, const RuntimeMethod* method)
{
	{
		// public TestAdaptivePerformanceSubsystem()
		AdaptivePerformanceSubsystem__ctor_m5A151079573ED0225D9FD5F8DA15204458E0BDC5(__this, /*hidden argument*/NULL);
		// Capabilities = Feature.CpuPerformanceLevel | Feature.GpuPerformanceLevel | Feature.PerformanceLevelControl |
		//     Feature.TemperatureLevel | Feature.WarningLevel | Feature.TemperatureTrend | Feature.CpuFrameTime | Feature.GpuFrameTime | Feature.OverallFrameTime;
		AdaptivePerformanceSubsystem_set_Capabilities_m6ACAED2EA7E44E3AF2D8AEB25A42EFA82EFB67F3_inline(__this, ((int32_t)511), /*hidden argument*/NULL);
		// updateResult.PerformanceLevelControlAvailable = true;
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * L_0 = __this->get_address_of_updateResult_3();
		PerformanceDataRecord_set_PerformanceLevelControlAvailable_m365175DA2C8A6D9EE5D000E389958FDF6F2E23A1_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)L_0, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem_Start_mD56DD4AA5CDF588D3A138C04D1DA6C99511D3302 (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, const RuntimeMethod* method)
{
	{
		// initialized = true;
		AdaptivePerformanceSubsystemBase_set_initialized_mCF60B3BF86B030D232CAC0EBE07AE58449808B0C_inline(__this, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem_OnDestroy_mBCFD1B203E11BF4EBDAE955608A74B18AEACB7BA (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, const RuntimeMethod* method)
{
	{
		// protected override void OnDestroy() {}
		return;
	}
}
// UnityEngine.AdaptivePerformance.Provider.PerformanceDataRecord UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF  TestAdaptivePerformanceSubsystem_Update_m2CA9F3AF71CD3331F10CF6F38C3441C3B3A7F9E8 (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, const RuntimeMethod* method)
{
	{
		// updateResult.ChangeFlags &= Capabilities;
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * L_0 = __this->get_address_of_updateResult_3();
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * L_1 = L_0;
		int32_t L_2 = PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)L_1, /*hidden argument*/NULL);
		int32_t L_3 = AdaptivePerformanceSubsystem_get_Capabilities_m42EEFCDECCBB0DFFEBE3E65BAA42ED01256FB18F_inline(__this, /*hidden argument*/NULL);
		PerformanceDataRecord_set_ChangeFlags_m637C86CD7C6A9D4766B6486C0D8CEDD4A0AB63B5_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)L_1, ((int32_t)((int32_t)L_2&(int32_t)L_3)), /*hidden argument*/NULL);
		// var result = updateResult;
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF  L_4 = __this->get_updateResult_3();
		// updateResult.ChangeFlags = Feature.None;
		PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * L_5 = __this->get_address_of_updateResult_3();
		PerformanceDataRecord_set_ChangeFlags_m637C86CD7C6A9D4766B6486C0D8CEDD4A0AB63B5_inline((PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF *)L_5, 0, /*hidden argument*/NULL);
		// return result;
		return L_4;
	}
}
// UnityEngine.AdaptivePerformance.Provider.IApplicationLifecycle UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::get_ApplicationLifecycle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TestAdaptivePerformanceSubsystem_get_ApplicationLifecycle_m11BB078ABCD25F53A4EB861E9890B7CB6F36ACC8 (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, const RuntimeMethod* method)
{
	{
		// public override IApplicationLifecycle ApplicationLifecycle { get { return null; } }
		return (RuntimeObject*)NULL;
	}
}
// UnityEngine.AdaptivePerformance.Provider.IDevicePerformanceLevelControl UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::get_PerformanceLevelControl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TestAdaptivePerformanceSubsystem_get_PerformanceLevelControl_mBE6C229991271152619D21A99BD8C4D1A5FC883A (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, const RuntimeMethod* method)
{
	{
		// public override IDevicePerformanceLevelControl PerformanceLevelControl { get { return this; } }
		return __this;
	}
}
// System.Void UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::set_LastRequestedCpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem_set_LastRequestedCpuLevel_m35EECCA0681188317D2619A80F6F3B64ABA5D592 (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int LastRequestedCpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CLastRequestedCpuLevelU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::set_LastRequestedGpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem_set_LastRequestedGpuLevel_m5FA5BDBAF556DE1F795CAC408AC077201BD34474 (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int LastRequestedGpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CLastRequestedGpuLevelU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.Provider.TestAdaptivePerformanceSubsystem::SetPerformanceLevel(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TestAdaptivePerformanceSubsystem_SetPerformanceLevel_m323D024AB90A4EA35173194438BED4E1315C6327 (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, int32_t ___cpuLevel0, int32_t ___gpuLevel1, const RuntimeMethod* method)
{
	{
		// LastRequestedCpuLevel = cpuLevel;
		int32_t L_0 = ___cpuLevel0;
		TestAdaptivePerformanceSubsystem_set_LastRequestedCpuLevel_m35EECCA0681188317D2619A80F6F3B64ABA5D592_inline(__this, L_0, /*hidden argument*/NULL);
		// LastRequestedGpuLevel = gpuLevel;
		int32_t L_1 = ___gpuLevel1;
		TestAdaptivePerformanceSubsystem_set_LastRequestedGpuLevel_m5FA5BDBAF556DE1F795CAC408AC077201BD34474_inline(__this, L_1, /*hidden argument*/NULL);
		// if (!AcceptsPerformanceLevel)
		bool L_2 = TestAdaptivePerformanceSubsystem_get_AcceptsPerformanceLevel_m5611C1AE91C8116552A769005EA126ABAA38D89D(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0026;
		}
	}
	{
		// CpuPerformanceLevel = AdaptivePerformance.Constants.UnknownPerformanceLevel;
		TestAdaptivePerformanceSubsystem_set_CpuPerformanceLevel_m59E29995AE25848FD051A770DA3D175A7F39098E(__this, (-1), /*hidden argument*/NULL);
		// GpuPerformanceLevel = AdaptivePerformance.Constants.UnknownPerformanceLevel;
		TestAdaptivePerformanceSubsystem_set_GpuPerformanceLevel_mDB2374F91AB158C04869CC2C33F3F483EFE6FFBC(__this, (-1), /*hidden argument*/NULL);
		// return false;
		return (bool)0;
	}

IL_0026:
	{
		// return cpuLevel >= 0 && gpuLevel >= 0 && cpuLevel <= MaxCpuPerformanceLevel && gpuLevel <= MaxGpuPerformanceLevel;
		int32_t L_3 = ___cpuLevel0;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_4 = ___gpuLevel1;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_5 = ___cpuLevel0;
		int32_t L_6 = TestAdaptivePerformanceSubsystem_get_MaxCpuPerformanceLevel_m607B05A021EFFA9160E56FCBBE2520526ADBF88A(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_5) > ((int32_t)L_6)))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_7 = ___gpuLevel1;
		int32_t L_8 = TestAdaptivePerformanceSubsystem_get_MaxGpuPerformanceLevel_mA0ACEFBE7A1697080A61BC518639D55893E065B5(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_7) > ((int32_t)L_8))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0044:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.RenderThreadCpuTime::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderThreadCpuTime__ctor_m4DF8E17D0BD885CC1F829E3A5E06C219AE3830C7 (RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * __this, const RuntimeMethod* method)
{
	{
		// public RenderThreadCpuTime() {}
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// public RenderThreadCpuTime() {}
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.RenderThreadCpuTime::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderThreadCpuTime_Reset_m3A491AE6D23434703AE0A91D9145592565EB2191 (RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * __this, const RuntimeMethod* method)
{
	{
		// public void Reset() {}
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.RenderThreadCpuTime::Measure()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderThreadCpuTime_Measure_m6C2F782E97EFF82696E79BC952500EDADA7D67EB (RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * __this, const RuntimeMethod* method)
{
	{
		// public void Measure() {}
		return;
	}
}
// System.Single UnityEngine.AdaptivePerformance.RenderThreadCpuTime::GetLatestResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RenderThreadCpuTime_GetLatestResult_mB9798FC4B9FE40428A1C6BF66EB6A37681F1F9C1 (RenderThreadCpuTime_tD3E9854D41D22B17EB5C2E0CD18C1F0186B4B710 * __this, const RuntimeMethod* method)
{
	{
		// public float GetLatestResult() { return -1.0f; }
		return (-1.0f);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.RunningAverage::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RunningAverage__ctor_m1D5CCDBE8A19B9CCE2F0644773094D4157AEC9AF (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, int32_t ___sampleWindowSize0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RunningAverage__ctor_m1D5CCDBE8A19B9CCE2F0644773094D4157AEC9AF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private int m_LastIndex = -1;
		__this->set_m_LastIndex_2((-1));
		// public RunningAverage(int sampleWindowSize = 100)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// m_Values = new float[sampleWindowSize];
		int32_t L_0 = ___sampleWindowSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_1 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)L_0);
		__this->set_m_Values_0(L_1);
		// }
		return;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.RunningAverage::GetNumValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RunningAverage_GetNumValues_mCAA37709BFE682697178E705A920598DBCD9D395 (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, const RuntimeMethod* method)
{
	{
		// return m_NumValues;
		int32_t L_0 = __this->get_m_NumValues_1();
		return L_0;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.RunningAverage::GetSampleWindowSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RunningAverage_GetSampleWindowSize_mCFD8C3F0168893D1C550FB0C88B62A03EDD35411 (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, const RuntimeMethod* method)
{
	{
		// return m_Values.Length;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = __this->get_m_Values_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length))));
	}
}
// System.Single UnityEngine.AdaptivePerformance.RunningAverage::GetAverageOr(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RunningAverage_GetAverageOr_mEE0503DACB72C6CF46D78B893479E23638E92484 (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, float ___defaultValue0, const RuntimeMethod* method)
{
	{
		// return (m_NumValues > 0) ? m_AverageValue : defaultValue;
		int32_t L_0 = __this->get_m_NumValues_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		float L_1 = ___defaultValue0;
		return L_1;
	}

IL_000b:
	{
		float L_2 = __this->get_m_AverageValue_3();
		return L_2;
	}
}
// System.Single UnityEngine.AdaptivePerformance.RunningAverage::GetMostRecentValueOr(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RunningAverage_GetMostRecentValueOr_mAC56F749F88BC98DAB20A5070A71B14FCE8E7566 (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, float ___defaultValue0, const RuntimeMethod* method)
{
	{
		// return (m_NumValues > 0) ? m_Values[m_LastIndex] : defaultValue;
		int32_t L_0 = __this->get_m_NumValues_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		float L_1 = ___defaultValue0;
		return L_1;
	}

IL_000b:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_2 = __this->get_m_Values_0();
		int32_t L_3 = __this->get_m_LastIndex_2();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		float L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}
}
// System.Void UnityEngine.AdaptivePerformance.RunningAverage::AddValue(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RunningAverage_AddValue_m5C454C088FA345480368C01D995C56D618CD4733 (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, float ___NewValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RunningAverage_AddValue_m5C454C088FA345480368C01D995C56D618CD4733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		// int oldestIndex = (m_LastIndex + 1) % m_Values.Length;
		int32_t L_0 = __this->get_m_LastIndex_2();
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_1 = __this->get_m_Values_0();
		NullCheck(L_1);
		V_0 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))))));
		// float oldestValue = m_Values[oldestIndex];
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_2 = __this->get_m_Values_0();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		float L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		// m_LastIndex = oldestIndex;
		int32_t L_6 = V_0;
		__this->set_m_LastIndex_2(L_6);
		// m_Values[m_LastIndex] = NewValue;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_7 = __this->get_m_Values_0();
		int32_t L_8 = __this->get_m_LastIndex_2();
		float L_9 = ___NewValue0;
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (float)L_9);
		// float totalValue = m_AverageValue * m_NumValues + NewValue - oldestValue;
		float L_10 = __this->get_m_AverageValue_3();
		int32_t L_11 = __this->get_m_NumValues_1();
		float L_12 = ___NewValue0;
		float L_13 = V_1;
		V_2 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_10, (float)(((float)((float)L_11))))), (float)L_12)), (float)L_13));
		// m_NumValues = Mathf.Min(m_NumValues + 1, m_Values.Length);
		int32_t L_14 = __this->get_m_NumValues_1();
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_15 = __this->get_m_Values_0();
		NullCheck(L_15);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		int32_t L_16 = Mathf_Min_m1A2CC204E361AE13C329B6535165179798D3313A(((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1)), (((int32_t)((int32_t)(((RuntimeArray*)L_15)->max_length)))), /*hidden argument*/NULL);
		__this->set_m_NumValues_1(L_16);
		// m_AverageValue = totalValue / m_NumValues;
		float L_17 = V_2;
		int32_t L_18 = __this->get_m_NumValues_1();
		__this->set_m_AverageValue_3(((float)((float)L_17/(float)(((float)((float)L_18))))));
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.RunningAverage::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RunningAverage_Reset_m3AEE75202E5AC2F0059A7FEBBEA32879886DDCC9 (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, const RuntimeMethod* method)
{
	{
		// m_NumValues = 0;
		__this->set_m_NumValues_1(0);
		// m_LastIndex = -1;
		__this->set_m_LastIndex_2((-1));
		// m_AverageValue = 0.0f;
		__this->set_m_AverageValue_3((0.0f));
		// System.Array.Clear(m_Values, 0, m_Values.Length);
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = __this->get_m_Values_0();
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_1 = __this->get_m_Values_0();
		NullCheck(L_1);
		Array_Clear_m174F4957D6DEDB6359835123005304B14E79132E((RuntimeArray *)(RuntimeArray *)L_0, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))), /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.StartupSettings::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StartupSettings__cctor_mE18523E72E597632CE086DFFA262106D1EBBB235 (const RuntimeMethod* method)
{
	{
		// Logging = false;
		StartupSettings_set_Logging_m6CD2E66D91308712825AECF2A1AF80A86F7C4A30_inline((bool)0, /*hidden argument*/NULL);
		// StatsLoggingFrequencyInFrames = 50;
		StartupSettings_set_StatsLoggingFrequencyInFrames_m4AA8C016FE0DAD38D37117A32154D4B542D1D115_inline(((int32_t)50), /*hidden argument*/NULL);
		// Enable = true;
		StartupSettings_set_Enable_mA1F811FA9465F51D5742B28BFCAA450D9298B6C1_inline((bool)1, /*hidden argument*/NULL);
		// PreferredSubsystem = null;
		StartupSettings_set_PreferredSubsystem_m84A3D06E2638662EE488242C3A5A8764ED31D8DD_inline((AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 *)NULL, /*hidden argument*/NULL);
		// AutomaticPerformanceControl = true;
		StartupSettings_set_AutomaticPerformanceControl_m9C2983846A71477818E1E33CBE4412E9D621A272_inline((bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.StartupSettings::get_Logging()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StartupSettings_get_Logging_m7A40AE632795747F6E17C5ABABEF45D83E01EAA3 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_get_Logging_m7A40AE632795747F6E17C5ABABEF45D83E01EAA3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool Logging { get; set; }
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		bool L_0 = ((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->get_U3CLoggingU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.StartupSettings::set_Logging(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StartupSettings_set_Logging_m6CD2E66D91308712825AECF2A1AF80A86F7C4A30 (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_set_Logging_m6CD2E66D91308712825AECF2A1AF80A86F7C4A30_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool Logging { get; set; }
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->set_U3CLoggingU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AdaptivePerformance.StartupSettings::get_StatsLoggingFrequencyInFrames()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StartupSettings_get_StatsLoggingFrequencyInFrames_m5F108EC9014719068FF27AD8D23E21E1FD1DF380 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_get_StatsLoggingFrequencyInFrames_m5F108EC9014719068FF27AD8D23E21E1FD1DF380_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public int StatsLoggingFrequencyInFrames { get; set; }
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		int32_t L_0 = ((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->get_U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.StartupSettings::set_StatsLoggingFrequencyInFrames(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StartupSettings_set_StatsLoggingFrequencyInFrames_m4AA8C016FE0DAD38D37117A32154D4B542D1D115 (int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_set_StatsLoggingFrequencyInFrames_m4AA8C016FE0DAD38D37117A32154D4B542D1D115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public int StatsLoggingFrequencyInFrames { get; set; }
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->set_U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.StartupSettings::get_Enable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StartupSettings_get_Enable_m8343935449F13DAF78A3EE247DA316A91695A9D2 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_get_Enable_m8343935449F13DAF78A3EE247DA316A91695A9D2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool Enable { get; set; }
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		bool L_0 = ((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->get_U3CEnableU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.StartupSettings::set_Enable(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StartupSettings_set_Enable_mA1F811FA9465F51D5742B28BFCAA450D9298B6C1 (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_set_Enable_mA1F811FA9465F51D5742B28BFCAA450D9298B6C1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool Enable { get; set; }
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->set_U3CEnableU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AdaptivePerformance.StartupSettings::get_AutomaticPerformanceControl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StartupSettings_get_AutomaticPerformanceControl_mD3FD90C9A974872C6E1F4C8F0F857CE658654E83 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_get_AutomaticPerformanceControl_mD3FD90C9A974872C6E1F4C8F0F857CE658654E83_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool AutomaticPerformanceControl { get; set; }
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		bool L_0 = ((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->get_U3CAutomaticPerformanceControlU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.StartupSettings::set_AutomaticPerformanceControl(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StartupSettings_set_AutomaticPerformanceControl_m9C2983846A71477818E1E33CBE4412E9D621A272 (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_set_AutomaticPerformanceControl_m9C2983846A71477818E1E33CBE4412E9D621A272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool AutomaticPerformanceControl { get; set; }
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->set_U3CAutomaticPerformanceControlU3Ek__BackingField_3(L_0);
		return;
	}
}
// UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem UnityEngine.AdaptivePerformance.StartupSettings::get_PreferredSubsystem()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * StartupSettings_get_PreferredSubsystem_m288E01FA3B91225A014D305D97163961E988C5B3 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_get_PreferredSubsystem_m288E01FA3B91225A014D305D97163961E988C5B3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public Provider.AdaptivePerformanceSubsystem PreferredSubsystem { get; set; }
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_0 = ((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->get_U3CPreferredSubsystemU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.StartupSettings::set_PreferredSubsystem(UnityEngine.AdaptivePerformance.Provider.AdaptivePerformanceSubsystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StartupSettings_set_PreferredSubsystem_m84A3D06E2638662EE488242C3A5A8764ED31D8DD (AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_set_PreferredSubsystem_m84A3D06E2638662EE488242C3A5A8764ED31D8DD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public Provider.AdaptivePerformanceSubsystem PreferredSubsystem { get; set; }
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->set_U3CPreferredSubsystemU3Ek__BackingField_4(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::PopOldestValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend_PopOldestValue_m1EE573E39718EADC81338CBC55C229FFB3D00E53 (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	{
		// double x = m_TimeStamps[m_OldestValueIndex];
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = __this->get_m_TimeStamps_10();
		int32_t L_1 = __this->get_m_OldestValueIndex_14();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		float L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = (((double)((double)L_3)));
		// double y = m_Temperature[m_OldestValueIndex];
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_4 = __this->get_m_Temperature_11();
		int32_t L_5 = __this->get_m_OldestValueIndex_14();
		NullCheck(L_4);
		int32_t L_6 = L_5;
		float L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = (((double)((double)L_7)));
		// m_SumX -= x;
		double L_8 = __this->get_m_SumX_1();
		double L_9 = V_0;
		__this->set_m_SumX_1(((double)il2cpp_codegen_subtract((double)L_8, (double)L_9)));
		// m_SumY -= y;
		double L_10 = __this->get_m_SumY_2();
		double L_11 = V_1;
		__this->set_m_SumY_2(((double)il2cpp_codegen_subtract((double)L_10, (double)L_11)));
		// m_SumXY -= x * y;
		double L_12 = __this->get_m_SumXY_3();
		double L_13 = V_0;
		double L_14 = V_1;
		__this->set_m_SumXY_3(((double)il2cpp_codegen_subtract((double)L_12, (double)((double)il2cpp_codegen_multiply((double)L_13, (double)L_14)))));
		// m_SumXX -= x * x;
		double L_15 = __this->get_m_SumXX_4();
		double L_16 = V_0;
		double L_17 = V_0;
		__this->set_m_SumXX_4(((double)il2cpp_codegen_subtract((double)L_15, (double)((double)il2cpp_codegen_multiply((double)L_16, (double)L_17)))));
		// m_OldestValueIndex = (m_OldestValueIndex + 1) % MaxValues;
		int32_t L_18 = __this->get_m_OldestValueIndex_14();
		__this->set_m_OldestValueIndex_14(((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1))%(int32_t)((int32_t)600))));
		// --m_NumValues;
		int32_t L_19 = __this->get_m_NumValues_12();
		__this->set_m_NumValues_12(((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1)));
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::PushNewValue(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend_PushNewValue_m39C040C880B6B36603F2DF3BA89C47ACB42193E0 (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, float ___tempLevel0, float ___timestamp1, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	{
		// m_TimeStamps[m_NextValueIndex] = timestamp;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = __this->get_m_TimeStamps_10();
		int32_t L_1 = __this->get_m_NextValueIndex_13();
		float L_2 = ___timestamp1;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (float)L_2);
		// m_Temperature[m_NextValueIndex] = tempLevel;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_3 = __this->get_m_Temperature_11();
		int32_t L_4 = __this->get_m_NextValueIndex_13();
		float L_5 = ___tempLevel0;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (float)L_5);
		// m_NextValueIndex = (m_NextValueIndex + 1) % MaxValues;
		int32_t L_6 = __this->get_m_NextValueIndex_13();
		__this->set_m_NextValueIndex_13(((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1))%(int32_t)((int32_t)600))));
		// ++m_NumValues;
		int32_t L_7 = __this->get_m_NumValues_12();
		__this->set_m_NumValues_12(((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1)));
		// double x = timestamp;
		float L_8 = ___timestamp1;
		V_0 = (((double)((double)L_8)));
		// double y = tempLevel;
		float L_9 = ___tempLevel0;
		V_1 = (((double)((double)L_9)));
		// m_SumX += x;
		double L_10 = __this->get_m_SumX_1();
		double L_11 = V_0;
		__this->set_m_SumX_1(((double)il2cpp_codegen_add((double)L_10, (double)L_11)));
		// m_SumY += y;
		double L_12 = __this->get_m_SumY_2();
		double L_13 = V_1;
		__this->set_m_SumY_2(((double)il2cpp_codegen_add((double)L_12, (double)L_13)));
		// m_SumXY += x * y;
		double L_14 = __this->get_m_SumXY_3();
		double L_15 = V_0;
		double L_16 = V_1;
		__this->set_m_SumXY_3(((double)il2cpp_codegen_add((double)L_14, (double)((double)il2cpp_codegen_multiply((double)L_15, (double)L_16)))));
		// m_SumXX += x * x;
		double L_17 = __this->get_m_SumXX_4();
		double L_18 = V_0;
		double L_19 = V_0;
		__this->set_m_SumXX_4(((double)il2cpp_codegen_add((double)L_17, (double)((double)il2cpp_codegen_multiply((double)L_18, (double)L_19)))));
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend__ctor_mC16C5848B70A106139BFAD5068FEE2332A93D8DB (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, bool ___useProviderTrend0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TemperatureTrend__ctor_mC16C5848B70A106139BFAD5068FEE2332A93D8DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// float[] m_TimeStamps = new float[MaxValues];
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)((int32_t)600));
		__this->set_m_TimeStamps_10(L_0);
		// float[] m_Temperature = new float[MaxValues];
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_1 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)((int32_t)600));
		__this->set_m_Temperature_11(L_1);
		// public TemperatureTrend(bool useProviderTrend)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// m_UseProviderTrend = useProviderTrend;
		bool L_2 = ___useProviderTrend0;
		__this->set_m_UseProviderTrend_0(L_2);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend_Reset_m71442095ADD319071142E1B14DA2EA27F3F8FEC4 (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, const RuntimeMethod* method)
{
	{
		// m_NumValues = 0;
		__this->set_m_NumValues_12(0);
		// m_OldestValueIndex = 0;
		__this->set_m_OldestValueIndex_14(0);
		// m_NextValueIndex = 0;
		__this->set_m_NextValueIndex_13(0);
		// m_SumX = 0.0;
		__this->set_m_SumX_1((0.0));
		// m_SumY = 0.0;
		__this->set_m_SumY_2((0.0));
		// m_SumXY = 0.0;
		__this->set_m_SumXY_3((0.0));
		// m_SumXX = 0.0;
		__this->set_m_SumXX_4((0.0));
		// ThermalTrend = 0.0f;
		TemperatureTrend_set_ThermalTrend_mA0E332378E70F80C414402AF81C001DDA7F2740B_inline(__this, (0.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Single UnityEngine.AdaptivePerformance.TemperatureTrend::get_ThermalTrend()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TemperatureTrend_get_ThermalTrend_m971750AEB690F8B4ED9FE84C5BA802B8C5EB4B5F (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, const RuntimeMethod* method)
{
	{
		// public float ThermalTrend { get; private set; }
		float L_0 = __this->get_U3CThermalTrendU3Ek__BackingField_15();
		return L_0;
	}
}
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::set_ThermalTrend(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend_set_ThermalTrend_mA0E332378E70F80C414402AF81C001DDA7F2740B (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float ThermalTrend { get; private set; }
		float L_0 = ___value0;
		__this->set_U3CThermalTrendU3Ek__BackingField_15(L_0);
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::UpdateTrend()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend_UpdateTrend_mBA99D62A42FAC09AF17975F27A4BEC2C5E05C015 (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TemperatureTrend_UpdateTrend_mBA99D62A42FAC09AF17975F27A4BEC2C5E05C015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	{
		// if (m_NumValues < 2)
		int32_t L_0 = __this->get_m_NumValues_12();
		if ((((int32_t)L_0) >= ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		// ThermalTrend = 0.0f;
		TemperatureTrend_set_ThermalTrend_mA0E332378E70F80C414402AF81C001DDA7F2740B_inline(__this, (0.0f), /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0015:
	{
		// double p = m_NumValues * m_SumXY - m_SumX * m_SumY;
		int32_t L_1 = __this->get_m_NumValues_12();
		double L_2 = __this->get_m_SumXY_3();
		double L_3 = __this->get_m_SumX_1();
		double L_4 = __this->get_m_SumY_2();
		// double q = m_NumValues * m_SumXX - m_SumX * m_SumX;
		int32_t L_5 = __this->get_m_NumValues_12();
		double L_6 = __this->get_m_SumXX_4();
		double L_7 = __this->get_m_SumX_1();
		double L_8 = __this->get_m_SumX_1();
		V_0 = ((double)il2cpp_codegen_subtract((double)((double)il2cpp_codegen_multiply((double)(((double)((double)L_5))), (double)L_6)), (double)((double)il2cpp_codegen_multiply((double)L_7, (double)L_8))));
		// double m = p / q;
		double L_9 = V_0;
		V_1 = ((double)((double)((double)il2cpp_codegen_subtract((double)((double)il2cpp_codegen_multiply((double)(((double)((double)L_1))), (double)L_2)), (double)((double)il2cpp_codegen_multiply((double)L_3, (double)L_4))))/(double)L_9));
		// m /= SlopeAtMaxTrend;
		double L_10 = V_1;
		V_1 = ((double)((double)L_10/(double)(0.005)));
		// if (m >= 1.0)
		double L_11 = V_1;
		if ((!(((double)L_11) >= ((double)(1.0)))))
		{
			goto IL_0075;
		}
	}
	{
		// ThermalTrend = 1.0f;
		TemperatureTrend_set_ThermalTrend_mA0E332378E70F80C414402AF81C001DDA7F2740B_inline(__this, (1.0f), /*hidden argument*/NULL);
		// }
		return;
	}

IL_0075:
	{
		// else if (m >= -1.0)
		double L_12 = V_1;
		if ((!(((double)L_12) >= ((double)(-1.0)))))
		{
			goto IL_00a7;
		}
	}
	{
		// if (Math.Abs(m) < 0.00001)
		double L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var);
		double L_14 = fabs(L_13);
		if ((!(((double)L_14) < ((double)(1.0E-05)))))
		{
			goto IL_009e;
		}
	}
	{
		// ThermalTrend = 0.0f;
		TemperatureTrend_set_ThermalTrend_mA0E332378E70F80C414402AF81C001DDA7F2740B_inline(__this, (0.0f), /*hidden argument*/NULL);
		return;
	}

IL_009e:
	{
		// ThermalTrend = (float)m;
		double L_15 = V_1;
		TemperatureTrend_set_ThermalTrend_mA0E332378E70F80C414402AF81C001DDA7F2740B_inline(__this, (((float)((float)L_15))), /*hidden argument*/NULL);
		// }
		return;
	}

IL_00a7:
	{
		// else if (m <= -1.0)
		double L_16 = V_1;
		if ((!(((double)L_16) <= ((double)(-1.0)))))
		{
			goto IL_00bf;
		}
	}
	{
		// ThermalTrend = -1.0f;
		TemperatureTrend_set_ThermalTrend_mA0E332378E70F80C414402AF81C001DDA7F2740B_inline(__this, (-1.0f), /*hidden argument*/NULL);
		// }
		return;
	}

IL_00bf:
	{
		// ThermalTrend = 0.0f;
		TemperatureTrend_set_ThermalTrend_mA0E332378E70F80C414402AF81C001DDA7F2740B_inline(__this, (0.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.AdaptivePerformance.TemperatureTrend::Update(System.Single,System.Single,System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TemperatureTrend_Update_m5BA796D968562EF7CEB9D5644CDC7EBC8FA9A1AE (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, float ___temperatureTrendFromProvider0, float ___newTemperatureLevel1, bool ___changed2, float ___newTemperatureTimestamp3, const RuntimeMethod* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		// if (m_UseProviderTrend)
		bool L_0 = __this->get_m_UseProviderTrend_0();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		// ThermalTrend = temperatureTrendFromProvider;
		float L_1 = ___temperatureTrendFromProvider0;
		TemperatureTrend_set_ThermalTrend_mA0E332378E70F80C414402AF81C001DDA7F2740B_inline(__this, L_1, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0010:
	{
		// newTemperatureLevel = newTemperatureLevel * newTemperatureLevel * newTemperatureLevel;
		float L_2 = ___newTemperatureLevel1;
		float L_3 = ___newTemperatureLevel1;
		float L_4 = ___newTemperatureLevel1;
		___newTemperatureLevel1 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_2, (float)L_3)), (float)L_4));
		// if (m_NumValues == 0)
		int32_t L_5 = __this->get_m_NumValues_12();
		if (L_5)
		{
			goto IL_002f;
		}
	}
	{
		// PushNewValue(newTemperatureLevel, newTemperatureTimestamp);
		float L_6 = ___newTemperatureLevel1;
		float L_7 = ___newTemperatureTimestamp3;
		TemperatureTrend_PushNewValue_m39C040C880B6B36603F2DF3BA89C47ACB42193E0(__this, L_6, L_7, /*hidden argument*/NULL);
		// UpdateTrend();
		TemperatureTrend_UpdateTrend_mBA99D62A42FAC09AF17975F27A4BEC2C5E05C015(__this, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_002f:
	{
		// bool updateTrend = false;
		V_0 = (bool)0;
		// float oldestTimeStamp = m_TimeStamps[m_OldestValueIndex];
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_8 = __this->get_m_TimeStamps_10();
		int32_t L_9 = __this->get_m_OldestValueIndex_14();
		NullCheck(L_8);
		int32_t L_10 = L_9;
		float L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_1 = L_11;
		// float timestampThresholdForNewValue = oldestTimeStamp + 1.0f / UpdateFrequency * m_NumValues;
		float L_12 = V_1;
		int32_t L_13 = __this->get_m_NumValues_12();
		V_2 = ((float)il2cpp_codegen_add((float)L_12, (float)((float)il2cpp_codegen_multiply((float)(0.1f), (float)(((float)((float)L_13)))))));
		// if (newTemperatureTimestamp - oldestTimeStamp > MaxTimeSeconds)
		float L_14 = ___newTemperatureTimestamp3;
		float L_15 = V_1;
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_14, (float)L_15))) > ((float)(60.0f)))))
		{
			goto IL_0062;
		}
	}
	{
		// PopOldestValue();
		TemperatureTrend_PopOldestValue_m1EE573E39718EADC81338CBC55C229FFB3D00E53(__this, /*hidden argument*/NULL);
		// updateTrend = true;
		V_0 = (bool)1;
	}

IL_0062:
	{
		// if (changed || newTemperatureTimestamp >= timestampThresholdForNewValue)
		bool L_16 = ___changed2;
		if (L_16)
		{
			goto IL_006a;
		}
	}
	{
		float L_17 = ___newTemperatureTimestamp3;
		float L_18 = V_2;
		if ((!(((float)L_17) >= ((float)L_18))))
		{
			goto IL_0088;
		}
	}

IL_006a:
	{
		// if (m_NumValues == MaxValues)
		int32_t L_19 = __this->get_m_NumValues_12();
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)600)))))
		{
			goto IL_007d;
		}
	}
	{
		// PopOldestValue();
		TemperatureTrend_PopOldestValue_m1EE573E39718EADC81338CBC55C229FFB3D00E53(__this, /*hidden argument*/NULL);
	}

IL_007d:
	{
		// PushNewValue(newTemperatureLevel, newTemperatureTimestamp);
		float L_20 = ___newTemperatureLevel1;
		float L_21 = ___newTemperatureTimestamp3;
		TemperatureTrend_PushNewValue_m39C040C880B6B36603F2DF3BA89C47ACB42193E0(__this, L_20, L_21, /*hidden argument*/NULL);
		// updateTrend = true;
		V_0 = (bool)1;
	}

IL_0088:
	{
		// if (updateTrend)
		bool L_22 = V_0;
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		// UpdateTrend();
		TemperatureTrend_UpdateTrend_mBA99D62A42FAC09AF17975F27A4BEC2C5E05C015(__this, /*hidden argument*/NULL);
	}

IL_0091:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 (ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * __this, ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  ___thermalMetrics0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___thermalMetrics0);

}
// System.Void UnityEngine.AdaptivePerformance.ThermalEventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThermalEventHandler__ctor_mAFECFD9819ACDC6C121411AE80642348AD64178E (ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AdaptivePerformance.ThermalEventHandler::Invoke(UnityEngine.AdaptivePerformance.ThermalMetrics)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThermalEventHandler_Invoke_m48931BBF9EBE5CFAA39300884D1550F27FBB1370 (ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * __this, ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  ___thermalMetrics0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___thermalMetrics0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___thermalMetrics0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___thermalMetrics0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  >::Invoke(targetMethod, targetThis, ___thermalMetrics0);
					else
						GenericVirtActionInvoker1< ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  >::Invoke(targetMethod, targetThis, ___thermalMetrics0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___thermalMetrics0);
					else
						VirtActionInvoker1< ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___thermalMetrics0);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(&___thermalMetrics0) - 1), targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 , const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___thermalMetrics0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult UnityEngine.AdaptivePerformance.ThermalEventHandler::BeginInvoke(UnityEngine.AdaptivePerformance.ThermalMetrics,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ThermalEventHandler_BeginInvoke_m1D8E5A8112745ACB6B9B9D3DF35A1ECAE4AE6409 (ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * __this, ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125  ___thermalMetrics0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThermalEventHandler_BeginInvoke_m1D8E5A8112745ACB6B9B9D3DF35A1ECAE4AE6409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125_il2cpp_TypeInfo_var, &___thermalMetrics0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.AdaptivePerformance.ThermalEventHandler::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThermalEventHandler_EndInvoke_mFC301538228DEF5972534783BA52636D355DAB86 (ThermalEventHandler_tAB4CFFD0EDF2DBD983687915D6AFB3F11663F128 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AdaptivePerformance.ThermalMetrics::set_WarningLevel(UnityEngine.AdaptivePerformance.WarningLevel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThermalMetrics_set_WarningLevel_m2B6C74F0609CAF7AA9FFE97018DB76D03D85CF89 (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public WarningLevel WarningLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CWarningLevelU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void ThermalMetrics_set_WarningLevel_m2B6C74F0609CAF7AA9FFE97018DB76D03D85CF89_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * _thisAdjusted = reinterpret_cast<ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 *>(__this + _offset);
	ThermalMetrics_set_WarningLevel_m2B6C74F0609CAF7AA9FFE97018DB76D03D85CF89_inline(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.AdaptivePerformance.ThermalMetrics::get_TemperatureLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ThermalMetrics_get_TemperatureLevel_m1E7E3A67FD29A5ABA322EB933CF7409CD0151212 (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * __this, const RuntimeMethod* method)
{
	{
		// public float TemperatureLevel { get; set; }
		float L_0 = __this->get_U3CTemperatureLevelU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C  float ThermalMetrics_get_TemperatureLevel_m1E7E3A67FD29A5ABA322EB933CF7409CD0151212_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * _thisAdjusted = reinterpret_cast<ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 *>(__this + _offset);
	return ThermalMetrics_get_TemperatureLevel_m1E7E3A67FD29A5ABA322EB933CF7409CD0151212_inline(_thisAdjusted, method);
}
// System.Void UnityEngine.AdaptivePerformance.ThermalMetrics::set_TemperatureLevel(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThermalMetrics_set_TemperatureLevel_m55E3869F1C0685354CEBC2FA58FBB009380487FD (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float TemperatureLevel { get; set; }
		float L_0 = ___value0;
		__this->set_U3CTemperatureLevelU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void ThermalMetrics_set_TemperatureLevel_m55E3869F1C0685354CEBC2FA58FBB009380487FD_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * _thisAdjusted = reinterpret_cast<ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 *>(__this + _offset);
	ThermalMetrics_set_TemperatureLevel_m55E3869F1C0685354CEBC2FA58FBB009380487FD_inline(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.AdaptivePerformance.ThermalMetrics::set_TemperatureTrend(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThermalMetrics_set_TemperatureTrend_mF48505C1BB0EBE4F530B0405D60D4396A24B9472 (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float TemperatureTrend { get; set; }
		float L_0 = ___value0;
		__this->set_U3CTemperatureTrendU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void ThermalMetrics_set_TemperatureTrend_mF48505C1BB0EBE4F530B0405D60D4396A24B9472_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * _thisAdjusted = reinterpret_cast<ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 *>(__this + _offset);
	ThermalMetrics_set_TemperatureTrend_mF48505C1BB0EBE4F530B0405D60D4396A24B9472_inline(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_PerformanceControlMode_m2E4EE8BB379495FD00D67FCB6675358333F1532D_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method)
{
	{
		// public PerformanceControlMode PerformanceControlMode { get; set; }
		int32_t L_0 = __this->get_U3CPerformanceControlModeU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ThermalMetrics_set_WarningLevel_m2B6C74F0609CAF7AA9FFE97018DB76D03D85CF89_inline (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public WarningLevel WarningLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CWarningLevelU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ThermalMetrics_set_TemperatureLevel_m55E3869F1C0685354CEBC2FA58FBB009380487FD_inline (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float TemperatureLevel { get; set; }
		float L_0 = ___value0;
		__this->set_U3CTemperatureLevelU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void ThermalMetrics_set_TemperatureTrend_mF48505C1BB0EBE4F530B0405D60D4396A24B9472_inline (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float TemperatureTrend { get; set; }
		float L_0 = ___value0;
		__this->set_U3CTemperatureTrendU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceMetrics_set_CurrentCpuLevel_mB0FFC61EEA0C4462338C8E728533FE48D4364D9A_inline (PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CurrentCpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentCpuLevelU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceMetrics_set_CurrentGpuLevel_m2807D7F1C39CF7C72DCA9CFDC0BB6F90AA2CE326_inline (PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CurrentGpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentGpuLevelU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceMetrics_set_PerformanceBottleneck_mFFE12B2A0C07877C363A05EF1F48B4C9BA27D268_inline (PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public PerformanceBottleneck PerformanceBottleneck { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CPerformanceBottleneckU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FrameTiming_set_CurrentFrameTime_m2DD61370159EDB8AE9612CC6303B25175788A0E5_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float CurrentFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CCurrentFrameTimeU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FrameTiming_set_AverageFrameTime_mBDC023C5DDCE8C8069CBEAF28DAC912744D648C5_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float AverageFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CAverageFrameTimeU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FrameTiming_set_CurrentGpuFrameTime_mBAB674B381CA46E589A79890A9BE0A0FDC2EE781_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float CurrentGpuFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CCurrentGpuFrameTimeU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FrameTiming_set_AverageGpuFrameTime_mADCD67452E1D4C92E7562EDD78A5BC411B3BC29B_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float AverageGpuFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CAverageGpuFrameTimeU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FrameTiming_set_CurrentCpuFrameTime_m95A32BC4E4AE8885D1738D309F2C39E96E887810_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float CurrentCpuFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CCurrentCpuFrameTimeU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void FrameTiming_set_AverageCpuFrameTime_m87D8B833265478A0EA25153B3D5299AACAE71BDE_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float AverageCpuFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CAverageCpuFrameTimeU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool StartupSettings_get_AutomaticPerformanceControl_mD3FD90C9A974872C6E1F4C8F0F857CE658654E83_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_get_AutomaticPerformanceControl_mD3FD90C9A974872C6E1F4C8F0F857CE658654E83Unity_AdaptivePerformance_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool AutomaticPerformanceControl { get; set; }
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		bool L_0 = ((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->get_U3CAutomaticPerformanceControlU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_set_AutomaticPerformanceControl_mE17FB23F321B2C59242F1CB53295DBEA1F7F94FC_inline (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool AutomaticPerformanceControl { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CAutomaticPerformanceControlU3Ek__BackingField_16(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool AdaptivePerformanceSubsystemBase_get_initialized_mCF8947352D834034868007E91F84485171F40ACA_inline (AdaptivePerformanceSubsystemBase_tFE4445DE6A7F0520A1707ECCC71C516E3E4FB12E * __this, const RuntimeMethod* method)
{
	{
		// public bool initialized { get; protected set; }
		bool L_0 = __this->get_U3CinitializedU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool StartupSettings_get_Logging_m7A40AE632795747F6E17C5ABABEF45D83E01EAA3_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_get_Logging_m7A40AE632795747F6E17C5ABABEF45D83E01EAA3Unity_AdaptivePerformance_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool Logging { get; set; }
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		bool L_0 = ((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->get_U3CLoggingU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t StartupSettings_get_StatsLoggingFrequencyInFrames_m5F108EC9014719068FF27AD8D23E21E1FD1DF380_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_get_StatsLoggingFrequencyInFrames_m5F108EC9014719068FF27AD8D23E21E1FD1DF380Unity_AdaptivePerformance_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public int StatsLoggingFrequencyInFrames { get; set; }
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		int32_t L_0 = ((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->get_U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AdaptivePerformanceManager_set_LoggingFrequencyInFrames_mD228BFEE1C9900E7E8A8063826562B3D7CED38DA_inline (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int LoggingFrequencyInFrames { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CLoggingFrequencyInFramesU3Ek__BackingField_15(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool StartupSettings_get_Enable_m8343935449F13DAF78A3EE247DA316A91695A9D2_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_get_Enable_m8343935449F13DAF78A3EE247DA316A91695A9D2Unity_AdaptivePerformance_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool Enable { get; set; }
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		bool L_0 = ((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->get_U3CEnableU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * StartupSettings_get_PreferredSubsystem_m288E01FA3B91225A014D305D97163961E988C5B3_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_get_PreferredSubsystem_m288E01FA3B91225A014D305D97163961E988C5B3Unity_AdaptivePerformance_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public Provider.AdaptivePerformanceSubsystem PreferredSubsystem { get; set; }
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_0 = ((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->get_U3CPreferredSubsystemU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t AdaptivePerformanceSubsystem_get_Capabilities_m42EEFCDECCBB0DFFEBE3E65BAA42ED01256FB18F_inline (AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * __this, const RuntimeMethod* method)
{
	{
		// public Feature Capabilities { get; protected set; }
		int32_t L_0 = __this->get_U3CCapabilitiesU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_PerformanceControlMode_m90A342E8781C151B8F940601A615EBE727ED68BD_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public PerformanceControlMode PerformanceControlMode { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CPerformanceControlModeU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool AdaptivePerformanceManager_get_AutomaticPerformanceControl_m7E8E0B158A408D6E61DFEB69C69998792A97A98B_inline (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public bool AutomaticPerformanceControl { get; set; }
		bool L_0 = __this->get_U3CAutomaticPerformanceControlU3Ek__BackingField_16();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t AdaptivePerformanceManager_get_LoggingFrequencyInFrames_mB92896362D49659E0136CADA5FA59D8C052C3B58_inline (AdaptivePerformanceManager_t1FD901BCA3BCA15CFB9049D59699755CA0BB67FA * __this, const RuntimeMethod* method)
{
	{
		// public int LoggingFrequencyInFrames { get; set; }
		int32_t L_0 = __this->get_U3CLoggingFrequencyInFramesU3Ek__BackingField_15();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PerformanceDataRecord_get_WarningLevel_m8A58D1CDD83B9F86781080C042029E2D4B4795D7_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public WarningLevel WarningLevel { get; set; }
		int32_t L_0 = __this->get_U3CWarningLevelU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_TemperatureLevel_m8ADD4135E271A910EB33ED6B113E6153A8B54CCE_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public float TemperatureLevel { get; set; }
		float L_0 = __this->get_U3CTemperatureLevelU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_OverallFrameTime_mD8BE680879118B05C2BFA66EC2F0BF8C69CC8C59_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public float OverallFrameTime { get; set; }
		float L_0 = __this->get_U3COverallFrameTimeU3Ek__BackingField_9();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_GpuFrameTime_m50329D6AADDB092BE7BA0E9EAD569D4885774BB1_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public float GpuFrameTime { get; set; }
		float L_0 = __this->get_U3CGpuFrameTimeU3Ek__BackingField_8();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_CpuFrameTime_m53BE7B6BA7C387D27C6B789F80487C19E48DD042_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public float CpuFrameTime { get; set; }
		float L_0 = __this->get_U3CCpuFrameTimeU3Ek__BackingField_7();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float PerformanceDataRecord_get_TemperatureTrend_m65CC0E6927BE74931532ACB8DE6BAA1CBBF12231_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public float TemperatureTrend { get; set; }
		float L_0 = __this->get_U3CTemperatureTrendU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PerformanceDataRecord_get_ChangeFlags_mEACF832964F5FB119F81F10E3C2BC4EEC9C33ABE_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public Feature ChangeFlags { get; set; }
		int32_t L_0 = __this->get_U3CChangeFlagsU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float TemperatureTrend_get_ThermalTrend_m971750AEB690F8B4ED9FE84C5BA802B8C5EB4B5F_inline (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, const RuntimeMethod* method)
{
	{
		// public float ThermalTrend { get; private set; }
		float L_0 = __this->get_U3CThermalTrendU3Ek__BackingField_15();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t RunningAverage_GetNumValues_mCAA37709BFE682697178E705A920598DBCD9D395_inline (RunningAverage_tAEED7F08B50C3CBD16EE877BD87D26F5500C52C1 * __this, const RuntimeMethod* method)
{
	{
		// return m_NumValues;
		int32_t L_0 = __this->get_m_NumValues_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PerformanceMetrics_get_PerformanceBottleneck_m518B78FAA9F20BD9F79F3EAFCE7CC81612F55CC6_inline (PerformanceMetrics_tB5D7E6DF83E373D66A9899BBE7AA8E35617C1C4F * __this, const RuntimeMethod* method)
{
	{
		// public PerformanceBottleneck PerformanceBottleneck { get; set; }
		int32_t L_0 = __this->get_U3CPerformanceBottleneckU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float FrameTiming_get_AverageCpuFrameTime_m1BAD83D813770DB099F29C2ACAB31D4C08A79ED0_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, const RuntimeMethod* method)
{
	{
		// public float AverageCpuFrameTime { get; set; }
		float L_0 = __this->get_U3CAverageCpuFrameTimeU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float FrameTiming_get_AverageGpuFrameTime_m2AA4C5FB0D07DA307EF04B40B60B211B21700386_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, const RuntimeMethod* method)
{
	{
		// public float AverageGpuFrameTime { get; set; }
		float L_0 = __this->get_U3CAverageGpuFrameTimeU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float FrameTiming_get_AverageFrameTime_m6C518BA12C5C330F471B31E364A37362AC439618_inline (FrameTiming_t093F714FB80B10383712ECFC2CE8D293349505B1 * __this, const RuntimeMethod* method)
{
	{
		// public float AverageFrameTime { get; set; }
		float L_0 = __this->get_U3CAverageFrameTimeU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceBottleneckChangeEventArgs_set_PerformanceBottleneck_m63560EBBBEA19F0D26251C884A8CEE5FD3C07B03_inline (PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public PerformanceBottleneck PerformanceBottleneck { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CPerformanceBottleneckU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PerformanceDataRecord_get_CpuPerformanceLevel_mAADDE6EE97531363BB97CBD7697B9B6D610DEE41_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public int CpuPerformanceLevel { get; set; }
		int32_t L_0 = __this->get_U3CCpuPerformanceLevelU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_CurrentCpuLevel_mA17D47BD068FBA9741C61F26D8869577FDC3B04E_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CurrentCpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentCpuLevelU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PerformanceDataRecord_get_GpuPerformanceLevel_mB1DD57E4C0ECDC9F74181D5E5CFC8265B52ED4B4_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public int GpuPerformanceLevel { get; set; }
		int32_t L_0 = __this->get_U3CGpuPerformanceLevelU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_CurrentGpuLevel_m58F4826025D590BCC9D4BA9CB9DC76452DD369B2_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CurrentGpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentGpuLevelU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool PerformanceDataRecord_get_PerformanceLevelControlAvailable_mC2FDF55F921B13DC69CCC033C952A1D293FE4572_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, const RuntimeMethod* method)
{
	{
		// public bool PerformanceLevelControlAvailable { get; set; }
		bool L_0 = __this->get_U3CPerformanceLevelControlAvailableU3Ek__BackingField_6();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_TargetFrameTime_m86953888D9B6699A5BA926B6C0260665B860E7B2_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float TargetFrameTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CTargetFrameTimeU3Ek__BackingField_11(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool AutoPerformanceLevelController_get_Enabled_mBA9F1DC76E6FC28B0DB7F4A7319E06EA8CFAA228_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// return m_Enabled;
		bool L_0 = __this->get_m_Enabled_9();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_ManualOverride_mF3CD9BE22E685E48E56D4974302749BA02F2A1FD_inline (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool ManualOverride { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CManualOverrideU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_CpuLevel_mE7771043E97430A856CDFAA1162D257801BDD1F4_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCpuLevelU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void DevicePerformanceControlImpl_set_GpuLevel_m09C65245F940E90A38B7C18326038C843F1CE005_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int GpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CGpuLevelU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_CurrentCpuLevel_m5B3842B5593E0ECD6FD2DC40C4A2B33495411C13_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method)
{
	{
		// public int CurrentCpuLevel { get; set; }
		int32_t L_0 = __this->get_U3CCurrentCpuLevelU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_CurrentGpuLevel_m8BDBF664287A7FD279C7A264AAA9818102B3B6F4_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method)
{
	{
		// public int CurrentGpuLevel { get; set; }
		int32_t L_0 = __this->get_U3CCurrentGpuLevelU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void Holder_set_Instance_m2786C3B74384A62BD2997F76802455BEAD572CAE_inline (RuntimeObject* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Holder_set_Instance_m2786C3B74384A62BD2997F76802455BEAD572CAEUnity_AdaptivePerformance_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public IAdaptivePerformance Instance { get; internal set; }
		RuntimeObject* L_0 = ___value0;
		((Holder_t3A26EE4375105172B35CF838E774E2DB72D06DF9_StaticFields*)il2cpp_codegen_static_fields_for(Holder_t3A26EE4375105172B35CF838E774E2DB72D06DF9_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_UpdateInterval_m802A56839C4049389329DBDE94C7DDA9A3C22680_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float UpdateInterval { get; set; }
		float L_0 = ___value0;
		__this->set_U3CUpdateIntervalU3Ek__BackingField_16(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_AllowedCpuActiveTimeRatio_m1526444FAD9758DC00AA9DB07425E26A779750C5_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float AllowedCpuActiveTimeRatio { get; set; }
		float L_0 = ___value0;
		__this->set_U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_AllowedGpuActiveTimeRatio_mD7660071F2B7DEB8381B364116922BE312A4AA0D_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float AllowedGpuActiveTimeRatio { get; set; }
		float L_0 = ___value0;
		__this->set_U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_GpuLevelBounceAvoidanceThreshold_m2EE42CF824872EDB3CC001F87B44082954C32627_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float GpuLevelBounceAvoidanceThreshold { get; set; }
		float L_0 = ___value0;
		__this->set_U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_CpuLevelBounceAvoidanceThreshold_m17883BE79795835F004C470CF5AD8B06829FCF6B_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float CpuLevelBounceAvoidanceThreshold { get; set; }
		float L_0 = ___value0;
		__this->set_U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_MinTargetFrameRateHitTime_mFED58D9C51678B867173E655AA4A6DF2B7060B48_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float MinTargetFrameRateHitTime { get; set; }
		float L_0 = ___value0;
		__this->set_U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AutoPerformanceLevelController_set_MaxTemperatureLevel_m9826A6487749ADC7D1A66096D1A41FB8D8A31E93_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float MaxTemperatureLevel { get; set; }
		float L_0 = ___value0;
		__this->set_U3CMaxTemperatureLevelU3Ek__BackingField_18(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_UpdateInterval_mFDABD8980DD503CD69B461CFC843D8BD428CF4B9_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float UpdateInterval { get; set; }
		float L_0 = __this->get_U3CUpdateIntervalU3Ek__BackingField_16();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_MinTargetFrameRateHitTime_m988C790CEF196B60A8154D3B5E6655501550D364_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float MinTargetFrameRateHitTime { get; set; }
		float L_0 = __this->get_U3CMinTargetFrameRateHitTimeU3Ek__BackingField_17();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PerformanceBottleneckChangeEventArgs_get_PerformanceBottleneck_m625E64FC86EF5C7DD4BEECE772F34AF22C07E4BD_inline (PerformanceBottleneckChangeEventArgs_tC13901ADF557B1C70105475422E686E18B27E4D1 * __this, const RuntimeMethod* method)
{
	{
		// public PerformanceBottleneck PerformanceBottleneck { get; set; }
		int32_t L_0 = __this->get_U3CPerformanceBottleneckU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_CpuLevelBounceAvoidanceThreshold_mD4671D4F90CB5F09AD26552591A1513A52206606_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float CpuLevelBounceAvoidanceThreshold { get; set; }
		float L_0 = __this->get_U3CCpuLevelBounceAvoidanceThresholdU3Ek__BackingField_15();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_TargetFrameTime_m3EF525DF9A1AAFFA8E7A3839A634D6A3413398C3_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float TargetFrameTime { get; set; }
		float L_0 = __this->get_U3CTargetFrameTimeU3Ek__BackingField_11();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_AllowedCpuActiveTimeRatio_mF966E1BBFDB821FEA660547F0CFFE0338F493782_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float AllowedCpuActiveTimeRatio { get; set; }
		float L_0 = __this->get_U3CAllowedCpuActiveTimeRatioU3Ek__BackingField_12();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_GpuLevelBounceAvoidanceThreshold_mB46CCEE95E662384E848FA16446C783A622BBB41_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float GpuLevelBounceAvoidanceThreshold { get; set; }
		float L_0 = __this->get_U3CGpuLevelBounceAvoidanceThresholdU3Ek__BackingField_14();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_AllowedGpuActiveTimeRatio_mED7543A79BF8EDB3F876FCF5E815E58C39EE98A4_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float AllowedGpuActiveTimeRatio { get; set; }
		float L_0 = __this->get_U3CAllowedGpuActiveTimeRatioU3Ek__BackingField_13();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float ThermalMetrics_get_TemperatureLevel_m1E7E3A67FD29A5ABA322EB933CF7409CD0151212_inline (ThermalMetrics_t2F84049ACC7ED8A10698534C996D6F4392494125 * __this, const RuntimeMethod* method)
{
	{
		// public float TemperatureLevel { get; set; }
		float L_0 = __this->get_U3CTemperatureLevelU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float AutoPerformanceLevelController_get_MaxTemperatureLevel_m121C40CD08B738C7B5B2646490700F20A3B4AFB7_inline (AutoPerformanceLevelController_t38F6A28E982918E09DE312328148F98EF56ECF8A * __this, const RuntimeMethod* method)
{
	{
		// public float MaxTemperatureLevel { get; set; }
		float L_0 = __this->get_U3CMaxTemperatureLevelU3Ek__BackingField_18();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float MainThreadCpuTime_GetLatestResult_m5E3D72F4A7FBEDD957340CDAB3E19A128269D5B6_inline (MainThreadCpuTime_tFC6D319F06E32AAF7D6F8C7EDD6DE5388C7E8887 * __this, const RuntimeMethod* method)
{
	{
		// return m_LatestMainthreadCpuTime;
		float L_0 = __this->get_m_LatestMainthreadCpuTime_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_PerformanceControlMode_mE4A2CDE249A57590CA4CD493ACA4DEEC745E1093_inline (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public PerformanceControlMode PerformanceControlMode { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CPerformanceControlModeU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_CpuLevel_m844481992806A1B152181FA8631B6033003CD640_inline (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCpuLevelU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_GpuLevel_mBF3785C7E3C4FDEBF29E2E8CE640582658A6BE91_inline (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int GpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CGpuLevelU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_CpuLevelDelta_mDE58CBE256AE306670BE51649DE8895060860A77_inline (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CpuLevelDelta { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCpuLevelDeltaU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceLevelChangeEventArgs_set_GpuLevelDelta_mBBD8DC5572E5FC9EEA8F67AB5995121010F09532_inline (PerformanceLevelChangeEventArgs_tCAA5D347C5E4AB569E5CBE742474FE8BF6576FDB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int GpuLevelDelta { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CGpuLevelDeltaU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_CpuLevel_m3222509C4D795522341F1E93150940DC1966B4B2_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method)
{
	{
		// public int CpuLevel { get; set; }
		int32_t L_0 = __this->get_U3CCpuLevelU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DevicePerformanceControlImpl_get_GpuLevel_m519D52125AE15CDFC3640EBA56B2D8D2B6F5A9B4_inline (DevicePerformanceControlImpl_t45D067B0073442BCA748F02C34685848B4FEB326 * __this, const RuntimeMethod* method)
{
	{
		// public int GpuLevel { get; set; }
		int32_t L_0 = __this->get_U3CGpuLevelU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceDataRecord_set_ChangeFlags_m637C86CD7C6A9D4766B6486C0D8CEDD4A0AB63B5_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public Feature ChangeFlags { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CChangeFlagsU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceDataRecord_set_CpuPerformanceLevel_m9B1602BD2C7D66ED154B2D2592667EB9A4270C8C_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int CpuPerformanceLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCpuPerformanceLevelU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceDataRecord_set_GpuPerformanceLevel_mCFD2AA62DA519F272118E5019AE48405BD618E2B_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int GpuPerformanceLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CGpuPerformanceLevelU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void PerformanceDataRecord_set_PerformanceLevelControlAvailable_m365175DA2C8A6D9EE5D000E389958FDF6F2E23A1_inline (PerformanceDataRecord_tAD337797C5D0893A3448FD9F9ABB648D07EE31DF * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool PerformanceLevelControlAvailable { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CPerformanceLevelControlAvailableU3Ek__BackingField_6(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AdaptivePerformanceSubsystem_set_Capabilities_m6ACAED2EA7E44E3AF2D8AEB25A42EFA82EFB67F3_inline (AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public Feature Capabilities { get; protected set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCapabilitiesU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void AdaptivePerformanceSubsystemBase_set_initialized_mCF60B3BF86B030D232CAC0EBE07AE58449808B0C_inline (AdaptivePerformanceSubsystemBase_tFE4445DE6A7F0520A1707ECCC71C516E3E4FB12E * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool initialized { get; protected set; }
		bool L_0 = ___value0;
		__this->set_U3CinitializedU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem_set_LastRequestedCpuLevel_m35EECCA0681188317D2619A80F6F3B64ABA5D592_inline (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int LastRequestedCpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CLastRequestedCpuLevelU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TestAdaptivePerformanceSubsystem_set_LastRequestedGpuLevel_m5FA5BDBAF556DE1F795CAC408AC077201BD34474_inline (TestAdaptivePerformanceSubsystem_t551B897A0E0CA567EA28CADDD5D7B79539ABA87F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int LastRequestedGpuLevel { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CLastRequestedGpuLevelU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void StartupSettings_set_Logging_m6CD2E66D91308712825AECF2A1AF80A86F7C4A30_inline (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_set_Logging_m6CD2E66D91308712825AECF2A1AF80A86F7C4A30Unity_AdaptivePerformance_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool Logging { get; set; }
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->set_U3CLoggingU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void StartupSettings_set_StatsLoggingFrequencyInFrames_m4AA8C016FE0DAD38D37117A32154D4B542D1D115_inline (int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_set_StatsLoggingFrequencyInFrames_m4AA8C016FE0DAD38D37117A32154D4B542D1D115Unity_AdaptivePerformance_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public int StatsLoggingFrequencyInFrames { get; set; }
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->set_U3CStatsLoggingFrequencyInFramesU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void StartupSettings_set_Enable_mA1F811FA9465F51D5742B28BFCAA450D9298B6C1_inline (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_set_Enable_mA1F811FA9465F51D5742B28BFCAA450D9298B6C1Unity_AdaptivePerformance_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool Enable { get; set; }
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->set_U3CEnableU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void StartupSettings_set_PreferredSubsystem_m84A3D06E2638662EE488242C3A5A8764ED31D8DD_inline (AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_set_PreferredSubsystem_m84A3D06E2638662EE488242C3A5A8764ED31D8DDUnity_AdaptivePerformance_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public Provider.AdaptivePerformanceSubsystem PreferredSubsystem { get; set; }
		AdaptivePerformanceSubsystem_t7026BBFB1493B26731FBA0A20AEB8A980F5D4528 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->set_U3CPreferredSubsystemU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void StartupSettings_set_AutomaticPerformanceControl_m9C2983846A71477818E1E33CBE4412E9D621A272_inline (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartupSettings_set_AutomaticPerformanceControl_m9C2983846A71477818E1E33CBE4412E9D621A272Unity_AdaptivePerformance_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool AutomaticPerformanceControl { get; set; }
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var);
		((StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_StaticFields*)il2cpp_codegen_static_fields_for(StartupSettings_tFE57C17938C40DE251D04E1C54A28904EAA14187_il2cpp_TypeInfo_var))->set_U3CAutomaticPerformanceControlU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TemperatureTrend_set_ThermalTrend_mA0E332378E70F80C414402AF81C001DDA7F2740B_inline (TemperatureTrend_t4E382E62C6530FF0AF056D371490D644B2B7D17B * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float ThermalTrend { get; private set; }
		float L_0 = ___value0;
		__this->set_U3CThermalTrendU3Ek__BackingField_15(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return L_0;
	}
}
