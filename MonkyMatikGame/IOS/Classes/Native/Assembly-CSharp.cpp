﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// ActivateOnKeypress
struct ActivateOnKeypress_t3EBED552416BF21197122C141DC4B936BB6DE1FB;
// BejucoController
struct BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4;
// Bullet
struct Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059;
// ChangeColor
struct ChangeColor_t11FE2BD8AD1F8A8AFB6E43B3737410A3C60AFC79;
// Cinemachine.CinemachineBlend
struct CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298;
// Cinemachine.CinemachineBlenderSettings
struct CinemachineBlenderSettings_tCD488C84B252394D8CDD7D0DB623D991BE77F3F0;
// Cinemachine.CinemachineBrain
struct CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB;
// Cinemachine.CinemachineBrain/BrainEvent
struct BrainEvent_tCB30E3A5C9244915F355517663D4E440A54F79C0;
// Cinemachine.CinemachineBrain/VcamActivatedEvent
struct VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B;
// Cinemachine.CinemachineComponentBase[]
struct CinemachineComponentBaseU5BU5D_t7DC13D28344CFE98AF56DA46BCDF936DA49CDA6E;
// Cinemachine.CinemachineComposer
struct CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8;
// Cinemachine.CinemachineCore/Stage[]
struct StageU5BU5D_tC107E846F8ED5DBF4CEF77B4AFCC26D9DBE723A2;
// Cinemachine.CinemachineFreeLook
struct CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152;
// Cinemachine.CinemachineFreeLook/CreateRigDelegate
struct CreateRigDelegate_tD3CD6B9257D1E55C62EA2C967131351472916D9B;
// Cinemachine.CinemachineFreeLook/DestroyRigDelegate
struct DestroyRigDelegate_t817A6E366317DBD7E47066614680E435F4753F43;
// Cinemachine.CinemachineFreeLook/Orbit[]
struct OrbitU5BU5D_t937BD50C0ED65D0BBC5ED836B1B32D9FD6792C4E;
// Cinemachine.CinemachineMixingCamera
struct CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE;
// Cinemachine.CinemachineOrbitalTransposer[]
struct CinemachineOrbitalTransposerU5BU5D_t3EE22418D270D58108A35D0162F2F44463218F5B;
// Cinemachine.CinemachineVirtualCamera
struct CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A;
// Cinemachine.CinemachineVirtualCamera/CreatePipelineDelegate
struct CreatePipelineDelegate_tDFD47CB61DD39569C0C372085A3461C5CAF5D269;
// Cinemachine.CinemachineVirtualCamera/DestroyPipelineDelegate
struct DestroyPipelineDelegate_t9942B8E054A78C0E7A3FCAAAC212E7CBC72D1CC1;
// Cinemachine.CinemachineVirtualCameraBase
struct CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD;
// Cinemachine.CinemachineVirtualCameraBase[]
struct CinemachineVirtualCameraBaseU5BU5D_tCD65D3C04B191624A196F05BE07D81E657436EF3;
// Cinemachine.CinemachineVirtualCamera[]
struct CinemachineVirtualCameraU5BU5D_tE70BE11F7F9BAE7CC377C1946095485E7796D026;
// Cinemachine.Examples.ActivateCamOnPlay
struct ActivateCamOnPlay_tCD29CD3A1DC809CC23DF4D6BC74437DB1EB8E7CE;
// Cinemachine.Examples.ActivateCameraWithDistance
struct ActivateCameraWithDistance_tC54EDA46FFCAA5DE57637DE63FE5FBB65E0AE247;
// Cinemachine.Examples.CharacterMovement
struct CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3;
// Cinemachine.Examples.CharacterMovement2D
struct CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484;
// Cinemachine.Examples.ExampleHelpWindow
struct ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378;
// Cinemachine.Examples.ExampleHelpWindow/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013;
// Cinemachine.Examples.GenericTrigger
struct GenericTrigger_t45676BF61EE6D8259E0E288AC627092B9E962CA7;
// Cinemachine.Examples.MixingCameraBlend
struct MixingCameraBlend_tCB7BBB9AC892ECC6DE8154D1F397E7F7B359D8E7;
// Cinemachine.Examples.ScriptingExample
struct ScriptingExample_t43DC6E8DA30C3BFFB9C51A0C22E54188F22A33E4;
// Cinemachine.ICinemachineCamera
struct ICinemachineCamera_t89A591897FD47A23EAD91D2B3DA94B7EB3B322FD;
// Cinemachine.ICinemachineTargetGroup
struct ICinemachineTargetGroup_tBD3C00BBD992B4AE3DE2C562A4EF8D50C0EDFD49;
// Cinemachine.Utility.PositionPredictor
struct PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223;
// EnemyPatrol
struct EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD;
// EnemyPatrol/<PatrolToTarget>d__11
struct U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED;
// InstantiatePrefab
struct InstantiatePrefab_t36976A57AFF7271A16E6F3932ACF6823CA3CA292;
// LoadScene
struct LoadScene_t674EFD288A318DA4B83B5427402567479C0B493D;
// PlayerController
struct PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85;
// PlayerMove
struct PlayerMove_tF4D209EF20A6DE6765B2D125AEA112228D5BD388;
// PlayerMoveOnSphere
struct PlayerMoveOnSphere_t82BC101F65B2488923B0D504BBA3BD830070009A;
// PlayerMovePhysics
struct PlayerMovePhysics_t4A6B0D15F96182589F0583094C84138989DDC1A8;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<UnityEngine.Playables.PlayableDirector>
struct Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Cinemachine.CinemachineVirtualCameraBase,System.Int32>
struct Dictionary_2_t0D2A029841990C10C2ACF3B66D554D04A3C6DBA6;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_tF2F04E1BE233EE6C4A6E90261E463B2A67A3FB6A;
// System.Collections.Generic.List`1<Cinemachine.CameraState/CustomBlendable>
struct List_1_t25C0DD5BAEE8988DC0A767964E16569E3A4880FD;
// System.Collections.Generic.List`1<Cinemachine.CinemachineBrain/BrainFrame>
struct List_1_t22F3D7410E401DD6A2908F6264F0046A93980F6F;
// System.Collections.Generic.List`1<Cinemachine.CinemachineExtension>
struct List_1_tB06968E265CE1689803547CD89D453ACFB3509D5;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.AnimationCurve[]
struct AnimationCurveU5BU5D_tAB8BA6D5F8B505DDDA61F8065F6E870AA1F560E1;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.Collider2D
struct Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Font
struct Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100;
// UnityEngine.GUIContent
struct GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B;
// UnityEngine.GUISettings
struct GUISettings_tA863524720A3C984BAE56598D922F2C04DC80EF4;
// UnityEngine.GUISkin
struct GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_tAB4CEEA8C8A0BDCFD51C9624AE173C46A40135D8;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GUIStyleState
struct GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t2F343713D6ADFF41BBCF1D17BAE79F51BD745DBB;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Playables.PlayableDirector
struct PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2;
// UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.SphereCollider
struct SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t8801328F075019AF6B6150B20EC343935A29FF97;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8;
// Weapon
struct Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9;
// ZuroStreet
struct ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256;
// ZuroStreet/<Attack>d__17
struct U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE;

IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GUI_t3E5CBC6B113E392EBBE1453DEF2B7CD020F345AA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICinemachineCamera_t89A591897FD47A23EAD91D2B3DA94B7EB3B322FD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Physics2D_tB21970F986016656D66D2922594F336E1EE7D5C7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral016337FC26A9B9DA6C098BECFA0937F0BE66B39A;
IL2CPP_EXTERN_C String_t* _stringLiteral0CB715D89D6589E699639FF0716A2BE52C44EEEE;
IL2CPP_EXTERN_C String_t* _stringLiteral1994557C9850CA12CEDF370B8F7E1A2E35748A9C;
IL2CPP_EXTERN_C String_t* _stringLiteral1A12A77F72140F5565E3C5D869CCFF5749CC4266;
IL2CPP_EXTERN_C String_t* _stringLiteral1B81F28B5AFBFCAAF5253D26B7DBA4028A92A860;
IL2CPP_EXTERN_C String_t* _stringLiteral1EBA140FDD9C6860A1730C408E3064AA417CA2A3;
IL2CPP_EXTERN_C String_t* _stringLiteral2D2CB022BC3D26BD1407C4AA787D5E46E1AD4C3B;
IL2CPP_EXTERN_C String_t* _stringLiteral3E441FC593FEA48A7F0A70675200FE569B0A781F;
IL2CPP_EXTERN_C String_t* _stringLiteral42BC272E41ECB68FE544AA26C6F3AC14C8E21034;
IL2CPP_EXTERN_C String_t* _stringLiteral4B937CC841D82F8936CEF1EFB88708AB5B0F1EE5;
IL2CPP_EXTERN_C String_t* _stringLiteral4E148B88EC5B524891DE6D50B0A87CF7A4A9BB8E;
IL2CPP_EXTERN_C String_t* _stringLiteral4F57A1CE99E68A7B05C42D0A7EA0070EAFABD31C;
IL2CPP_EXTERN_C String_t* _stringLiteral53DFE6C47A184FEE0A0DFEE574C2C6B5DD53D52F;
IL2CPP_EXTERN_C String_t* _stringLiteral61AD50A9B9189CC3CF1874568E35E7901FF4C982;
IL2CPP_EXTERN_C String_t* _stringLiteral69C81CA8C8D9D9FD760315C6D4E17A09327BA88D;
IL2CPP_EXTERN_C String_t* _stringLiteral6AF8D9EBBAF3BFBF8030673F1331CC3F10B2692C;
IL2CPP_EXTERN_C String_t* _stringLiteral817B354DC01A34895EAB0C37E21AF38DC0850D08;
IL2CPP_EXTERN_C String_t* _stringLiteral8297BB17C6AA1537D802E8AB86DCD018FFB7B6D0;
IL2CPP_EXTERN_C String_t* _stringLiteral90CCC9AA84EF50B2769F7D99C20C539D78F3915C;
IL2CPP_EXTERN_C String_t* _stringLiteralA7DE103B35721CCBF53276B75061293F51C3272B;
IL2CPP_EXTERN_C String_t* _stringLiteralB301C10B3D0A20C4D34F04D383741C386814B593;
IL2CPP_EXTERN_C String_t* _stringLiteralB9ACF0BA2F7AFB72061E78F24B2813707BC0174A;
IL2CPP_EXTERN_C String_t* _stringLiteralCC1EBDD04E76A4B8BAFFB29A22FA00446D382B18;
IL2CPP_EXTERN_C String_t* _stringLiteralCC8D7979F8F6B6CAC74FEF89F920AA835E515A9E;
IL2CPP_EXTERN_C String_t* _stringLiteralDC06D8A75872E885632CFBEFAA0BC18E6C22F90F;
IL2CPP_EXTERN_C String_t* _stringLiteralE53407CFE1A5156B9F0D1EED3BAB5EF3AE75CFD8;
IL2CPP_EXTERN_C String_t* _stringLiteralE952365FDE19D5E2968E1F673DF7C02ED2CC2C11;
IL2CPP_EXTERN_C String_t* _stringLiteralF914411ACA4F8D2182F5CD69FE08BE5A0733A5B7;
IL2CPP_EXTERN_C String_t* _stringLiteralF9502D1E54CC48D9377D8E7EC34EB756CF17D1B1;
IL2CPP_EXTERN_C String_t* _stringLiteralFD8E45BAC7D354790EC0A82D8A61341BF8ACD055;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CinemachineVirtualCamera_AddCinemachineComponent_TisCinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8_m345831406FEA1F9C352CFC582909DC8CF50E2FED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8_mA61ACEA9FC833694511132FC13E74FB7D7E14FD7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInChildren_TisWeapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9_m0B9B6D77DDFC65C7C8FD16BD86CAD38A1B44861C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB_m1AAD5BCEF9186B5DDC28BFB469CB07416D89C563_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE_m42F414DD742DED90FDEB557D0B6DB4DD06E22328_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD_m24A679371AC104E31BD5057C9CC036DE1983342F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisPlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2_m8EC8AA73D38CD6B2F7B6282C65E9C51946E3EC26_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisCinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB_m8C98E6FD74E6A35274E48DD803B5900F1F291263_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisCinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152_mC55F5EE96B665C6498ED43145950B14496235AD3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisCinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A_m7CF035809E82878DBC2A8ED4C21104BED3421143_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisBullet_t1B228DBA3982FDB21DE04329BDC49915421B9059_mC44AB386177196D8110F3E6D8F0E814D44438271_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m4F397BCC6697902B40033E61129D4EA6FE93570F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CAttackU3Ed__17_System_Collections_IEnumerator_Reset_m44994CB80C794CD5F3C627870E3B2AC5AC6EAE5F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CPatrolToTargetU3Ed__11_System_Collections_IEnumerator_Reset_m4A790E2666FDFBE6B63FCC94B70F2522443C9B9B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass4_0_U3COnGUIU3Eb__0_m84E30E3962AC9F5BA3EB82DEF5ABDA2DDE85D6EE_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t ActivateCamOnPlay_Start_m7CC36D9EF8A6D1AE712C67BF62FD15EF7392E65A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ActivateCameraWithDistance_SwitchCam_m5736882F44A433FF71B32E13605739D7B7B13A85_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ActivateCameraWithDistance_Update_m61B1F1909206E419F849BE447BC51F5197118976_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ActivateOnKeypress_Start_mC9C71BD760F97DBC0F686569A13EA91AA8F643C6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ActivateOnKeypress_Update_m7C06B9EF28D5420A29C17EC1D8D6C8239D489312_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t BejucoController_Awake_m795D66747ED8AB6D70E60E609312039CFDF739F9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t BejucoController_LateUpdate_m27F7BD50A67CFBA6B218859321D78044D1D9D939_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t BejucoController_Update_m2785342A43F73D03858EB28BCC340AA0CFBABD74_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Bullet_Awake_m37FD11C4734664815AF4EEF3C3814AEBCF88B6CD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Bullet_Start_m0C6AD7FCC791ADF08593B040294D8B7329617D04_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Bullet_Update_mEDD99D9C6CBBEBCD7C211792A42CBF1D965EB80E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ChangeColor_Awake_m4A2850FB998ED3DB1E294F7107D4C72787F81977_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ChangeColor_Start_m6D47E8F30AEAB75D8E034168972BEC8C4D0A6869_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ChangeColor_Update_m0F7BC07040DEF2BFDAFF5F448E1A8D2F92534890_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CharacterMovement2D_FixedUpdate_m0706C4622DDDFB6F424C3DC6B4F1DC144A0605C8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CharacterMovement2D_Start_m6C3CC414568A67E6FC1EF370C9E702A997416272_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CharacterMovement2D_isGrounded_m9181E8749ED948212A8D0A8E4631D625944BDD70_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CharacterMovement_FixedUpdate_m8615F2B05324419DBB1CEB9506DBDFD2F0F66A0F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CharacterMovement_Start_mEA069E540139D2289966584FC5067053E21BD85D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CharacterMovement_UpdateTargetDirection_m8571B65E5FCA3579F722687F2D53F0FCFF8AC214_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t EnemyPatrol_Awake_mB4793D74956B3910848A4B4DF141FE6A7F826A25_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t EnemyPatrol_CanShoot_mFBD932DF4BC7E42F3CD1ED71F77C9646FB51C7CE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t EnemyPatrol_PatrolToTarget_m675B9C77F20C755079DFE6B4AFFA4E1798C092ED_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t EnemyPatrol_Start_mF69D7343C30458904BF576DC87C4EE0D94D5E913_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t EnemyPatrol_UpdateTarget_mBF1D57F2C57B1B19634CFBEE098102D0F2C58819_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExampleHelpWindow_DrawWindow_m42568AC283611EA80B03CAAEE7FDD2C7025ED016_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExampleHelpWindow_OnGUI_mDE54F4CCAA35ABCC00159E6C883AEC6DC3BE4A66_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GenericTrigger_OnTriggerEnter_mFD4689405DEA14F65E2F8554BF3D571CD73C8E2C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GenericTrigger_OnTriggerExit_m9B5A0ECBA53C54E0C12DDFA32144509226B0E54B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GenericTrigger_Start_m9795906FC841CC4B2236CF8888A8306E1C94BBA4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InstantiatePrefab_Instantiate_m31E43D0401E2D3691190E3780516A2EE9A881B23_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MixingCameraBlend_Start_mE6154055F56E65873E236AB81E1C7A53780F90D6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t MixingCameraBlend_Update_m42804BA540A1712167B1EE7E8AEE3AEF359FAF90_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayerController_Awake_m118C1D205A374B627E1EC963E5837E23CF554573_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayerController_LateUpdate_mAF83D0B2354FDB3E9EEC98CB72E9D77EC004B881_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayerController_Update_m38903EF1C8F12B9388303741F8040EE26C33DC33_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayerMoveOnSphere_Update_m8E045C01FD8CCCCFC85B46DDD68978730E97C9B5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayerMovePhysics_FixedUpdate_m41FE39EEE213101002AA90DB208FF4611B71B42E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayerMovePhysics_OnEnable_m43E1851523FAECBAEDD18FF7072145D99CE6CEDF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayerMovePhysics_Start_m68C6BC74B80E0258B34E06A348E6C32F8FB48154_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayerMove_Update_m816082939684D509B053B2848EE5BB2A4B655F45_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ScriptingExample_Start_m79B8DBE9F2711279277D13BF1308C3516C19F90A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CAttackU3Ed__17_MoveNext_mD7EA6147D3EE57A0195060599A0A3A6217BB1B49_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CAttackU3Ed__17_System_Collections_IEnumerator_Reset_m44994CB80C794CD5F3C627870E3B2AC5AC6EAE5F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CPatrolToTargetU3Ed__11_MoveNext_m03BFEC4A71DDD0D46580989D40E65F070AD55DD3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CPatrolToTargetU3Ed__11_System_Collections_IEnumerator_Reset_m4A790E2666FDFBE6B63FCC94B70F2522443C9B9B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Weapon_Awake_m2AC14C75327F3526C722CACC9F4CCD7D95B21F98_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Weapon_Shoot_mF5B36E6C4428C1E8858F42526F5EEC7C7AF9D4E3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZuroStreet_AttackMovement_mFC008314CD71D1B0D3D15FD3ED1C5F3F778F78AA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZuroStreet_Attack_m586D7DE76436AE4116587A2A0A68809FF4D20347_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZuroStreet_Awake_mF39B194AFCADDAB34B04D3D49D262BC9EE9B7336_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZuroStreet_LateUpdate_m0866F695A7D62507AF67715D8439F810D8155A73_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZuroStreet_OnDisable_mB0DACFD3640047DD56EF34506911FED3EB1F0583_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZuroStreet_OnTriggerStay2D_m09FD5E717BF2CBB97B122544254BB7521866D51D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZuroStreet_Update_mF55F06C2957CFD22027E9DC239991E181ADB7228_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com;
struct GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke;
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com;

struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};


// System.Object


// Cinemachine.Examples.ExampleHelpWindow_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013  : public RuntimeObject
{
public:
	// System.Single Cinemachine.Examples.ExampleHelpWindow_<>c__DisplayClass4_0::maxWidth
	float ___maxWidth_0;
	// Cinemachine.Examples.ExampleHelpWindow Cinemachine.Examples.ExampleHelpWindow_<>c__DisplayClass4_0::<>4__this
	ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_maxWidth_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013, ___maxWidth_0)); }
	inline float get_maxWidth_0() const { return ___maxWidth_0; }
	inline float* get_address_of_maxWidth_0() { return &___maxWidth_0; }
	inline void set_maxWidth_0(float value)
	{
		___maxWidth_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013, ___U3CU3E4__this_1)); }
	inline ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// EnemyPatrol_<PatrolToTarget>d__11
struct  U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED  : public RuntimeObject
{
public:
	// System.Int32 EnemyPatrol_<PatrolToTarget>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object EnemyPatrol_<PatrolToTarget>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// EnemyPatrol EnemyPatrol_<PatrolToTarget>d__11::<>4__this
	EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED, ___U3CU3E4__this_2)); }
	inline EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.EmptyArray`1<System.Object>
struct  EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26  : public RuntimeObject
{
public:

public:
};

struct EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26_StaticFields
{
public:
	// T[] System.EmptyArray`1::Value
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26_StaticFields, ___Value_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_Value_0() const { return ___Value_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_0), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.GUIContent
struct  GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0  : public RuntimeObject
{
public:
	// System.String UnityEngine.GUIContent::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Texture UnityEngine.GUIContent::m_Image
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_Image_1;
	// System.String UnityEngine.GUIContent::m_Tooltip
	String_t* ___m_Tooltip_2;

public:
	inline static int32_t get_offset_of_m_Text_0() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0, ___m_Text_0)); }
	inline String_t* get_m_Text_0() const { return ___m_Text_0; }
	inline String_t** get_address_of_m_Text_0() { return &___m_Text_0; }
	inline void set_m_Text_0(String_t* value)
	{
		___m_Text_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Image_1() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0, ___m_Image_1)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_Image_1() const { return ___m_Image_1; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_Image_1() { return &___m_Image_1; }
	inline void set_m_Image_1(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_Image_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Image_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Tooltip_2), (void*)value);
	}
};

struct GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_StaticFields
{
public:
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Text
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___s_Text_3;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Image
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___s_Image_4;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_TextImage
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___s_TextImage_5;
	// UnityEngine.GUIContent UnityEngine.GUIContent::none
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___none_6;

public:
	inline static int32_t get_offset_of_s_Text_3() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_StaticFields, ___s_Text_3)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_s_Text_3() const { return ___s_Text_3; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_s_Text_3() { return &___s_Text_3; }
	inline void set_s_Text_3(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___s_Text_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Text_3), (void*)value);
	}

	inline static int32_t get_offset_of_s_Image_4() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_StaticFields, ___s_Image_4)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_s_Image_4() const { return ___s_Image_4; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_s_Image_4() { return &___s_Image_4; }
	inline void set_s_Image_4(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___s_Image_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Image_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_TextImage_5() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_StaticFields, ___s_TextImage_5)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_s_TextImage_5() const { return ___s_TextImage_5; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_s_TextImage_5() { return &___s_TextImage_5; }
	inline void set_s_TextImage_5(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___s_TextImage_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_TextImage_5), (void*)value);
	}

	inline static int32_t get_offset_of_none_6() { return static_cast<int32_t>(offsetof(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_StaticFields, ___none_6)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_none_6() const { return ___none_6; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_none_6() { return &___none_6; }
	inline void set_none_6(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___none_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___none_6), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.GUIContent
struct GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_marshaled_pinvoke
{
	char* ___m_Text_0;
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_Image_1;
	char* ___m_Tooltip_2;
};
// Native definition for COM marshalling of UnityEngine.GUIContent
struct GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_marshaled_com
{
	Il2CppChar* ___m_Text_0;
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_Image_1;
	Il2CppChar* ___m_Tooltip_2;
};

// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};

// ZuroStreet_<Attack>d__17
struct  U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE  : public RuntimeObject
{
public:
	// System.Int32 ZuroStreet_<Attack>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ZuroStreet_<Attack>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ZuroStreet ZuroStreet_<Attack>d__17::<>4__this
	ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE, ___U3CU3E4__this_2)); }
	inline ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// Cinemachine.AxisState_Recentering
struct  Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A 
{
public:
	// System.Boolean Cinemachine.AxisState_Recentering::m_enabled
	bool ___m_enabled_0;
	// System.Single Cinemachine.AxisState_Recentering::m_WaitTime
	float ___m_WaitTime_1;
	// System.Single Cinemachine.AxisState_Recentering::m_RecenteringTime
	float ___m_RecenteringTime_2;
	// System.Single Cinemachine.AxisState_Recentering::mLastAxisInputTime
	float ___mLastAxisInputTime_3;
	// System.Single Cinemachine.AxisState_Recentering::mRecenteringVelocity
	float ___mRecenteringVelocity_4;
	// System.Int32 Cinemachine.AxisState_Recentering::m_LegacyHeadingDefinition
	int32_t ___m_LegacyHeadingDefinition_5;
	// System.Int32 Cinemachine.AxisState_Recentering::m_LegacyVelocityFilterStrength
	int32_t ___m_LegacyVelocityFilterStrength_6;

public:
	inline static int32_t get_offset_of_m_enabled_0() { return static_cast<int32_t>(offsetof(Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A, ___m_enabled_0)); }
	inline bool get_m_enabled_0() const { return ___m_enabled_0; }
	inline bool* get_address_of_m_enabled_0() { return &___m_enabled_0; }
	inline void set_m_enabled_0(bool value)
	{
		___m_enabled_0 = value;
	}

	inline static int32_t get_offset_of_m_WaitTime_1() { return static_cast<int32_t>(offsetof(Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A, ___m_WaitTime_1)); }
	inline float get_m_WaitTime_1() const { return ___m_WaitTime_1; }
	inline float* get_address_of_m_WaitTime_1() { return &___m_WaitTime_1; }
	inline void set_m_WaitTime_1(float value)
	{
		___m_WaitTime_1 = value;
	}

	inline static int32_t get_offset_of_m_RecenteringTime_2() { return static_cast<int32_t>(offsetof(Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A, ___m_RecenteringTime_2)); }
	inline float get_m_RecenteringTime_2() const { return ___m_RecenteringTime_2; }
	inline float* get_address_of_m_RecenteringTime_2() { return &___m_RecenteringTime_2; }
	inline void set_m_RecenteringTime_2(float value)
	{
		___m_RecenteringTime_2 = value;
	}

	inline static int32_t get_offset_of_mLastAxisInputTime_3() { return static_cast<int32_t>(offsetof(Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A, ___mLastAxisInputTime_3)); }
	inline float get_mLastAxisInputTime_3() const { return ___mLastAxisInputTime_3; }
	inline float* get_address_of_mLastAxisInputTime_3() { return &___mLastAxisInputTime_3; }
	inline void set_mLastAxisInputTime_3(float value)
	{
		___mLastAxisInputTime_3 = value;
	}

	inline static int32_t get_offset_of_mRecenteringVelocity_4() { return static_cast<int32_t>(offsetof(Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A, ___mRecenteringVelocity_4)); }
	inline float get_mRecenteringVelocity_4() const { return ___mRecenteringVelocity_4; }
	inline float* get_address_of_mRecenteringVelocity_4() { return &___mRecenteringVelocity_4; }
	inline void set_mRecenteringVelocity_4(float value)
	{
		___mRecenteringVelocity_4 = value;
	}

	inline static int32_t get_offset_of_m_LegacyHeadingDefinition_5() { return static_cast<int32_t>(offsetof(Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A, ___m_LegacyHeadingDefinition_5)); }
	inline int32_t get_m_LegacyHeadingDefinition_5() const { return ___m_LegacyHeadingDefinition_5; }
	inline int32_t* get_address_of_m_LegacyHeadingDefinition_5() { return &___m_LegacyHeadingDefinition_5; }
	inline void set_m_LegacyHeadingDefinition_5(int32_t value)
	{
		___m_LegacyHeadingDefinition_5 = value;
	}

	inline static int32_t get_offset_of_m_LegacyVelocityFilterStrength_6() { return static_cast<int32_t>(offsetof(Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A, ___m_LegacyVelocityFilterStrength_6)); }
	inline int32_t get_m_LegacyVelocityFilterStrength_6() const { return ___m_LegacyVelocityFilterStrength_6; }
	inline int32_t* get_address_of_m_LegacyVelocityFilterStrength_6() { return &___m_LegacyVelocityFilterStrength_6; }
	inline void set_m_LegacyVelocityFilterStrength_6(int32_t value)
	{
		___m_LegacyVelocityFilterStrength_6 = value;
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.AxisState/Recentering
struct Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A_marshaled_pinvoke
{
	int32_t ___m_enabled_0;
	float ___m_WaitTime_1;
	float ___m_RecenteringTime_2;
	float ___mLastAxisInputTime_3;
	float ___mRecenteringVelocity_4;
	int32_t ___m_LegacyHeadingDefinition_5;
	int32_t ___m_LegacyVelocityFilterStrength_6;
};
// Native definition for COM marshalling of Cinemachine.AxisState/Recentering
struct Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A_marshaled_com
{
	int32_t ___m_enabled_0;
	float ___m_WaitTime_1;
	float ___m_RecenteringTime_2;
	float ___mLastAxisInputTime_3;
	float ___mRecenteringVelocity_4;
	int32_t ___m_LegacyHeadingDefinition_5;
	int32_t ___m_LegacyVelocityFilterStrength_6;
};

// Cinemachine.CameraState_CustomBlendable
struct  CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253 
{
public:
	// UnityEngine.Object Cinemachine.CameraState_CustomBlendable::m_Custom
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___m_Custom_0;
	// System.Single Cinemachine.CameraState_CustomBlendable::m_Weight
	float ___m_Weight_1;

public:
	inline static int32_t get_offset_of_m_Custom_0() { return static_cast<int32_t>(offsetof(CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253, ___m_Custom_0)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_m_Custom_0() const { return ___m_Custom_0; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_m_Custom_0() { return &___m_Custom_0; }
	inline void set_m_Custom_0(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___m_Custom_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Custom_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Weight_1() { return static_cast<int32_t>(offsetof(CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253, ___m_Weight_1)); }
	inline float get_m_Weight_1() const { return ___m_Weight_1; }
	inline float* get_address_of_m_Weight_1() { return &___m_Weight_1; }
	inline void set_m_Weight_1(float value)
	{
		___m_Weight_1 = value;
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	float ___m_Seconds_0;
};

// Cinemachine.AxisState_SpeedMode
struct  SpeedMode_t19B23D6F219F09E161A3E91EF7DF7BA6CCC4E60E 
{
public:
	// System.Int32 Cinemachine.AxisState_SpeedMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpeedMode_t19B23D6F219F09E161A3E91EF7DF7BA6CCC4E60E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CameraState_BlendHintValue
struct  BlendHintValue_tCF9A24D7B4477067080DB09C7E1BAD86C14B4B52 
{
public:
	// System.Int32 Cinemachine.CameraState_BlendHintValue::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlendHintValue_tCF9A24D7B4477067080DB09C7E1BAD86C14B4B52, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineBlendDefinition_Style
struct  Style_t709ACC1BF602ECD71DBFC58A23673543021BBF0F 
{
public:
	// System.Int32 Cinemachine.CinemachineBlendDefinition_Style::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Style_t709ACC1BF602ECD71DBFC58A23673543021BBF0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineBrain_BrainUpdateMethod
struct  BrainUpdateMethod_t0563DEDF54EDDA6F830CB1426DE00AA27B3D3A5E 
{
public:
	// System.Int32 Cinemachine.CinemachineBrain_BrainUpdateMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BrainUpdateMethod_t0563DEDF54EDDA6F830CB1426DE00AA27B3D3A5E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineBrain_UpdateMethod
struct  UpdateMethod_t24F3391B8C1CB67A2AF181A8EF51E994224C9FA2 
{
public:
	// System.Int32 Cinemachine.CinemachineBrain_UpdateMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateMethod_t24F3391B8C1CB67A2AF181A8EF51E994224C9FA2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineComposer_FovCache
struct  FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43 
{
public:
	// UnityEngine.Rect Cinemachine.CinemachineComposer_FovCache::mFovSoftGuideRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___mFovSoftGuideRect_0;
	// UnityEngine.Rect Cinemachine.CinemachineComposer_FovCache::mFovHardGuideRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___mFovHardGuideRect_1;
	// System.Single Cinemachine.CinemachineComposer_FovCache::mFovH
	float ___mFovH_2;
	// System.Single Cinemachine.CinemachineComposer_FovCache::mFov
	float ___mFov_3;
	// System.Single Cinemachine.CinemachineComposer_FovCache::mOrthoSizeOverDistance
	float ___mOrthoSizeOverDistance_4;
	// System.Single Cinemachine.CinemachineComposer_FovCache::mAspect
	float ___mAspect_5;
	// UnityEngine.Rect Cinemachine.CinemachineComposer_FovCache::mSoftGuideRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___mSoftGuideRect_6;
	// UnityEngine.Rect Cinemachine.CinemachineComposer_FovCache::mHardGuideRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___mHardGuideRect_7;

public:
	inline static int32_t get_offset_of_mFovSoftGuideRect_0() { return static_cast<int32_t>(offsetof(FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43, ___mFovSoftGuideRect_0)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_mFovSoftGuideRect_0() const { return ___mFovSoftGuideRect_0; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_mFovSoftGuideRect_0() { return &___mFovSoftGuideRect_0; }
	inline void set_mFovSoftGuideRect_0(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___mFovSoftGuideRect_0 = value;
	}

	inline static int32_t get_offset_of_mFovHardGuideRect_1() { return static_cast<int32_t>(offsetof(FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43, ___mFovHardGuideRect_1)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_mFovHardGuideRect_1() const { return ___mFovHardGuideRect_1; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_mFovHardGuideRect_1() { return &___mFovHardGuideRect_1; }
	inline void set_mFovHardGuideRect_1(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___mFovHardGuideRect_1 = value;
	}

	inline static int32_t get_offset_of_mFovH_2() { return static_cast<int32_t>(offsetof(FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43, ___mFovH_2)); }
	inline float get_mFovH_2() const { return ___mFovH_2; }
	inline float* get_address_of_mFovH_2() { return &___mFovH_2; }
	inline void set_mFovH_2(float value)
	{
		___mFovH_2 = value;
	}

	inline static int32_t get_offset_of_mFov_3() { return static_cast<int32_t>(offsetof(FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43, ___mFov_3)); }
	inline float get_mFov_3() const { return ___mFov_3; }
	inline float* get_address_of_mFov_3() { return &___mFov_3; }
	inline void set_mFov_3(float value)
	{
		___mFov_3 = value;
	}

	inline static int32_t get_offset_of_mOrthoSizeOverDistance_4() { return static_cast<int32_t>(offsetof(FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43, ___mOrthoSizeOverDistance_4)); }
	inline float get_mOrthoSizeOverDistance_4() const { return ___mOrthoSizeOverDistance_4; }
	inline float* get_address_of_mOrthoSizeOverDistance_4() { return &___mOrthoSizeOverDistance_4; }
	inline void set_mOrthoSizeOverDistance_4(float value)
	{
		___mOrthoSizeOverDistance_4 = value;
	}

	inline static int32_t get_offset_of_mAspect_5() { return static_cast<int32_t>(offsetof(FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43, ___mAspect_5)); }
	inline float get_mAspect_5() const { return ___mAspect_5; }
	inline float* get_address_of_mAspect_5() { return &___mAspect_5; }
	inline void set_mAspect_5(float value)
	{
		___mAspect_5 = value;
	}

	inline static int32_t get_offset_of_mSoftGuideRect_6() { return static_cast<int32_t>(offsetof(FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43, ___mSoftGuideRect_6)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_mSoftGuideRect_6() const { return ___mSoftGuideRect_6; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_mSoftGuideRect_6() { return &___mSoftGuideRect_6; }
	inline void set_mSoftGuideRect_6(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___mSoftGuideRect_6 = value;
	}

	inline static int32_t get_offset_of_mHardGuideRect_7() { return static_cast<int32_t>(offsetof(FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43, ___mHardGuideRect_7)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_mHardGuideRect_7() const { return ___mHardGuideRect_7; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_mHardGuideRect_7() { return &___mHardGuideRect_7; }
	inline void set_mHardGuideRect_7(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___mHardGuideRect_7 = value;
	}
};


// Cinemachine.CinemachineOrbitalTransposer_Heading_HeadingDefinition
struct  HeadingDefinition_t6D843C978E13F26B65C4942A247A80CF46524E5A 
{
public:
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer_Heading_HeadingDefinition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HeadingDefinition_t6D843C978E13F26B65C4942A247A80CF46524E5A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineTransposer_BindingMode
struct  BindingMode_t5CC9BD27C67513F5107A97CE21AF5600063DB6A6 
{
public:
	// System.Int32 Cinemachine.CinemachineTransposer_BindingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingMode_t5CC9BD27C67513F5107A97CE21AF5600063DB6A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineVirtualCameraBase_BlendHint
struct  BlendHint_t7D5604B2298076677193EF9C12950B17EA52ACA5 
{
public:
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase_BlendHint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlendHint_t7D5604B2298076677193EF9C12950B17EA52ACA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineVirtualCameraBase_StandbyUpdateMode
struct  StandbyUpdateMode_tDD387031AFF8EB8516338BC71517029CB6F1BE18 
{
public:
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase_StandbyUpdateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StandbyUpdateMode_tDD387031AFF8EB8516338BC71517029CB6F1BE18, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.Examples.MixingCameraBlend_AxisEnum
struct  AxisEnum_tFCFB8D242C33C863B0666247A5FCB10D06877C14 
{
public:
	// System.Int32 Cinemachine.Examples.MixingCameraBlend_AxisEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisEnum_tFCFB8D242C33C863B0666247A5FCB10D06877C14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.LensSettings
struct  LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC 
{
public:
	// System.Single Cinemachine.LensSettings::FieldOfView
	float ___FieldOfView_1;
	// System.Single Cinemachine.LensSettings::OrthographicSize
	float ___OrthographicSize_2;
	// System.Single Cinemachine.LensSettings::NearClipPlane
	float ___NearClipPlane_3;
	// System.Single Cinemachine.LensSettings::FarClipPlane
	float ___FarClipPlane_4;
	// System.Single Cinemachine.LensSettings::Dutch
	float ___Dutch_5;
	// System.Boolean Cinemachine.LensSettings::<Orthographic>k__BackingField
	bool ___U3COrthographicU3Ek__BackingField_6;
	// UnityEngine.Vector2 Cinemachine.LensSettings::<SensorSize>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CSensorSizeU3Ek__BackingField_7;
	// System.Boolean Cinemachine.LensSettings::<IsPhysicalCamera>k__BackingField
	bool ___U3CIsPhysicalCameraU3Ek__BackingField_8;
	// UnityEngine.Vector2 Cinemachine.LensSettings::LensShift
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___LensShift_9;

public:
	inline static int32_t get_offset_of_FieldOfView_1() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___FieldOfView_1)); }
	inline float get_FieldOfView_1() const { return ___FieldOfView_1; }
	inline float* get_address_of_FieldOfView_1() { return &___FieldOfView_1; }
	inline void set_FieldOfView_1(float value)
	{
		___FieldOfView_1 = value;
	}

	inline static int32_t get_offset_of_OrthographicSize_2() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___OrthographicSize_2)); }
	inline float get_OrthographicSize_2() const { return ___OrthographicSize_2; }
	inline float* get_address_of_OrthographicSize_2() { return &___OrthographicSize_2; }
	inline void set_OrthographicSize_2(float value)
	{
		___OrthographicSize_2 = value;
	}

	inline static int32_t get_offset_of_NearClipPlane_3() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___NearClipPlane_3)); }
	inline float get_NearClipPlane_3() const { return ___NearClipPlane_3; }
	inline float* get_address_of_NearClipPlane_3() { return &___NearClipPlane_3; }
	inline void set_NearClipPlane_3(float value)
	{
		___NearClipPlane_3 = value;
	}

	inline static int32_t get_offset_of_FarClipPlane_4() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___FarClipPlane_4)); }
	inline float get_FarClipPlane_4() const { return ___FarClipPlane_4; }
	inline float* get_address_of_FarClipPlane_4() { return &___FarClipPlane_4; }
	inline void set_FarClipPlane_4(float value)
	{
		___FarClipPlane_4 = value;
	}

	inline static int32_t get_offset_of_Dutch_5() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___Dutch_5)); }
	inline float get_Dutch_5() const { return ___Dutch_5; }
	inline float* get_address_of_Dutch_5() { return &___Dutch_5; }
	inline void set_Dutch_5(float value)
	{
		___Dutch_5 = value;
	}

	inline static int32_t get_offset_of_U3COrthographicU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___U3COrthographicU3Ek__BackingField_6)); }
	inline bool get_U3COrthographicU3Ek__BackingField_6() const { return ___U3COrthographicU3Ek__BackingField_6; }
	inline bool* get_address_of_U3COrthographicU3Ek__BackingField_6() { return &___U3COrthographicU3Ek__BackingField_6; }
	inline void set_U3COrthographicU3Ek__BackingField_6(bool value)
	{
		___U3COrthographicU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CSensorSizeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___U3CSensorSizeU3Ek__BackingField_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CSensorSizeU3Ek__BackingField_7() const { return ___U3CSensorSizeU3Ek__BackingField_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CSensorSizeU3Ek__BackingField_7() { return &___U3CSensorSizeU3Ek__BackingField_7; }
	inline void set_U3CSensorSizeU3Ek__BackingField_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CSensorSizeU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsPhysicalCameraU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___U3CIsPhysicalCameraU3Ek__BackingField_8)); }
	inline bool get_U3CIsPhysicalCameraU3Ek__BackingField_8() const { return ___U3CIsPhysicalCameraU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsPhysicalCameraU3Ek__BackingField_8() { return &___U3CIsPhysicalCameraU3Ek__BackingField_8; }
	inline void set_U3CIsPhysicalCameraU3Ek__BackingField_8(bool value)
	{
		___U3CIsPhysicalCameraU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_LensShift_9() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___LensShift_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_LensShift_9() const { return ___LensShift_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_LensShift_9() { return &___LensShift_9; }
	inline void set_LensShift_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___LensShift_9 = value;
	}
};

struct LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_StaticFields
{
public:
	// Cinemachine.LensSettings Cinemachine.LensSettings::Default
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_StaticFields, ___Default_0)); }
	inline LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  get_Default_0() const { return ___Default_0; }
	inline LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC * get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  value)
	{
		___Default_0 = value;
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.LensSettings
struct LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_marshaled_pinvoke
{
	float ___FieldOfView_1;
	float ___OrthographicSize_2;
	float ___NearClipPlane_3;
	float ___FarClipPlane_4;
	float ___Dutch_5;
	int32_t ___U3COrthographicU3Ek__BackingField_6;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CSensorSizeU3Ek__BackingField_7;
	int32_t ___U3CIsPhysicalCameraU3Ek__BackingField_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___LensShift_9;
};
// Native definition for COM marshalling of Cinemachine.LensSettings
struct LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_marshaled_com
{
	float ___FieldOfView_1;
	float ___OrthographicSize_2;
	float ___NearClipPlane_3;
	float ___FarClipPlane_4;
	float ___Dutch_5;
	int32_t ___U3COrthographicU3Ek__BackingField_6;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CSensorSizeU3Ek__BackingField_7;
	int32_t ___U3CIsPhysicalCameraU3Ek__BackingField_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___LensShift_9;
};

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.Coroutine
struct  Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.ForceMode2D
struct  ForceMode2D_t80F82E2BBC1733F0B88593968BA9CE9BC96A7E36 
{
public:
	// System.Int32 UnityEngine.ForceMode2D::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ForceMode2D_t80F82E2BBC1733F0B88593968BA9CE9BC96A7E36, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.GUILayoutOption_Type
struct  Type_t1060D19522CDA0F7C9A26733BE1E8C8E20AC1278 
{
public:
	// System.Int32 UnityEngine.GUILayoutOption_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t1060D19522CDA0F7C9A26733BE1E8C8E20AC1278, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RectOffset
struct  RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SourceStyle_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};

// Cinemachine.AxisState
struct  AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B 
{
public:
	// System.Single Cinemachine.AxisState::Value
	float ___Value_0;
	// Cinemachine.AxisState_SpeedMode Cinemachine.AxisState::m_SpeedMode
	int32_t ___m_SpeedMode_1;
	// System.Single Cinemachine.AxisState::m_MaxSpeed
	float ___m_MaxSpeed_2;
	// System.Single Cinemachine.AxisState::m_AccelTime
	float ___m_AccelTime_3;
	// System.Single Cinemachine.AxisState::m_DecelTime
	float ___m_DecelTime_4;
	// System.String Cinemachine.AxisState::m_InputAxisName
	String_t* ___m_InputAxisName_5;
	// System.Single Cinemachine.AxisState::m_InputAxisValue
	float ___m_InputAxisValue_6;
	// System.Boolean Cinemachine.AxisState::m_InvertInput
	bool ___m_InvertInput_7;
	// System.Single Cinemachine.AxisState::m_MinValue
	float ___m_MinValue_8;
	// System.Single Cinemachine.AxisState::m_MaxValue
	float ___m_MaxValue_9;
	// System.Boolean Cinemachine.AxisState::m_Wrap
	bool ___m_Wrap_10;
	// Cinemachine.AxisState_Recentering Cinemachine.AxisState::m_Recentering
	Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A  ___m_Recentering_11;
	// System.Single Cinemachine.AxisState::mCurrentSpeed
	float ___mCurrentSpeed_12;
	// System.Boolean Cinemachine.AxisState::<ValueRangeLocked>k__BackingField
	bool ___U3CValueRangeLockedU3Ek__BackingField_14;
	// System.Boolean Cinemachine.AxisState::<HasRecentering>k__BackingField
	bool ___U3CHasRecenteringU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___Value_0)); }
	inline float get_Value_0() const { return ___Value_0; }
	inline float* get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(float value)
	{
		___Value_0 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMode_1() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___m_SpeedMode_1)); }
	inline int32_t get_m_SpeedMode_1() const { return ___m_SpeedMode_1; }
	inline int32_t* get_address_of_m_SpeedMode_1() { return &___m_SpeedMode_1; }
	inline void set_m_SpeedMode_1(int32_t value)
	{
		___m_SpeedMode_1 = value;
	}

	inline static int32_t get_offset_of_m_MaxSpeed_2() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___m_MaxSpeed_2)); }
	inline float get_m_MaxSpeed_2() const { return ___m_MaxSpeed_2; }
	inline float* get_address_of_m_MaxSpeed_2() { return &___m_MaxSpeed_2; }
	inline void set_m_MaxSpeed_2(float value)
	{
		___m_MaxSpeed_2 = value;
	}

	inline static int32_t get_offset_of_m_AccelTime_3() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___m_AccelTime_3)); }
	inline float get_m_AccelTime_3() const { return ___m_AccelTime_3; }
	inline float* get_address_of_m_AccelTime_3() { return &___m_AccelTime_3; }
	inline void set_m_AccelTime_3(float value)
	{
		___m_AccelTime_3 = value;
	}

	inline static int32_t get_offset_of_m_DecelTime_4() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___m_DecelTime_4)); }
	inline float get_m_DecelTime_4() const { return ___m_DecelTime_4; }
	inline float* get_address_of_m_DecelTime_4() { return &___m_DecelTime_4; }
	inline void set_m_DecelTime_4(float value)
	{
		___m_DecelTime_4 = value;
	}

	inline static int32_t get_offset_of_m_InputAxisName_5() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___m_InputAxisName_5)); }
	inline String_t* get_m_InputAxisName_5() const { return ___m_InputAxisName_5; }
	inline String_t** get_address_of_m_InputAxisName_5() { return &___m_InputAxisName_5; }
	inline void set_m_InputAxisName_5(String_t* value)
	{
		___m_InputAxisName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InputAxisName_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_InputAxisValue_6() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___m_InputAxisValue_6)); }
	inline float get_m_InputAxisValue_6() const { return ___m_InputAxisValue_6; }
	inline float* get_address_of_m_InputAxisValue_6() { return &___m_InputAxisValue_6; }
	inline void set_m_InputAxisValue_6(float value)
	{
		___m_InputAxisValue_6 = value;
	}

	inline static int32_t get_offset_of_m_InvertInput_7() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___m_InvertInput_7)); }
	inline bool get_m_InvertInput_7() const { return ___m_InvertInput_7; }
	inline bool* get_address_of_m_InvertInput_7() { return &___m_InvertInput_7; }
	inline void set_m_InvertInput_7(bool value)
	{
		___m_InvertInput_7 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_8() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___m_MinValue_8)); }
	inline float get_m_MinValue_8() const { return ___m_MinValue_8; }
	inline float* get_address_of_m_MinValue_8() { return &___m_MinValue_8; }
	inline void set_m_MinValue_8(float value)
	{
		___m_MinValue_8 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_9() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___m_MaxValue_9)); }
	inline float get_m_MaxValue_9() const { return ___m_MaxValue_9; }
	inline float* get_address_of_m_MaxValue_9() { return &___m_MaxValue_9; }
	inline void set_m_MaxValue_9(float value)
	{
		___m_MaxValue_9 = value;
	}

	inline static int32_t get_offset_of_m_Wrap_10() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___m_Wrap_10)); }
	inline bool get_m_Wrap_10() const { return ___m_Wrap_10; }
	inline bool* get_address_of_m_Wrap_10() { return &___m_Wrap_10; }
	inline void set_m_Wrap_10(bool value)
	{
		___m_Wrap_10 = value;
	}

	inline static int32_t get_offset_of_m_Recentering_11() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___m_Recentering_11)); }
	inline Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A  get_m_Recentering_11() const { return ___m_Recentering_11; }
	inline Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A * get_address_of_m_Recentering_11() { return &___m_Recentering_11; }
	inline void set_m_Recentering_11(Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A  value)
	{
		___m_Recentering_11 = value;
	}

	inline static int32_t get_offset_of_mCurrentSpeed_12() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___mCurrentSpeed_12)); }
	inline float get_mCurrentSpeed_12() const { return ___mCurrentSpeed_12; }
	inline float* get_address_of_mCurrentSpeed_12() { return &___mCurrentSpeed_12; }
	inline void set_mCurrentSpeed_12(float value)
	{
		___mCurrentSpeed_12 = value;
	}

	inline static int32_t get_offset_of_U3CValueRangeLockedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___U3CValueRangeLockedU3Ek__BackingField_14)); }
	inline bool get_U3CValueRangeLockedU3Ek__BackingField_14() const { return ___U3CValueRangeLockedU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CValueRangeLockedU3Ek__BackingField_14() { return &___U3CValueRangeLockedU3Ek__BackingField_14; }
	inline void set_U3CValueRangeLockedU3Ek__BackingField_14(bool value)
	{
		___U3CValueRangeLockedU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CHasRecenteringU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B, ___U3CHasRecenteringU3Ek__BackingField_15)); }
	inline bool get_U3CHasRecenteringU3Ek__BackingField_15() const { return ___U3CHasRecenteringU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CHasRecenteringU3Ek__BackingField_15() { return &___U3CHasRecenteringU3Ek__BackingField_15; }
	inline void set_U3CHasRecenteringU3Ek__BackingField_15(bool value)
	{
		___U3CHasRecenteringU3Ek__BackingField_15 = value;
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.AxisState
struct AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B_marshaled_pinvoke
{
	float ___Value_0;
	int32_t ___m_SpeedMode_1;
	float ___m_MaxSpeed_2;
	float ___m_AccelTime_3;
	float ___m_DecelTime_4;
	char* ___m_InputAxisName_5;
	float ___m_InputAxisValue_6;
	int32_t ___m_InvertInput_7;
	float ___m_MinValue_8;
	float ___m_MaxValue_9;
	int32_t ___m_Wrap_10;
	Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A_marshaled_pinvoke ___m_Recentering_11;
	float ___mCurrentSpeed_12;
	int32_t ___U3CValueRangeLockedU3Ek__BackingField_14;
	int32_t ___U3CHasRecenteringU3Ek__BackingField_15;
};
// Native definition for COM marshalling of Cinemachine.AxisState
struct AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B_marshaled_com
{
	float ___Value_0;
	int32_t ___m_SpeedMode_1;
	float ___m_MaxSpeed_2;
	float ___m_AccelTime_3;
	float ___m_DecelTime_4;
	Il2CppChar* ___m_InputAxisName_5;
	float ___m_InputAxisValue_6;
	int32_t ___m_InvertInput_7;
	float ___m_MinValue_8;
	float ___m_MaxValue_9;
	int32_t ___m_Wrap_10;
	Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A_marshaled_com ___m_Recentering_11;
	float ___mCurrentSpeed_12;
	int32_t ___U3CValueRangeLockedU3Ek__BackingField_14;
	int32_t ___U3CHasRecenteringU3Ek__BackingField_15;
};

// Cinemachine.CameraState
struct  CameraState_t308F3A442112B7464D2B21A417D325662E3399B1 
{
public:
	// Cinemachine.LensSettings Cinemachine.CameraState::<Lens>k__BackingField
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  ___U3CLensU3Ek__BackingField_0;
	// UnityEngine.Vector3 Cinemachine.CameraState::<ReferenceUp>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CReferenceUpU3Ek__BackingField_1;
	// UnityEngine.Vector3 Cinemachine.CameraState::<ReferenceLookAt>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CReferenceLookAtU3Ek__BackingField_2;
	// UnityEngine.Vector3 Cinemachine.CameraState::<RawPosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CRawPositionU3Ek__BackingField_4;
	// UnityEngine.Quaternion Cinemachine.CameraState::<RawOrientation>k__BackingField
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3CRawOrientationU3Ek__BackingField_5;
	// UnityEngine.Vector3 Cinemachine.CameraState::<PositionDampingBypass>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CPositionDampingBypassU3Ek__BackingField_6;
	// System.Single Cinemachine.CameraState::<ShotQuality>k__BackingField
	float ___U3CShotQualityU3Ek__BackingField_7;
	// UnityEngine.Vector3 Cinemachine.CameraState::<PositionCorrection>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CPositionCorrectionU3Ek__BackingField_8;
	// UnityEngine.Quaternion Cinemachine.CameraState::<OrientationCorrection>k__BackingField
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3COrientationCorrectionU3Ek__BackingField_9;
	// Cinemachine.CameraState_BlendHintValue Cinemachine.CameraState::<BlendHint>k__BackingField
	int32_t ___U3CBlendHintU3Ek__BackingField_10;
	// Cinemachine.CameraState_CustomBlendable Cinemachine.CameraState::mCustom0
	CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  ___mCustom0_11;
	// Cinemachine.CameraState_CustomBlendable Cinemachine.CameraState::mCustom1
	CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  ___mCustom1_12;
	// Cinemachine.CameraState_CustomBlendable Cinemachine.CameraState::mCustom2
	CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  ___mCustom2_13;
	// Cinemachine.CameraState_CustomBlendable Cinemachine.CameraState::mCustom3
	CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  ___mCustom3_14;
	// System.Collections.Generic.List`1<Cinemachine.CameraState_CustomBlendable> Cinemachine.CameraState::m_CustomOverflow
	List_1_t25C0DD5BAEE8988DC0A767964E16569E3A4880FD * ___m_CustomOverflow_15;
	// System.Int32 Cinemachine.CameraState::<NumCustomBlendables>k__BackingField
	int32_t ___U3CNumCustomBlendablesU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CLensU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___U3CLensU3Ek__BackingField_0)); }
	inline LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  get_U3CLensU3Ek__BackingField_0() const { return ___U3CLensU3Ek__BackingField_0; }
	inline LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC * get_address_of_U3CLensU3Ek__BackingField_0() { return &___U3CLensU3Ek__BackingField_0; }
	inline void set_U3CLensU3Ek__BackingField_0(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  value)
	{
		___U3CLensU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceUpU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___U3CReferenceUpU3Ek__BackingField_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CReferenceUpU3Ek__BackingField_1() const { return ___U3CReferenceUpU3Ek__BackingField_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CReferenceUpU3Ek__BackingField_1() { return &___U3CReferenceUpU3Ek__BackingField_1; }
	inline void set_U3CReferenceUpU3Ek__BackingField_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CReferenceUpU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceLookAtU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___U3CReferenceLookAtU3Ek__BackingField_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CReferenceLookAtU3Ek__BackingField_2() const { return ___U3CReferenceLookAtU3Ek__BackingField_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CReferenceLookAtU3Ek__BackingField_2() { return &___U3CReferenceLookAtU3Ek__BackingField_2; }
	inline void set_U3CReferenceLookAtU3Ek__BackingField_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CReferenceLookAtU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CRawPositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___U3CRawPositionU3Ek__BackingField_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CRawPositionU3Ek__BackingField_4() const { return ___U3CRawPositionU3Ek__BackingField_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CRawPositionU3Ek__BackingField_4() { return &___U3CRawPositionU3Ek__BackingField_4; }
	inline void set_U3CRawPositionU3Ek__BackingField_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CRawPositionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRawOrientationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___U3CRawOrientationU3Ek__BackingField_5)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_U3CRawOrientationU3Ek__BackingField_5() const { return ___U3CRawOrientationU3Ek__BackingField_5; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_U3CRawOrientationU3Ek__BackingField_5() { return &___U3CRawOrientationU3Ek__BackingField_5; }
	inline void set_U3CRawOrientationU3Ek__BackingField_5(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___U3CRawOrientationU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CPositionDampingBypassU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___U3CPositionDampingBypassU3Ek__BackingField_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CPositionDampingBypassU3Ek__BackingField_6() const { return ___U3CPositionDampingBypassU3Ek__BackingField_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CPositionDampingBypassU3Ek__BackingField_6() { return &___U3CPositionDampingBypassU3Ek__BackingField_6; }
	inline void set_U3CPositionDampingBypassU3Ek__BackingField_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CPositionDampingBypassU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CShotQualityU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___U3CShotQualityU3Ek__BackingField_7)); }
	inline float get_U3CShotQualityU3Ek__BackingField_7() const { return ___U3CShotQualityU3Ek__BackingField_7; }
	inline float* get_address_of_U3CShotQualityU3Ek__BackingField_7() { return &___U3CShotQualityU3Ek__BackingField_7; }
	inline void set_U3CShotQualityU3Ek__BackingField_7(float value)
	{
		___U3CShotQualityU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CPositionCorrectionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___U3CPositionCorrectionU3Ek__BackingField_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CPositionCorrectionU3Ek__BackingField_8() const { return ___U3CPositionCorrectionU3Ek__BackingField_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CPositionCorrectionU3Ek__BackingField_8() { return &___U3CPositionCorrectionU3Ek__BackingField_8; }
	inline void set_U3CPositionCorrectionU3Ek__BackingField_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CPositionCorrectionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3COrientationCorrectionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___U3COrientationCorrectionU3Ek__BackingField_9)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_U3COrientationCorrectionU3Ek__BackingField_9() const { return ___U3COrientationCorrectionU3Ek__BackingField_9; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_U3COrientationCorrectionU3Ek__BackingField_9() { return &___U3COrientationCorrectionU3Ek__BackingField_9; }
	inline void set_U3COrientationCorrectionU3Ek__BackingField_9(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___U3COrientationCorrectionU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CBlendHintU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___U3CBlendHintU3Ek__BackingField_10)); }
	inline int32_t get_U3CBlendHintU3Ek__BackingField_10() const { return ___U3CBlendHintU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CBlendHintU3Ek__BackingField_10() { return &___U3CBlendHintU3Ek__BackingField_10; }
	inline void set_U3CBlendHintU3Ek__BackingField_10(int32_t value)
	{
		___U3CBlendHintU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_mCustom0_11() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___mCustom0_11)); }
	inline CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  get_mCustom0_11() const { return ___mCustom0_11; }
	inline CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253 * get_address_of_mCustom0_11() { return &___mCustom0_11; }
	inline void set_mCustom0_11(CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  value)
	{
		___mCustom0_11 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___mCustom0_11))->___m_Custom_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_mCustom1_12() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___mCustom1_12)); }
	inline CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  get_mCustom1_12() const { return ___mCustom1_12; }
	inline CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253 * get_address_of_mCustom1_12() { return &___mCustom1_12; }
	inline void set_mCustom1_12(CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  value)
	{
		___mCustom1_12 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___mCustom1_12))->___m_Custom_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_mCustom2_13() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___mCustom2_13)); }
	inline CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  get_mCustom2_13() const { return ___mCustom2_13; }
	inline CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253 * get_address_of_mCustom2_13() { return &___mCustom2_13; }
	inline void set_mCustom2_13(CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  value)
	{
		___mCustom2_13 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___mCustom2_13))->___m_Custom_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_mCustom3_14() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___mCustom3_14)); }
	inline CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  get_mCustom3_14() const { return ___mCustom3_14; }
	inline CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253 * get_address_of_mCustom3_14() { return &___mCustom3_14; }
	inline void set_mCustom3_14(CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  value)
	{
		___mCustom3_14 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___mCustom3_14))->___m_Custom_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_CustomOverflow_15() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___m_CustomOverflow_15)); }
	inline List_1_t25C0DD5BAEE8988DC0A767964E16569E3A4880FD * get_m_CustomOverflow_15() const { return ___m_CustomOverflow_15; }
	inline List_1_t25C0DD5BAEE8988DC0A767964E16569E3A4880FD ** get_address_of_m_CustomOverflow_15() { return &___m_CustomOverflow_15; }
	inline void set_m_CustomOverflow_15(List_1_t25C0DD5BAEE8988DC0A767964E16569E3A4880FD * value)
	{
		___m_CustomOverflow_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CustomOverflow_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3CNumCustomBlendablesU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1, ___U3CNumCustomBlendablesU3Ek__BackingField_16)); }
	inline int32_t get_U3CNumCustomBlendablesU3Ek__BackingField_16() const { return ___U3CNumCustomBlendablesU3Ek__BackingField_16; }
	inline int32_t* get_address_of_U3CNumCustomBlendablesU3Ek__BackingField_16() { return &___U3CNumCustomBlendablesU3Ek__BackingField_16; }
	inline void set_U3CNumCustomBlendablesU3Ek__BackingField_16(int32_t value)
	{
		___U3CNumCustomBlendablesU3Ek__BackingField_16 = value;
	}
};

struct CameraState_t308F3A442112B7464D2B21A417D325662E3399B1_StaticFields
{
public:
	// UnityEngine.Vector3 Cinemachine.CameraState::kNoPoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___kNoPoint_3;

public:
	inline static int32_t get_offset_of_kNoPoint_3() { return static_cast<int32_t>(offsetof(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1_StaticFields, ___kNoPoint_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_kNoPoint_3() const { return ___kNoPoint_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_kNoPoint_3() { return &___kNoPoint_3; }
	inline void set_kNoPoint_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___kNoPoint_3 = value;
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.CameraState
struct CameraState_t308F3A442112B7464D2B21A417D325662E3399B1_marshaled_pinvoke
{
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_marshaled_pinvoke ___U3CLensU3Ek__BackingField_0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CReferenceUpU3Ek__BackingField_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CReferenceLookAtU3Ek__BackingField_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CRawPositionU3Ek__BackingField_4;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3CRawOrientationU3Ek__BackingField_5;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CPositionDampingBypassU3Ek__BackingField_6;
	float ___U3CShotQualityU3Ek__BackingField_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CPositionCorrectionU3Ek__BackingField_8;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3COrientationCorrectionU3Ek__BackingField_9;
	int32_t ___U3CBlendHintU3Ek__BackingField_10;
	CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  ___mCustom0_11;
	CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  ___mCustom1_12;
	CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  ___mCustom2_13;
	CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  ___mCustom3_14;
	List_1_t25C0DD5BAEE8988DC0A767964E16569E3A4880FD * ___m_CustomOverflow_15;
	int32_t ___U3CNumCustomBlendablesU3Ek__BackingField_16;
};
// Native definition for COM marshalling of Cinemachine.CameraState
struct CameraState_t308F3A442112B7464D2B21A417D325662E3399B1_marshaled_com
{
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_marshaled_com ___U3CLensU3Ek__BackingField_0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CReferenceUpU3Ek__BackingField_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CReferenceLookAtU3Ek__BackingField_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CRawPositionU3Ek__BackingField_4;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3CRawOrientationU3Ek__BackingField_5;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CPositionDampingBypassU3Ek__BackingField_6;
	float ___U3CShotQualityU3Ek__BackingField_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CPositionCorrectionU3Ek__BackingField_8;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3COrientationCorrectionU3Ek__BackingField_9;
	int32_t ___U3CBlendHintU3Ek__BackingField_10;
	CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  ___mCustom0_11;
	CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  ___mCustom1_12;
	CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  ___mCustom2_13;
	CustomBlendable_tC203F291B1DBDDF7E40E2CEE984F4755BDF6D253  ___mCustom3_14;
	List_1_t25C0DD5BAEE8988DC0A767964E16569E3A4880FD * ___m_CustomOverflow_15;
	int32_t ___U3CNumCustomBlendablesU3Ek__BackingField_16;
};

// Cinemachine.CinemachineBlendDefinition
struct  CinemachineBlendDefinition_t2AC0334955F853560C7F95D4B43BE701F099EF04 
{
public:
	// Cinemachine.CinemachineBlendDefinition_Style Cinemachine.CinemachineBlendDefinition::m_Style
	int32_t ___m_Style_0;
	// System.Single Cinemachine.CinemachineBlendDefinition::m_Time
	float ___m_Time_1;
	// UnityEngine.AnimationCurve Cinemachine.CinemachineBlendDefinition::m_CustomCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_CustomCurve_2;

public:
	inline static int32_t get_offset_of_m_Style_0() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_t2AC0334955F853560C7F95D4B43BE701F099EF04, ___m_Style_0)); }
	inline int32_t get_m_Style_0() const { return ___m_Style_0; }
	inline int32_t* get_address_of_m_Style_0() { return &___m_Style_0; }
	inline void set_m_Style_0(int32_t value)
	{
		___m_Style_0 = value;
	}

	inline static int32_t get_offset_of_m_Time_1() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_t2AC0334955F853560C7F95D4B43BE701F099EF04, ___m_Time_1)); }
	inline float get_m_Time_1() const { return ___m_Time_1; }
	inline float* get_address_of_m_Time_1() { return &___m_Time_1; }
	inline void set_m_Time_1(float value)
	{
		___m_Time_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomCurve_2() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_t2AC0334955F853560C7F95D4B43BE701F099EF04, ___m_CustomCurve_2)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_CustomCurve_2() const { return ___m_CustomCurve_2; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_CustomCurve_2() { return &___m_CustomCurve_2; }
	inline void set_m_CustomCurve_2(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_CustomCurve_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CustomCurve_2), (void*)value);
	}
};

struct CinemachineBlendDefinition_t2AC0334955F853560C7F95D4B43BE701F099EF04_StaticFields
{
public:
	// UnityEngine.AnimationCurve[] Cinemachine.CinemachineBlendDefinition::sStandardCurves
	AnimationCurveU5BU5D_tAB8BA6D5F8B505DDDA61F8065F6E870AA1F560E1* ___sStandardCurves_3;

public:
	inline static int32_t get_offset_of_sStandardCurves_3() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_t2AC0334955F853560C7F95D4B43BE701F099EF04_StaticFields, ___sStandardCurves_3)); }
	inline AnimationCurveU5BU5D_tAB8BA6D5F8B505DDDA61F8065F6E870AA1F560E1* get_sStandardCurves_3() const { return ___sStandardCurves_3; }
	inline AnimationCurveU5BU5D_tAB8BA6D5F8B505DDDA61F8065F6E870AA1F560E1** get_address_of_sStandardCurves_3() { return &___sStandardCurves_3; }
	inline void set_sStandardCurves_3(AnimationCurveU5BU5D_tAB8BA6D5F8B505DDDA61F8065F6E870AA1F560E1* value)
	{
		___sStandardCurves_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sStandardCurves_3), (void*)value);
	}
};


// Cinemachine.CinemachineOrbitalTransposer_Heading
struct  Heading_t66611F284762C65851075D4BF681E9034E65612D 
{
public:
	// Cinemachine.CinemachineOrbitalTransposer_Heading_HeadingDefinition Cinemachine.CinemachineOrbitalTransposer_Heading::m_Definition
	int32_t ___m_Definition_0;
	// System.Int32 Cinemachine.CinemachineOrbitalTransposer_Heading::m_VelocityFilterStrength
	int32_t ___m_VelocityFilterStrength_1;
	// System.Single Cinemachine.CinemachineOrbitalTransposer_Heading::m_Bias
	float ___m_Bias_2;

public:
	inline static int32_t get_offset_of_m_Definition_0() { return static_cast<int32_t>(offsetof(Heading_t66611F284762C65851075D4BF681E9034E65612D, ___m_Definition_0)); }
	inline int32_t get_m_Definition_0() const { return ___m_Definition_0; }
	inline int32_t* get_address_of_m_Definition_0() { return &___m_Definition_0; }
	inline void set_m_Definition_0(int32_t value)
	{
		___m_Definition_0 = value;
	}

	inline static int32_t get_offset_of_m_VelocityFilterStrength_1() { return static_cast<int32_t>(offsetof(Heading_t66611F284762C65851075D4BF681E9034E65612D, ___m_VelocityFilterStrength_1)); }
	inline int32_t get_m_VelocityFilterStrength_1() const { return ___m_VelocityFilterStrength_1; }
	inline int32_t* get_address_of_m_VelocityFilterStrength_1() { return &___m_VelocityFilterStrength_1; }
	inline void set_m_VelocityFilterStrength_1(int32_t value)
	{
		___m_VelocityFilterStrength_1 = value;
	}

	inline static int32_t get_offset_of_m_Bias_2() { return static_cast<int32_t>(offsetof(Heading_t66611F284762C65851075D4BF681E9034E65612D, ___m_Bias_2)); }
	inline float get_m_Bias_2() const { return ___m_Bias_2; }
	inline float* get_address_of_m_Bias_2() { return &___m_Bias_2; }
	inline void set_m_Bias_2(float value)
	{
		___m_Bias_2 = value;
	}
};


// Cinemachine.CinemachineVirtualCameraBase_TransitionParams
struct  TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8 
{
public:
	// Cinemachine.CinemachineVirtualCameraBase_BlendHint Cinemachine.CinemachineVirtualCameraBase_TransitionParams::m_BlendHint
	int32_t ___m_BlendHint_0;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase_TransitionParams::m_InheritPosition
	bool ___m_InheritPosition_1;
	// Cinemachine.CinemachineBrain_VcamActivatedEvent Cinemachine.CinemachineVirtualCameraBase_TransitionParams::m_OnCameraLive
	VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * ___m_OnCameraLive_2;

public:
	inline static int32_t get_offset_of_m_BlendHint_0() { return static_cast<int32_t>(offsetof(TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8, ___m_BlendHint_0)); }
	inline int32_t get_m_BlendHint_0() const { return ___m_BlendHint_0; }
	inline int32_t* get_address_of_m_BlendHint_0() { return &___m_BlendHint_0; }
	inline void set_m_BlendHint_0(int32_t value)
	{
		___m_BlendHint_0 = value;
	}

	inline static int32_t get_offset_of_m_InheritPosition_1() { return static_cast<int32_t>(offsetof(TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8, ___m_InheritPosition_1)); }
	inline bool get_m_InheritPosition_1() const { return ___m_InheritPosition_1; }
	inline bool* get_address_of_m_InheritPosition_1() { return &___m_InheritPosition_1; }
	inline void set_m_InheritPosition_1(bool value)
	{
		___m_InheritPosition_1 = value;
	}

	inline static int32_t get_offset_of_m_OnCameraLive_2() { return static_cast<int32_t>(offsetof(TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8, ___m_OnCameraLive_2)); }
	inline VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * get_m_OnCameraLive_2() const { return ___m_OnCameraLive_2; }
	inline VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B ** get_address_of_m_OnCameraLive_2() { return &___m_OnCameraLive_2; }
	inline void set_m_OnCameraLive_2(VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * value)
	{
		___m_OnCameraLive_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCameraLive_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.CinemachineVirtualCameraBase/TransitionParams
struct TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8_marshaled_pinvoke
{
	int32_t ___m_BlendHint_0;
	int32_t ___m_InheritPosition_1;
	VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * ___m_OnCameraLive_2;
};
// Native definition for COM marshalling of Cinemachine.CinemachineVirtualCameraBase/TransitionParams
struct TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8_marshaled_com
{
	int32_t ___m_BlendHint_0;
	int32_t ___m_InheritPosition_1;
	VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * ___m_OnCameraLive_2;
};

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GUILayoutOption
struct  GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6  : public RuntimeObject
{
public:
	// UnityEngine.GUILayoutOption_Type UnityEngine.GUILayoutOption::type
	int32_t ___type_0;
	// System.Object UnityEngine.GUILayoutOption::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}
};


// UnityEngine.GUIStyle
struct  GUIStyle_t671F175A201A19166385EE3392292A5F50070572  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Overflow_12;
	// System.String UnityEngine.GUIStyle::m_Name
	String_t* ___m_Name_13;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Normal_1)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_Normal_1() const { return ___m_Normal_1; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_Normal_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Normal_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Hover_2() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Hover_2)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_Hover_2() const { return ___m_Hover_2; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_Hover_2() { return &___m_Hover_2; }
	inline void set_m_Hover_2(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_Hover_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Hover_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Active_3() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Active_3)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_Active_3() const { return ___m_Active_3; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_Active_3() { return &___m_Active_3; }
	inline void set_m_Active_3(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_Active_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Active_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Focused_4() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Focused_4)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_Focused_4() const { return ___m_Focused_4; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_Focused_4() { return &___m_Focused_4; }
	inline void set_m_Focused_4(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_Focused_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Focused_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnNormal_5() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_OnNormal_5)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_OnNormal_5() const { return ___m_OnNormal_5; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_OnNormal_5() { return &___m_OnNormal_5; }
	inline void set_m_OnNormal_5(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_OnNormal_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnNormal_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnHover_6() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_OnHover_6)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_OnHover_6() const { return ___m_OnHover_6; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_OnHover_6() { return &___m_OnHover_6; }
	inline void set_m_OnHover_6(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_OnHover_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnHover_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnActive_7() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_OnActive_7)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_OnActive_7() const { return ___m_OnActive_7; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_OnActive_7() { return &___m_OnActive_7; }
	inline void set_m_OnActive_7(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_OnActive_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnActive_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnFocused_8() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_OnFocused_8)); }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * get_m_OnFocused_8() const { return ___m_OnFocused_8; }
	inline GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 ** get_address_of_m_OnFocused_8() { return &___m_OnFocused_8; }
	inline void set_m_OnFocused_8(GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5 * value)
	{
		___m_OnFocused_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnFocused_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Border_9() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Border_9)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Border_9() const { return ___m_Border_9; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Border_9() { return &___m_Border_9; }
	inline void set_m_Border_9(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Border_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Border_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Padding_10() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Padding_10)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Padding_10() const { return ___m_Padding_10; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Padding_10() { return &___m_Padding_10; }
	inline void set_m_Padding_10(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Padding_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Padding_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_Margin_11() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Margin_11)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Margin_11() const { return ___m_Margin_11; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Margin_11() { return &___m_Margin_11; }
	inline void set_m_Margin_11(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Margin_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Margin_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Overflow_12() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Overflow_12)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Overflow_12() const { return ___m_Overflow_12; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Overflow_12() { return &___m_Overflow_12; }
	inline void set_m_Overflow_12(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Overflow_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Overflow_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Name_13() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572, ___m_Name_13)); }
	inline String_t* get_m_Name_13() const { return ___m_Name_13; }
	inline String_t** get_address_of_m_Name_13() { return &___m_Name_13; }
	inline void set_m_Name_13(String_t* value)
	{
		___m_Name_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_13), (void*)value);
	}
};

struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572_StaticFields
{
public:
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_14;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___s_None_15;

public:
	inline static int32_t get_offset_of_showKeyboardFocus_14() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572_StaticFields, ___showKeyboardFocus_14)); }
	inline bool get_showKeyboardFocus_14() const { return ___showKeyboardFocus_14; }
	inline bool* get_address_of_showKeyboardFocus_14() { return &___showKeyboardFocus_14; }
	inline void set_showKeyboardFocus_14(bool value)
	{
		___showKeyboardFocus_14 = value;
	}

	inline static int32_t get_offset_of_s_None_15() { return static_cast<int32_t>(offsetof(GUIStyle_t671F175A201A19166385EE3392292A5F50070572_StaticFields, ___s_None_15)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_s_None_15() const { return ___s_None_15; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_s_None_15() { return &___s_None_15; }
	inline void set_s_None_15(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___s_None_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_None_15), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke ___m_Border_9;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke ___m_Padding_10;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke ___m_Margin_11;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_pinvoke ___m_Overflow_12;
	char* ___m_Name_13;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_Normal_1;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_Hover_2;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_Active_3;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_Focused_4;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t2AA5CB82EB2571B0496D1F0B9D29D2B8D8B1E7E5_marshaled_com* ___m_OnFocused_8;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com* ___m_Border_9;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com* ___m_Padding_10;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com* ___m_Margin_11;
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A_marshaled_com* ___m_Overflow_12;
	Il2CppChar* ___m_Name_13;
};

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};

// System.Action
struct  Action_t591D2A86165F896B4B800BB5C25CE18672A55579  : public MulticastDelegate_t
{
public:

public:
};


// System.NotSupportedException
struct  NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Collider
struct  Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.GUI_WindowFunction
struct  WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.GUISkin
struct  GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.Font UnityEngine.GUISkin::m_Font
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ___m_Font_4;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_box
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_box_5;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_button
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_button_6;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_toggle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_toggle_7;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_label
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_label_8;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textField
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_textField_9;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textArea
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_textArea_10;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_window
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_window_11;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSlider
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_horizontalSlider_12;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumb
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_horizontalSliderThumb_13;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumbExtent
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_horizontalSliderThumbExtent_14;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSlider
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_verticalSlider_15;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumb
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_verticalSliderThumb_16;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumbExtent
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_verticalSliderThumbExtent_17;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbar
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_horizontalScrollbar_18;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarThumb
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_horizontalScrollbarThumb_19;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarLeftButton
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_horizontalScrollbarLeftButton_20;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarRightButton
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_horizontalScrollbarRightButton_21;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbar
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_verticalScrollbar_22;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarThumb
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_verticalScrollbarThumb_23;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarUpButton
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_verticalScrollbarUpButton_24;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarDownButton
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_verticalScrollbarDownButton_25;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_ScrollView
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___m_ScrollView_26;
	// UnityEngine.GUIStyle[] UnityEngine.GUISkin::m_CustomStyles
	GUIStyleU5BU5D_t2F343713D6ADFF41BBCF1D17BAE79F51BD745DBB* ___m_CustomStyles_27;
	// UnityEngine.GUISettings UnityEngine.GUISkin::m_Settings
	GUISettings_tA863524720A3C984BAE56598D922F2C04DC80EF4 * ___m_Settings_28;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle> UnityEngine.GUISkin::m_Styles
	Dictionary_2_tF2F04E1BE233EE6C4A6E90261E463B2A67A3FB6A * ___m_Styles_30;

public:
	inline static int32_t get_offset_of_m_Font_4() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_Font_4)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get_m_Font_4() const { return ___m_Font_4; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of_m_Font_4() { return &___m_Font_4; }
	inline void set_m_Font_4(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		___m_Font_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Font_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_box_5() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_box_5)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_box_5() const { return ___m_box_5; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_box_5() { return &___m_box_5; }
	inline void set_m_box_5(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_box_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_box_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_button_6() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_button_6)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_button_6() const { return ___m_button_6; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_button_6() { return &___m_button_6; }
	inline void set_m_button_6(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_button_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_button_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_toggle_7() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_toggle_7)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_toggle_7() const { return ___m_toggle_7; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_toggle_7() { return &___m_toggle_7; }
	inline void set_m_toggle_7(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_toggle_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_toggle_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_label_8() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_label_8)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_label_8() const { return ___m_label_8; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_label_8() { return &___m_label_8; }
	inline void set_m_label_8(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_label_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_label_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_textField_9() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_textField_9)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_textField_9() const { return ___m_textField_9; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_textField_9() { return &___m_textField_9; }
	inline void set_m_textField_9(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_textField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textField_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_textArea_10() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_textArea_10)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_textArea_10() const { return ___m_textArea_10; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_textArea_10() { return &___m_textArea_10; }
	inline void set_m_textArea_10(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_textArea_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textArea_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_window_11() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_window_11)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_window_11() const { return ___m_window_11; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_window_11() { return &___m_window_11; }
	inline void set_m_window_11(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_window_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_window_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_horizontalSlider_12() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_horizontalSlider_12)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_horizontalSlider_12() const { return ___m_horizontalSlider_12; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_horizontalSlider_12() { return &___m_horizontalSlider_12; }
	inline void set_m_horizontalSlider_12(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_horizontalSlider_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_horizontalSlider_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_horizontalSliderThumb_13() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_horizontalSliderThumb_13)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_horizontalSliderThumb_13() const { return ___m_horizontalSliderThumb_13; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_horizontalSliderThumb_13() { return &___m_horizontalSliderThumb_13; }
	inline void set_m_horizontalSliderThumb_13(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_horizontalSliderThumb_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_horizontalSliderThumb_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_horizontalSliderThumbExtent_14() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_horizontalSliderThumbExtent_14)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_horizontalSliderThumbExtent_14() const { return ___m_horizontalSliderThumbExtent_14; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_horizontalSliderThumbExtent_14() { return &___m_horizontalSliderThumbExtent_14; }
	inline void set_m_horizontalSliderThumbExtent_14(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_horizontalSliderThumbExtent_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_horizontalSliderThumbExtent_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_verticalSlider_15() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_verticalSlider_15)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_verticalSlider_15() const { return ___m_verticalSlider_15; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_verticalSlider_15() { return &___m_verticalSlider_15; }
	inline void set_m_verticalSlider_15(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_verticalSlider_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_verticalSlider_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_verticalSliderThumb_16() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_verticalSliderThumb_16)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_verticalSliderThumb_16() const { return ___m_verticalSliderThumb_16; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_verticalSliderThumb_16() { return &___m_verticalSliderThumb_16; }
	inline void set_m_verticalSliderThumb_16(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_verticalSliderThumb_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_verticalSliderThumb_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_verticalSliderThumbExtent_17() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_verticalSliderThumbExtent_17)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_verticalSliderThumbExtent_17() const { return ___m_verticalSliderThumbExtent_17; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_verticalSliderThumbExtent_17() { return &___m_verticalSliderThumbExtent_17; }
	inline void set_m_verticalSliderThumbExtent_17(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_verticalSliderThumbExtent_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_verticalSliderThumbExtent_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbar_18() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_horizontalScrollbar_18)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_horizontalScrollbar_18() const { return ___m_horizontalScrollbar_18; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_horizontalScrollbar_18() { return &___m_horizontalScrollbar_18; }
	inline void set_m_horizontalScrollbar_18(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_horizontalScrollbar_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_horizontalScrollbar_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarThumb_19() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_horizontalScrollbarThumb_19)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_horizontalScrollbarThumb_19() const { return ___m_horizontalScrollbarThumb_19; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_horizontalScrollbarThumb_19() { return &___m_horizontalScrollbarThumb_19; }
	inline void set_m_horizontalScrollbarThumb_19(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_horizontalScrollbarThumb_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_horizontalScrollbarThumb_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarLeftButton_20() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_horizontalScrollbarLeftButton_20)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_horizontalScrollbarLeftButton_20() const { return ___m_horizontalScrollbarLeftButton_20; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_horizontalScrollbarLeftButton_20() { return &___m_horizontalScrollbarLeftButton_20; }
	inline void set_m_horizontalScrollbarLeftButton_20(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_horizontalScrollbarLeftButton_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_horizontalScrollbarLeftButton_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarRightButton_21() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_horizontalScrollbarRightButton_21)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_horizontalScrollbarRightButton_21() const { return ___m_horizontalScrollbarRightButton_21; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_horizontalScrollbarRightButton_21() { return &___m_horizontalScrollbarRightButton_21; }
	inline void set_m_horizontalScrollbarRightButton_21(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_horizontalScrollbarRightButton_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_horizontalScrollbarRightButton_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbar_22() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_verticalScrollbar_22)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_verticalScrollbar_22() const { return ___m_verticalScrollbar_22; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_verticalScrollbar_22() { return &___m_verticalScrollbar_22; }
	inline void set_m_verticalScrollbar_22(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_verticalScrollbar_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_verticalScrollbar_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarThumb_23() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_verticalScrollbarThumb_23)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_verticalScrollbarThumb_23() const { return ___m_verticalScrollbarThumb_23; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_verticalScrollbarThumb_23() { return &___m_verticalScrollbarThumb_23; }
	inline void set_m_verticalScrollbarThumb_23(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_verticalScrollbarThumb_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_verticalScrollbarThumb_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarUpButton_24() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_verticalScrollbarUpButton_24)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_verticalScrollbarUpButton_24() const { return ___m_verticalScrollbarUpButton_24; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_verticalScrollbarUpButton_24() { return &___m_verticalScrollbarUpButton_24; }
	inline void set_m_verticalScrollbarUpButton_24(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_verticalScrollbarUpButton_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_verticalScrollbarUpButton_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarDownButton_25() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_verticalScrollbarDownButton_25)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_verticalScrollbarDownButton_25() const { return ___m_verticalScrollbarDownButton_25; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_verticalScrollbarDownButton_25() { return &___m_verticalScrollbarDownButton_25; }
	inline void set_m_verticalScrollbarDownButton_25(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_verticalScrollbarDownButton_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_verticalScrollbarDownButton_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_ScrollView_26() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_ScrollView_26)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_m_ScrollView_26() const { return ___m_ScrollView_26; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_m_ScrollView_26() { return &___m_ScrollView_26; }
	inline void set_m_ScrollView_26(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___m_ScrollView_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ScrollView_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_CustomStyles_27() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_CustomStyles_27)); }
	inline GUIStyleU5BU5D_t2F343713D6ADFF41BBCF1D17BAE79F51BD745DBB* get_m_CustomStyles_27() const { return ___m_CustomStyles_27; }
	inline GUIStyleU5BU5D_t2F343713D6ADFF41BBCF1D17BAE79F51BD745DBB** get_address_of_m_CustomStyles_27() { return &___m_CustomStyles_27; }
	inline void set_m_CustomStyles_27(GUIStyleU5BU5D_t2F343713D6ADFF41BBCF1D17BAE79F51BD745DBB* value)
	{
		___m_CustomStyles_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CustomStyles_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_Settings_28() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_Settings_28)); }
	inline GUISettings_tA863524720A3C984BAE56598D922F2C04DC80EF4 * get_m_Settings_28() const { return ___m_Settings_28; }
	inline GUISettings_tA863524720A3C984BAE56598D922F2C04DC80EF4 ** get_address_of_m_Settings_28() { return &___m_Settings_28; }
	inline void set_m_Settings_28(GUISettings_tA863524720A3C984BAE56598D922F2C04DC80EF4 * value)
	{
		___m_Settings_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Settings_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Styles_30() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7, ___m_Styles_30)); }
	inline Dictionary_2_tF2F04E1BE233EE6C4A6E90261E463B2A67A3FB6A * get_m_Styles_30() const { return ___m_Styles_30; }
	inline Dictionary_2_tF2F04E1BE233EE6C4A6E90261E463B2A67A3FB6A ** get_address_of_m_Styles_30() { return &___m_Styles_30; }
	inline void set_m_Styles_30(Dictionary_2_tF2F04E1BE233EE6C4A6E90261E463B2A67A3FB6A * value)
	{
		___m_Styles_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Styles_30), (void*)value);
	}
};

struct GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7_StaticFields
{
public:
	// UnityEngine.GUIStyle UnityEngine.GUISkin::ms_Error
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___ms_Error_29;
	// UnityEngine.GUISkin_SkinChangedDelegate UnityEngine.GUISkin::m_SkinChanged
	SkinChangedDelegate_tAB4CEEA8C8A0BDCFD51C9624AE173C46A40135D8 * ___m_SkinChanged_31;
	// UnityEngine.GUISkin UnityEngine.GUISkin::current
	GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * ___current_32;

public:
	inline static int32_t get_offset_of_ms_Error_29() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7_StaticFields, ___ms_Error_29)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_ms_Error_29() const { return ___ms_Error_29; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_ms_Error_29() { return &___ms_Error_29; }
	inline void set_ms_Error_29(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___ms_Error_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ms_Error_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_SkinChanged_31() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7_StaticFields, ___m_SkinChanged_31)); }
	inline SkinChangedDelegate_tAB4CEEA8C8A0BDCFD51C9624AE173C46A40135D8 * get_m_SkinChanged_31() const { return ___m_SkinChanged_31; }
	inline SkinChangedDelegate_tAB4CEEA8C8A0BDCFD51C9624AE173C46A40135D8 ** get_address_of_m_SkinChanged_31() { return &___m_SkinChanged_31; }
	inline void set_m_SkinChanged_31(SkinChangedDelegate_tAB4CEEA8C8A0BDCFD51C9624AE173C46A40135D8 * value)
	{
		___m_SkinChanged_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SkinChanged_31), (void*)value);
	}

	inline static int32_t get_offset_of_current_32() { return static_cast<int32_t>(offsetof(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7_StaticFields, ___current_32)); }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * get_current_32() const { return ___current_32; }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 ** get_address_of_current_32() { return &___current_32; }
	inline void set_current_32(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * value)
	{
		___current_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_32), (void*)value);
	}
};


// UnityEngine.Renderer
struct  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Rigidbody
struct  Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Rigidbody2D
struct  Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Animator
struct  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.Collider2D
struct  Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.Playables.PlayableDirector
struct  PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:
	// System.Action`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Playables.PlayableDirector::played
	Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021 * ___played_4;
	// System.Action`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Playables.PlayableDirector::paused
	Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021 * ___paused_5;
	// System.Action`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Playables.PlayableDirector::stopped
	Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021 * ___stopped_6;

public:
	inline static int32_t get_offset_of_played_4() { return static_cast<int32_t>(offsetof(PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2, ___played_4)); }
	inline Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021 * get_played_4() const { return ___played_4; }
	inline Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021 ** get_address_of_played_4() { return &___played_4; }
	inline void set_played_4(Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021 * value)
	{
		___played_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___played_4), (void*)value);
	}

	inline static int32_t get_offset_of_paused_5() { return static_cast<int32_t>(offsetof(PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2, ___paused_5)); }
	inline Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021 * get_paused_5() const { return ___paused_5; }
	inline Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021 ** get_address_of_paused_5() { return &___paused_5; }
	inline void set_paused_5(Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021 * value)
	{
		___paused_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___paused_5), (void*)value);
	}

	inline static int32_t get_offset_of_stopped_6() { return static_cast<int32_t>(offsetof(PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2, ___stopped_6)); }
	inline Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021 * get_stopped_6() const { return ___stopped_6; }
	inline Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021 ** get_address_of_stopped_6() { return &___stopped_6; }
	inline void set_stopped_6(Action_1_t25E50453F931760A3FC110C4EC79B73BBC203021 * value)
	{
		___stopped_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stopped_6), (void*)value);
	}
};


// UnityEngine.SphereCollider
struct  SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F  : public Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF
{
public:

public:
};


// UnityEngine.SpriteRenderer
struct  SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F  : public Renderer_t0556D67DD582620D1F495627EDE30D03284151F4
{
public:

public:
};


// ActivateOnKeypress
struct  ActivateOnKeypress_t3EBED552416BF21197122C141DC4B936BB6DE1FB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.KeyCode ActivateOnKeypress::ActivationKey
	int32_t ___ActivationKey_4;
	// System.Int32 ActivateOnKeypress::PriorityBoostAmount
	int32_t ___PriorityBoostAmount_5;
	// UnityEngine.GameObject ActivateOnKeypress::Reticle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Reticle_6;
	// Cinemachine.CinemachineVirtualCameraBase ActivateOnKeypress::vcam
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___vcam_7;
	// System.Boolean ActivateOnKeypress::boosted
	bool ___boosted_8;

public:
	inline static int32_t get_offset_of_ActivationKey_4() { return static_cast<int32_t>(offsetof(ActivateOnKeypress_t3EBED552416BF21197122C141DC4B936BB6DE1FB, ___ActivationKey_4)); }
	inline int32_t get_ActivationKey_4() const { return ___ActivationKey_4; }
	inline int32_t* get_address_of_ActivationKey_4() { return &___ActivationKey_4; }
	inline void set_ActivationKey_4(int32_t value)
	{
		___ActivationKey_4 = value;
	}

	inline static int32_t get_offset_of_PriorityBoostAmount_5() { return static_cast<int32_t>(offsetof(ActivateOnKeypress_t3EBED552416BF21197122C141DC4B936BB6DE1FB, ___PriorityBoostAmount_5)); }
	inline int32_t get_PriorityBoostAmount_5() const { return ___PriorityBoostAmount_5; }
	inline int32_t* get_address_of_PriorityBoostAmount_5() { return &___PriorityBoostAmount_5; }
	inline void set_PriorityBoostAmount_5(int32_t value)
	{
		___PriorityBoostAmount_5 = value;
	}

	inline static int32_t get_offset_of_Reticle_6() { return static_cast<int32_t>(offsetof(ActivateOnKeypress_t3EBED552416BF21197122C141DC4B936BB6DE1FB, ___Reticle_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Reticle_6() const { return ___Reticle_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Reticle_6() { return &___Reticle_6; }
	inline void set_Reticle_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Reticle_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Reticle_6), (void*)value);
	}

	inline static int32_t get_offset_of_vcam_7() { return static_cast<int32_t>(offsetof(ActivateOnKeypress_t3EBED552416BF21197122C141DC4B936BB6DE1FB, ___vcam_7)); }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * get_vcam_7() const { return ___vcam_7; }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD ** get_address_of_vcam_7() { return &___vcam_7; }
	inline void set_vcam_7(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * value)
	{
		___vcam_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vcam_7), (void*)value);
	}

	inline static int32_t get_offset_of_boosted_8() { return static_cast<int32_t>(offsetof(ActivateOnKeypress_t3EBED552416BF21197122C141DC4B936BB6DE1FB, ___boosted_8)); }
	inline bool get_boosted_8() const { return ___boosted_8; }
	inline bool* get_address_of_boosted_8() { return &___boosted_8; }
	inline void set_boosted_8(bool value)
	{
		___boosted_8 = value;
	}
};


// BejucoController
struct  BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single BejucoController::longIdleTime
	float ___longIdleTime_4;
	// System.Single BejucoController::_longIdleTimer
	float ____longIdleTimer_5;
	// System.Single BejucoController::speed
	float ___speed_6;
	// System.Single BejucoController::jumpForce
	float ___jumpForce_7;
	// UnityEngine.Transform BejucoController::groundCheck
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___groundCheck_8;
	// UnityEngine.LayerMask BejucoController::groundLayer
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___groundLayer_9;
	// System.Single BejucoController::groundCheckRadius
	float ___groundCheckRadius_10;
	// UnityEngine.Rigidbody2D BejucoController::_rigidbody
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ____rigidbody_11;
	// UnityEngine.Animator BejucoController::_animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____animator_12;
	// UnityEngine.Vector2 BejucoController::_movement
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____movement_13;
	// System.Boolean BejucoController::_facingRight
	bool ____facingRight_14;
	// System.Boolean BejucoController::_isGrounded
	bool ____isGrounded_15;
	// System.Boolean BejucoController::_isAttacking
	bool ____isAttacking_16;

public:
	inline static int32_t get_offset_of_longIdleTime_4() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ___longIdleTime_4)); }
	inline float get_longIdleTime_4() const { return ___longIdleTime_4; }
	inline float* get_address_of_longIdleTime_4() { return &___longIdleTime_4; }
	inline void set_longIdleTime_4(float value)
	{
		___longIdleTime_4 = value;
	}

	inline static int32_t get_offset_of__longIdleTimer_5() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ____longIdleTimer_5)); }
	inline float get__longIdleTimer_5() const { return ____longIdleTimer_5; }
	inline float* get_address_of__longIdleTimer_5() { return &____longIdleTimer_5; }
	inline void set__longIdleTimer_5(float value)
	{
		____longIdleTimer_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_jumpForce_7() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ___jumpForce_7)); }
	inline float get_jumpForce_7() const { return ___jumpForce_7; }
	inline float* get_address_of_jumpForce_7() { return &___jumpForce_7; }
	inline void set_jumpForce_7(float value)
	{
		___jumpForce_7 = value;
	}

	inline static int32_t get_offset_of_groundCheck_8() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ___groundCheck_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_groundCheck_8() const { return ___groundCheck_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_groundCheck_8() { return &___groundCheck_8; }
	inline void set_groundCheck_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___groundCheck_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groundCheck_8), (void*)value);
	}

	inline static int32_t get_offset_of_groundLayer_9() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ___groundLayer_9)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_groundLayer_9() const { return ___groundLayer_9; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_groundLayer_9() { return &___groundLayer_9; }
	inline void set_groundLayer_9(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___groundLayer_9 = value;
	}

	inline static int32_t get_offset_of_groundCheckRadius_10() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ___groundCheckRadius_10)); }
	inline float get_groundCheckRadius_10() const { return ___groundCheckRadius_10; }
	inline float* get_address_of_groundCheckRadius_10() { return &___groundCheckRadius_10; }
	inline void set_groundCheckRadius_10(float value)
	{
		___groundCheckRadius_10 = value;
	}

	inline static int32_t get_offset_of__rigidbody_11() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ____rigidbody_11)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get__rigidbody_11() const { return ____rigidbody_11; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of__rigidbody_11() { return &____rigidbody_11; }
	inline void set__rigidbody_11(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		____rigidbody_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rigidbody_11), (void*)value);
	}

	inline static int32_t get_offset_of__animator_12() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ____animator_12)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__animator_12() const { return ____animator_12; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__animator_12() { return &____animator_12; }
	inline void set__animator_12(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____animator_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_12), (void*)value);
	}

	inline static int32_t get_offset_of__movement_13() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ____movement_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__movement_13() const { return ____movement_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__movement_13() { return &____movement_13; }
	inline void set__movement_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____movement_13 = value;
	}

	inline static int32_t get_offset_of__facingRight_14() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ____facingRight_14)); }
	inline bool get__facingRight_14() const { return ____facingRight_14; }
	inline bool* get_address_of__facingRight_14() { return &____facingRight_14; }
	inline void set__facingRight_14(bool value)
	{
		____facingRight_14 = value;
	}

	inline static int32_t get_offset_of__isGrounded_15() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ____isGrounded_15)); }
	inline bool get__isGrounded_15() const { return ____isGrounded_15; }
	inline bool* get_address_of__isGrounded_15() { return &____isGrounded_15; }
	inline void set__isGrounded_15(bool value)
	{
		____isGrounded_15 = value;
	}

	inline static int32_t get_offset_of__isAttacking_16() { return static_cast<int32_t>(offsetof(BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4, ____isAttacking_16)); }
	inline bool get__isAttacking_16() const { return ____isAttacking_16; }
	inline bool* get_address_of__isAttacking_16() { return &____isAttacking_16; }
	inline void set__isAttacking_16(bool value)
	{
		____isAttacking_16 = value;
	}
};


// Bullet
struct  Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Bullet::speed
	float ___speed_4;
	// UnityEngine.Vector2 Bullet::direction
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___direction_5;
	// System.Single Bullet::livingTime
	float ___livingTime_6;
	// UnityEngine.Color Bullet::initialColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___initialColor_7;
	// UnityEngine.Color Bullet::finalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___finalColor_8;
	// System.Single Bullet::_startingTime
	float ____startingTime_9;
	// UnityEngine.SpriteRenderer Bullet::_renderer
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____renderer_10;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_direction_5() { return static_cast<int32_t>(offsetof(Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059, ___direction_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_direction_5() const { return ___direction_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_direction_5() { return &___direction_5; }
	inline void set_direction_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___direction_5 = value;
	}

	inline static int32_t get_offset_of_livingTime_6() { return static_cast<int32_t>(offsetof(Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059, ___livingTime_6)); }
	inline float get_livingTime_6() const { return ___livingTime_6; }
	inline float* get_address_of_livingTime_6() { return &___livingTime_6; }
	inline void set_livingTime_6(float value)
	{
		___livingTime_6 = value;
	}

	inline static int32_t get_offset_of_initialColor_7() { return static_cast<int32_t>(offsetof(Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059, ___initialColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_initialColor_7() const { return ___initialColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_initialColor_7() { return &___initialColor_7; }
	inline void set_initialColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___initialColor_7 = value;
	}

	inline static int32_t get_offset_of_finalColor_8() { return static_cast<int32_t>(offsetof(Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059, ___finalColor_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_finalColor_8() const { return ___finalColor_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_finalColor_8() { return &___finalColor_8; }
	inline void set_finalColor_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___finalColor_8 = value;
	}

	inline static int32_t get_offset_of__startingTime_9() { return static_cast<int32_t>(offsetof(Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059, ____startingTime_9)); }
	inline float get__startingTime_9() const { return ____startingTime_9; }
	inline float* get_address_of__startingTime_9() { return &____startingTime_9; }
	inline void set__startingTime_9(float value)
	{
		____startingTime_9 = value;
	}

	inline static int32_t get_offset_of__renderer_10() { return static_cast<int32_t>(offsetof(Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059, ____renderer_10)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__renderer_10() const { return ____renderer_10; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__renderer_10() { return &____renderer_10; }
	inline void set__renderer_10(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____renderer_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____renderer_10), (void*)value);
	}
};


// ChangeColor
struct  ChangeColor_t11FE2BD8AD1F8A8AFB6E43B3737410A3C60AFC79  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// Cinemachine.CinemachineBrain
struct  CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Cinemachine.CinemachineBrain::m_ShowDebugText
	bool ___m_ShowDebugText_4;
	// System.Boolean Cinemachine.CinemachineBrain::m_ShowCameraFrustum
	bool ___m_ShowCameraFrustum_5;
	// System.Boolean Cinemachine.CinemachineBrain::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_6;
	// UnityEngine.Transform Cinemachine.CinemachineBrain::m_WorldUpOverride
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_WorldUpOverride_7;
	// Cinemachine.CinemachineBrain_UpdateMethod Cinemachine.CinemachineBrain::m_UpdateMethod
	int32_t ___m_UpdateMethod_8;
	// Cinemachine.CinemachineBrain_BrainUpdateMethod Cinemachine.CinemachineBrain::m_BlendUpdateMethod
	int32_t ___m_BlendUpdateMethod_9;
	// Cinemachine.CinemachineBlendDefinition Cinemachine.CinemachineBrain::m_DefaultBlend
	CinemachineBlendDefinition_t2AC0334955F853560C7F95D4B43BE701F099EF04  ___m_DefaultBlend_10;
	// Cinemachine.CinemachineBlenderSettings Cinemachine.CinemachineBrain::m_CustomBlends
	CinemachineBlenderSettings_tCD488C84B252394D8CDD7D0DB623D991BE77F3F0 * ___m_CustomBlends_11;
	// UnityEngine.Camera Cinemachine.CinemachineBrain::m_OutputCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_OutputCamera_12;
	// Cinemachine.CinemachineBrain_BrainEvent Cinemachine.CinemachineBrain::m_CameraCutEvent
	BrainEvent_tCB30E3A5C9244915F355517663D4E440A54F79C0 * ___m_CameraCutEvent_13;
	// Cinemachine.CinemachineBrain_VcamActivatedEvent Cinemachine.CinemachineBrain::m_CameraActivatedEvent
	VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * ___m_CameraActivatedEvent_14;
	// UnityEngine.Coroutine Cinemachine.CinemachineBrain::mPhysicsCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___mPhysicsCoroutine_16;
	// UnityEngine.WaitForFixedUpdate Cinemachine.CinemachineBrain::mWaitForFixedUpdate
	WaitForFixedUpdate_t8801328F075019AF6B6150B20EC343935A29FF97 * ___mWaitForFixedUpdate_17;
	// System.Collections.Generic.List`1<Cinemachine.CinemachineBrain_BrainFrame> Cinemachine.CinemachineBrain::mFrameStack
	List_1_t22F3D7410E401DD6A2908F6264F0046A93980F6F * ___mFrameStack_18;
	// System.Int32 Cinemachine.CinemachineBrain::mNextFrameId
	int32_t ___mNextFrameId_19;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineBrain::mCurrentLiveCameras
	CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298 * ___mCurrentLiveCameras_20;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBrain::mActiveCameraPreviousFrame
	RuntimeObject* ___mActiveCameraPreviousFrame_21;
	// Cinemachine.CameraState Cinemachine.CinemachineBrain::<CurrentCameraState>k__BackingField
	CameraState_t308F3A442112B7464D2B21A417D325662E3399B1  ___U3CCurrentCameraStateU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_m_ShowDebugText_4() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___m_ShowDebugText_4)); }
	inline bool get_m_ShowDebugText_4() const { return ___m_ShowDebugText_4; }
	inline bool* get_address_of_m_ShowDebugText_4() { return &___m_ShowDebugText_4; }
	inline void set_m_ShowDebugText_4(bool value)
	{
		___m_ShowDebugText_4 = value;
	}

	inline static int32_t get_offset_of_m_ShowCameraFrustum_5() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___m_ShowCameraFrustum_5)); }
	inline bool get_m_ShowCameraFrustum_5() const { return ___m_ShowCameraFrustum_5; }
	inline bool* get_address_of_m_ShowCameraFrustum_5() { return &___m_ShowCameraFrustum_5; }
	inline void set_m_ShowCameraFrustum_5(bool value)
	{
		___m_ShowCameraFrustum_5 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_6() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___m_IgnoreTimeScale_6)); }
	inline bool get_m_IgnoreTimeScale_6() const { return ___m_IgnoreTimeScale_6; }
	inline bool* get_address_of_m_IgnoreTimeScale_6() { return &___m_IgnoreTimeScale_6; }
	inline void set_m_IgnoreTimeScale_6(bool value)
	{
		___m_IgnoreTimeScale_6 = value;
	}

	inline static int32_t get_offset_of_m_WorldUpOverride_7() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___m_WorldUpOverride_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_WorldUpOverride_7() const { return ___m_WorldUpOverride_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_WorldUpOverride_7() { return &___m_WorldUpOverride_7; }
	inline void set_m_WorldUpOverride_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_WorldUpOverride_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_WorldUpOverride_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdateMethod_8() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___m_UpdateMethod_8)); }
	inline int32_t get_m_UpdateMethod_8() const { return ___m_UpdateMethod_8; }
	inline int32_t* get_address_of_m_UpdateMethod_8() { return &___m_UpdateMethod_8; }
	inline void set_m_UpdateMethod_8(int32_t value)
	{
		___m_UpdateMethod_8 = value;
	}

	inline static int32_t get_offset_of_m_BlendUpdateMethod_9() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___m_BlendUpdateMethod_9)); }
	inline int32_t get_m_BlendUpdateMethod_9() const { return ___m_BlendUpdateMethod_9; }
	inline int32_t* get_address_of_m_BlendUpdateMethod_9() { return &___m_BlendUpdateMethod_9; }
	inline void set_m_BlendUpdateMethod_9(int32_t value)
	{
		___m_BlendUpdateMethod_9 = value;
	}

	inline static int32_t get_offset_of_m_DefaultBlend_10() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___m_DefaultBlend_10)); }
	inline CinemachineBlendDefinition_t2AC0334955F853560C7F95D4B43BE701F099EF04  get_m_DefaultBlend_10() const { return ___m_DefaultBlend_10; }
	inline CinemachineBlendDefinition_t2AC0334955F853560C7F95D4B43BE701F099EF04 * get_address_of_m_DefaultBlend_10() { return &___m_DefaultBlend_10; }
	inline void set_m_DefaultBlend_10(CinemachineBlendDefinition_t2AC0334955F853560C7F95D4B43BE701F099EF04  value)
	{
		___m_DefaultBlend_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DefaultBlend_10))->___m_CustomCurve_2), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_CustomBlends_11() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___m_CustomBlends_11)); }
	inline CinemachineBlenderSettings_tCD488C84B252394D8CDD7D0DB623D991BE77F3F0 * get_m_CustomBlends_11() const { return ___m_CustomBlends_11; }
	inline CinemachineBlenderSettings_tCD488C84B252394D8CDD7D0DB623D991BE77F3F0 ** get_address_of_m_CustomBlends_11() { return &___m_CustomBlends_11; }
	inline void set_m_CustomBlends_11(CinemachineBlenderSettings_tCD488C84B252394D8CDD7D0DB623D991BE77F3F0 * value)
	{
		___m_CustomBlends_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CustomBlends_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_OutputCamera_12() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___m_OutputCamera_12)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_OutputCamera_12() const { return ___m_OutputCamera_12; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_OutputCamera_12() { return &___m_OutputCamera_12; }
	inline void set_m_OutputCamera_12(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_OutputCamera_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OutputCamera_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CameraCutEvent_13() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___m_CameraCutEvent_13)); }
	inline BrainEvent_tCB30E3A5C9244915F355517663D4E440A54F79C0 * get_m_CameraCutEvent_13() const { return ___m_CameraCutEvent_13; }
	inline BrainEvent_tCB30E3A5C9244915F355517663D4E440A54F79C0 ** get_address_of_m_CameraCutEvent_13() { return &___m_CameraCutEvent_13; }
	inline void set_m_CameraCutEvent_13(BrainEvent_tCB30E3A5C9244915F355517663D4E440A54F79C0 * value)
	{
		___m_CameraCutEvent_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CameraCutEvent_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_CameraActivatedEvent_14() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___m_CameraActivatedEvent_14)); }
	inline VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * get_m_CameraActivatedEvent_14() const { return ___m_CameraActivatedEvent_14; }
	inline VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B ** get_address_of_m_CameraActivatedEvent_14() { return &___m_CameraActivatedEvent_14; }
	inline void set_m_CameraActivatedEvent_14(VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * value)
	{
		___m_CameraActivatedEvent_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CameraActivatedEvent_14), (void*)value);
	}

	inline static int32_t get_offset_of_mPhysicsCoroutine_16() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___mPhysicsCoroutine_16)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_mPhysicsCoroutine_16() const { return ___mPhysicsCoroutine_16; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_mPhysicsCoroutine_16() { return &___mPhysicsCoroutine_16; }
	inline void set_mPhysicsCoroutine_16(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___mPhysicsCoroutine_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mPhysicsCoroutine_16), (void*)value);
	}

	inline static int32_t get_offset_of_mWaitForFixedUpdate_17() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___mWaitForFixedUpdate_17)); }
	inline WaitForFixedUpdate_t8801328F075019AF6B6150B20EC343935A29FF97 * get_mWaitForFixedUpdate_17() const { return ___mWaitForFixedUpdate_17; }
	inline WaitForFixedUpdate_t8801328F075019AF6B6150B20EC343935A29FF97 ** get_address_of_mWaitForFixedUpdate_17() { return &___mWaitForFixedUpdate_17; }
	inline void set_mWaitForFixedUpdate_17(WaitForFixedUpdate_t8801328F075019AF6B6150B20EC343935A29FF97 * value)
	{
		___mWaitForFixedUpdate_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mWaitForFixedUpdate_17), (void*)value);
	}

	inline static int32_t get_offset_of_mFrameStack_18() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___mFrameStack_18)); }
	inline List_1_t22F3D7410E401DD6A2908F6264F0046A93980F6F * get_mFrameStack_18() const { return ___mFrameStack_18; }
	inline List_1_t22F3D7410E401DD6A2908F6264F0046A93980F6F ** get_address_of_mFrameStack_18() { return &___mFrameStack_18; }
	inline void set_mFrameStack_18(List_1_t22F3D7410E401DD6A2908F6264F0046A93980F6F * value)
	{
		___mFrameStack_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mFrameStack_18), (void*)value);
	}

	inline static int32_t get_offset_of_mNextFrameId_19() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___mNextFrameId_19)); }
	inline int32_t get_mNextFrameId_19() const { return ___mNextFrameId_19; }
	inline int32_t* get_address_of_mNextFrameId_19() { return &___mNextFrameId_19; }
	inline void set_mNextFrameId_19(int32_t value)
	{
		___mNextFrameId_19 = value;
	}

	inline static int32_t get_offset_of_mCurrentLiveCameras_20() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___mCurrentLiveCameras_20)); }
	inline CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298 * get_mCurrentLiveCameras_20() const { return ___mCurrentLiveCameras_20; }
	inline CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298 ** get_address_of_mCurrentLiveCameras_20() { return &___mCurrentLiveCameras_20; }
	inline void set_mCurrentLiveCameras_20(CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298 * value)
	{
		___mCurrentLiveCameras_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCurrentLiveCameras_20), (void*)value);
	}

	inline static int32_t get_offset_of_mActiveCameraPreviousFrame_21() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___mActiveCameraPreviousFrame_21)); }
	inline RuntimeObject* get_mActiveCameraPreviousFrame_21() const { return ___mActiveCameraPreviousFrame_21; }
	inline RuntimeObject** get_address_of_mActiveCameraPreviousFrame_21() { return &___mActiveCameraPreviousFrame_21; }
	inline void set_mActiveCameraPreviousFrame_21(RuntimeObject* value)
	{
		___mActiveCameraPreviousFrame_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mActiveCameraPreviousFrame_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentCameraStateU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB, ___U3CCurrentCameraStateU3Ek__BackingField_22)); }
	inline CameraState_t308F3A442112B7464D2B21A417D325662E3399B1  get_U3CCurrentCameraStateU3Ek__BackingField_22() const { return ___U3CCurrentCameraStateU3Ek__BackingField_22; }
	inline CameraState_t308F3A442112B7464D2B21A417D325662E3399B1 * get_address_of_U3CCurrentCameraStateU3Ek__BackingField_22() { return &___U3CCurrentCameraStateU3Ek__BackingField_22; }
	inline void set_U3CCurrentCameraStateU3Ek__BackingField_22(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1  value)
	{
		___U3CCurrentCameraStateU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CCurrentCameraStateU3Ek__BackingField_22))->___mCustom0_11))->___m_Custom_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CCurrentCameraStateU3Ek__BackingField_22))->___mCustom1_12))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CCurrentCameraStateU3Ek__BackingField_22))->___mCustom2_13))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CCurrentCameraStateU3Ek__BackingField_22))->___mCustom3_14))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CCurrentCameraStateU3Ek__BackingField_22))->___m_CustomOverflow_15), (void*)NULL);
		#endif
	}
};

struct CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB_StaticFields
{
public:
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBrain::mSoloCamera
	RuntimeObject* ___mSoloCamera_15;

public:
	inline static int32_t get_offset_of_mSoloCamera_15() { return static_cast<int32_t>(offsetof(CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB_StaticFields, ___mSoloCamera_15)); }
	inline RuntimeObject* get_mSoloCamera_15() const { return ___mSoloCamera_15; }
	inline RuntimeObject** get_address_of_mSoloCamera_15() { return &___mSoloCamera_15; }
	inline void set_mSoloCamera_15(RuntimeObject* value)
	{
		___mSoloCamera_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mSoloCamera_15), (void*)value);
	}
};


// Cinemachine.CinemachineComponentBase
struct  CinemachineComponentBase_tC2AC9880A0A85AE8136352639E33911E7FCB013D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineComponentBase::m_vcamOwner
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___m_vcamOwner_5;
	// UnityEngine.Transform Cinemachine.CinemachineComponentBase::mCachedFollowTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mCachedFollowTarget_6;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineComponentBase::mCachedFollowTargetVcam
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___mCachedFollowTargetVcam_7;
	// Cinemachine.ICinemachineTargetGroup Cinemachine.CinemachineComponentBase::mCachedFollowTargetGroup
	RuntimeObject* ___mCachedFollowTargetGroup_8;
	// UnityEngine.Transform Cinemachine.CinemachineComponentBase::mCachedLookAtTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mCachedLookAtTarget_9;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineComponentBase::mCachedLookAtTargetVcam
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___mCachedLookAtTargetVcam_10;
	// Cinemachine.ICinemachineTargetGroup Cinemachine.CinemachineComponentBase::mCachedLookAtTargetGroup
	RuntimeObject* ___mCachedLookAtTargetGroup_11;

public:
	inline static int32_t get_offset_of_m_vcamOwner_5() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tC2AC9880A0A85AE8136352639E33911E7FCB013D, ___m_vcamOwner_5)); }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * get_m_vcamOwner_5() const { return ___m_vcamOwner_5; }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD ** get_address_of_m_vcamOwner_5() { return &___m_vcamOwner_5; }
	inline void set_m_vcamOwner_5(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * value)
	{
		___m_vcamOwner_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_vcamOwner_5), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedFollowTarget_6() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tC2AC9880A0A85AE8136352639E33911E7FCB013D, ___mCachedFollowTarget_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mCachedFollowTarget_6() const { return ___mCachedFollowTarget_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mCachedFollowTarget_6() { return &___mCachedFollowTarget_6; }
	inline void set_mCachedFollowTarget_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mCachedFollowTarget_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedFollowTarget_6), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedFollowTargetVcam_7() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tC2AC9880A0A85AE8136352639E33911E7FCB013D, ___mCachedFollowTargetVcam_7)); }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * get_mCachedFollowTargetVcam_7() const { return ___mCachedFollowTargetVcam_7; }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD ** get_address_of_mCachedFollowTargetVcam_7() { return &___mCachedFollowTargetVcam_7; }
	inline void set_mCachedFollowTargetVcam_7(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * value)
	{
		___mCachedFollowTargetVcam_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedFollowTargetVcam_7), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedFollowTargetGroup_8() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tC2AC9880A0A85AE8136352639E33911E7FCB013D, ___mCachedFollowTargetGroup_8)); }
	inline RuntimeObject* get_mCachedFollowTargetGroup_8() const { return ___mCachedFollowTargetGroup_8; }
	inline RuntimeObject** get_address_of_mCachedFollowTargetGroup_8() { return &___mCachedFollowTargetGroup_8; }
	inline void set_mCachedFollowTargetGroup_8(RuntimeObject* value)
	{
		___mCachedFollowTargetGroup_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedFollowTargetGroup_8), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedLookAtTarget_9() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tC2AC9880A0A85AE8136352639E33911E7FCB013D, ___mCachedLookAtTarget_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mCachedLookAtTarget_9() const { return ___mCachedLookAtTarget_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mCachedLookAtTarget_9() { return &___mCachedLookAtTarget_9; }
	inline void set_mCachedLookAtTarget_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mCachedLookAtTarget_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedLookAtTarget_9), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedLookAtTargetVcam_10() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tC2AC9880A0A85AE8136352639E33911E7FCB013D, ___mCachedLookAtTargetVcam_10)); }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * get_mCachedLookAtTargetVcam_10() const { return ___mCachedLookAtTargetVcam_10; }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD ** get_address_of_mCachedLookAtTargetVcam_10() { return &___mCachedLookAtTargetVcam_10; }
	inline void set_mCachedLookAtTargetVcam_10(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * value)
	{
		___mCachedLookAtTargetVcam_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedLookAtTargetVcam_10), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedLookAtTargetGroup_11() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tC2AC9880A0A85AE8136352639E33911E7FCB013D, ___mCachedLookAtTargetGroup_11)); }
	inline RuntimeObject* get_mCachedLookAtTargetGroup_11() const { return ___mCachedLookAtTargetGroup_11; }
	inline RuntimeObject** get_address_of_mCachedLookAtTargetGroup_11() { return &___mCachedLookAtTargetGroup_11; }
	inline void set_mCachedLookAtTargetGroup_11(RuntimeObject* value)
	{
		___mCachedLookAtTargetGroup_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedLookAtTargetGroup_11), (void*)value);
	}
};


// Cinemachine.CinemachineVirtualCameraBase
struct  CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] Cinemachine.CinemachineVirtualCameraBase::m_ExcludedPropertiesInInspector
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_ExcludedPropertiesInInspector_4;
	// Cinemachine.CinemachineCore_Stage[] Cinemachine.CinemachineVirtualCameraBase::m_LockStageInInspector
	StageU5BU5D_tC107E846F8ED5DBF4CEF77B4AFCC26D9DBE723A2* ___m_LockStageInInspector_5;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_ValidatingStreamVersion
	int32_t ___m_ValidatingStreamVersion_6;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::m_OnValidateCalled
	bool ___m_OnValidateCalled_7;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_StreamingVersion
	int32_t ___m_StreamingVersion_8;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_Priority
	int32_t ___m_Priority_9;
	// Cinemachine.CinemachineVirtualCameraBase_StandbyUpdateMode Cinemachine.CinemachineVirtualCameraBase::m_StandbyUpdate
	int32_t ___m_StandbyUpdate_10;
	// System.Collections.Generic.List`1<Cinemachine.CinemachineExtension> Cinemachine.CinemachineVirtualCameraBase::mExtensions
	List_1_tB06968E265CE1689803547CD89D453ACFB3509D5 * ___mExtensions_11;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::<PreviousStateIsValid>k__BackingField
	bool ___U3CPreviousStateIsValidU3Ek__BackingField_12;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::mSlaveStatusUpdated
	bool ___mSlaveStatusUpdated_13;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineVirtualCameraBase::m_parentVcam
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___m_parentVcam_14;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_QueuePriority
	int32_t ___m_QueuePriority_15;

public:
	inline static int32_t get_offset_of_m_ExcludedPropertiesInInspector_4() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_ExcludedPropertiesInInspector_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_ExcludedPropertiesInInspector_4() const { return ___m_ExcludedPropertiesInInspector_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_ExcludedPropertiesInInspector_4() { return &___m_ExcludedPropertiesInInspector_4; }
	inline void set_m_ExcludedPropertiesInInspector_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_ExcludedPropertiesInInspector_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExcludedPropertiesInInspector_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_LockStageInInspector_5() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_LockStageInInspector_5)); }
	inline StageU5BU5D_tC107E846F8ED5DBF4CEF77B4AFCC26D9DBE723A2* get_m_LockStageInInspector_5() const { return ___m_LockStageInInspector_5; }
	inline StageU5BU5D_tC107E846F8ED5DBF4CEF77B4AFCC26D9DBE723A2** get_address_of_m_LockStageInInspector_5() { return &___m_LockStageInInspector_5; }
	inline void set_m_LockStageInInspector_5(StageU5BU5D_tC107E846F8ED5DBF4CEF77B4AFCC26D9DBE723A2* value)
	{
		___m_LockStageInInspector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LockStageInInspector_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ValidatingStreamVersion_6() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_ValidatingStreamVersion_6)); }
	inline int32_t get_m_ValidatingStreamVersion_6() const { return ___m_ValidatingStreamVersion_6; }
	inline int32_t* get_address_of_m_ValidatingStreamVersion_6() { return &___m_ValidatingStreamVersion_6; }
	inline void set_m_ValidatingStreamVersion_6(int32_t value)
	{
		___m_ValidatingStreamVersion_6 = value;
	}

	inline static int32_t get_offset_of_m_OnValidateCalled_7() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_OnValidateCalled_7)); }
	inline bool get_m_OnValidateCalled_7() const { return ___m_OnValidateCalled_7; }
	inline bool* get_address_of_m_OnValidateCalled_7() { return &___m_OnValidateCalled_7; }
	inline void set_m_OnValidateCalled_7(bool value)
	{
		___m_OnValidateCalled_7 = value;
	}

	inline static int32_t get_offset_of_m_StreamingVersion_8() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_StreamingVersion_8)); }
	inline int32_t get_m_StreamingVersion_8() const { return ___m_StreamingVersion_8; }
	inline int32_t* get_address_of_m_StreamingVersion_8() { return &___m_StreamingVersion_8; }
	inline void set_m_StreamingVersion_8(int32_t value)
	{
		___m_StreamingVersion_8 = value;
	}

	inline static int32_t get_offset_of_m_Priority_9() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_Priority_9)); }
	inline int32_t get_m_Priority_9() const { return ___m_Priority_9; }
	inline int32_t* get_address_of_m_Priority_9() { return &___m_Priority_9; }
	inline void set_m_Priority_9(int32_t value)
	{
		___m_Priority_9 = value;
	}

	inline static int32_t get_offset_of_m_StandbyUpdate_10() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_StandbyUpdate_10)); }
	inline int32_t get_m_StandbyUpdate_10() const { return ___m_StandbyUpdate_10; }
	inline int32_t* get_address_of_m_StandbyUpdate_10() { return &___m_StandbyUpdate_10; }
	inline void set_m_StandbyUpdate_10(int32_t value)
	{
		___m_StandbyUpdate_10 = value;
	}

	inline static int32_t get_offset_of_mExtensions_11() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___mExtensions_11)); }
	inline List_1_tB06968E265CE1689803547CD89D453ACFB3509D5 * get_mExtensions_11() const { return ___mExtensions_11; }
	inline List_1_tB06968E265CE1689803547CD89D453ACFB3509D5 ** get_address_of_mExtensions_11() { return &___mExtensions_11; }
	inline void set_mExtensions_11(List_1_tB06968E265CE1689803547CD89D453ACFB3509D5 * value)
	{
		___mExtensions_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mExtensions_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPreviousStateIsValidU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___U3CPreviousStateIsValidU3Ek__BackingField_12)); }
	inline bool get_U3CPreviousStateIsValidU3Ek__BackingField_12() const { return ___U3CPreviousStateIsValidU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CPreviousStateIsValidU3Ek__BackingField_12() { return &___U3CPreviousStateIsValidU3Ek__BackingField_12; }
	inline void set_U3CPreviousStateIsValidU3Ek__BackingField_12(bool value)
	{
		___U3CPreviousStateIsValidU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_mSlaveStatusUpdated_13() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___mSlaveStatusUpdated_13)); }
	inline bool get_mSlaveStatusUpdated_13() const { return ___mSlaveStatusUpdated_13; }
	inline bool* get_address_of_mSlaveStatusUpdated_13() { return &___mSlaveStatusUpdated_13; }
	inline void set_mSlaveStatusUpdated_13(bool value)
	{
		___mSlaveStatusUpdated_13 = value;
	}

	inline static int32_t get_offset_of_m_parentVcam_14() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_parentVcam_14)); }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * get_m_parentVcam_14() const { return ___m_parentVcam_14; }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD ** get_address_of_m_parentVcam_14() { return &___m_parentVcam_14; }
	inline void set_m_parentVcam_14(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * value)
	{
		___m_parentVcam_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_parentVcam_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_QueuePriority_15() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_QueuePriority_15)); }
	inline int32_t get_m_QueuePriority_15() const { return ___m_QueuePriority_15; }
	inline int32_t* get_address_of_m_QueuePriority_15() { return &___m_QueuePriority_15; }
	inline void set_m_QueuePriority_15(int32_t value)
	{
		___m_QueuePriority_15 = value;
	}
};


// Cinemachine.Examples.ActivateCamOnPlay
struct  ActivateCamOnPlay_tCD29CD3A1DC809CC23DF4D6BC74437DB1EB8E7CE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.Examples.ActivateCamOnPlay::vcam
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___vcam_4;

public:
	inline static int32_t get_offset_of_vcam_4() { return static_cast<int32_t>(offsetof(ActivateCamOnPlay_tCD29CD3A1DC809CC23DF4D6BC74437DB1EB8E7CE, ___vcam_4)); }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * get_vcam_4() const { return ___vcam_4; }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD ** get_address_of_vcam_4() { return &___vcam_4; }
	inline void set_vcam_4(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * value)
	{
		___vcam_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vcam_4), (void*)value);
	}
};


// Cinemachine.Examples.ActivateCameraWithDistance
struct  ActivateCameraWithDistance_tC54EDA46FFCAA5DE57637DE63FE5FBB65E0AE247  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Cinemachine.Examples.ActivateCameraWithDistance::objectToCheck
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___objectToCheck_4;
	// System.Single Cinemachine.Examples.ActivateCameraWithDistance::distanceToObject
	float ___distanceToObject_5;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.Examples.ActivateCameraWithDistance::initialActiveCam
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___initialActiveCam_6;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.Examples.ActivateCameraWithDistance::switchCameraTo
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___switchCameraTo_7;

public:
	inline static int32_t get_offset_of_objectToCheck_4() { return static_cast<int32_t>(offsetof(ActivateCameraWithDistance_tC54EDA46FFCAA5DE57637DE63FE5FBB65E0AE247, ___objectToCheck_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_objectToCheck_4() const { return ___objectToCheck_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_objectToCheck_4() { return &___objectToCheck_4; }
	inline void set_objectToCheck_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___objectToCheck_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectToCheck_4), (void*)value);
	}

	inline static int32_t get_offset_of_distanceToObject_5() { return static_cast<int32_t>(offsetof(ActivateCameraWithDistance_tC54EDA46FFCAA5DE57637DE63FE5FBB65E0AE247, ___distanceToObject_5)); }
	inline float get_distanceToObject_5() const { return ___distanceToObject_5; }
	inline float* get_address_of_distanceToObject_5() { return &___distanceToObject_5; }
	inline void set_distanceToObject_5(float value)
	{
		___distanceToObject_5 = value;
	}

	inline static int32_t get_offset_of_initialActiveCam_6() { return static_cast<int32_t>(offsetof(ActivateCameraWithDistance_tC54EDA46FFCAA5DE57637DE63FE5FBB65E0AE247, ___initialActiveCam_6)); }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * get_initialActiveCam_6() const { return ___initialActiveCam_6; }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD ** get_address_of_initialActiveCam_6() { return &___initialActiveCam_6; }
	inline void set_initialActiveCam_6(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * value)
	{
		___initialActiveCam_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___initialActiveCam_6), (void*)value);
	}

	inline static int32_t get_offset_of_switchCameraTo_7() { return static_cast<int32_t>(offsetof(ActivateCameraWithDistance_tC54EDA46FFCAA5DE57637DE63FE5FBB65E0AE247, ___switchCameraTo_7)); }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * get_switchCameraTo_7() const { return ___switchCameraTo_7; }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD ** get_address_of_switchCameraTo_7() { return &___switchCameraTo_7; }
	inline void set_switchCameraTo_7(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * value)
	{
		___switchCameraTo_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___switchCameraTo_7), (void*)value);
	}
};


// Cinemachine.Examples.CharacterMovement
struct  CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Cinemachine.Examples.CharacterMovement::useCharacterForward
	bool ___useCharacterForward_4;
	// System.Boolean Cinemachine.Examples.CharacterMovement::lockToCameraForward
	bool ___lockToCameraForward_5;
	// System.Single Cinemachine.Examples.CharacterMovement::turnSpeed
	float ___turnSpeed_6;
	// UnityEngine.KeyCode Cinemachine.Examples.CharacterMovement::sprintJoystick
	int32_t ___sprintJoystick_7;
	// UnityEngine.KeyCode Cinemachine.Examples.CharacterMovement::sprintKeyboard
	int32_t ___sprintKeyboard_8;
	// System.Single Cinemachine.Examples.CharacterMovement::turnSpeedMultiplier
	float ___turnSpeedMultiplier_9;
	// System.Single Cinemachine.Examples.CharacterMovement::speed
	float ___speed_10;
	// System.Single Cinemachine.Examples.CharacterMovement::direction
	float ___direction_11;
	// System.Boolean Cinemachine.Examples.CharacterMovement::isSprinting
	bool ___isSprinting_12;
	// UnityEngine.Animator Cinemachine.Examples.CharacterMovement::anim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___anim_13;
	// UnityEngine.Vector3 Cinemachine.Examples.CharacterMovement::targetDirection
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetDirection_14;
	// UnityEngine.Vector2 Cinemachine.Examples.CharacterMovement::input
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___input_15;
	// UnityEngine.Quaternion Cinemachine.Examples.CharacterMovement::freeRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___freeRotation_16;
	// UnityEngine.Camera Cinemachine.Examples.CharacterMovement::mainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCamera_17;
	// System.Single Cinemachine.Examples.CharacterMovement::velocity
	float ___velocity_18;

public:
	inline static int32_t get_offset_of_useCharacterForward_4() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___useCharacterForward_4)); }
	inline bool get_useCharacterForward_4() const { return ___useCharacterForward_4; }
	inline bool* get_address_of_useCharacterForward_4() { return &___useCharacterForward_4; }
	inline void set_useCharacterForward_4(bool value)
	{
		___useCharacterForward_4 = value;
	}

	inline static int32_t get_offset_of_lockToCameraForward_5() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___lockToCameraForward_5)); }
	inline bool get_lockToCameraForward_5() const { return ___lockToCameraForward_5; }
	inline bool* get_address_of_lockToCameraForward_5() { return &___lockToCameraForward_5; }
	inline void set_lockToCameraForward_5(bool value)
	{
		___lockToCameraForward_5 = value;
	}

	inline static int32_t get_offset_of_turnSpeed_6() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___turnSpeed_6)); }
	inline float get_turnSpeed_6() const { return ___turnSpeed_6; }
	inline float* get_address_of_turnSpeed_6() { return &___turnSpeed_6; }
	inline void set_turnSpeed_6(float value)
	{
		___turnSpeed_6 = value;
	}

	inline static int32_t get_offset_of_sprintJoystick_7() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___sprintJoystick_7)); }
	inline int32_t get_sprintJoystick_7() const { return ___sprintJoystick_7; }
	inline int32_t* get_address_of_sprintJoystick_7() { return &___sprintJoystick_7; }
	inline void set_sprintJoystick_7(int32_t value)
	{
		___sprintJoystick_7 = value;
	}

	inline static int32_t get_offset_of_sprintKeyboard_8() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___sprintKeyboard_8)); }
	inline int32_t get_sprintKeyboard_8() const { return ___sprintKeyboard_8; }
	inline int32_t* get_address_of_sprintKeyboard_8() { return &___sprintKeyboard_8; }
	inline void set_sprintKeyboard_8(int32_t value)
	{
		___sprintKeyboard_8 = value;
	}

	inline static int32_t get_offset_of_turnSpeedMultiplier_9() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___turnSpeedMultiplier_9)); }
	inline float get_turnSpeedMultiplier_9() const { return ___turnSpeedMultiplier_9; }
	inline float* get_address_of_turnSpeedMultiplier_9() { return &___turnSpeedMultiplier_9; }
	inline void set_turnSpeedMultiplier_9(float value)
	{
		___turnSpeedMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___speed_10)); }
	inline float get_speed_10() const { return ___speed_10; }
	inline float* get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(float value)
	{
		___speed_10 = value;
	}

	inline static int32_t get_offset_of_direction_11() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___direction_11)); }
	inline float get_direction_11() const { return ___direction_11; }
	inline float* get_address_of_direction_11() { return &___direction_11; }
	inline void set_direction_11(float value)
	{
		___direction_11 = value;
	}

	inline static int32_t get_offset_of_isSprinting_12() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___isSprinting_12)); }
	inline bool get_isSprinting_12() const { return ___isSprinting_12; }
	inline bool* get_address_of_isSprinting_12() { return &___isSprinting_12; }
	inline void set_isSprinting_12(bool value)
	{
		___isSprinting_12 = value;
	}

	inline static int32_t get_offset_of_anim_13() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___anim_13)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_anim_13() const { return ___anim_13; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_anim_13() { return &___anim_13; }
	inline void set_anim_13(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___anim_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anim_13), (void*)value);
	}

	inline static int32_t get_offset_of_targetDirection_14() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___targetDirection_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetDirection_14() const { return ___targetDirection_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetDirection_14() { return &___targetDirection_14; }
	inline void set_targetDirection_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetDirection_14 = value;
	}

	inline static int32_t get_offset_of_input_15() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___input_15)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_input_15() const { return ___input_15; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_input_15() { return &___input_15; }
	inline void set_input_15(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___input_15 = value;
	}

	inline static int32_t get_offset_of_freeRotation_16() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___freeRotation_16)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_freeRotation_16() const { return ___freeRotation_16; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_freeRotation_16() { return &___freeRotation_16; }
	inline void set_freeRotation_16(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___freeRotation_16 = value;
	}

	inline static int32_t get_offset_of_mainCamera_17() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___mainCamera_17)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mainCamera_17() const { return ___mainCamera_17; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mainCamera_17() { return &___mainCamera_17; }
	inline void set_mainCamera_17(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mainCamera_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainCamera_17), (void*)value);
	}

	inline static int32_t get_offset_of_velocity_18() { return static_cast<int32_t>(offsetof(CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3, ___velocity_18)); }
	inline float get_velocity_18() const { return ___velocity_18; }
	inline float* get_address_of_velocity_18() { return &___velocity_18; }
	inline void set_velocity_18(float value)
	{
		___velocity_18 = value;
	}
};


// Cinemachine.Examples.CharacterMovement2D
struct  CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.KeyCode Cinemachine.Examples.CharacterMovement2D::sprintJoystick
	int32_t ___sprintJoystick_4;
	// UnityEngine.KeyCode Cinemachine.Examples.CharacterMovement2D::jumpJoystick
	int32_t ___jumpJoystick_5;
	// UnityEngine.KeyCode Cinemachine.Examples.CharacterMovement2D::sprintKeyboard
	int32_t ___sprintKeyboard_6;
	// UnityEngine.KeyCode Cinemachine.Examples.CharacterMovement2D::jumpKeyboard
	int32_t ___jumpKeyboard_7;
	// System.Single Cinemachine.Examples.CharacterMovement2D::jumpVelocity
	float ___jumpVelocity_8;
	// System.Single Cinemachine.Examples.CharacterMovement2D::groundTolerance
	float ___groundTolerance_9;
	// System.Boolean Cinemachine.Examples.CharacterMovement2D::checkGroundForJump
	bool ___checkGroundForJump_10;
	// System.Single Cinemachine.Examples.CharacterMovement2D::speed
	float ___speed_11;
	// System.Boolean Cinemachine.Examples.CharacterMovement2D::isSprinting
	bool ___isSprinting_12;
	// UnityEngine.Animator Cinemachine.Examples.CharacterMovement2D::anim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___anim_13;
	// UnityEngine.Vector2 Cinemachine.Examples.CharacterMovement2D::input
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___input_14;
	// System.Single Cinemachine.Examples.CharacterMovement2D::velocity
	float ___velocity_15;
	// System.Boolean Cinemachine.Examples.CharacterMovement2D::headingleft
	bool ___headingleft_16;
	// UnityEngine.Quaternion Cinemachine.Examples.CharacterMovement2D::targetrot
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___targetrot_17;
	// UnityEngine.Rigidbody Cinemachine.Examples.CharacterMovement2D::rigbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___rigbody_18;

public:
	inline static int32_t get_offset_of_sprintJoystick_4() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___sprintJoystick_4)); }
	inline int32_t get_sprintJoystick_4() const { return ___sprintJoystick_4; }
	inline int32_t* get_address_of_sprintJoystick_4() { return &___sprintJoystick_4; }
	inline void set_sprintJoystick_4(int32_t value)
	{
		___sprintJoystick_4 = value;
	}

	inline static int32_t get_offset_of_jumpJoystick_5() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___jumpJoystick_5)); }
	inline int32_t get_jumpJoystick_5() const { return ___jumpJoystick_5; }
	inline int32_t* get_address_of_jumpJoystick_5() { return &___jumpJoystick_5; }
	inline void set_jumpJoystick_5(int32_t value)
	{
		___jumpJoystick_5 = value;
	}

	inline static int32_t get_offset_of_sprintKeyboard_6() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___sprintKeyboard_6)); }
	inline int32_t get_sprintKeyboard_6() const { return ___sprintKeyboard_6; }
	inline int32_t* get_address_of_sprintKeyboard_6() { return &___sprintKeyboard_6; }
	inline void set_sprintKeyboard_6(int32_t value)
	{
		___sprintKeyboard_6 = value;
	}

	inline static int32_t get_offset_of_jumpKeyboard_7() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___jumpKeyboard_7)); }
	inline int32_t get_jumpKeyboard_7() const { return ___jumpKeyboard_7; }
	inline int32_t* get_address_of_jumpKeyboard_7() { return &___jumpKeyboard_7; }
	inline void set_jumpKeyboard_7(int32_t value)
	{
		___jumpKeyboard_7 = value;
	}

	inline static int32_t get_offset_of_jumpVelocity_8() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___jumpVelocity_8)); }
	inline float get_jumpVelocity_8() const { return ___jumpVelocity_8; }
	inline float* get_address_of_jumpVelocity_8() { return &___jumpVelocity_8; }
	inline void set_jumpVelocity_8(float value)
	{
		___jumpVelocity_8 = value;
	}

	inline static int32_t get_offset_of_groundTolerance_9() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___groundTolerance_9)); }
	inline float get_groundTolerance_9() const { return ___groundTolerance_9; }
	inline float* get_address_of_groundTolerance_9() { return &___groundTolerance_9; }
	inline void set_groundTolerance_9(float value)
	{
		___groundTolerance_9 = value;
	}

	inline static int32_t get_offset_of_checkGroundForJump_10() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___checkGroundForJump_10)); }
	inline bool get_checkGroundForJump_10() const { return ___checkGroundForJump_10; }
	inline bool* get_address_of_checkGroundForJump_10() { return &___checkGroundForJump_10; }
	inline void set_checkGroundForJump_10(bool value)
	{
		___checkGroundForJump_10 = value;
	}

	inline static int32_t get_offset_of_speed_11() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___speed_11)); }
	inline float get_speed_11() const { return ___speed_11; }
	inline float* get_address_of_speed_11() { return &___speed_11; }
	inline void set_speed_11(float value)
	{
		___speed_11 = value;
	}

	inline static int32_t get_offset_of_isSprinting_12() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___isSprinting_12)); }
	inline bool get_isSprinting_12() const { return ___isSprinting_12; }
	inline bool* get_address_of_isSprinting_12() { return &___isSprinting_12; }
	inline void set_isSprinting_12(bool value)
	{
		___isSprinting_12 = value;
	}

	inline static int32_t get_offset_of_anim_13() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___anim_13)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_anim_13() const { return ___anim_13; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_anim_13() { return &___anim_13; }
	inline void set_anim_13(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___anim_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anim_13), (void*)value);
	}

	inline static int32_t get_offset_of_input_14() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___input_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_input_14() const { return ___input_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_input_14() { return &___input_14; }
	inline void set_input_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___input_14 = value;
	}

	inline static int32_t get_offset_of_velocity_15() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___velocity_15)); }
	inline float get_velocity_15() const { return ___velocity_15; }
	inline float* get_address_of_velocity_15() { return &___velocity_15; }
	inline void set_velocity_15(float value)
	{
		___velocity_15 = value;
	}

	inline static int32_t get_offset_of_headingleft_16() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___headingleft_16)); }
	inline bool get_headingleft_16() const { return ___headingleft_16; }
	inline bool* get_address_of_headingleft_16() { return &___headingleft_16; }
	inline void set_headingleft_16(bool value)
	{
		___headingleft_16 = value;
	}

	inline static int32_t get_offset_of_targetrot_17() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___targetrot_17)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_targetrot_17() const { return ___targetrot_17; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_targetrot_17() { return &___targetrot_17; }
	inline void set_targetrot_17(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___targetrot_17 = value;
	}

	inline static int32_t get_offset_of_rigbody_18() { return static_cast<int32_t>(offsetof(CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484, ___rigbody_18)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_rigbody_18() const { return ___rigbody_18; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_rigbody_18() { return &___rigbody_18; }
	inline void set_rigbody_18(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___rigbody_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rigbody_18), (void*)value);
	}
};


// Cinemachine.Examples.ExampleHelpWindow
struct  ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Cinemachine.Examples.ExampleHelpWindow::m_Title
	String_t* ___m_Title_4;
	// System.String Cinemachine.Examples.ExampleHelpWindow::m_Description
	String_t* ___m_Description_5;
	// System.Boolean Cinemachine.Examples.ExampleHelpWindow::mShowingHelpWindow
	bool ___mShowingHelpWindow_6;

public:
	inline static int32_t get_offset_of_m_Title_4() { return static_cast<int32_t>(offsetof(ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378, ___m_Title_4)); }
	inline String_t* get_m_Title_4() const { return ___m_Title_4; }
	inline String_t** get_address_of_m_Title_4() { return &___m_Title_4; }
	inline void set_m_Title_4(String_t* value)
	{
		___m_Title_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Title_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Description_5() { return static_cast<int32_t>(offsetof(ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378, ___m_Description_5)); }
	inline String_t* get_m_Description_5() const { return ___m_Description_5; }
	inline String_t** get_address_of_m_Description_5() { return &___m_Description_5; }
	inline void set_m_Description_5(String_t* value)
	{
		___m_Description_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Description_5), (void*)value);
	}

	inline static int32_t get_offset_of_mShowingHelpWindow_6() { return static_cast<int32_t>(offsetof(ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378, ___mShowingHelpWindow_6)); }
	inline bool get_mShowingHelpWindow_6() const { return ___mShowingHelpWindow_6; }
	inline bool* get_address_of_mShowingHelpWindow_6() { return &___mShowingHelpWindow_6; }
	inline void set_mShowingHelpWindow_6(bool value)
	{
		___mShowingHelpWindow_6 = value;
	}
};


// Cinemachine.Examples.GenericTrigger
struct  GenericTrigger_t45676BF61EE6D8259E0E288AC627092B9E962CA7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Playables.PlayableDirector Cinemachine.Examples.GenericTrigger::timeline
	PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * ___timeline_4;

public:
	inline static int32_t get_offset_of_timeline_4() { return static_cast<int32_t>(offsetof(GenericTrigger_t45676BF61EE6D8259E0E288AC627092B9E962CA7, ___timeline_4)); }
	inline PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * get_timeline_4() const { return ___timeline_4; }
	inline PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 ** get_address_of_timeline_4() { return &___timeline_4; }
	inline void set_timeline_4(PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * value)
	{
		___timeline_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timeline_4), (void*)value);
	}
};


// Cinemachine.Examples.MixingCameraBlend
struct  MixingCameraBlend_tCB7BBB9AC892ECC6DE8154D1F397E7F7B359D8E7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform Cinemachine.Examples.MixingCameraBlend::followTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___followTarget_4;
	// System.Single Cinemachine.Examples.MixingCameraBlend::initialBottomWeight
	float ___initialBottomWeight_5;
	// Cinemachine.Examples.MixingCameraBlend_AxisEnum Cinemachine.Examples.MixingCameraBlend::axisToTrack
	int32_t ___axisToTrack_6;
	// Cinemachine.CinemachineMixingCamera Cinemachine.Examples.MixingCameraBlend::vcam
	CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE * ___vcam_7;

public:
	inline static int32_t get_offset_of_followTarget_4() { return static_cast<int32_t>(offsetof(MixingCameraBlend_tCB7BBB9AC892ECC6DE8154D1F397E7F7B359D8E7, ___followTarget_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_followTarget_4() const { return ___followTarget_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_followTarget_4() { return &___followTarget_4; }
	inline void set_followTarget_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___followTarget_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___followTarget_4), (void*)value);
	}

	inline static int32_t get_offset_of_initialBottomWeight_5() { return static_cast<int32_t>(offsetof(MixingCameraBlend_tCB7BBB9AC892ECC6DE8154D1F397E7F7B359D8E7, ___initialBottomWeight_5)); }
	inline float get_initialBottomWeight_5() const { return ___initialBottomWeight_5; }
	inline float* get_address_of_initialBottomWeight_5() { return &___initialBottomWeight_5; }
	inline void set_initialBottomWeight_5(float value)
	{
		___initialBottomWeight_5 = value;
	}

	inline static int32_t get_offset_of_axisToTrack_6() { return static_cast<int32_t>(offsetof(MixingCameraBlend_tCB7BBB9AC892ECC6DE8154D1F397E7F7B359D8E7, ___axisToTrack_6)); }
	inline int32_t get_axisToTrack_6() const { return ___axisToTrack_6; }
	inline int32_t* get_address_of_axisToTrack_6() { return &___axisToTrack_6; }
	inline void set_axisToTrack_6(int32_t value)
	{
		___axisToTrack_6 = value;
	}

	inline static int32_t get_offset_of_vcam_7() { return static_cast<int32_t>(offsetof(MixingCameraBlend_tCB7BBB9AC892ECC6DE8154D1F397E7F7B359D8E7, ___vcam_7)); }
	inline CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE * get_vcam_7() const { return ___vcam_7; }
	inline CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE ** get_address_of_vcam_7() { return &___vcam_7; }
	inline void set_vcam_7(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE * value)
	{
		___vcam_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vcam_7), (void*)value);
	}
};


// Cinemachine.Examples.ScriptingExample
struct  ScriptingExample_t43DC6E8DA30C3BFFB9C51A0C22E54188F22A33E4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Cinemachine.CinemachineVirtualCamera Cinemachine.Examples.ScriptingExample::vcam
	CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * ___vcam_4;
	// Cinemachine.CinemachineFreeLook Cinemachine.Examples.ScriptingExample::freelook
	CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * ___freelook_5;
	// System.Single Cinemachine.Examples.ScriptingExample::lastSwapTime
	float ___lastSwapTime_6;

public:
	inline static int32_t get_offset_of_vcam_4() { return static_cast<int32_t>(offsetof(ScriptingExample_t43DC6E8DA30C3BFFB9C51A0C22E54188F22A33E4, ___vcam_4)); }
	inline CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * get_vcam_4() const { return ___vcam_4; }
	inline CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A ** get_address_of_vcam_4() { return &___vcam_4; }
	inline void set_vcam_4(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * value)
	{
		___vcam_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vcam_4), (void*)value);
	}

	inline static int32_t get_offset_of_freelook_5() { return static_cast<int32_t>(offsetof(ScriptingExample_t43DC6E8DA30C3BFFB9C51A0C22E54188F22A33E4, ___freelook_5)); }
	inline CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * get_freelook_5() const { return ___freelook_5; }
	inline CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 ** get_address_of_freelook_5() { return &___freelook_5; }
	inline void set_freelook_5(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * value)
	{
		___freelook_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___freelook_5), (void*)value);
	}

	inline static int32_t get_offset_of_lastSwapTime_6() { return static_cast<int32_t>(offsetof(ScriptingExample_t43DC6E8DA30C3BFFB9C51A0C22E54188F22A33E4, ___lastSwapTime_6)); }
	inline float get_lastSwapTime_6() const { return ___lastSwapTime_6; }
	inline float* get_address_of_lastSwapTime_6() { return &___lastSwapTime_6; }
	inline void set_lastSwapTime_6(float value)
	{
		___lastSwapTime_6 = value;
	}
};


// EnemyPatrol
struct  EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single EnemyPatrol::speed
	float ___speed_4;
	// System.Single EnemyPatrol::minX
	float ___minX_5;
	// System.Single EnemyPatrol::maxX
	float ___maxX_6;
	// System.Single EnemyPatrol::waitingTime
	float ___waitingTime_7;
	// UnityEngine.GameObject EnemyPatrol::_target
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____target_8;
	// UnityEngine.Animator EnemyPatrol::_animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____animator_9;
	// Weapon EnemyPatrol::_weapon
	Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * ____weapon_10;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_minX_5() { return static_cast<int32_t>(offsetof(EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD, ___minX_5)); }
	inline float get_minX_5() const { return ___minX_5; }
	inline float* get_address_of_minX_5() { return &___minX_5; }
	inline void set_minX_5(float value)
	{
		___minX_5 = value;
	}

	inline static int32_t get_offset_of_maxX_6() { return static_cast<int32_t>(offsetof(EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD, ___maxX_6)); }
	inline float get_maxX_6() const { return ___maxX_6; }
	inline float* get_address_of_maxX_6() { return &___maxX_6; }
	inline void set_maxX_6(float value)
	{
		___maxX_6 = value;
	}

	inline static int32_t get_offset_of_waitingTime_7() { return static_cast<int32_t>(offsetof(EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD, ___waitingTime_7)); }
	inline float get_waitingTime_7() const { return ___waitingTime_7; }
	inline float* get_address_of_waitingTime_7() { return &___waitingTime_7; }
	inline void set_waitingTime_7(float value)
	{
		___waitingTime_7 = value;
	}

	inline static int32_t get_offset_of__target_8() { return static_cast<int32_t>(offsetof(EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD, ____target_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__target_8() const { return ____target_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__target_8() { return &____target_8; }
	inline void set__target_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____target_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____target_8), (void*)value);
	}

	inline static int32_t get_offset_of__animator_9() { return static_cast<int32_t>(offsetof(EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD, ____animator_9)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__animator_9() const { return ____animator_9; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__animator_9() { return &____animator_9; }
	inline void set__animator_9(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____animator_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_9), (void*)value);
	}

	inline static int32_t get_offset_of__weapon_10() { return static_cast<int32_t>(offsetof(EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD, ____weapon_10)); }
	inline Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * get__weapon_10() const { return ____weapon_10; }
	inline Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 ** get_address_of__weapon_10() { return &____weapon_10; }
	inline void set__weapon_10(Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * value)
	{
		____weapon_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____weapon_10), (void*)value);
	}
};


// InstantiatePrefab
struct  InstantiatePrefab_t36976A57AFF7271A16E6F3932ACF6823CA3CA292  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject InstantiatePrefab::prefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___prefab_4;
	// UnityEngine.Transform InstantiatePrefab::point
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___point_5;
	// System.Single InstantiatePrefab::livingTime
	float ___livingTime_6;

public:
	inline static int32_t get_offset_of_prefab_4() { return static_cast<int32_t>(offsetof(InstantiatePrefab_t36976A57AFF7271A16E6F3932ACF6823CA3CA292, ___prefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_prefab_4() const { return ___prefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_prefab_4() { return &___prefab_4; }
	inline void set_prefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___prefab_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefab_4), (void*)value);
	}

	inline static int32_t get_offset_of_point_5() { return static_cast<int32_t>(offsetof(InstantiatePrefab_t36976A57AFF7271A16E6F3932ACF6823CA3CA292, ___point_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_point_5() const { return ___point_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_point_5() { return &___point_5; }
	inline void set_point_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___point_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___point_5), (void*)value);
	}

	inline static int32_t get_offset_of_livingTime_6() { return static_cast<int32_t>(offsetof(InstantiatePrefab_t36976A57AFF7271A16E6F3932ACF6823CA3CA292, ___livingTime_6)); }
	inline float get_livingTime_6() const { return ___livingTime_6; }
	inline float* get_address_of_livingTime_6() { return &___livingTime_6; }
	inline void set_livingTime_6(float value)
	{
		___livingTime_6 = value;
	}
};


// LoadScene
struct  LoadScene_t674EFD288A318DA4B83B5427402567479C0B493D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String LoadScene::scene
	String_t* ___scene_4;

public:
	inline static int32_t get_offset_of_scene_4() { return static_cast<int32_t>(offsetof(LoadScene_t674EFD288A318DA4B83B5427402567479C0B493D, ___scene_4)); }
	inline String_t* get_scene_4() const { return ___scene_4; }
	inline String_t** get_address_of_scene_4() { return &___scene_4; }
	inline void set_scene_4(String_t* value)
	{
		___scene_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scene_4), (void*)value);
	}
};


// PlayerController
struct  PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single PlayerController::longIdleTime
	float ___longIdleTime_4;
	// System.Single PlayerController::_longIdleTimer
	float ____longIdleTimer_5;
	// System.Single PlayerController::speed
	float ___speed_6;
	// System.Single PlayerController::jumpForce
	float ___jumpForce_7;
	// UnityEngine.Transform PlayerController::groundCheck
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___groundCheck_8;
	// UnityEngine.LayerMask PlayerController::groundLayer
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___groundLayer_9;
	// System.Single PlayerController::groundCheckRadius
	float ___groundCheckRadius_10;
	// UnityEngine.Rigidbody2D PlayerController::_rigidbody
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ____rigidbody_11;
	// UnityEngine.Animator PlayerController::_animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____animator_12;
	// UnityEngine.Vector2 PlayerController::_movement
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____movement_13;
	// System.Boolean PlayerController::_facingRight
	bool ____facingRight_14;
	// System.Boolean PlayerController::_isGrounded
	bool ____isGrounded_15;
	// System.Boolean PlayerController::_isAttacking
	bool ____isAttacking_16;

public:
	inline static int32_t get_offset_of_longIdleTime_4() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___longIdleTime_4)); }
	inline float get_longIdleTime_4() const { return ___longIdleTime_4; }
	inline float* get_address_of_longIdleTime_4() { return &___longIdleTime_4; }
	inline void set_longIdleTime_4(float value)
	{
		___longIdleTime_4 = value;
	}

	inline static int32_t get_offset_of__longIdleTimer_5() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ____longIdleTimer_5)); }
	inline float get__longIdleTimer_5() const { return ____longIdleTimer_5; }
	inline float* get_address_of__longIdleTimer_5() { return &____longIdleTimer_5; }
	inline void set__longIdleTimer_5(float value)
	{
		____longIdleTimer_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_jumpForce_7() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___jumpForce_7)); }
	inline float get_jumpForce_7() const { return ___jumpForce_7; }
	inline float* get_address_of_jumpForce_7() { return &___jumpForce_7; }
	inline void set_jumpForce_7(float value)
	{
		___jumpForce_7 = value;
	}

	inline static int32_t get_offset_of_groundCheck_8() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___groundCheck_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_groundCheck_8() const { return ___groundCheck_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_groundCheck_8() { return &___groundCheck_8; }
	inline void set_groundCheck_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___groundCheck_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groundCheck_8), (void*)value);
	}

	inline static int32_t get_offset_of_groundLayer_9() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___groundLayer_9)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_groundLayer_9() const { return ___groundLayer_9; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_groundLayer_9() { return &___groundLayer_9; }
	inline void set_groundLayer_9(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___groundLayer_9 = value;
	}

	inline static int32_t get_offset_of_groundCheckRadius_10() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___groundCheckRadius_10)); }
	inline float get_groundCheckRadius_10() const { return ___groundCheckRadius_10; }
	inline float* get_address_of_groundCheckRadius_10() { return &___groundCheckRadius_10; }
	inline void set_groundCheckRadius_10(float value)
	{
		___groundCheckRadius_10 = value;
	}

	inline static int32_t get_offset_of__rigidbody_11() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ____rigidbody_11)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get__rigidbody_11() const { return ____rigidbody_11; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of__rigidbody_11() { return &____rigidbody_11; }
	inline void set__rigidbody_11(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		____rigidbody_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rigidbody_11), (void*)value);
	}

	inline static int32_t get_offset_of__animator_12() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ____animator_12)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__animator_12() const { return ____animator_12; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__animator_12() { return &____animator_12; }
	inline void set__animator_12(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____animator_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_12), (void*)value);
	}

	inline static int32_t get_offset_of__movement_13() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ____movement_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__movement_13() const { return ____movement_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__movement_13() { return &____movement_13; }
	inline void set__movement_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____movement_13 = value;
	}

	inline static int32_t get_offset_of__facingRight_14() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ____facingRight_14)); }
	inline bool get__facingRight_14() const { return ____facingRight_14; }
	inline bool* get_address_of__facingRight_14() { return &____facingRight_14; }
	inline void set__facingRight_14(bool value)
	{
		____facingRight_14 = value;
	}

	inline static int32_t get_offset_of__isGrounded_15() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ____isGrounded_15)); }
	inline bool get__isGrounded_15() const { return ____isGrounded_15; }
	inline bool* get_address_of__isGrounded_15() { return &____isGrounded_15; }
	inline void set__isGrounded_15(bool value)
	{
		____isGrounded_15 = value;
	}

	inline static int32_t get_offset_of__isAttacking_16() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ____isAttacking_16)); }
	inline bool get__isAttacking_16() const { return ____isAttacking_16; }
	inline bool* get_address_of__isAttacking_16() { return &____isAttacking_16; }
	inline void set__isAttacking_16(bool value)
	{
		____isAttacking_16 = value;
	}
};


// PlayerMove
struct  PlayerMove_tF4D209EF20A6DE6765B2D125AEA112228D5BD388  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single PlayerMove::speed
	float ___speed_4;
	// System.Boolean PlayerMove::worldDirection
	bool ___worldDirection_5;
	// System.Boolean PlayerMove::rotatePlayer
	bool ___rotatePlayer_6;
	// System.Single PlayerMove::rotationDamping
	float ___rotationDamping_7;
	// System.Action PlayerMove::spaceAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___spaceAction_8;
	// System.Action PlayerMove::enterAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___enterAction_9;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(PlayerMove_tF4D209EF20A6DE6765B2D125AEA112228D5BD388, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_worldDirection_5() { return static_cast<int32_t>(offsetof(PlayerMove_tF4D209EF20A6DE6765B2D125AEA112228D5BD388, ___worldDirection_5)); }
	inline bool get_worldDirection_5() const { return ___worldDirection_5; }
	inline bool* get_address_of_worldDirection_5() { return &___worldDirection_5; }
	inline void set_worldDirection_5(bool value)
	{
		___worldDirection_5 = value;
	}

	inline static int32_t get_offset_of_rotatePlayer_6() { return static_cast<int32_t>(offsetof(PlayerMove_tF4D209EF20A6DE6765B2D125AEA112228D5BD388, ___rotatePlayer_6)); }
	inline bool get_rotatePlayer_6() const { return ___rotatePlayer_6; }
	inline bool* get_address_of_rotatePlayer_6() { return &___rotatePlayer_6; }
	inline void set_rotatePlayer_6(bool value)
	{
		___rotatePlayer_6 = value;
	}

	inline static int32_t get_offset_of_rotationDamping_7() { return static_cast<int32_t>(offsetof(PlayerMove_tF4D209EF20A6DE6765B2D125AEA112228D5BD388, ___rotationDamping_7)); }
	inline float get_rotationDamping_7() const { return ___rotationDamping_7; }
	inline float* get_address_of_rotationDamping_7() { return &___rotationDamping_7; }
	inline void set_rotationDamping_7(float value)
	{
		___rotationDamping_7 = value;
	}

	inline static int32_t get_offset_of_spaceAction_8() { return static_cast<int32_t>(offsetof(PlayerMove_tF4D209EF20A6DE6765B2D125AEA112228D5BD388, ___spaceAction_8)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_spaceAction_8() const { return ___spaceAction_8; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_spaceAction_8() { return &___spaceAction_8; }
	inline void set_spaceAction_8(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___spaceAction_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spaceAction_8), (void*)value);
	}

	inline static int32_t get_offset_of_enterAction_9() { return static_cast<int32_t>(offsetof(PlayerMove_tF4D209EF20A6DE6765B2D125AEA112228D5BD388, ___enterAction_9)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_enterAction_9() const { return ___enterAction_9; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_enterAction_9() { return &___enterAction_9; }
	inline void set_enterAction_9(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___enterAction_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enterAction_9), (void*)value);
	}
};


// PlayerMoveOnSphere
struct  PlayerMoveOnSphere_t82BC101F65B2488923B0D504BBA3BD830070009A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SphereCollider PlayerMoveOnSphere::Sphere
	SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ___Sphere_4;
	// System.Single PlayerMoveOnSphere::speed
	float ___speed_5;
	// System.Boolean PlayerMoveOnSphere::rotatePlayer
	bool ___rotatePlayer_6;
	// System.Single PlayerMoveOnSphere::rotationDamping
	float ___rotationDamping_7;

public:
	inline static int32_t get_offset_of_Sphere_4() { return static_cast<int32_t>(offsetof(PlayerMoveOnSphere_t82BC101F65B2488923B0D504BBA3BD830070009A, ___Sphere_4)); }
	inline SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * get_Sphere_4() const { return ___Sphere_4; }
	inline SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F ** get_address_of_Sphere_4() { return &___Sphere_4; }
	inline void set_Sphere_4(SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * value)
	{
		___Sphere_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Sphere_4), (void*)value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(PlayerMoveOnSphere_t82BC101F65B2488923B0D504BBA3BD830070009A, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_rotatePlayer_6() { return static_cast<int32_t>(offsetof(PlayerMoveOnSphere_t82BC101F65B2488923B0D504BBA3BD830070009A, ___rotatePlayer_6)); }
	inline bool get_rotatePlayer_6() const { return ___rotatePlayer_6; }
	inline bool* get_address_of_rotatePlayer_6() { return &___rotatePlayer_6; }
	inline void set_rotatePlayer_6(bool value)
	{
		___rotatePlayer_6 = value;
	}

	inline static int32_t get_offset_of_rotationDamping_7() { return static_cast<int32_t>(offsetof(PlayerMoveOnSphere_t82BC101F65B2488923B0D504BBA3BD830070009A, ___rotationDamping_7)); }
	inline float get_rotationDamping_7() const { return ___rotationDamping_7; }
	inline float* get_address_of_rotationDamping_7() { return &___rotationDamping_7; }
	inline void set_rotationDamping_7(float value)
	{
		___rotationDamping_7 = value;
	}
};


// PlayerMovePhysics
struct  PlayerMovePhysics_t4A6B0D15F96182589F0583094C84138989DDC1A8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single PlayerMovePhysics::speed
	float ___speed_4;
	// System.Boolean PlayerMovePhysics::worldDirection
	bool ___worldDirection_5;
	// System.Boolean PlayerMovePhysics::rotatePlayer
	bool ___rotatePlayer_6;
	// System.Action PlayerMovePhysics::spaceAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___spaceAction_7;
	// System.Action PlayerMovePhysics::enterAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___enterAction_8;
	// UnityEngine.Rigidbody PlayerMovePhysics::rb
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___rb_9;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(PlayerMovePhysics_t4A6B0D15F96182589F0583094C84138989DDC1A8, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_worldDirection_5() { return static_cast<int32_t>(offsetof(PlayerMovePhysics_t4A6B0D15F96182589F0583094C84138989DDC1A8, ___worldDirection_5)); }
	inline bool get_worldDirection_5() const { return ___worldDirection_5; }
	inline bool* get_address_of_worldDirection_5() { return &___worldDirection_5; }
	inline void set_worldDirection_5(bool value)
	{
		___worldDirection_5 = value;
	}

	inline static int32_t get_offset_of_rotatePlayer_6() { return static_cast<int32_t>(offsetof(PlayerMovePhysics_t4A6B0D15F96182589F0583094C84138989DDC1A8, ___rotatePlayer_6)); }
	inline bool get_rotatePlayer_6() const { return ___rotatePlayer_6; }
	inline bool* get_address_of_rotatePlayer_6() { return &___rotatePlayer_6; }
	inline void set_rotatePlayer_6(bool value)
	{
		___rotatePlayer_6 = value;
	}

	inline static int32_t get_offset_of_spaceAction_7() { return static_cast<int32_t>(offsetof(PlayerMovePhysics_t4A6B0D15F96182589F0583094C84138989DDC1A8, ___spaceAction_7)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_spaceAction_7() const { return ___spaceAction_7; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_spaceAction_7() { return &___spaceAction_7; }
	inline void set_spaceAction_7(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___spaceAction_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spaceAction_7), (void*)value);
	}

	inline static int32_t get_offset_of_enterAction_8() { return static_cast<int32_t>(offsetof(PlayerMovePhysics_t4A6B0D15F96182589F0583094C84138989DDC1A8, ___enterAction_8)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_enterAction_8() const { return ___enterAction_8; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_enterAction_8() { return &___enterAction_8; }
	inline void set_enterAction_8(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___enterAction_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enterAction_8), (void*)value);
	}

	inline static int32_t get_offset_of_rb_9() { return static_cast<int32_t>(offsetof(PlayerMovePhysics_t4A6B0D15F96182589F0583094C84138989DDC1A8, ___rb_9)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_rb_9() const { return ___rb_9; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_rb_9() { return &___rb_9; }
	inline void set_rb_9(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___rb_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rb_9), (void*)value);
	}
};


// Weapon
struct  Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Weapon::bulletPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bulletPrefab_4;
	// UnityEngine.GameObject Weapon::shooter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___shooter_5;
	// UnityEngine.Transform Weapon::_firePoint
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____firePoint_6;

public:
	inline static int32_t get_offset_of_bulletPrefab_4() { return static_cast<int32_t>(offsetof(Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9, ___bulletPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bulletPrefab_4() const { return ___bulletPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bulletPrefab_4() { return &___bulletPrefab_4; }
	inline void set_bulletPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bulletPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bulletPrefab_4), (void*)value);
	}

	inline static int32_t get_offset_of_shooter_5() { return static_cast<int32_t>(offsetof(Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9, ___shooter_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_shooter_5() const { return ___shooter_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_shooter_5() { return &___shooter_5; }
	inline void set_shooter_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___shooter_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shooter_5), (void*)value);
	}

	inline static int32_t get_offset_of__firePoint_6() { return static_cast<int32_t>(offsetof(Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9, ____firePoint_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__firePoint_6() const { return ____firePoint_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__firePoint_6() { return &____firePoint_6; }
	inline void set__firePoint_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____firePoint_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____firePoint_6), (void*)value);
	}
};


// ZuroStreet
struct  ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single ZuroStreet::horizontalVelocity
	float ___horizontalVelocity_4;
	// System.Single ZuroStreet::jumpForce
	float ___jumpForce_5;
	// System.Int32 ZuroStreet::numberOfJumps
	int32_t ___numberOfJumps_6;
	// System.Boolean ZuroStreet::isGrounded
	bool ___isGrounded_7;
	// System.Boolean ZuroStreet::isAttacking
	bool ___isAttacking_8;
	// UnityEngine.Rigidbody2D ZuroStreet::_rigidbody
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ____rigidbody_9;
	// UnityEngine.Animator ZuroStreet::_animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____animator_10;
	// System.Single ZuroStreet::aimingTime
	float ___aimingTime_11;
	// UnityEngine.Transform ZuroStreet::groundCheck
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___groundCheck_12;
	// UnityEngine.LayerMask ZuroStreet::groundLayer
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___groundLayer_13;
	// System.Single ZuroStreet::groundCheckRadius
	float ___groundCheckRadius_14;

public:
	inline static int32_t get_offset_of_horizontalVelocity_4() { return static_cast<int32_t>(offsetof(ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256, ___horizontalVelocity_4)); }
	inline float get_horizontalVelocity_4() const { return ___horizontalVelocity_4; }
	inline float* get_address_of_horizontalVelocity_4() { return &___horizontalVelocity_4; }
	inline void set_horizontalVelocity_4(float value)
	{
		___horizontalVelocity_4 = value;
	}

	inline static int32_t get_offset_of_jumpForce_5() { return static_cast<int32_t>(offsetof(ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256, ___jumpForce_5)); }
	inline float get_jumpForce_5() const { return ___jumpForce_5; }
	inline float* get_address_of_jumpForce_5() { return &___jumpForce_5; }
	inline void set_jumpForce_5(float value)
	{
		___jumpForce_5 = value;
	}

	inline static int32_t get_offset_of_numberOfJumps_6() { return static_cast<int32_t>(offsetof(ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256, ___numberOfJumps_6)); }
	inline int32_t get_numberOfJumps_6() const { return ___numberOfJumps_6; }
	inline int32_t* get_address_of_numberOfJumps_6() { return &___numberOfJumps_6; }
	inline void set_numberOfJumps_6(int32_t value)
	{
		___numberOfJumps_6 = value;
	}

	inline static int32_t get_offset_of_isGrounded_7() { return static_cast<int32_t>(offsetof(ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256, ___isGrounded_7)); }
	inline bool get_isGrounded_7() const { return ___isGrounded_7; }
	inline bool* get_address_of_isGrounded_7() { return &___isGrounded_7; }
	inline void set_isGrounded_7(bool value)
	{
		___isGrounded_7 = value;
	}

	inline static int32_t get_offset_of_isAttacking_8() { return static_cast<int32_t>(offsetof(ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256, ___isAttacking_8)); }
	inline bool get_isAttacking_8() const { return ___isAttacking_8; }
	inline bool* get_address_of_isAttacking_8() { return &___isAttacking_8; }
	inline void set_isAttacking_8(bool value)
	{
		___isAttacking_8 = value;
	}

	inline static int32_t get_offset_of__rigidbody_9() { return static_cast<int32_t>(offsetof(ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256, ____rigidbody_9)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get__rigidbody_9() const { return ____rigidbody_9; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of__rigidbody_9() { return &____rigidbody_9; }
	inline void set__rigidbody_9(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		____rigidbody_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rigidbody_9), (void*)value);
	}

	inline static int32_t get_offset_of__animator_10() { return static_cast<int32_t>(offsetof(ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256, ____animator_10)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__animator_10() const { return ____animator_10; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__animator_10() { return &____animator_10; }
	inline void set__animator_10(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____animator_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_10), (void*)value);
	}

	inline static int32_t get_offset_of_aimingTime_11() { return static_cast<int32_t>(offsetof(ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256, ___aimingTime_11)); }
	inline float get_aimingTime_11() const { return ___aimingTime_11; }
	inline float* get_address_of_aimingTime_11() { return &___aimingTime_11; }
	inline void set_aimingTime_11(float value)
	{
		___aimingTime_11 = value;
	}

	inline static int32_t get_offset_of_groundCheck_12() { return static_cast<int32_t>(offsetof(ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256, ___groundCheck_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_groundCheck_12() const { return ___groundCheck_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_groundCheck_12() { return &___groundCheck_12; }
	inline void set_groundCheck_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___groundCheck_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groundCheck_12), (void*)value);
	}

	inline static int32_t get_offset_of_groundLayer_13() { return static_cast<int32_t>(offsetof(ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256, ___groundLayer_13)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_groundLayer_13() const { return ___groundLayer_13; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_groundLayer_13() { return &___groundLayer_13; }
	inline void set_groundLayer_13(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___groundLayer_13 = value;
	}

	inline static int32_t get_offset_of_groundCheckRadius_14() { return static_cast<int32_t>(offsetof(ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256, ___groundCheckRadius_14)); }
	inline float get_groundCheckRadius_14() const { return ___groundCheckRadius_14; }
	inline float* get_address_of_groundCheckRadius_14() { return &___groundCheckRadius_14; }
	inline void set_groundCheckRadius_14(float value)
	{
		___groundCheckRadius_14 = value;
	}
};


// Cinemachine.CinemachineComposer
struct  CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8  : public CinemachineComponentBase_tC2AC9880A0A85AE8136352639E33911E7FCB013D
{
public:
	// UnityEngine.Vector3 Cinemachine.CinemachineComposer::m_TrackedObjectOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_TrackedObjectOffset_12;
	// System.Single Cinemachine.CinemachineComposer::m_LookaheadTime
	float ___m_LookaheadTime_13;
	// System.Single Cinemachine.CinemachineComposer::m_LookaheadSmoothing
	float ___m_LookaheadSmoothing_14;
	// System.Boolean Cinemachine.CinemachineComposer::m_LookaheadIgnoreY
	bool ___m_LookaheadIgnoreY_15;
	// System.Single Cinemachine.CinemachineComposer::m_HorizontalDamping
	float ___m_HorizontalDamping_16;
	// System.Single Cinemachine.CinemachineComposer::m_VerticalDamping
	float ___m_VerticalDamping_17;
	// System.Single Cinemachine.CinemachineComposer::m_ScreenX
	float ___m_ScreenX_18;
	// System.Single Cinemachine.CinemachineComposer::m_ScreenY
	float ___m_ScreenY_19;
	// System.Single Cinemachine.CinemachineComposer::m_DeadZoneWidth
	float ___m_DeadZoneWidth_20;
	// System.Single Cinemachine.CinemachineComposer::m_DeadZoneHeight
	float ___m_DeadZoneHeight_21;
	// System.Single Cinemachine.CinemachineComposer::m_SoftZoneWidth
	float ___m_SoftZoneWidth_22;
	// System.Single Cinemachine.CinemachineComposer::m_SoftZoneHeight
	float ___m_SoftZoneHeight_23;
	// System.Single Cinemachine.CinemachineComposer::m_BiasX
	float ___m_BiasX_24;
	// System.Single Cinemachine.CinemachineComposer::m_BiasY
	float ___m_BiasY_25;
	// System.Boolean Cinemachine.CinemachineComposer::m_CenterOnActivate
	bool ___m_CenterOnActivate_26;
	// UnityEngine.Vector3 Cinemachine.CinemachineComposer::<TrackedPoint>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CTrackedPointU3Ek__BackingField_27;
	// UnityEngine.Vector3 Cinemachine.CinemachineComposer::m_CameraPosPrevFrame
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_CameraPosPrevFrame_28;
	// UnityEngine.Vector3 Cinemachine.CinemachineComposer::m_LookAtPrevFrame
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_LookAtPrevFrame_29;
	// UnityEngine.Vector2 Cinemachine.CinemachineComposer::m_ScreenOffsetPrevFrame
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_ScreenOffsetPrevFrame_30;
	// UnityEngine.Quaternion Cinemachine.CinemachineComposer::m_CameraOrientationPrevFrame
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_CameraOrientationPrevFrame_31;
	// Cinemachine.Utility.PositionPredictor Cinemachine.CinemachineComposer::m_Predictor
	PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223 * ___m_Predictor_32;
	// Cinemachine.CinemachineComposer_FovCache Cinemachine.CinemachineComposer::mCache
	FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43  ___mCache_33;

public:
	inline static int32_t get_offset_of_m_TrackedObjectOffset_12() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_TrackedObjectOffset_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_TrackedObjectOffset_12() const { return ___m_TrackedObjectOffset_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_TrackedObjectOffset_12() { return &___m_TrackedObjectOffset_12; }
	inline void set_m_TrackedObjectOffset_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_TrackedObjectOffset_12 = value;
	}

	inline static int32_t get_offset_of_m_LookaheadTime_13() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_LookaheadTime_13)); }
	inline float get_m_LookaheadTime_13() const { return ___m_LookaheadTime_13; }
	inline float* get_address_of_m_LookaheadTime_13() { return &___m_LookaheadTime_13; }
	inline void set_m_LookaheadTime_13(float value)
	{
		___m_LookaheadTime_13 = value;
	}

	inline static int32_t get_offset_of_m_LookaheadSmoothing_14() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_LookaheadSmoothing_14)); }
	inline float get_m_LookaheadSmoothing_14() const { return ___m_LookaheadSmoothing_14; }
	inline float* get_address_of_m_LookaheadSmoothing_14() { return &___m_LookaheadSmoothing_14; }
	inline void set_m_LookaheadSmoothing_14(float value)
	{
		___m_LookaheadSmoothing_14 = value;
	}

	inline static int32_t get_offset_of_m_LookaheadIgnoreY_15() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_LookaheadIgnoreY_15)); }
	inline bool get_m_LookaheadIgnoreY_15() const { return ___m_LookaheadIgnoreY_15; }
	inline bool* get_address_of_m_LookaheadIgnoreY_15() { return &___m_LookaheadIgnoreY_15; }
	inline void set_m_LookaheadIgnoreY_15(bool value)
	{
		___m_LookaheadIgnoreY_15 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalDamping_16() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_HorizontalDamping_16)); }
	inline float get_m_HorizontalDamping_16() const { return ___m_HorizontalDamping_16; }
	inline float* get_address_of_m_HorizontalDamping_16() { return &___m_HorizontalDamping_16; }
	inline void set_m_HorizontalDamping_16(float value)
	{
		___m_HorizontalDamping_16 = value;
	}

	inline static int32_t get_offset_of_m_VerticalDamping_17() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_VerticalDamping_17)); }
	inline float get_m_VerticalDamping_17() const { return ___m_VerticalDamping_17; }
	inline float* get_address_of_m_VerticalDamping_17() { return &___m_VerticalDamping_17; }
	inline void set_m_VerticalDamping_17(float value)
	{
		___m_VerticalDamping_17 = value;
	}

	inline static int32_t get_offset_of_m_ScreenX_18() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_ScreenX_18)); }
	inline float get_m_ScreenX_18() const { return ___m_ScreenX_18; }
	inline float* get_address_of_m_ScreenX_18() { return &___m_ScreenX_18; }
	inline void set_m_ScreenX_18(float value)
	{
		___m_ScreenX_18 = value;
	}

	inline static int32_t get_offset_of_m_ScreenY_19() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_ScreenY_19)); }
	inline float get_m_ScreenY_19() const { return ___m_ScreenY_19; }
	inline float* get_address_of_m_ScreenY_19() { return &___m_ScreenY_19; }
	inline void set_m_ScreenY_19(float value)
	{
		___m_ScreenY_19 = value;
	}

	inline static int32_t get_offset_of_m_DeadZoneWidth_20() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_DeadZoneWidth_20)); }
	inline float get_m_DeadZoneWidth_20() const { return ___m_DeadZoneWidth_20; }
	inline float* get_address_of_m_DeadZoneWidth_20() { return &___m_DeadZoneWidth_20; }
	inline void set_m_DeadZoneWidth_20(float value)
	{
		___m_DeadZoneWidth_20 = value;
	}

	inline static int32_t get_offset_of_m_DeadZoneHeight_21() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_DeadZoneHeight_21)); }
	inline float get_m_DeadZoneHeight_21() const { return ___m_DeadZoneHeight_21; }
	inline float* get_address_of_m_DeadZoneHeight_21() { return &___m_DeadZoneHeight_21; }
	inline void set_m_DeadZoneHeight_21(float value)
	{
		___m_DeadZoneHeight_21 = value;
	}

	inline static int32_t get_offset_of_m_SoftZoneWidth_22() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_SoftZoneWidth_22)); }
	inline float get_m_SoftZoneWidth_22() const { return ___m_SoftZoneWidth_22; }
	inline float* get_address_of_m_SoftZoneWidth_22() { return &___m_SoftZoneWidth_22; }
	inline void set_m_SoftZoneWidth_22(float value)
	{
		___m_SoftZoneWidth_22 = value;
	}

	inline static int32_t get_offset_of_m_SoftZoneHeight_23() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_SoftZoneHeight_23)); }
	inline float get_m_SoftZoneHeight_23() const { return ___m_SoftZoneHeight_23; }
	inline float* get_address_of_m_SoftZoneHeight_23() { return &___m_SoftZoneHeight_23; }
	inline void set_m_SoftZoneHeight_23(float value)
	{
		___m_SoftZoneHeight_23 = value;
	}

	inline static int32_t get_offset_of_m_BiasX_24() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_BiasX_24)); }
	inline float get_m_BiasX_24() const { return ___m_BiasX_24; }
	inline float* get_address_of_m_BiasX_24() { return &___m_BiasX_24; }
	inline void set_m_BiasX_24(float value)
	{
		___m_BiasX_24 = value;
	}

	inline static int32_t get_offset_of_m_BiasY_25() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_BiasY_25)); }
	inline float get_m_BiasY_25() const { return ___m_BiasY_25; }
	inline float* get_address_of_m_BiasY_25() { return &___m_BiasY_25; }
	inline void set_m_BiasY_25(float value)
	{
		___m_BiasY_25 = value;
	}

	inline static int32_t get_offset_of_m_CenterOnActivate_26() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_CenterOnActivate_26)); }
	inline bool get_m_CenterOnActivate_26() const { return ___m_CenterOnActivate_26; }
	inline bool* get_address_of_m_CenterOnActivate_26() { return &___m_CenterOnActivate_26; }
	inline void set_m_CenterOnActivate_26(bool value)
	{
		___m_CenterOnActivate_26 = value;
	}

	inline static int32_t get_offset_of_U3CTrackedPointU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___U3CTrackedPointU3Ek__BackingField_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CTrackedPointU3Ek__BackingField_27() const { return ___U3CTrackedPointU3Ek__BackingField_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CTrackedPointU3Ek__BackingField_27() { return &___U3CTrackedPointU3Ek__BackingField_27; }
	inline void set_U3CTrackedPointU3Ek__BackingField_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CTrackedPointU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_m_CameraPosPrevFrame_28() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_CameraPosPrevFrame_28)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_CameraPosPrevFrame_28() const { return ___m_CameraPosPrevFrame_28; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_CameraPosPrevFrame_28() { return &___m_CameraPosPrevFrame_28; }
	inline void set_m_CameraPosPrevFrame_28(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_CameraPosPrevFrame_28 = value;
	}

	inline static int32_t get_offset_of_m_LookAtPrevFrame_29() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_LookAtPrevFrame_29)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_LookAtPrevFrame_29() const { return ___m_LookAtPrevFrame_29; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_LookAtPrevFrame_29() { return &___m_LookAtPrevFrame_29; }
	inline void set_m_LookAtPrevFrame_29(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_LookAtPrevFrame_29 = value;
	}

	inline static int32_t get_offset_of_m_ScreenOffsetPrevFrame_30() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_ScreenOffsetPrevFrame_30)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_ScreenOffsetPrevFrame_30() const { return ___m_ScreenOffsetPrevFrame_30; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_ScreenOffsetPrevFrame_30() { return &___m_ScreenOffsetPrevFrame_30; }
	inline void set_m_ScreenOffsetPrevFrame_30(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_ScreenOffsetPrevFrame_30 = value;
	}

	inline static int32_t get_offset_of_m_CameraOrientationPrevFrame_31() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_CameraOrientationPrevFrame_31)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_CameraOrientationPrevFrame_31() const { return ___m_CameraOrientationPrevFrame_31; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_CameraOrientationPrevFrame_31() { return &___m_CameraOrientationPrevFrame_31; }
	inline void set_m_CameraOrientationPrevFrame_31(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_CameraOrientationPrevFrame_31 = value;
	}

	inline static int32_t get_offset_of_m_Predictor_32() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___m_Predictor_32)); }
	inline PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223 * get_m_Predictor_32() const { return ___m_Predictor_32; }
	inline PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223 ** get_address_of_m_Predictor_32() { return &___m_Predictor_32; }
	inline void set_m_Predictor_32(PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223 * value)
	{
		___m_Predictor_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Predictor_32), (void*)value);
	}

	inline static int32_t get_offset_of_mCache_33() { return static_cast<int32_t>(offsetof(CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8, ___mCache_33)); }
	inline FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43  get_mCache_33() const { return ___mCache_33; }
	inline FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43 * get_address_of_mCache_33() { return &___mCache_33; }
	inline void set_mCache_33(FovCache_t8A885CB5BEFFFA5579A86C9360E7E2AE4A990A43  value)
	{
		___mCache_33 = value;
	}
};


// Cinemachine.CinemachineFreeLook
struct  CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152  : public CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineFreeLook::m_LookAt
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_LookAt_16;
	// UnityEngine.Transform Cinemachine.CinemachineFreeLook::m_Follow
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_Follow_17;
	// System.Boolean Cinemachine.CinemachineFreeLook::m_CommonLens
	bool ___m_CommonLens_18;
	// Cinemachine.LensSettings Cinemachine.CinemachineFreeLook::m_Lens
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  ___m_Lens_19;
	// Cinemachine.CinemachineVirtualCameraBase_TransitionParams Cinemachine.CinemachineFreeLook::m_Transitions
	TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8  ___m_Transitions_20;
	// Cinemachine.CinemachineVirtualCameraBase_BlendHint Cinemachine.CinemachineFreeLook::m_LegacyBlendHint
	int32_t ___m_LegacyBlendHint_21;
	// Cinemachine.AxisState Cinemachine.CinemachineFreeLook::m_YAxis
	AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B  ___m_YAxis_22;
	// Cinemachine.AxisState_Recentering Cinemachine.CinemachineFreeLook::m_YAxisRecentering
	Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A  ___m_YAxisRecentering_23;
	// Cinemachine.AxisState Cinemachine.CinemachineFreeLook::m_XAxis
	AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B  ___m_XAxis_24;
	// Cinemachine.CinemachineOrbitalTransposer_Heading Cinemachine.CinemachineFreeLook::m_Heading
	Heading_t66611F284762C65851075D4BF681E9034E65612D  ___m_Heading_25;
	// Cinemachine.AxisState_Recentering Cinemachine.CinemachineFreeLook::m_RecenterToTargetHeading
	Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A  ___m_RecenterToTargetHeading_26;
	// Cinemachine.CinemachineTransposer_BindingMode Cinemachine.CinemachineFreeLook::m_BindingMode
	int32_t ___m_BindingMode_27;
	// System.Single Cinemachine.CinemachineFreeLook::m_SplineCurvature
	float ___m_SplineCurvature_28;
	// Cinemachine.CinemachineFreeLook_Orbit[] Cinemachine.CinemachineFreeLook::m_Orbits
	OrbitU5BU5D_t937BD50C0ED65D0BBC5ED836B1B32D9FD6792C4E* ___m_Orbits_29;
	// System.Single Cinemachine.CinemachineFreeLook::m_LegacyHeadingBias
	float ___m_LegacyHeadingBias_30;
	// System.Boolean Cinemachine.CinemachineFreeLook::mUseLegacyRigDefinitions
	bool ___mUseLegacyRigDefinitions_31;
	// System.Boolean Cinemachine.CinemachineFreeLook::mIsDestroyed
	bool ___mIsDestroyed_32;
	// Cinemachine.CameraState Cinemachine.CinemachineFreeLook::m_State
	CameraState_t308F3A442112B7464D2B21A417D325662E3399B1  ___m_State_33;
	// Cinemachine.CinemachineVirtualCamera[] Cinemachine.CinemachineFreeLook::m_Rigs
	CinemachineVirtualCameraU5BU5D_tE70BE11F7F9BAE7CC377C1946095485E7796D026* ___m_Rigs_34;
	// Cinemachine.CinemachineOrbitalTransposer[] Cinemachine.CinemachineFreeLook::mOrbitals
	CinemachineOrbitalTransposerU5BU5D_t3EE22418D270D58108A35D0162F2F44463218F5B* ___mOrbitals_35;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineFreeLook::mBlendA
	CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298 * ___mBlendA_36;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineFreeLook::mBlendB
	CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298 * ___mBlendB_37;
	// System.Single Cinemachine.CinemachineFreeLook::<CachedXAxisHeading>k__BackingField
	float ___U3CCachedXAxisHeadingU3Ek__BackingField_40;
	// Cinemachine.CinemachineFreeLook_Orbit[] Cinemachine.CinemachineFreeLook::m_CachedOrbits
	OrbitU5BU5D_t937BD50C0ED65D0BBC5ED836B1B32D9FD6792C4E* ___m_CachedOrbits_41;
	// System.Single Cinemachine.CinemachineFreeLook::m_CachedTension
	float ___m_CachedTension_42;
	// UnityEngine.Vector4[] Cinemachine.CinemachineFreeLook::m_CachedKnots
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___m_CachedKnots_43;
	// UnityEngine.Vector4[] Cinemachine.CinemachineFreeLook::m_CachedCtrl1
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___m_CachedCtrl1_44;
	// UnityEngine.Vector4[] Cinemachine.CinemachineFreeLook::m_CachedCtrl2
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___m_CachedCtrl2_45;

public:
	inline static int32_t get_offset_of_m_LookAt_16() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_LookAt_16)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_LookAt_16() const { return ___m_LookAt_16; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_LookAt_16() { return &___m_LookAt_16; }
	inline void set_m_LookAt_16(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_LookAt_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LookAt_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_Follow_17() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_Follow_17)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_Follow_17() const { return ___m_Follow_17; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_Follow_17() { return &___m_Follow_17; }
	inline void set_m_Follow_17(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_Follow_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Follow_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_CommonLens_18() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_CommonLens_18)); }
	inline bool get_m_CommonLens_18() const { return ___m_CommonLens_18; }
	inline bool* get_address_of_m_CommonLens_18() { return &___m_CommonLens_18; }
	inline void set_m_CommonLens_18(bool value)
	{
		___m_CommonLens_18 = value;
	}

	inline static int32_t get_offset_of_m_Lens_19() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_Lens_19)); }
	inline LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  get_m_Lens_19() const { return ___m_Lens_19; }
	inline LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC * get_address_of_m_Lens_19() { return &___m_Lens_19; }
	inline void set_m_Lens_19(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  value)
	{
		___m_Lens_19 = value;
	}

	inline static int32_t get_offset_of_m_Transitions_20() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_Transitions_20)); }
	inline TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8  get_m_Transitions_20() const { return ___m_Transitions_20; }
	inline TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8 * get_address_of_m_Transitions_20() { return &___m_Transitions_20; }
	inline void set_m_Transitions_20(TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8  value)
	{
		___m_Transitions_20 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Transitions_20))->___m_OnCameraLive_2), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_LegacyBlendHint_21() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_LegacyBlendHint_21)); }
	inline int32_t get_m_LegacyBlendHint_21() const { return ___m_LegacyBlendHint_21; }
	inline int32_t* get_address_of_m_LegacyBlendHint_21() { return &___m_LegacyBlendHint_21; }
	inline void set_m_LegacyBlendHint_21(int32_t value)
	{
		___m_LegacyBlendHint_21 = value;
	}

	inline static int32_t get_offset_of_m_YAxis_22() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_YAxis_22)); }
	inline AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B  get_m_YAxis_22() const { return ___m_YAxis_22; }
	inline AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B * get_address_of_m_YAxis_22() { return &___m_YAxis_22; }
	inline void set_m_YAxis_22(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B  value)
	{
		___m_YAxis_22 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_YAxis_22))->___m_InputAxisName_5), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_YAxisRecentering_23() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_YAxisRecentering_23)); }
	inline Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A  get_m_YAxisRecentering_23() const { return ___m_YAxisRecentering_23; }
	inline Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A * get_address_of_m_YAxisRecentering_23() { return &___m_YAxisRecentering_23; }
	inline void set_m_YAxisRecentering_23(Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A  value)
	{
		___m_YAxisRecentering_23 = value;
	}

	inline static int32_t get_offset_of_m_XAxis_24() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_XAxis_24)); }
	inline AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B  get_m_XAxis_24() const { return ___m_XAxis_24; }
	inline AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B * get_address_of_m_XAxis_24() { return &___m_XAxis_24; }
	inline void set_m_XAxis_24(AxisState_tF3A1DFDC83DE8E3EE38D0D44B05C3EFAF6FA952B  value)
	{
		___m_XAxis_24 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_XAxis_24))->___m_InputAxisName_5), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_Heading_25() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_Heading_25)); }
	inline Heading_t66611F284762C65851075D4BF681E9034E65612D  get_m_Heading_25() const { return ___m_Heading_25; }
	inline Heading_t66611F284762C65851075D4BF681E9034E65612D * get_address_of_m_Heading_25() { return &___m_Heading_25; }
	inline void set_m_Heading_25(Heading_t66611F284762C65851075D4BF681E9034E65612D  value)
	{
		___m_Heading_25 = value;
	}

	inline static int32_t get_offset_of_m_RecenterToTargetHeading_26() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_RecenterToTargetHeading_26)); }
	inline Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A  get_m_RecenterToTargetHeading_26() const { return ___m_RecenterToTargetHeading_26; }
	inline Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A * get_address_of_m_RecenterToTargetHeading_26() { return &___m_RecenterToTargetHeading_26; }
	inline void set_m_RecenterToTargetHeading_26(Recentering_tC0B9A295A525E6CAB60ACF4E75AD40A59A56D12A  value)
	{
		___m_RecenterToTargetHeading_26 = value;
	}

	inline static int32_t get_offset_of_m_BindingMode_27() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_BindingMode_27)); }
	inline int32_t get_m_BindingMode_27() const { return ___m_BindingMode_27; }
	inline int32_t* get_address_of_m_BindingMode_27() { return &___m_BindingMode_27; }
	inline void set_m_BindingMode_27(int32_t value)
	{
		___m_BindingMode_27 = value;
	}

	inline static int32_t get_offset_of_m_SplineCurvature_28() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_SplineCurvature_28)); }
	inline float get_m_SplineCurvature_28() const { return ___m_SplineCurvature_28; }
	inline float* get_address_of_m_SplineCurvature_28() { return &___m_SplineCurvature_28; }
	inline void set_m_SplineCurvature_28(float value)
	{
		___m_SplineCurvature_28 = value;
	}

	inline static int32_t get_offset_of_m_Orbits_29() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_Orbits_29)); }
	inline OrbitU5BU5D_t937BD50C0ED65D0BBC5ED836B1B32D9FD6792C4E* get_m_Orbits_29() const { return ___m_Orbits_29; }
	inline OrbitU5BU5D_t937BD50C0ED65D0BBC5ED836B1B32D9FD6792C4E** get_address_of_m_Orbits_29() { return &___m_Orbits_29; }
	inline void set_m_Orbits_29(OrbitU5BU5D_t937BD50C0ED65D0BBC5ED836B1B32D9FD6792C4E* value)
	{
		___m_Orbits_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Orbits_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_LegacyHeadingBias_30() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_LegacyHeadingBias_30)); }
	inline float get_m_LegacyHeadingBias_30() const { return ___m_LegacyHeadingBias_30; }
	inline float* get_address_of_m_LegacyHeadingBias_30() { return &___m_LegacyHeadingBias_30; }
	inline void set_m_LegacyHeadingBias_30(float value)
	{
		___m_LegacyHeadingBias_30 = value;
	}

	inline static int32_t get_offset_of_mUseLegacyRigDefinitions_31() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___mUseLegacyRigDefinitions_31)); }
	inline bool get_mUseLegacyRigDefinitions_31() const { return ___mUseLegacyRigDefinitions_31; }
	inline bool* get_address_of_mUseLegacyRigDefinitions_31() { return &___mUseLegacyRigDefinitions_31; }
	inline void set_mUseLegacyRigDefinitions_31(bool value)
	{
		___mUseLegacyRigDefinitions_31 = value;
	}

	inline static int32_t get_offset_of_mIsDestroyed_32() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___mIsDestroyed_32)); }
	inline bool get_mIsDestroyed_32() const { return ___mIsDestroyed_32; }
	inline bool* get_address_of_mIsDestroyed_32() { return &___mIsDestroyed_32; }
	inline void set_mIsDestroyed_32(bool value)
	{
		___mIsDestroyed_32 = value;
	}

	inline static int32_t get_offset_of_m_State_33() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_State_33)); }
	inline CameraState_t308F3A442112B7464D2B21A417D325662E3399B1  get_m_State_33() const { return ___m_State_33; }
	inline CameraState_t308F3A442112B7464D2B21A417D325662E3399B1 * get_address_of_m_State_33() { return &___m_State_33; }
	inline void set_m_State_33(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1  value)
	{
		___m_State_33 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_33))->___mCustom0_11))->___m_Custom_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_33))->___mCustom1_12))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_33))->___mCustom2_13))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_33))->___mCustom3_14))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_State_33))->___m_CustomOverflow_15), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Rigs_34() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_Rigs_34)); }
	inline CinemachineVirtualCameraU5BU5D_tE70BE11F7F9BAE7CC377C1946095485E7796D026* get_m_Rigs_34() const { return ___m_Rigs_34; }
	inline CinemachineVirtualCameraU5BU5D_tE70BE11F7F9BAE7CC377C1946095485E7796D026** get_address_of_m_Rigs_34() { return &___m_Rigs_34; }
	inline void set_m_Rigs_34(CinemachineVirtualCameraU5BU5D_tE70BE11F7F9BAE7CC377C1946095485E7796D026* value)
	{
		___m_Rigs_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Rigs_34), (void*)value);
	}

	inline static int32_t get_offset_of_mOrbitals_35() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___mOrbitals_35)); }
	inline CinemachineOrbitalTransposerU5BU5D_t3EE22418D270D58108A35D0162F2F44463218F5B* get_mOrbitals_35() const { return ___mOrbitals_35; }
	inline CinemachineOrbitalTransposerU5BU5D_t3EE22418D270D58108A35D0162F2F44463218F5B** get_address_of_mOrbitals_35() { return &___mOrbitals_35; }
	inline void set_mOrbitals_35(CinemachineOrbitalTransposerU5BU5D_t3EE22418D270D58108A35D0162F2F44463218F5B* value)
	{
		___mOrbitals_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mOrbitals_35), (void*)value);
	}

	inline static int32_t get_offset_of_mBlendA_36() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___mBlendA_36)); }
	inline CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298 * get_mBlendA_36() const { return ___mBlendA_36; }
	inline CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298 ** get_address_of_mBlendA_36() { return &___mBlendA_36; }
	inline void set_mBlendA_36(CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298 * value)
	{
		___mBlendA_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mBlendA_36), (void*)value);
	}

	inline static int32_t get_offset_of_mBlendB_37() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___mBlendB_37)); }
	inline CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298 * get_mBlendB_37() const { return ___mBlendB_37; }
	inline CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298 ** get_address_of_mBlendB_37() { return &___mBlendB_37; }
	inline void set_mBlendB_37(CinemachineBlend_t7A2A71833A7E2C44AB1D7874B3D076EF46A56298 * value)
	{
		___mBlendB_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mBlendB_37), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCachedXAxisHeadingU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___U3CCachedXAxisHeadingU3Ek__BackingField_40)); }
	inline float get_U3CCachedXAxisHeadingU3Ek__BackingField_40() const { return ___U3CCachedXAxisHeadingU3Ek__BackingField_40; }
	inline float* get_address_of_U3CCachedXAxisHeadingU3Ek__BackingField_40() { return &___U3CCachedXAxisHeadingU3Ek__BackingField_40; }
	inline void set_U3CCachedXAxisHeadingU3Ek__BackingField_40(float value)
	{
		___U3CCachedXAxisHeadingU3Ek__BackingField_40 = value;
	}

	inline static int32_t get_offset_of_m_CachedOrbits_41() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_CachedOrbits_41)); }
	inline OrbitU5BU5D_t937BD50C0ED65D0BBC5ED836B1B32D9FD6792C4E* get_m_CachedOrbits_41() const { return ___m_CachedOrbits_41; }
	inline OrbitU5BU5D_t937BD50C0ED65D0BBC5ED836B1B32D9FD6792C4E** get_address_of_m_CachedOrbits_41() { return &___m_CachedOrbits_41; }
	inline void set_m_CachedOrbits_41(OrbitU5BU5D_t937BD50C0ED65D0BBC5ED836B1B32D9FD6792C4E* value)
	{
		___m_CachedOrbits_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedOrbits_41), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedTension_42() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_CachedTension_42)); }
	inline float get_m_CachedTension_42() const { return ___m_CachedTension_42; }
	inline float* get_address_of_m_CachedTension_42() { return &___m_CachedTension_42; }
	inline void set_m_CachedTension_42(float value)
	{
		___m_CachedTension_42 = value;
	}

	inline static int32_t get_offset_of_m_CachedKnots_43() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_CachedKnots_43)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_m_CachedKnots_43() const { return ___m_CachedKnots_43; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_m_CachedKnots_43() { return &___m_CachedKnots_43; }
	inline void set_m_CachedKnots_43(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___m_CachedKnots_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedKnots_43), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedCtrl1_44() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_CachedCtrl1_44)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_m_CachedCtrl1_44() const { return ___m_CachedCtrl1_44; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_m_CachedCtrl1_44() { return &___m_CachedCtrl1_44; }
	inline void set_m_CachedCtrl1_44(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___m_CachedCtrl1_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedCtrl1_44), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedCtrl2_45() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152, ___m_CachedCtrl2_45)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_m_CachedCtrl2_45() const { return ___m_CachedCtrl2_45; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_m_CachedCtrl2_45() { return &___m_CachedCtrl2_45; }
	inline void set_m_CachedCtrl2_45(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___m_CachedCtrl2_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedCtrl2_45), (void*)value);
	}
};

struct CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152_StaticFields
{
public:
	// Cinemachine.CinemachineFreeLook_CreateRigDelegate Cinemachine.CinemachineFreeLook::CreateRigOverride
	CreateRigDelegate_tD3CD6B9257D1E55C62EA2C967131351472916D9B * ___CreateRigOverride_38;
	// Cinemachine.CinemachineFreeLook_DestroyRigDelegate Cinemachine.CinemachineFreeLook::DestroyRigOverride
	DestroyRigDelegate_t817A6E366317DBD7E47066614680E435F4753F43 * ___DestroyRigOverride_39;

public:
	inline static int32_t get_offset_of_CreateRigOverride_38() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152_StaticFields, ___CreateRigOverride_38)); }
	inline CreateRigDelegate_tD3CD6B9257D1E55C62EA2C967131351472916D9B * get_CreateRigOverride_38() const { return ___CreateRigOverride_38; }
	inline CreateRigDelegate_tD3CD6B9257D1E55C62EA2C967131351472916D9B ** get_address_of_CreateRigOverride_38() { return &___CreateRigOverride_38; }
	inline void set_CreateRigOverride_38(CreateRigDelegate_tD3CD6B9257D1E55C62EA2C967131351472916D9B * value)
	{
		___CreateRigOverride_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CreateRigOverride_38), (void*)value);
	}

	inline static int32_t get_offset_of_DestroyRigOverride_39() { return static_cast<int32_t>(offsetof(CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152_StaticFields, ___DestroyRigOverride_39)); }
	inline DestroyRigDelegate_t817A6E366317DBD7E47066614680E435F4753F43 * get_DestroyRigOverride_39() const { return ___DestroyRigOverride_39; }
	inline DestroyRigDelegate_t817A6E366317DBD7E47066614680E435F4753F43 ** get_address_of_DestroyRigOverride_39() { return &___DestroyRigOverride_39; }
	inline void set_DestroyRigOverride_39(DestroyRigDelegate_t817A6E366317DBD7E47066614680E435F4753F43 * value)
	{
		___DestroyRigOverride_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DestroyRigOverride_39), (void*)value);
	}
};


// Cinemachine.CinemachineMixingCamera
struct  CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE  : public CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD
{
public:
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight0
	float ___m_Weight0_17;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight1
	float ___m_Weight1_18;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight2
	float ___m_Weight2_19;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight3
	float ___m_Weight3_20;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight4
	float ___m_Weight4_21;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight5
	float ___m_Weight5_22;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight6
	float ___m_Weight6_23;
	// System.Single Cinemachine.CinemachineMixingCamera::m_Weight7
	float ___m_Weight7_24;
	// Cinemachine.CameraState Cinemachine.CinemachineMixingCamera::m_State
	CameraState_t308F3A442112B7464D2B21A417D325662E3399B1  ___m_State_25;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineMixingCamera::<LiveChild>k__BackingField
	RuntimeObject* ___U3CLiveChildU3Ek__BackingField_26;
	// UnityEngine.Transform Cinemachine.CinemachineMixingCamera::<LookAt>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CLookAtU3Ek__BackingField_27;
	// UnityEngine.Transform Cinemachine.CinemachineMixingCamera::<Follow>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CFollowU3Ek__BackingField_28;
	// Cinemachine.CinemachineVirtualCameraBase[] Cinemachine.CinemachineMixingCamera::m_ChildCameras
	CinemachineVirtualCameraBaseU5BU5D_tCD65D3C04B191624A196F05BE07D81E657436EF3* ___m_ChildCameras_29;
	// System.Collections.Generic.Dictionary`2<Cinemachine.CinemachineVirtualCameraBase,System.Int32> Cinemachine.CinemachineMixingCamera::m_indexMap
	Dictionary_2_t0D2A029841990C10C2ACF3B66D554D04A3C6DBA6 * ___m_indexMap_30;

public:
	inline static int32_t get_offset_of_m_Weight0_17() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___m_Weight0_17)); }
	inline float get_m_Weight0_17() const { return ___m_Weight0_17; }
	inline float* get_address_of_m_Weight0_17() { return &___m_Weight0_17; }
	inline void set_m_Weight0_17(float value)
	{
		___m_Weight0_17 = value;
	}

	inline static int32_t get_offset_of_m_Weight1_18() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___m_Weight1_18)); }
	inline float get_m_Weight1_18() const { return ___m_Weight1_18; }
	inline float* get_address_of_m_Weight1_18() { return &___m_Weight1_18; }
	inline void set_m_Weight1_18(float value)
	{
		___m_Weight1_18 = value;
	}

	inline static int32_t get_offset_of_m_Weight2_19() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___m_Weight2_19)); }
	inline float get_m_Weight2_19() const { return ___m_Weight2_19; }
	inline float* get_address_of_m_Weight2_19() { return &___m_Weight2_19; }
	inline void set_m_Weight2_19(float value)
	{
		___m_Weight2_19 = value;
	}

	inline static int32_t get_offset_of_m_Weight3_20() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___m_Weight3_20)); }
	inline float get_m_Weight3_20() const { return ___m_Weight3_20; }
	inline float* get_address_of_m_Weight3_20() { return &___m_Weight3_20; }
	inline void set_m_Weight3_20(float value)
	{
		___m_Weight3_20 = value;
	}

	inline static int32_t get_offset_of_m_Weight4_21() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___m_Weight4_21)); }
	inline float get_m_Weight4_21() const { return ___m_Weight4_21; }
	inline float* get_address_of_m_Weight4_21() { return &___m_Weight4_21; }
	inline void set_m_Weight4_21(float value)
	{
		___m_Weight4_21 = value;
	}

	inline static int32_t get_offset_of_m_Weight5_22() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___m_Weight5_22)); }
	inline float get_m_Weight5_22() const { return ___m_Weight5_22; }
	inline float* get_address_of_m_Weight5_22() { return &___m_Weight5_22; }
	inline void set_m_Weight5_22(float value)
	{
		___m_Weight5_22 = value;
	}

	inline static int32_t get_offset_of_m_Weight6_23() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___m_Weight6_23)); }
	inline float get_m_Weight6_23() const { return ___m_Weight6_23; }
	inline float* get_address_of_m_Weight6_23() { return &___m_Weight6_23; }
	inline void set_m_Weight6_23(float value)
	{
		___m_Weight6_23 = value;
	}

	inline static int32_t get_offset_of_m_Weight7_24() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___m_Weight7_24)); }
	inline float get_m_Weight7_24() const { return ___m_Weight7_24; }
	inline float* get_address_of_m_Weight7_24() { return &___m_Weight7_24; }
	inline void set_m_Weight7_24(float value)
	{
		___m_Weight7_24 = value;
	}

	inline static int32_t get_offset_of_m_State_25() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___m_State_25)); }
	inline CameraState_t308F3A442112B7464D2B21A417D325662E3399B1  get_m_State_25() const { return ___m_State_25; }
	inline CameraState_t308F3A442112B7464D2B21A417D325662E3399B1 * get_address_of_m_State_25() { return &___m_State_25; }
	inline void set_m_State_25(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1  value)
	{
		___m_State_25 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_25))->___mCustom0_11))->___m_Custom_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_25))->___mCustom1_12))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_25))->___mCustom2_13))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_25))->___mCustom3_14))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_State_25))->___m_CustomOverflow_15), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CLiveChildU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___U3CLiveChildU3Ek__BackingField_26)); }
	inline RuntimeObject* get_U3CLiveChildU3Ek__BackingField_26() const { return ___U3CLiveChildU3Ek__BackingField_26; }
	inline RuntimeObject** get_address_of_U3CLiveChildU3Ek__BackingField_26() { return &___U3CLiveChildU3Ek__BackingField_26; }
	inline void set_U3CLiveChildU3Ek__BackingField_26(RuntimeObject* value)
	{
		___U3CLiveChildU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLiveChildU3Ek__BackingField_26), (void*)value);
	}

	inline static int32_t get_offset_of_U3CLookAtU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___U3CLookAtU3Ek__BackingField_27)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CLookAtU3Ek__BackingField_27() const { return ___U3CLookAtU3Ek__BackingField_27; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CLookAtU3Ek__BackingField_27() { return &___U3CLookAtU3Ek__BackingField_27; }
	inline void set_U3CLookAtU3Ek__BackingField_27(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CLookAtU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLookAtU3Ek__BackingField_27), (void*)value);
	}

	inline static int32_t get_offset_of_U3CFollowU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___U3CFollowU3Ek__BackingField_28)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CFollowU3Ek__BackingField_28() const { return ___U3CFollowU3Ek__BackingField_28; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CFollowU3Ek__BackingField_28() { return &___U3CFollowU3Ek__BackingField_28; }
	inline void set_U3CFollowU3Ek__BackingField_28(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CFollowU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFollowU3Ek__BackingField_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChildCameras_29() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___m_ChildCameras_29)); }
	inline CinemachineVirtualCameraBaseU5BU5D_tCD65D3C04B191624A196F05BE07D81E657436EF3* get_m_ChildCameras_29() const { return ___m_ChildCameras_29; }
	inline CinemachineVirtualCameraBaseU5BU5D_tCD65D3C04B191624A196F05BE07D81E657436EF3** get_address_of_m_ChildCameras_29() { return &___m_ChildCameras_29; }
	inline void set_m_ChildCameras_29(CinemachineVirtualCameraBaseU5BU5D_tCD65D3C04B191624A196F05BE07D81E657436EF3* value)
	{
		___m_ChildCameras_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChildCameras_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_indexMap_30() { return static_cast<int32_t>(offsetof(CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE, ___m_indexMap_30)); }
	inline Dictionary_2_t0D2A029841990C10C2ACF3B66D554D04A3C6DBA6 * get_m_indexMap_30() const { return ___m_indexMap_30; }
	inline Dictionary_2_t0D2A029841990C10C2ACF3B66D554D04A3C6DBA6 ** get_address_of_m_indexMap_30() { return &___m_indexMap_30; }
	inline void set_m_indexMap_30(Dictionary_2_t0D2A029841990C10C2ACF3B66D554D04A3C6DBA6 * value)
	{
		___m_indexMap_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_indexMap_30), (void*)value);
	}
};


// Cinemachine.CinemachineVirtualCamera
struct  CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A  : public CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::m_LookAt
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_LookAt_16;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::m_Follow
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_Follow_17;
	// Cinemachine.LensSettings Cinemachine.CinemachineVirtualCamera::m_Lens
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  ___m_Lens_18;
	// Cinemachine.CinemachineVirtualCameraBase_TransitionParams Cinemachine.CinemachineVirtualCamera::m_Transitions
	TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8  ___m_Transitions_19;
	// Cinemachine.CinemachineVirtualCameraBase_BlendHint Cinemachine.CinemachineVirtualCamera::m_LegacyBlendHint
	int32_t ___m_LegacyBlendHint_20;
	// System.Boolean Cinemachine.CinemachineVirtualCamera::<UserIsDragging>k__BackingField
	bool ___U3CUserIsDraggingU3Ek__BackingField_24;
	// Cinemachine.CameraState Cinemachine.CinemachineVirtualCamera::m_State
	CameraState_t308F3A442112B7464D2B21A417D325662E3399B1  ___m_State_25;
	// Cinemachine.CinemachineComponentBase[] Cinemachine.CinemachineVirtualCamera::m_ComponentPipeline
	CinemachineComponentBaseU5BU5D_t7DC13D28344CFE98AF56DA46BCDF936DA49CDA6E* ___m_ComponentPipeline_26;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::m_ComponentOwner
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_ComponentOwner_27;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::mCachedLookAtTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mCachedLookAtTarget_28;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineVirtualCamera::mCachedLookAtTargetVcam
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___mCachedLookAtTargetVcam_29;

public:
	inline static int32_t get_offset_of_m_LookAt_16() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A, ___m_LookAt_16)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_LookAt_16() const { return ___m_LookAt_16; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_LookAt_16() { return &___m_LookAt_16; }
	inline void set_m_LookAt_16(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_LookAt_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LookAt_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_Follow_17() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A, ___m_Follow_17)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_Follow_17() const { return ___m_Follow_17; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_Follow_17() { return &___m_Follow_17; }
	inline void set_m_Follow_17(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_Follow_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Follow_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_Lens_18() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A, ___m_Lens_18)); }
	inline LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  get_m_Lens_18() const { return ___m_Lens_18; }
	inline LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC * get_address_of_m_Lens_18() { return &___m_Lens_18; }
	inline void set_m_Lens_18(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  value)
	{
		___m_Lens_18 = value;
	}

	inline static int32_t get_offset_of_m_Transitions_19() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A, ___m_Transitions_19)); }
	inline TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8  get_m_Transitions_19() const { return ___m_Transitions_19; }
	inline TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8 * get_address_of_m_Transitions_19() { return &___m_Transitions_19; }
	inline void set_m_Transitions_19(TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8  value)
	{
		___m_Transitions_19 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Transitions_19))->___m_OnCameraLive_2), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_LegacyBlendHint_20() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A, ___m_LegacyBlendHint_20)); }
	inline int32_t get_m_LegacyBlendHint_20() const { return ___m_LegacyBlendHint_20; }
	inline int32_t* get_address_of_m_LegacyBlendHint_20() { return &___m_LegacyBlendHint_20; }
	inline void set_m_LegacyBlendHint_20(int32_t value)
	{
		___m_LegacyBlendHint_20 = value;
	}

	inline static int32_t get_offset_of_U3CUserIsDraggingU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A, ___U3CUserIsDraggingU3Ek__BackingField_24)); }
	inline bool get_U3CUserIsDraggingU3Ek__BackingField_24() const { return ___U3CUserIsDraggingU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CUserIsDraggingU3Ek__BackingField_24() { return &___U3CUserIsDraggingU3Ek__BackingField_24; }
	inline void set_U3CUserIsDraggingU3Ek__BackingField_24(bool value)
	{
		___U3CUserIsDraggingU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_m_State_25() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A, ___m_State_25)); }
	inline CameraState_t308F3A442112B7464D2B21A417D325662E3399B1  get_m_State_25() const { return ___m_State_25; }
	inline CameraState_t308F3A442112B7464D2B21A417D325662E3399B1 * get_address_of_m_State_25() { return &___m_State_25; }
	inline void set_m_State_25(CameraState_t308F3A442112B7464D2B21A417D325662E3399B1  value)
	{
		___m_State_25 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_25))->___mCustom0_11))->___m_Custom_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_25))->___mCustom1_12))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_25))->___mCustom2_13))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_25))->___mCustom3_14))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_State_25))->___m_CustomOverflow_15), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_ComponentPipeline_26() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A, ___m_ComponentPipeline_26)); }
	inline CinemachineComponentBaseU5BU5D_t7DC13D28344CFE98AF56DA46BCDF936DA49CDA6E* get_m_ComponentPipeline_26() const { return ___m_ComponentPipeline_26; }
	inline CinemachineComponentBaseU5BU5D_t7DC13D28344CFE98AF56DA46BCDF936DA49CDA6E** get_address_of_m_ComponentPipeline_26() { return &___m_ComponentPipeline_26; }
	inline void set_m_ComponentPipeline_26(CinemachineComponentBaseU5BU5D_t7DC13D28344CFE98AF56DA46BCDF936DA49CDA6E* value)
	{
		___m_ComponentPipeline_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComponentPipeline_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_ComponentOwner_27() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A, ___m_ComponentOwner_27)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_ComponentOwner_27() const { return ___m_ComponentOwner_27; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_ComponentOwner_27() { return &___m_ComponentOwner_27; }
	inline void set_m_ComponentOwner_27(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_ComponentOwner_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComponentOwner_27), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedLookAtTarget_28() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A, ___mCachedLookAtTarget_28)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mCachedLookAtTarget_28() const { return ___mCachedLookAtTarget_28; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mCachedLookAtTarget_28() { return &___mCachedLookAtTarget_28; }
	inline void set_mCachedLookAtTarget_28(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mCachedLookAtTarget_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedLookAtTarget_28), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedLookAtTargetVcam_29() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A, ___mCachedLookAtTargetVcam_29)); }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * get_mCachedLookAtTargetVcam_29() const { return ___mCachedLookAtTargetVcam_29; }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD ** get_address_of_mCachedLookAtTargetVcam_29() { return &___mCachedLookAtTargetVcam_29; }
	inline void set_mCachedLookAtTargetVcam_29(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * value)
	{
		___mCachedLookAtTargetVcam_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedLookAtTargetVcam_29), (void*)value);
	}
};

struct CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A_StaticFields
{
public:
	// Cinemachine.CinemachineVirtualCamera_CreatePipelineDelegate Cinemachine.CinemachineVirtualCamera::CreatePipelineOverride
	CreatePipelineDelegate_tDFD47CB61DD39569C0C372085A3461C5CAF5D269 * ___CreatePipelineOverride_22;
	// Cinemachine.CinemachineVirtualCamera_DestroyPipelineDelegate Cinemachine.CinemachineVirtualCamera::DestroyPipelineOverride
	DestroyPipelineDelegate_t9942B8E054A78C0E7A3FCAAAC212E7CBC72D1CC1 * ___DestroyPipelineOverride_23;

public:
	inline static int32_t get_offset_of_CreatePipelineOverride_22() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A_StaticFields, ___CreatePipelineOverride_22)); }
	inline CreatePipelineDelegate_tDFD47CB61DD39569C0C372085A3461C5CAF5D269 * get_CreatePipelineOverride_22() const { return ___CreatePipelineOverride_22; }
	inline CreatePipelineDelegate_tDFD47CB61DD39569C0C372085A3461C5CAF5D269 ** get_address_of_CreatePipelineOverride_22() { return &___CreatePipelineOverride_22; }
	inline void set_CreatePipelineOverride_22(CreatePipelineDelegate_tDFD47CB61DD39569C0C372085A3461C5CAF5D269 * value)
	{
		___CreatePipelineOverride_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CreatePipelineOverride_22), (void*)value);
	}

	inline static int32_t get_offset_of_DestroyPipelineOverride_23() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A_StaticFields, ___DestroyPipelineOverride_23)); }
	inline DestroyPipelineDelegate_t9942B8E054A78C0E7A3FCAAAC212E7CBC72D1CC1 * get_DestroyPipelineOverride_23() const { return ___DestroyPipelineOverride_23; }
	inline DestroyPipelineDelegate_t9942B8E054A78C0E7A3FCAAAC212E7CBC72D1CC1 ** get_address_of_DestroyPipelineOverride_23() { return &___DestroyPipelineOverride_23; }
	inline void set_DestroyPipelineOverride_23(DestroyPipelineDelegate_t9942B8E054A78C0E7A3FCAAAC212E7CBC72D1CC1 * value)
	{
		___DestroyPipelineOverride_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DestroyPipelineOverride_23), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * m_Items[1];

public:
	inline GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Object>()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared_inline (const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// !!0 Cinemachine.CinemachineVirtualCamera::AddCinemachineComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * CinemachineVirtualCamera_AddCinemachineComponent_TisRuntimeObject_mB55B75CFCEAA144E3328B236969280772A3499A8_gshared (CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * __this, const RuntimeMethod* method);
// !!0 Cinemachine.CinemachineVirtualCamera::GetCinemachineComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * CinemachineVirtualCamera_GetCinemachineComponent_TisRuntimeObject_mD77CC3D47FFD72E5468109D8D7BA38DEA00A0B0C_gshared (CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_m06B65DD1CC6DFB48DB4DEE2CC185788D42837D30_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m352D452C728667C9C76C942525CDE26444568ECD_gshared (RuntimeObject * ___original0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation2, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);

// !!0 UnityEngine.Component::GetComponent<Cinemachine.CinemachineVirtualCameraBase>()
inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * Component_GetComponent_TisCinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD_m24A679371AC104E31BD5057C9CC036DE1983342F (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKey_m46AA83E14F9C3A75E06FE0A8C55740D47B2DB784 (int32_t ___key0, const RuntimeMethod* method);
// System.Int32 Cinemachine.CinemachineVirtualCameraBase::get_Priority()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t CinemachineVirtualCameraBase_get_Priority_m720C0CC7978B5E520E08B21B8F1E639A17243683_inline (CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * __this, const RuntimeMethod* method);
// System.Void Cinemachine.CinemachineVirtualCameraBase::set_Priority(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void CinemachineVirtualCameraBase_set_Priority_m0B66572EE643EE753EFCA3F5D22E34D6AF77A98F_inline (CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxisRaw_mC68301A9D93702F0C393E45C6337348062EACE21 (String_t* ___axisName0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void BejucoController::Flip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BejucoController_Flip_m8C71F2D07618D312308C169CF4F4D3D59DEDA419 (BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___v0, const RuntimeMethod* method);
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LayerMask_op_Implicit_m2AFFC7F931005437E8F356C953F439829AF4CFA5 (LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___mask0, const RuntimeMethod* method);
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapCircle(UnityEngine.Vector2,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * Physics2D_OverlapCircle_m627FB9EE641A74B942877F57DD2FED656FDA5DC9 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___point0, float ___radius1, int32_t ___layerMask2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___exists0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetButtonDown_m1E80BAC5CCBE9E0151491B8F8F5FFD6AB050BBF0 (String_t* ___buttonName0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_up_mC4548731D5E7C71164D18C390A1AC32501DAE441 (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, float ___d1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody2D_AddForce_m09E1A2E24DABA5BBC613E35772AE2C1C35C6E40C (Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___force0, int32_t ___mode1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8 (const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83 (Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetTrigger(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetTrigger_m68D29B7FA54C2F230F5AD780D462612B18E74245 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, String_t* ___name0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Rigidbody2D_get_velocity_m5ABF36BDF90FD7308BE608667B9E8F3DA5A207F1 (Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Vector2_op_Equality_m0E86E1B1038DDB8554A8A0D58729A7788D989588 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lhs0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rhs1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, String_t* ___name0, bool ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetFloat_mE4C29F6980EBBBD954637721E6E13A0BE2B13C43 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, String_t* ___name0, float ___value1, const RuntimeMethod* method);
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  Animator_GetCurrentAnimatorStateInfo_mBE5ED0D60A6F5CD0EDD40AF1494098D4E7BF84F2 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, int32_t ___layerIndex0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AnimatorStateInfo_IsTag_m4E330F31FDC7A13E1B93F3ED09A7AEBF3F43A2D6 (AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 * __this, String_t* ___tag0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8 (const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m09F51D8BDECFD2E8C618498EF7377029B669030D (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, float ___t1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___v0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Translate_m0F354939D5084DDFF6B9902E62D3DCA7A3B53EA3 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___translation0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_Lerp_mD37EF718F1BAC65A7416655F0BC902CE76559C46 (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___a0, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_color_m0729526C86891ADD11611CD13A7A18B851355580 (SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905 (const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC (int32_t ___key0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_red_m5562DD438931CF0D1FBBBB29BF7F8B752AF38957 (const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_yellow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_yellow_mC8BD62CCC364EA5FC4273D4C2E116D0E2DE135AE (const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_cyan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_cyan_m4E9C84C7E1003311C2D4BDB281F2D11DF5F7FDE2 (const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_green()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_green_mD53D8F980E92A0755759FBB2981E3DDEFCD084C0 (const RuntimeMethod* method);
// System.Void Cinemachine.CinemachineVirtualCameraBase::MoveToTopOfPrioritySubqueue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineVirtualCameraBase_MoveToTopOfPrioritySubqueue_m045F215463AB3848C6BA93C43B0D7B2667F69838 (CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * __this, const RuntimeMethod* method);
// System.Void Cinemachine.Examples.ActivateCameraWithDistance::SwitchCam(Cinemachine.CinemachineVirtualCameraBase)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActivateCameraWithDistance_SwitchCam_m5736882F44A433FF71B32E13605739D7B7B13A85 (ActivateCameraWithDistance_tC54EDA46FFCAA5DE57637DE63FE5FBB65E0AE247 * __this, CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___vcam0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA (const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Cinemachine.CinemachineBrain>()
inline CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB * Component_GetComponent_TisCinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB_m1AAD5BCEF9186B5DDC28BFB469CB07416D89C563 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBrain::get_ActiveVirtualCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CinemachineBrain_get_ActiveVirtualCamera_m7723C396AE81CA2882CE6FF3C335147E9D6233BF (CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB * __this, const RuntimeMethod* method);
// System.String Cinemachine.CinemachineVirtualCameraBase::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CinemachineVirtualCameraBase_get_Name_m4596F7C91EA5A47577FC2A9EBDB78CE6A503B66A (CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m6454498C755B9A2C964875927FB557CA9E75D387 (String_t* ___axisName0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp_m033DD894F89E6DCCDAFC580091053059C86A4507 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method);
// System.Single UnityEngine.Animator::GetFloat(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Animator_GetFloat_m1C8B853CA82031CE4775FBDCDEDDB3C9E702695F (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_SmoothDamp_m170F84B677B5059B5FBF9259F12EC1C469F8B454 (float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Vector2_op_Inequality_mC16161C640C89D98A00800924F83FF09FD7C100E (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lhs0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rhs1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_up_m3E443F6EB278D547946E80D77065A871BEEEE282 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forward0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upwards1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Quaternion_get_eulerAngles_mF8ABA8EB77CD682017E92F0F457374E54BC943F9 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_eulerAngles_mF2D798FA8B18F7A1A0C4A2198329ADBAF07E37CA (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Euler_m55C96FCD397CC69109261572710608D12A4CBD2B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___euler0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Slerp_m56DE173C3520C83DF3F1C6EDFA82FF88A2C9E756 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___a0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839 (float ___d0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Lerp_m749B3988EE2EF387CC9BFB76C81B7465A7534E27 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___a0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___b1, float ___t2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyUp(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyUp_m5345ECFA25B7AC99D6D4223DA23BB9FB991B7193 (int32_t ___key0, const RuntimeMethod* method);
// System.Boolean Cinemachine.Examples.CharacterMovement2D::isGrounded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CharacterMovement2D_isGrounded_m9181E8749ED948212A8D0A8E4631D625944BDD70 (CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_velocity_m8D129E88E62AD02AB81CFC8BE694C4A5A2B2B380 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_down_m3F76A48E5B7C82B35EE047375538AFD91A305F55 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m0583CCAA9E2F3BD031F12FA080837E9A48EEC16D (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, float ___maxDistance2, const RuntimeMethod* method);
// System.Void Cinemachine.Examples.ExampleHelpWindow/<>c__DisplayClass4_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0__ctor_m0B4D84D4DCE8F38EF3771D4A27665027FB4BC4EC (U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013 * __this, const RuntimeMethod* method);
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * GUI_get_skin_mF6804BE33CFD74FF0665C1185AC7B5C1687AE866 (const RuntimeMethod* method);
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_label()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * GUISkin_get_label_mC5C9D4CD377D7F5BB970B01E665EE077565AFF72 (GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GUIContent::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUIContent__ctor_m8EE36BB9048C2A279E7EAD258D63F2A1BDEA8A81 (GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * __this, String_t* ___text0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.GUIStyle::CalcSize(UnityEngine.GUIContent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GUIStyle_CalcSize_m122C915B2050F60D120BDDDBD84433F26EC21E9F (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * __this, GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___content0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3 (const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Min_mCF9BE0E9CAC9F18D207692BB2DAC7F3E1D4E1CB7 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150 (const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method);
// System.Void UnityEngine.GUI/WindowFunction::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WindowFunction__ctor_m216C357C45DF9A8ABE74056B8BDB1B7F94EE2D81 (WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0[] System.Array::Empty<UnityEngine.GUILayoutOption>()
inline GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66_inline (const RuntimeMethod* method)
{
	return ((  GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* (*) (const RuntimeMethod*))Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared_inline)(method);
}
// UnityEngine.Rect UnityEngine.GUILayout::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String,UnityEngine.GUILayoutOption[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t35B976DE901B5423C11705E156938EA27AB402CE  GUILayout_Window_m25EBB2AE5252AE09E50F1E3DB3B1073A4CEA3205 (int32_t ___id0, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___screenRect1, WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100 * ___func2, String_t* ___text3, GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* ___options4, const RuntimeMethod* method);
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_box()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * GUISkin_get_box_m7305FA20CDC5A31518AE753AEF7B9075F6242B11 (GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUILayout_BeginVertical_mEB1D15C4BE7F3FF88E50D55A89C44825CE81DFE7 (GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___style0, GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* ___options1, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUILayoutOption[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUILayout_Label_m0DD571F45BDDDCB45E9D195AB77F3D7050A2A030 (String_t* ___text0, GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* ___options1, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::EndVertical()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUILayout_EndVertical_m7AAD929902547940A6076D23277B7255B96584FE (const RuntimeMethod* method);
// System.Boolean UnityEngine.GUILayout::Button(System.String,UnityEngine.GUILayoutOption[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GUILayout_Button_mACAF3D25298F91F12A312DB687F53258DB0B9918 (String_t* ___text0, GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* ___options1, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Cinemachine.Examples.ExampleHelpWindow::DrawWindow(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExampleHelpWindow_DrawWindow_m42568AC283611EA80B03CAAEE7FDD2C7025ED016 (ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378 * __this, int32_t ___id0, float ___maxWidth1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Playables.PlayableDirector>()
inline PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * Component_GetComponent_TisPlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2_m8EC8AA73D38CD6B2F7B6282C65E9C51946E3EC26 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// System.Boolean UnityEngine.GameObject::CompareTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_CompareTag_mF66519C9DAE4CC8873C36A04C3CAF7DDEC3C7EFE (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* ___tag0, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableDirector::set_time(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayableDirector_set_time_mB9484F170C9751EF11842AB232AE04497DD47E96 (PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * __this, double ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableDirector::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayableDirector_Stop_m7F0EE5192D66EBBE4A530BE98CF5E788A65D9776 (PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableDirector::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayableDirector_Play_m10B6B7392DBAFF0C77513FDE4A805AD0B54DF919 (PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Cinemachine.CinemachineMixingCamera>()
inline CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE * Component_GetComponent_TisCinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE_m42F414DD742DED90FDEB557D0B6DB4DD06E22328 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m129DEF8A66683189ED44B21496135824743EF617_gshared)(__this, method);
}
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD (String_t* ___name0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<Cinemachine.CinemachineBrain>()
inline CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB * GameObject_AddComponent_TisCinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB_m8C98E6FD74E6A35274E48DD803B5900F1F291263 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared)(__this, method);
}
// System.Void UnityEngine.GameObject::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* ___name0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<Cinemachine.CinemachineVirtualCamera>()
inline CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * GameObject_AddComponent_TisCinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A_m7CF035809E82878DBC2A8ED4C21104BED3421143 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared)(__this, method);
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// !!0 Cinemachine.CinemachineVirtualCamera::AddCinemachineComponent<Cinemachine.CinemachineComposer>()
inline CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8 * CinemachineVirtualCamera_AddCinemachineComponent_TisCinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8_m345831406FEA1F9C352CFC582909DC8CF50E2FED (CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * __this, const RuntimeMethod* method)
{
	return ((  CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8 * (*) (CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A *, const RuntimeMethod*))CinemachineVirtualCamera_AddCinemachineComponent_TisRuntimeObject_mB55B75CFCEAA144E3328B236969280772A3499A8_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::AddComponent<Cinemachine.CinemachineFreeLook>()
inline CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * GameObject_AddComponent_TisCinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152_mC55F5EE96B665C6498ED43145950B14496235AD3 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared)(__this, method);
}
// Cinemachine.CinemachineVirtualCamera Cinemachine.CinemachineFreeLook::GetRig(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * CinemachineFreeLook_GetRig_m27DEABB380ACF8A98E8C289F0EAE9E123315A470 (CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * __this, int32_t ___i0, const RuntimeMethod* method);
// !!0 Cinemachine.CinemachineVirtualCamera::GetCinemachineComponent<Cinemachine.CinemachineComposer>()
inline CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8 * CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8_mA61ACEA9FC833694511132FC13E74FB7D7E14FD7 (CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * __this, const RuntimeMethod* method)
{
	return ((  CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8 * (*) (CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A *, const RuntimeMethod*))CinemachineVirtualCamera_GetCinemachineComponent_TisRuntimeObject_mD77CC3D47FFD72E5468109D8D7BA38DEA00A0B0C_gshared)(__this, method);
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_realtimeSinceStartup_mCA1086EC9DFCF135F77BC46D3B7127711EA3DE03 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_mAA0C9ED5A3D1589C1C8AA22636543528DB353CFB (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, bool ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<Weapon>()
inline Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * Component_GetComponentInChildren_TisWeapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9_m0B9B6D77DDFC65C7C8FD16BD86CAD38A1B44861C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_m06B65DD1CC6DFB48DB4DEE2CC185788D42837D30_gshared)(__this, method);
}
// System.Void EnemyPatrol::UpdateTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyPatrol_UpdateTarget_mBF1D57F2C57B1B19634CFBEE098102D0F2C58819 (EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * MonoBehaviour_StartCoroutine_m590A0A7F161D579C18E678B4C5ACCE77B1B318DD (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, String_t* ___methodName0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void EnemyPatrol/<PatrolToTarget>d__11::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPatrolToTargetU3Ed__11__ctor_m55E022613EF7239E767DC35B5D591AFF3361E960 (U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void Weapon::Shoot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Weapon_Shoot_mF5B36E6C4428C1E8858F42526F5EEC7C7AF9D4E3 (Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC (RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559 (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * __this, float ___seconds0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33 (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m4F397BCC6697902B40033E61129D4EA6FE93570F (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___original0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation2, const RuntimeMethod* method)
{
	return ((  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 , Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m352D452C728667C9C76C942525CDE26444568ECD_gshared)(___original0, ___position1, ___rotation2, method);
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9 (String_t* ___sceneName0, const RuntimeMethod* method);
// System.Void PlayerController::Flip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_Flip_m829B4A4842ABD034CB56BCB1966D3C462A9D6AD4 (PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, float ___d1, const RuntimeMethod* method);
// System.Single Cinemachine.Utility.Damper::Damp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Damper_Damp_mA4664317F3E046AEA0F530941E50442262BD5F3B (float ___initial0, float ___dampTime1, float ___deltaTime2, const RuntimeMethod* method);
// System.Void System.Action::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_forward_m0BE1E88B86049ADA39391C3ACED2314A624BC67F (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 Cinemachine.Utility.UnityVectorExtensions::ProjectOntoPlane(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  UnityVectorExtensions_ProjectOntoPlane_m4011502AB317965F1142F74D9B5F17C1EBB639EA (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vector0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___planeNormal1, const RuntimeMethod* method);
// System.Single UnityEngine.SphereCollider::get_radius()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094 (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForce_mC8140D90B806634A733624F671C45AD7CDBEDB38 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___force0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Transform_Find_m673797B6329C2669A543904532ABA1680DA4EAD1 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, String_t* ___n0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<Bullet>()
inline Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059 * GameObject_GetComponent_TisBullet_t1B228DBA3982FDB21DE04329BDC49915421B9059_mC44AB386177196D8110F3E6D8F0E814D44438271 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_left()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_left_mDD2A2AFDC4B0C1C876093F3E9C405579287FF4F8 (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_right_mB4BD67462D579461853F297C0DE85D81E07E911E (const RuntimeMethod* method);
// System.Boolean UnityEngine.Component::CompareTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Component_CompareTag_mD074608D7FEC96A53C57A456BA613EE01C31D4B7 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, String_t* ___tag0, const RuntimeMethod* method);
// System.Void ZuroStreet/<Attack>d__17::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAttackU3Ed__17__ctor_mBEBE2F6358D4DA86E68B52BB5F59AB1E5CFC7318 (U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_StopCoroutine_mC2C29B39556BFC68657F27343602BCC57AA6604F (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, String_t* ___methodName0, const RuntimeMethod* method);
// System.Void ZuroStreet::AttackMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZuroStreet_AttackMovement_mFC008314CD71D1B0D3D15FD3ED1C5F3F778F78AA (ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ActivateOnKeypress::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActivateOnKeypress_Start_mC9C71BD760F97DBC0F686569A13EA91AA8F643C6 (ActivateOnKeypress_t3EBED552416BF21197122C141DC4B936BB6DE1FB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ActivateOnKeypress_Start_mC9C71BD760F97DBC0F686569A13EA91AA8F643C6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// vcam = GetComponent<Cinemachine.CinemachineVirtualCameraBase>();
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_0 = Component_GetComponent_TisCinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD_m24A679371AC104E31BD5057C9CC036DE1983342F(__this, /*hidden argument*/Component_GetComponent_TisCinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD_m24A679371AC104E31BD5057C9CC036DE1983342F_RuntimeMethod_var);
		__this->set_vcam_7(L_0);
		// }
		return;
	}
}
// System.Void ActivateOnKeypress::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActivateOnKeypress_Update_m7C06B9EF28D5420A29C17EC1D8D6C8239D489312 (ActivateOnKeypress_t3EBED552416BF21197122C141DC4B936BB6DE1FB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ActivateOnKeypress_Update_m7C06B9EF28D5420A29C17EC1D8D6C8239D489312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (vcam != null)
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_0 = __this->get_vcam_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006b;
		}
	}
	{
		// if (Input.GetKey(ActivationKey))
		int32_t L_2 = __this->get_ActivationKey_4();
		bool L_3 = Input_GetKey_m46AA83E14F9C3A75E06FE0A8C55740D47B2DB784(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0044;
		}
	}
	{
		// if (!boosted)
		bool L_4 = __this->get_boosted_8();
		if (L_4)
		{
			goto IL_006b;
		}
	}
	{
		// vcam.Priority += PriorityBoostAmount;
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_5 = __this->get_vcam_7();
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_6 = L_5;
		NullCheck(L_6);
		int32_t L_7 = CinemachineVirtualCameraBase_get_Priority_m720C0CC7978B5E520E08B21B8F1E639A17243683_inline(L_6, /*hidden argument*/NULL);
		int32_t L_8 = __this->get_PriorityBoostAmount_5();
		NullCheck(L_6);
		CinemachineVirtualCameraBase_set_Priority_m0B66572EE643EE753EFCA3F5D22E34D6AF77A98F_inline(L_6, ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8)), /*hidden argument*/NULL);
		// boosted = true;
		__this->set_boosted_8((bool)1);
		// }
		goto IL_006b;
	}

IL_0044:
	{
		// else if (boosted)
		bool L_9 = __this->get_boosted_8();
		if (!L_9)
		{
			goto IL_006b;
		}
	}
	{
		// vcam.Priority -= PriorityBoostAmount;
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_10 = __this->get_vcam_7();
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_11 = L_10;
		NullCheck(L_11);
		int32_t L_12 = CinemachineVirtualCameraBase_get_Priority_m720C0CC7978B5E520E08B21B8F1E639A17243683_inline(L_11, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_PriorityBoostAmount_5();
		NullCheck(L_11);
		CinemachineVirtualCameraBase_set_Priority_m0B66572EE643EE753EFCA3F5D22E34D6AF77A98F_inline(L_11, ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)L_13)), /*hidden argument*/NULL);
		// boosted = false;
		__this->set_boosted_8((bool)0);
	}

IL_006b:
	{
		// if (Reticle != null)
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = __this->get_Reticle_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_14, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008a;
		}
	}
	{
		// Reticle.SetActive(boosted);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_16 = __this->get_Reticle_6();
		bool L_17 = __this->get_boosted_8();
		NullCheck(L_16);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_16, L_17, /*hidden argument*/NULL);
	}

IL_008a:
	{
		// }
		return;
	}
}
// System.Void ActivateOnKeypress::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActivateOnKeypress__ctor_m75534AB7F9820B5C53A22B8244CE4BD87F1A5538 (ActivateOnKeypress_t3EBED552416BF21197122C141DC4B936BB6DE1FB * __this, const RuntimeMethod* method)
{
	{
		// public KeyCode ActivationKey = KeyCode.LeftControl;
		__this->set_ActivationKey_4(((int32_t)306));
		// public int PriorityBoostAmount = 10;
		__this->set_PriorityBoostAmount_5(((int32_t)10));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BejucoController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BejucoController_Awake_m795D66747ED8AB6D70E60E609312039CFDF739F9 (BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BejucoController_Awake_m795D66747ED8AB6D70E60E609312039CFDF739F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _rigidbody = GetComponent<Rigidbody2D>();
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_0 = Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625_RuntimeMethod_var);
		__this->set__rigidbody_11(L_0);
		// _animator = GetComponent<Animator>();
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_1 = Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719(__this, /*hidden argument*/Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719_RuntimeMethod_var);
		__this->set__animator_12(L_1);
		// }
		return;
	}
}
// System.Void BejucoController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BejucoController_Start_m18761ACCAC20C2B734018F62F7D81D05EB760330 (BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void BejucoController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BejucoController_Update_m2785342A43F73D03858EB28BCC340AA0CFBABD74 (BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BejucoController_Update_m2785342A43F73D03858EB28BCC340AA0CFBABD74_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// if (_isAttacking == false)
		bool L_0 = __this->get__isAttacking_16();
		if (L_0)
		{
			goto IL_0056;
		}
	}
	{
		// float horizontalInput = Input.GetAxisRaw("Horizontal");
		float L_1 = Input_GetAxisRaw_mC68301A9D93702F0C393E45C6337348062EACE21(_stringLiteral4F57A1CE99E68A7B05C42D0A7EA0070EAFABD31C, /*hidden argument*/NULL);
		V_0 = L_1;
		// _movement = new Vector2(1f, 0f);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_2), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->set__movement_13(L_2);
		// if (horizontalInput < 0f && _facingRight == true)
		float L_3 = V_0;
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_0040;
		}
	}
	{
		bool L_4 = __this->get__facingRight_14();
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		// Flip();
		BejucoController_Flip_m8C71F2D07618D312308C169CF4F4D3D59DEDA419(__this, /*hidden argument*/NULL);
		// }
		goto IL_0056;
	}

IL_0040:
	{
		// else if (horizontalInput > 0f && _facingRight == false)
		float L_5 = V_0;
		if ((!(((float)L_5) > ((float)(0.0f)))))
		{
			goto IL_0056;
		}
	}
	{
		bool L_6 = __this->get__facingRight_14();
		if (L_6)
		{
			goto IL_0056;
		}
	}
	{
		// Flip();
		BejucoController_Flip_m8C71F2D07618D312308C169CF4F4D3D59DEDA419(__this, /*hidden argument*/NULL);
	}

IL_0056:
	{
		// _isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = __this->get_groundCheck_8();
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_8, /*hidden argument*/NULL);
		float L_10 = __this->get_groundCheckRadius_10();
		LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  L_11 = __this->get_groundLayer_9();
		int32_t L_12 = LayerMask_op_Implicit_m2AFFC7F931005437E8F356C953F439829AF4CFA5(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_tB21970F986016656D66D2922594F336E1EE7D5C7_il2cpp_TypeInfo_var);
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_13 = Physics2D_OverlapCircle_m627FB9EE641A74B942877F57DD2FED656FDA5DC9(L_9, L_10, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_13, /*hidden argument*/NULL);
		__this->set__isGrounded_15(L_14);
		// if (Input.GetButtonDown("Jump") && _isGrounded == true && _isAttacking == false)
		bool L_15 = Input_GetButtonDown_m1E80BAC5CCBE9E0151491B8F8F5FFD6AB050BBF0(_stringLiteral1EBA140FDD9C6860A1730C408E3064AA417CA2A3, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00bf;
		}
	}
	{
		bool L_16 = __this->get__isGrounded_15();
		if (!L_16)
		{
			goto IL_00bf;
		}
	}
	{
		bool L_17 = __this->get__isAttacking_16();
		if (L_17)
		{
			goto IL_00bf;
		}
	}
	{
		// _rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_18 = __this->get__rigidbody_11();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_19 = Vector2_get_up_mC4548731D5E7C71164D18C390A1AC32501DAE441(/*hidden argument*/NULL);
		float L_20 = __this->get_jumpForce_7();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_21 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		Rigidbody2D_AddForce_m09E1A2E24DABA5BBC613E35772AE2C1C35C6E40C(L_18, L_21, 1, /*hidden argument*/NULL);
	}

IL_00bf:
	{
		// if (Input.GetButtonDown("Fire1") && _isGrounded == true && _isAttacking == false)
		bool L_22 = Input_GetButtonDown_m1E80BAC5CCBE9E0151491B8F8F5FFD6AB050BBF0(_stringLiteral0CB715D89D6589E699639FF0716A2BE52C44EEEE, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0106;
		}
	}
	{
		bool L_23 = __this->get__isGrounded_15();
		if (!L_23)
		{
			goto IL_0106;
		}
	}
	{
		bool L_24 = __this->get__isAttacking_16();
		if (L_24)
		{
			goto IL_0106;
		}
	}
	{
		// _movement = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_25 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		__this->set__movement_13(L_25);
		// _rigidbody.velocity = Vector2.zero;
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_26 = __this->get__rigidbody_11();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_27 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		NullCheck(L_26);
		Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83(L_26, L_27, /*hidden argument*/NULL);
		// _animator.SetTrigger("Attack");
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_28 = __this->get__animator_12();
		NullCheck(L_28);
		Animator_SetTrigger_m68D29B7FA54C2F230F5AD780D462612B18E74245(L_28, _stringLiteral1B81F28B5AFBFCAAF5253D26B7DBA4028A92A860, /*hidden argument*/NULL);
	}

IL_0106:
	{
		// }
		return;
	}
}
// System.Void BejucoController::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BejucoController_FixedUpdate_m748A21EE1003312AC103B07902A1412984896853 (BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// if (_isAttacking == false)
		bool L_0 = __this->get__isAttacking_16();
		if (L_0)
		{
			goto IL_0041;
		}
	}
	{
		// float horizontalVelocity = _movement.normalized.x * speed;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_1 = __this->get_address_of__movement_13();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_x_0();
		float L_4 = __this->get_speed_6();
		V_0 = ((float)il2cpp_codegen_multiply((float)L_3, (float)L_4));
		// _rigidbody.velocity = new Vector2(horizontalVelocity, _rigidbody.velocity.y);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_5 = __this->get__rigidbody_11();
		float L_6 = V_0;
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_7 = __this->get__rigidbody_11();
		NullCheck(L_7);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = Rigidbody2D_get_velocity_m5ABF36BDF90FD7308BE608667B9E8F3DA5A207F1(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_10), L_6, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83(L_5, L_10, /*hidden argument*/NULL);
	}

IL_0041:
	{
		// }
		return;
	}
}
// System.Void BejucoController::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BejucoController_LateUpdate_m27F7BD50A67CFBA6B218859321D78044D1D9D939 (BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BejucoController_LateUpdate_m27F7BD50A67CFBA6B218859321D78044D1D9D939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// _animator.SetBool("Idle", _movement == Vector2.zero);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = __this->get__animator_12();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = __this->get__movement_13();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		bool L_3 = Vector2_op_Equality_m0E86E1B1038DDB8554A8A0D58729A7788D989588(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E(L_0, _stringLiteralCC1EBDD04E76A4B8BAFFB29A22FA00446D382B18, L_3, /*hidden argument*/NULL);
		// _animator.SetBool("IsGrounded", _isGrounded);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_4 = __this->get__animator_12();
		bool L_5 = __this->get__isGrounded_15();
		NullCheck(L_4);
		Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E(L_4, _stringLiteralCC8D7979F8F6B6CAC74FEF89F920AA835E515A9E, L_5, /*hidden argument*/NULL);
		// _animator.SetFloat("VerticalVelocity", _rigidbody.velocity.y);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_6 = __this->get__animator_12();
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_7 = __this->get__rigidbody_11();
		NullCheck(L_7);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = Rigidbody2D_get_velocity_m5ABF36BDF90FD7308BE608667B9E8F3DA5A207F1(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_y_1();
		NullCheck(L_6);
		Animator_SetFloat_mE4C29F6980EBBBD954637721E6E13A0BE2B13C43(L_6, _stringLiteral42BC272E41ECB68FE544AA26C6F3AC14C8E21034, L_9, /*hidden argument*/NULL);
		// if (_animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_10 = __this->get__animator_12();
		NullCheck(L_10);
		AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  L_11 = Animator_GetCurrentAnimatorStateInfo_mBE5ED0D60A6F5CD0EDD40AF1494098D4E7BF84F2(L_10, 0, /*hidden argument*/NULL);
		V_0 = L_11;
		bool L_12 = AnimatorStateInfo_IsTag_m4E330F31FDC7A13E1B93F3ED09A7AEBF3F43A2D6((AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 *)(&V_0), _stringLiteral1B81F28B5AFBFCAAF5253D26B7DBA4028A92A860, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007a;
		}
	}
	{
		// _isAttacking = true;
		__this->set__isAttacking_16((bool)1);
		// } else
		goto IL_0081;
	}

IL_007a:
	{
		// _isAttacking = false;
		__this->set__isAttacking_16((bool)0);
	}

IL_0081:
	{
		// if (_animator.GetCurrentAnimatorStateInfo(0).IsTag("Idle"))
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_13 = __this->get__animator_12();
		NullCheck(L_13);
		AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  L_14 = Animator_GetCurrentAnimatorStateInfo_mBE5ED0D60A6F5CD0EDD40AF1494098D4E7BF84F2(L_13, 0, /*hidden argument*/NULL);
		V_0 = L_14;
		bool L_15 = AnimatorStateInfo_IsTag_m4E330F31FDC7A13E1B93F3ED09A7AEBF3F43A2D6((AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 *)(&V_0), _stringLiteralCC1EBDD04E76A4B8BAFFB29A22FA00446D382B18, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00cd;
		}
	}
	{
		// _longIdleTimer += Time.deltaTime;
		float L_16 = __this->get__longIdleTimer_5();
		float L_17 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		__this->set__longIdleTimer_5(((float)il2cpp_codegen_add((float)L_16, (float)L_17)));
		// if (_longIdleTimer >= longIdleTime)
		float L_18 = __this->get__longIdleTimer_5();
		float L_19 = __this->get_longIdleTime_4();
		if ((!(((float)L_18) >= ((float)L_19))))
		{
			goto IL_00d8;
		}
	}
	{
		// _animator.SetTrigger("LongIdle");
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_20 = __this->get__animator_12();
		NullCheck(L_20);
		Animator_SetTrigger_m68D29B7FA54C2F230F5AD780D462612B18E74245(L_20, _stringLiteral1A12A77F72140F5565E3C5D869CCFF5749CC4266, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00cd:
	{
		// _longIdleTimer = 0f;
		__this->set__longIdleTimer_5((0.0f));
	}

IL_00d8:
	{
		// }
		return;
	}
}
// System.Void BejucoController::Flip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BejucoController_Flip_m8C71F2D07618D312308C169CF4F4D3D59DEDA419 (BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// _facingRight = !_facingRight;
		bool L_0 = __this->get__facingRight_14();
		__this->set__facingRight_14((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		// float localScaleX = transform.localScale.x;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_x_2();
		V_0 = L_3;
		// localScaleX = localScaleX * -1f;
		float L_4 = V_0;
		V_0 = ((float)il2cpp_codegen_multiply((float)L_4, (float)(-1.0f)));
		// transform.localScale = new Vector3(localScaleX, transform.localScale.y, transform.localScale.z);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		float L_6 = V_0;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_y_3();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_10, /*hidden argument*/NULL);
		float L_12 = L_11.get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_13), L_6, L_9, L_12, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_5, L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BejucoController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BejucoController__ctor_m8363B965BACF48CFDCB7C01427DB40E694A5482D (BejucoController_tD4C8714C5BD5E71314A353E8E443728BDE0838C4 * __this, const RuntimeMethod* method)
{
	{
		// public float longIdleTime = 5f;
		__this->set_longIdleTime_4((5.0f));
		// public float speed = 2.5f;
		__this->set_speed_6((2.5f));
		// public float jumpForce = 2.5f;
		__this->set_jumpForce_7((2.5f));
		// private bool _facingRight = true;
		__this->set__facingRight_14((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Bullet::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Bullet_Awake_m37FD11C4734664815AF4EEF3C3814AEBCF88B6CD (Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bullet_Awake_m37FD11C4734664815AF4EEF3C3814AEBCF88B6CD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _renderer = GetComponent<SpriteRenderer>();
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_0 = Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693_RuntimeMethod_var);
		__this->set__renderer_10(L_0);
		// }
		return;
	}
}
// System.Void Bullet::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Bullet_Start_m0C6AD7FCC791ADF08593B040294D8B7329617D04 (Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bullet_Start_m0C6AD7FCC791ADF08593B040294D8B7329617D04_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _startingTime = Time.time;
		float L_0 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		__this->set__startingTime_9(L_0);
		// Destroy(this.gameObject, livingTime);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		float L_2 = __this->get_livingTime_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m09F51D8BDECFD2E8C618498EF7377029B669030D(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Bullet::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Bullet_Update_mEDD99D9C6CBBEBCD7C211792A42CBF1D965EB80E (Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bullet_Update_mEDD99D9C6CBBEBCD7C211792A42CBF1D965EB80E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		// Vector2 movement = direction.normalized * speed * Time.deltaTime;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_0 = __this->get_address_of_direction_5();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)L_0, /*hidden argument*/NULL);
		float L_2 = __this->get_speed_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_1, L_2, /*hidden argument*/NULL);
		float L_4 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// transform.Translate(movement);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_Translate_m0F354939D5084DDFF6B9902E62D3DCA7A3B53EA3(L_6, L_8, /*hidden argument*/NULL);
		// float _timeSinceStarted = Time.time - _startingTime;
		float L_9 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		float L_10 = __this->get__startingTime_9();
		// float _percentageCompleted = _timeSinceStarted / livingTime;
		float L_11 = __this->get_livingTime_6();
		V_1 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_9, (float)L_10))/(float)L_11));
		// _renderer.color = Color.Lerp(initialColor, finalColor, _percentageCompleted);
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_12 = __this->get__renderer_10();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_13 = __this->get_initialColor_7();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_14 = __this->get_finalColor_8();
		float L_15 = V_1;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_16 = Color_Lerp_mD37EF718F1BAC65A7416655F0BC902CE76559C46(L_13, L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_12);
		SpriteRenderer_set_color_m0729526C86891ADD11611CD13A7A18B851355580(L_12, L_16, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Bullet::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Bullet__ctor_mFBA1E7297C133AE06ADA2EF2BA04567AD213A9D4 (Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059 * __this, const RuntimeMethod* method)
{
	{
		// public float speed = 2f;
		__this->set_speed_4((2.0f));
		// public float livingTime = 3f;
		__this->set_livingTime_6((3.0f));
		// public Color initialColor = Color.white;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_0 = Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905(/*hidden argument*/NULL);
		__this->set_initialColor_7(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ChangeColor::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeColor_Awake_m4A2850FB998ED3DB1E294F7107D4C72787F81977 (ChangeColor_t11FE2BD8AD1F8A8AFB6E43B3737410A3C60AFC79 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChangeColor_Awake_m4A2850FB998ED3DB1E294F7107D4C72787F81977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("I'm attached as a component of an object in the Scene!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral6AF8D9EBBAF3BFBF8030673F1331CC3F10B2692C, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ChangeColor::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeColor_Start_m6D47E8F30AEAB75D8E034168972BEC8C4D0A6869 (ChangeColor_t11FE2BD8AD1F8A8AFB6E43B3737410A3C60AFC79 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChangeColor_Start_m6D47E8F30AEAB75D8E034168972BEC8C4D0A6869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("You have just pressed PLAY BUTTON!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteralF9502D1E54CC48D9377D8E7EC34EB756CF17D1B1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ChangeColor::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeColor_Update_m0F7BC07040DEF2BFDAFF5F448E1A8D2F92534890 (ChangeColor_t11FE2BD8AD1F8A8AFB6E43B3737410A3C60AFC79 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChangeColor_Update_m0F7BC07040DEF2BFDAFF5F448E1A8D2F92534890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetKeyDown(KeyCode.R)) {
		bool L_0 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(((int32_t)114), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		// this.GetComponent<SpriteRenderer>().color = Color.red;
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_1 = Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693_RuntimeMethod_var);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_2 = Color_get_red_m5562DD438931CF0D1FBBBB29BF7F8B752AF38957(/*hidden argument*/NULL);
		NullCheck(L_1);
		SpriteRenderer_set_color_m0729526C86891ADD11611CD13A7A18B851355580(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// if (Input.GetKeyDown(KeyCode.Y)) {
		bool L_3 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(((int32_t)121), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		// this.GetComponent<SpriteRenderer>().color = Color.yellow;
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_4 = Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693_RuntimeMethod_var);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_5 = Color_get_yellow_mC8BD62CCC364EA5FC4273D4C2E116D0E2DE135AE(/*hidden argument*/NULL);
		NullCheck(L_4);
		SpriteRenderer_set_color_m0729526C86891ADD11611CD13A7A18B851355580(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0032:
	{
		// if (Input.GetKeyDown(KeyCode.C)) {
		bool L_6 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(((int32_t)99), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004b;
		}
	}
	{
		// this.GetComponent<SpriteRenderer>().color = Color.cyan;
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_7 = Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693_RuntimeMethod_var);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_8 = Color_get_cyan_m4E9C84C7E1003311C2D4BDB281F2D11DF5F7FDE2(/*hidden argument*/NULL);
		NullCheck(L_7);
		SpriteRenderer_set_color_m0729526C86891ADD11611CD13A7A18B851355580(L_7, L_8, /*hidden argument*/NULL);
	}

IL_004b:
	{
		// if (Input.GetKeyDown(KeyCode.G)) {
		bool L_9 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(((int32_t)103), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0064;
		}
	}
	{
		// this.GetComponent<SpriteRenderer>().color = Color.green;
		SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * L_10 = Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F_mCC209A7A6A8D6878F0CB813ED5722A59C2262693_RuntimeMethod_var);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_11 = Color_get_green_mD53D8F980E92A0755759FBB2981E3DDEFCD084C0(/*hidden argument*/NULL);
		NullCheck(L_10);
		SpriteRenderer_set_color_m0729526C86891ADD11611CD13A7A18B851355580(L_10, L_11, /*hidden argument*/NULL);
	}

IL_0064:
	{
		// }
		return;
	}
}
// System.Void ChangeColor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeColor__ctor_m6611480670D0461C94C966D87A0AB2BF4F992A55 (ChangeColor_t11FE2BD8AD1F8A8AFB6E43B3737410A3C60AFC79 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cinemachine.Examples.ActivateCamOnPlay::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActivateCamOnPlay_Start_m7CC36D9EF8A6D1AE712C67BF62FD15EF7392E65A (ActivateCamOnPlay_tCD29CD3A1DC809CC23DF4D6BC74437DB1EB8E7CE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ActivateCamOnPlay_Start_m7CC36D9EF8A6D1AE712C67BF62FD15EF7392E65A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (vcam)
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_0 = __this->get_vcam_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		// vcam.MoveToTopOfPrioritySubqueue();
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_2 = __this->get_vcam_4();
		NullCheck(L_2);
		CinemachineVirtualCameraBase_MoveToTopOfPrioritySubqueue_m045F215463AB3848C6BA93C43B0D7B2667F69838(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.ActivateCamOnPlay::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActivateCamOnPlay__ctor_mE0248D3F391298D30C06500EAF092416093327B8 (ActivateCamOnPlay_tCD29CD3A1DC809CC23DF4D6BC74437DB1EB8E7CE * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cinemachine.Examples.ActivateCameraWithDistance::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActivateCameraWithDistance_Start_m435B7DEF0A2173F42274135E588903E9127C9995 (ActivateCameraWithDistance_tC54EDA46FFCAA5DE57637DE63FE5FBB65E0AE247 * __this, const RuntimeMethod* method)
{
	{
		// SwitchCam(initialActiveCam);
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_0 = __this->get_initialActiveCam_6();
		ActivateCameraWithDistance_SwitchCam_m5736882F44A433FF71B32E13605739D7B7B13A85(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.ActivateCameraWithDistance::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActivateCameraWithDistance_Update_m61B1F1909206E419F849BE447BC51F5197118976 (ActivateCameraWithDistance_tC54EDA46FFCAA5DE57637DE63FE5FBB65E0AE247 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ActivateCameraWithDistance_Update_m61B1F1909206E419F849BE447BC51F5197118976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (objectToCheck && switchCameraTo)
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_objectToCheck_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005b;
		}
	}
	{
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_2 = __this->get_switchCameraTo_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005b;
		}
	}
	{
		// if (Vector3.Distance(transform.position, objectToCheck.transform.position) < distanceToObject)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_4, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = __this->get_objectToCheck_4();
		NullCheck(L_6);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		float L_9 = Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7(L_5, L_8, /*hidden argument*/NULL);
		float L_10 = __this->get_distanceToObject_5();
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_004f;
		}
	}
	{
		// SwitchCam(switchCameraTo);
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_11 = __this->get_switchCameraTo_7();
		ActivateCameraWithDistance_SwitchCam_m5736882F44A433FF71B32E13605739D7B7B13A85(__this, L_11, /*hidden argument*/NULL);
		// }
		return;
	}

IL_004f:
	{
		// SwitchCam(initialActiveCam);
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_12 = __this->get_initialActiveCam_6();
		ActivateCameraWithDistance_SwitchCam_m5736882F44A433FF71B32E13605739D7B7B13A85(__this, L_12, /*hidden argument*/NULL);
	}

IL_005b:
	{
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.ActivateCameraWithDistance::SwitchCam(Cinemachine.CinemachineVirtualCameraBase)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActivateCameraWithDistance_SwitchCam_m5736882F44A433FF71B32E13605739D7B7B13A85 (ActivateCameraWithDistance_tC54EDA46FFCAA5DE57637DE63FE5FBB65E0AE247 * __this, CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___vcam0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ActivateCameraWithDistance_SwitchCam_m5736882F44A433FF71B32E13605739D7B7B13A85_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Camera.main.GetComponent<CinemachineBrain>().ActiveVirtualCamera.Name != vcam.Name)
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		NullCheck(L_0);
		CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB * L_1 = Component_GetComponent_TisCinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB_m1AAD5BCEF9186B5DDC28BFB469CB07416D89C563(L_0, /*hidden argument*/Component_GetComponent_TisCinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB_m1AAD5BCEF9186B5DDC28BFB469CB07416D89C563_RuntimeMethod_var);
		NullCheck(L_1);
		RuntimeObject* L_2 = CinemachineBrain_get_ActiveVirtualCamera_m7723C396AE81CA2882CE6FF3C335147E9D6233BF(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Cinemachine.ICinemachineCamera::get_Name() */, ICinemachineCamera_t89A591897FD47A23EAD91D2B3DA94B7EB3B322FD_il2cpp_TypeInfo_var, L_2);
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_4 = ___vcam0;
		NullCheck(L_4);
		String_t* L_5 = CinemachineVirtualCameraBase_get_Name_m4596F7C91EA5A47577FC2A9EBDB78CE6A503B66A(L_4, /*hidden argument*/NULL);
		bool L_6 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(L_3, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0027;
		}
	}
	{
		// vcam.MoveToTopOfPrioritySubqueue();
		CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * L_7 = ___vcam0;
		NullCheck(L_7);
		CinemachineVirtualCameraBase_MoveToTopOfPrioritySubqueue_m045F215463AB3848C6BA93C43B0D7B2667F69838(L_7, /*hidden argument*/NULL);
	}

IL_0027:
	{
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.ActivateCameraWithDistance::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActivateCameraWithDistance__ctor_m0AFD25A3578C0BF0E63A2A36215B71F2792006BF (ActivateCameraWithDistance_tC54EDA46FFCAA5DE57637DE63FE5FBB65E0AE247 * __this, const RuntimeMethod* method)
{
	{
		// public float distanceToObject = 15f;
		__this->set_distanceToObject_5((15.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cinemachine.Examples.CharacterMovement::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterMovement_Start_mEA069E540139D2289966584FC5067053E21BD85D (CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMovement_Start_mEA069E540139D2289966584FC5067053E21BD85D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// anim = GetComponent<Animator>();
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719(__this, /*hidden argument*/Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719_RuntimeMethod_var);
		__this->set_anim_13(L_0);
		// mainCamera = Camera.main;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_1 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		__this->set_mainCamera_17(L_1);
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.CharacterMovement::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterMovement_FixedUpdate_m8615F2B05324419DBB1CEB9506DBDFD2F0F66A0F (CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMovement_FixedUpdate_m8615F2B05324419DBB1CEB9506DBDFD2F0F66A0F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_3;
	memset((&V_3), 0, sizeof(V_3));
	CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3 * G_B9_0 = NULL;
	CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3 * G_B8_0 = NULL;
	CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3 * G_B11_0 = NULL;
	CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3 * G_B10_0 = NULL;
	int32_t G_B12_0 = 0;
	CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3 * G_B12_1 = NULL;
	{
		// input.x = Input.GetAxis("Horizontal");
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_0 = __this->get_address_of_input_15();
		float L_1 = Input_GetAxis_m6454498C755B9A2C964875927FB557CA9E75D387(_stringLiteral4F57A1CE99E68A7B05C42D0A7EA0070EAFABD31C, /*hidden argument*/NULL);
		L_0->set_x_0(L_1);
		// input.y = Input.GetAxis("Vertical");
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_2 = __this->get_address_of_input_15();
		float L_3 = Input_GetAxis_m6454498C755B9A2C964875927FB557CA9E75D387(_stringLiteral4B937CC841D82F8936CEF1EFB88708AB5B0F1EE5, /*hidden argument*/NULL);
		L_2->set_y_1(L_3);
		// if (useCharacterForward)
		bool L_4 = __this->get_useCharacterForward_4();
		if (!L_4)
		{
			goto IL_0056;
		}
	}
	{
		// speed = Mathf.Abs(input.x) + input.y;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_5 = __this->get_address_of_input_15();
		float L_6 = L_5->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_7 = fabsf(L_6);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_8 = __this->get_address_of_input_15();
		float L_9 = L_8->get_y_1();
		__this->set_speed_10(((float)il2cpp_codegen_add((float)L_7, (float)L_9)));
		goto IL_007d;
	}

IL_0056:
	{
		// speed = Mathf.Abs(input.x) + Mathf.Abs(input.y);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_10 = __this->get_address_of_input_15();
		float L_11 = L_10->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_12 = fabsf(L_11);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_13 = __this->get_address_of_input_15();
		float L_14 = L_13->get_y_1();
		float L_15 = fabsf(L_14);
		__this->set_speed_10(((float)il2cpp_codegen_add((float)L_12, (float)L_15)));
	}

IL_007d:
	{
		// speed = Mathf.Clamp(speed, 0f, 1f);
		float L_16 = __this->get_speed_10();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_17 = Mathf_Clamp_m033DD894F89E6DCCDAFC580091053059C86A4507(L_16, (0.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_speed_10(L_17);
		// speed = Mathf.SmoothDamp(anim.GetFloat("Speed"), speed, ref velocity, 0.1f);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_18 = __this->get_anim_13();
		NullCheck(L_18);
		float L_19 = Animator_GetFloat_m1C8B853CA82031CE4775FBDCDEDDB3C9E702695F(L_18, _stringLiteral2D2CB022BC3D26BD1407C4AA787D5E46E1AD4C3B, /*hidden argument*/NULL);
		float L_20 = __this->get_speed_10();
		float* L_21 = __this->get_address_of_velocity_18();
		float L_22 = Mathf_SmoothDamp_m170F84B677B5059B5FBF9259F12EC1C469F8B454(L_19, L_20, (float*)L_21, (0.1f), /*hidden argument*/NULL);
		__this->set_speed_10(L_22);
		// anim.SetFloat("Speed", speed);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_23 = __this->get_anim_13();
		float L_24 = __this->get_speed_10();
		NullCheck(L_23);
		Animator_SetFloat_mE4C29F6980EBBBD954637721E6E13A0BE2B13C43(L_23, _stringLiteral2D2CB022BC3D26BD1407C4AA787D5E46E1AD4C3B, L_24, /*hidden argument*/NULL);
		// if (input.y < 0f && useCharacterForward)
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_25 = __this->get_address_of_input_15();
		float L_26 = L_25->get_y_1();
		if ((!(((float)L_26) < ((float)(0.0f)))))
		{
			goto IL_0107;
		}
	}
	{
		bool L_27 = __this->get_useCharacterForward_4();
		if (!L_27)
		{
			goto IL_0107;
		}
	}
	{
		// direction = input.y;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_28 = __this->get_address_of_input_15();
		float L_29 = L_28->get_y_1();
		__this->set_direction_11(L_29);
		goto IL_0112;
	}

IL_0107:
	{
		// direction = 0f;
		__this->set_direction_11((0.0f));
	}

IL_0112:
	{
		// anim.SetFloat("Direction", direction);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_30 = __this->get_anim_13();
		float L_31 = __this->get_direction_11();
		NullCheck(L_30);
		Animator_SetFloat_mE4C29F6980EBBBD954637721E6E13A0BE2B13C43(L_30, _stringLiteralFD8E45BAC7D354790EC0A82D8A61341BF8ACD055, L_31, /*hidden argument*/NULL);
		// isSprinting = ((Input.GetKey(sprintJoystick) || Input.GetKey(sprintKeyboard)) && input != Vector2.zero && direction >= 0f);
		int32_t L_32 = __this->get_sprintJoystick_7();
		bool L_33 = Input_GetKey_m46AA83E14F9C3A75E06FE0A8C55740D47B2DB784(L_32, /*hidden argument*/NULL);
		G_B8_0 = __this;
		if (L_33)
		{
			G_B9_0 = __this;
			goto IL_0143;
		}
	}
	{
		int32_t L_34 = __this->get_sprintKeyboard_8();
		bool L_35 = Input_GetKey_m46AA83E14F9C3A75E06FE0A8C55740D47B2DB784(L_34, /*hidden argument*/NULL);
		G_B9_0 = G_B8_0;
		if (!L_35)
		{
			G_B11_0 = G_B8_0;
			goto IL_0167;
		}
	}

IL_0143:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_36 = __this->get_input_15();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_37 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		bool L_38 = Vector2_op_Inequality_mC16161C640C89D98A00800924F83FF09FD7C100E(L_36, L_37, /*hidden argument*/NULL);
		G_B10_0 = G_B9_0;
		if (!L_38)
		{
			G_B11_0 = G_B9_0;
			goto IL_0167;
		}
	}
	{
		float L_39 = __this->get_direction_11();
		G_B12_0 = ((((int32_t)((!(((float)L_39) >= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		G_B12_1 = G_B10_0;
		goto IL_0168;
	}

IL_0167:
	{
		G_B12_0 = 0;
		G_B12_1 = G_B11_0;
	}

IL_0168:
	{
		NullCheck(G_B12_1);
		G_B12_1->set_isSprinting_12((bool)G_B12_0);
		// anim.SetBool("isSprinting", isSprinting);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_40 = __this->get_anim_13();
		bool L_41 = __this->get_isSprinting_12();
		NullCheck(L_40);
		Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E(L_40, _stringLiteral4E148B88EC5B524891DE6D50B0A87CF7A4A9BB8E, L_41, /*hidden argument*/NULL);
		// UpdateTargetDirection();
		VirtActionInvoker0::Invoke(4 /* System.Void Cinemachine.Examples.CharacterMovement::UpdateTargetDirection() */, __this);
		// if (input != Vector2.zero && targetDirection.magnitude > 0.1f)
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_42 = __this->get_input_15();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_43 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		bool L_44 = Vector2_op_Inequality_mC16161C640C89D98A00800924F83FF09FD7C100E(L_42, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0270;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_45 = __this->get_address_of_targetDirection_14();
		float L_46 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)L_45, /*hidden argument*/NULL);
		if ((!(((float)L_46) > ((float)(0.1f)))))
		{
			goto IL_0270;
		}
	}
	{
		// Vector3 lookDirection = targetDirection.normalized;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_47 = __this->get_address_of_targetDirection_14();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_48 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)L_47, /*hidden argument*/NULL);
		V_0 = L_48;
		// freeRotation = Quaternion.LookRotation(lookDirection, transform.up);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_49 = V_0;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_50 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_50);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_51 = Transform_get_up_m3E443F6EB278D547946E80D77065A871BEEEE282(L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_52 = Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672(L_49, L_51, /*hidden argument*/NULL);
		__this->set_freeRotation_16(L_52);
		// var diferenceRotation = freeRotation.eulerAngles.y - transform.eulerAngles.y;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * L_53 = __this->get_address_of_freeRotation_16();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_54 = Quaternion_get_eulerAngles_mF8ABA8EB77CD682017E92F0F457374E54BC943F9((Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *)L_53, /*hidden argument*/NULL);
		float L_55 = L_54.get_y_3();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_56 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_57 = Transform_get_eulerAngles_mF2D798FA8B18F7A1A0C4A2198329ADBAF07E37CA(L_56, /*hidden argument*/NULL);
		float L_58 = L_57.get_y_3();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_55, (float)L_58));
		// var eulerY = transform.eulerAngles.y;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_59 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_59);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_60 = Transform_get_eulerAngles_mF2D798FA8B18F7A1A0C4A2198329ADBAF07E37CA(L_59, /*hidden argument*/NULL);
		float L_61 = L_60.get_y_3();
		V_2 = L_61;
		// if (diferenceRotation < 0 || diferenceRotation > 0) eulerY = freeRotation.eulerAngles.y;
		float L_62 = V_1;
		if ((((float)L_62) < ((float)(0.0f))))
		{
			goto IL_0219;
		}
	}
	{
		float L_63 = V_1;
		if ((!(((float)L_63) > ((float)(0.0f)))))
		{
			goto IL_022a;
		}
	}

IL_0219:
	{
		// if (diferenceRotation < 0 || diferenceRotation > 0) eulerY = freeRotation.eulerAngles.y;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * L_64 = __this->get_address_of_freeRotation_16();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_65 = Quaternion_get_eulerAngles_mF8ABA8EB77CD682017E92F0F457374E54BC943F9((Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *)L_64, /*hidden argument*/NULL);
		float L_66 = L_65.get_y_3();
		V_2 = L_66;
	}

IL_022a:
	{
		// var euler = new Vector3(0, eulerY, 0);
		float L_67 = V_2;
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_3), (0.0f), L_67, (0.0f), /*hidden argument*/NULL);
		// transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(euler), turnSpeed * turnSpeedMultiplier * Time.deltaTime);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_68 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_69 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_69);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_70 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_69, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_71 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_72 = Quaternion_Euler_m55C96FCD397CC69109261572710608D12A4CBD2B(L_71, /*hidden argument*/NULL);
		float L_73 = __this->get_turnSpeed_6();
		float L_74 = __this->get_turnSpeedMultiplier_9();
		float L_75 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_76 = Quaternion_Slerp_m56DE173C3520C83DF3F1C6EDFA82FF88A2C9E756(L_70, L_72, ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_73, (float)L_74)), (float)L_75)), /*hidden argument*/NULL);
		NullCheck(L_68);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_68, L_76, /*hidden argument*/NULL);
	}

IL_0270:
	{
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.CharacterMovement::UpdateTargetDirection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterMovement_UpdateTargetDirection_m8571B65E5FCA3579F722687F2D53F0FCFF8AC214 (CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMovement_UpdateTargetDirection_m8571B65E5FCA3579F722687F2D53F0FCFF8AC214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		// if (!useCharacterForward)
		bool L_0 = __this->get_useCharacterForward_4();
		if (L_0)
		{
			goto IL_0079;
		}
	}
	{
		// turnSpeedMultiplier = 1f;
		__this->set_turnSpeedMultiplier_9((1.0f));
		// var forward = mainCamera.transform.TransformDirection(Vector3.forward);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_1 = __this->get_mainCamera_17();
		NullCheck(L_1);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D(/*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// forward.y = 0;
		(&V_0)->set_y_3((0.0f));
		// var right = mainCamera.transform.TransformDirection(Vector3.right);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_5 = __this->get_mainCamera_17();
		NullCheck(L_5);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_5, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5(/*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF(L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		// targetDirection = input.x * right + input.y * forward;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_9 = __this->get_address_of_input_15();
		float L_10 = L_9->get_x_0();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839(L_10, L_11, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_13 = __this->get_address_of_input_15();
		float L_14 = L_13->get_y_1();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839(L_14, L_15, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_12, L_16, /*hidden argument*/NULL);
		__this->set_targetDirection_14(L_17);
		// }
		return;
	}

IL_0079:
	{
		// turnSpeedMultiplier = 0.2f;
		__this->set_turnSpeedMultiplier_9((0.2f));
		// var forward = transform.TransformDirection(Vector3.forward);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D(/*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF(L_18, L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		// forward.y = 0;
		(&V_2)->set_y_3((0.0f));
		// var right = transform.TransformDirection(Vector3.right);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5(/*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF(L_21, L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		// targetDirection = input.x * right + Mathf.Abs(input.y) * forward;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_24 = __this->get_address_of_input_15();
		float L_25 = L_24->get_x_0();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839(L_25, L_26, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_28 = __this->get_address_of_input_15();
		float L_29 = L_28->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_30 = fabsf(L_29);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32 = Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839(L_30, L_31, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_33 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_27, L_32, /*hidden argument*/NULL);
		__this->set_targetDirection_14(L_33);
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.CharacterMovement::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterMovement__ctor_m6BF3D71AAF2FB1160BD26A58549F946488BDFE7B (CharacterMovement_t4177C0AF9ED16D0F106697E1A7968DE08CFD63D3 * __this, const RuntimeMethod* method)
{
	{
		// public float turnSpeed = 10f;
		__this->set_turnSpeed_6((10.0f));
		// public KeyCode sprintJoystick = KeyCode.JoystickButton2;
		__this->set_sprintJoystick_7(((int32_t)332));
		// public KeyCode sprintKeyboard = KeyCode.Space;
		__this->set_sprintKeyboard_8(((int32_t)32));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cinemachine.Examples.CharacterMovement2D::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterMovement2D_Start_m6C3CC414568A67E6FC1EF370C9E702A997416272 (CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMovement2D_Start_m6C3CC414568A67E6FC1EF370C9E702A997416272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// anim = GetComponent<Animator>();
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719(__this, /*hidden argument*/Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719_RuntimeMethod_var);
		__this->set_anim_13(L_0);
		// rigbody = GetComponent<Rigidbody>();
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_1 = Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C_RuntimeMethod_var);
		__this->set_rigbody_18(L_1);
		// targetrot = transform.rotation;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_3 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_2, /*hidden argument*/NULL);
		__this->set_targetrot_17(L_3);
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.CharacterMovement2D::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterMovement2D_FixedUpdate_m0706C4622DDDFB6F424C3DC6B4F1DC144A0605C8 (CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMovement2D_FixedUpdate_m0706C4622DDDFB6F424C3DC6B4F1DC144A0605C8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// input.x = Input.GetAxis("Horizontal");
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_0 = __this->get_address_of_input_14();
		float L_1 = Input_GetAxis_m6454498C755B9A2C964875927FB557CA9E75D387(_stringLiteral4F57A1CE99E68A7B05C42D0A7EA0070EAFABD31C, /*hidden argument*/NULL);
		L_0->set_x_0(L_1);
		// if ((input.x < 0f && !headingleft) || (input.x > 0f && headingleft))
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_2 = __this->get_address_of_input_14();
		float L_3 = L_2->get_x_0();
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_002f;
		}
	}
	{
		bool L_4 = __this->get_headingleft_16();
		if (!L_4)
		{
			goto IL_0049;
		}
	}

IL_002f:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_5 = __this->get_address_of_input_14();
		float L_6 = L_5->get_x_0();
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_00b0;
		}
	}
	{
		bool L_7 = __this->get_headingleft_16();
		if (!L_7)
		{
			goto IL_00b0;
		}
	}

IL_0049:
	{
		// if (input.x < 0f) targetrot = Quaternion.Euler(0, 270, 0);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_8 = __this->get_address_of_input_14();
		float L_9 = L_8->get_x_0();
		if ((!(((float)L_9) < ((float)(0.0f)))))
		{
			goto IL_0075;
		}
	}
	{
		// if (input.x < 0f) targetrot = Quaternion.Euler(0, 270, 0);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_10 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((0.0f), (270.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_targetrot_17(L_10);
	}

IL_0075:
	{
		// if (input.x > 0f) targetrot = Quaternion.Euler(0, 90, 0);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_11 = __this->get_address_of_input_14();
		float L_12 = L_11->get_x_0();
		if ((!(((float)L_12) > ((float)(0.0f)))))
		{
			goto IL_00a1;
		}
	}
	{
		// if (input.x > 0f) targetrot = Quaternion.Euler(0, 90, 0);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_13 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((0.0f), (90.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_targetrot_17(L_13);
	}

IL_00a1:
	{
		// headingleft = !headingleft;
		bool L_14 = __this->get_headingleft_16();
		__this->set_headingleft_16((bool)((((int32_t)L_14) == ((int32_t)0))? 1 : 0));
	}

IL_00b0:
	{
		// transform.rotation = Quaternion.Lerp(transform.rotation, targetrot, Time.deltaTime * 20f);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_16 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_17 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_16, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_18 = __this->get_targetrot_17();
		float L_19 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_20 = Quaternion_Lerp_m749B3988EE2EF387CC9BFB76C81B7465A7534E27(L_17, L_18, ((float)il2cpp_codegen_multiply((float)L_19, (float)(20.0f))), /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_15, L_20, /*hidden argument*/NULL);
		// speed = Mathf.Abs(input.x);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_21 = __this->get_address_of_input_14();
		float L_22 = L_21->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_23 = fabsf(L_22);
		__this->set_speed_11(L_23);
		// speed = Mathf.SmoothDamp(anim.GetFloat("Speed"), speed, ref velocity, 0.1f);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_24 = __this->get_anim_13();
		NullCheck(L_24);
		float L_25 = Animator_GetFloat_m1C8B853CA82031CE4775FBDCDEDDB3C9E702695F(L_24, _stringLiteral2D2CB022BC3D26BD1407C4AA787D5E46E1AD4C3B, /*hidden argument*/NULL);
		float L_26 = __this->get_speed_11();
		float* L_27 = __this->get_address_of_velocity_15();
		float L_28 = Mathf_SmoothDamp_m170F84B677B5059B5FBF9259F12EC1C469F8B454(L_25, L_26, (float*)L_27, (0.1f), /*hidden argument*/NULL);
		__this->set_speed_11(L_28);
		// anim.SetFloat("Speed", speed);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_29 = __this->get_anim_13();
		float L_30 = __this->get_speed_11();
		NullCheck(L_29);
		Animator_SetFloat_mE4C29F6980EBBBD954637721E6E13A0BE2B13C43(L_29, _stringLiteral2D2CB022BC3D26BD1407C4AA787D5E46E1AD4C3B, L_30, /*hidden argument*/NULL);
		// if ((Input.GetKeyDown(sprintJoystick) || Input.GetKeyDown(sprintKeyboard))&& input != Vector2.zero) isSprinting = true;
		int32_t L_31 = __this->get_sprintJoystick_4();
		bool L_32 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(L_31, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_014e;
		}
	}
	{
		int32_t L_33 = __this->get_sprintKeyboard_6();
		bool L_34 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_0167;
		}
	}

IL_014e:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_35 = __this->get_input_14();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_36 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		bool L_37 = Vector2_op_Inequality_mC16161C640C89D98A00800924F83FF09FD7C100E(L_35, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0167;
		}
	}
	{
		// if ((Input.GetKeyDown(sprintJoystick) || Input.GetKeyDown(sprintKeyboard))&& input != Vector2.zero) isSprinting = true;
		__this->set_isSprinting_12((bool)1);
	}

IL_0167:
	{
		// if ((Input.GetKeyUp(sprintJoystick) || Input.GetKeyUp(sprintKeyboard))|| input == Vector2.zero) isSprinting = false;
		int32_t L_38 = __this->get_sprintJoystick_4();
		bool L_39 = Input_GetKeyUp_m5345ECFA25B7AC99D6D4223DA23BB9FB991B7193(L_38, /*hidden argument*/NULL);
		if (L_39)
		{
			goto IL_0193;
		}
	}
	{
		int32_t L_40 = __this->get_sprintKeyboard_6();
		bool L_41 = Input_GetKeyUp_m5345ECFA25B7AC99D6D4223DA23BB9FB991B7193(L_40, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_0193;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_42 = __this->get_input_14();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_43 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		bool L_44 = Vector2_op_Equality_m0E86E1B1038DDB8554A8A0D58729A7788D989588(L_42, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_019a;
		}
	}

IL_0193:
	{
		// if ((Input.GetKeyUp(sprintJoystick) || Input.GetKeyUp(sprintKeyboard))|| input == Vector2.zero) isSprinting = false;
		__this->set_isSprinting_12((bool)0);
	}

IL_019a:
	{
		// anim.SetBool("isSprinting", isSprinting);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_45 = __this->get_anim_13();
		bool L_46 = __this->get_isSprinting_12();
		NullCheck(L_45);
		Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E(L_45, _stringLiteral4E148B88EC5B524891DE6D50B0A87CF7A4A9BB8E, L_46, /*hidden argument*/NULL);
		// if ((Input.GetKeyDown(jumpJoystick) || Input.GetKeyDown(jumpKeyboard)) && isGrounded())
		int32_t L_47 = __this->get_jumpJoystick_5();
		bool L_48 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(L_47, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_01ca;
		}
	}
	{
		int32_t L_49 = __this->get_jumpKeyboard_7();
		bool L_50 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_01f8;
		}
	}

IL_01ca:
	{
		bool L_51 = CharacterMovement2D_isGrounded_m9181E8749ED948212A8D0A8E4631D625944BDD70(__this, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_01f8;
		}
	}
	{
		// rigbody.velocity = new Vector3(input.x, jumpVelocity, 0f);
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_52 = __this->get_rigbody_18();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_53 = __this->get_address_of_input_14();
		float L_54 = L_53->get_x_0();
		float L_55 = __this->get_jumpVelocity_8();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_56;
		memset((&L_56), 0, sizeof(L_56));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_56), L_54, L_55, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_52);
		Rigidbody_set_velocity_m8D129E88E62AD02AB81CFC8BE694C4A5A2B2B380(L_52, L_56, /*hidden argument*/NULL);
	}

IL_01f8:
	{
		// }
		return;
	}
}
// System.Boolean Cinemachine.Examples.CharacterMovement2D::isGrounded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CharacterMovement2D_isGrounded_m9181E8749ED948212A8D0A8E4631D625944BDD70 (CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterMovement2D_isGrounded_m9181E8749ED948212A8D0A8E4631D625944BDD70_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (checkGroundForJump)
		bool L_0 = __this->get_checkGroundForJump_10();
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		// return Physics.Raycast(transform.position, Vector3.down, groundTolerance);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_get_down_m3F76A48E5B7C82B35EE047375538AFD91A305F55(/*hidden argument*/NULL);
		float L_4 = __this->get_groundTolerance_9();
		bool L_5 = Physics_Raycast_m0583CCAA9E2F3BD031F12FA080837E9A48EEC16D(L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0024:
	{
		// return true;
		return (bool)1;
	}
}
// System.Void Cinemachine.Examples.CharacterMovement2D::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterMovement2D__ctor_m405EFCAAAAE1436EA68509865F6A69B1FAAFBC9F (CharacterMovement2D_tF985AEBDEC46A6F54A92A92E196CB3D907693484 * __this, const RuntimeMethod* method)
{
	{
		// public KeyCode sprintJoystick = KeyCode.JoystickButton2;
		__this->set_sprintJoystick_4(((int32_t)332));
		// public KeyCode jumpJoystick = KeyCode.JoystickButton0;
		__this->set_jumpJoystick_5(((int32_t)330));
		// public KeyCode sprintKeyboard = KeyCode.LeftShift;
		__this->set_sprintKeyboard_6(((int32_t)304));
		// public KeyCode jumpKeyboard = KeyCode.Space;
		__this->set_jumpKeyboard_7(((int32_t)32));
		// public float jumpVelocity = 7f;
		__this->set_jumpVelocity_8((7.0f));
		// public float groundTolerance = 0.2f;
		__this->set_groundTolerance_9((0.2f));
		// public bool checkGroundForJump = true;
		__this->set_checkGroundForJump_10((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cinemachine.Examples.ExampleHelpWindow::OnGUI()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExampleHelpWindow_OnGUI_mDE54F4CCAA35ABCC00159E6C883AEC6DC3BE4A66 (ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExampleHelpWindow_OnGUI_mDE54F4CCAA35ABCC00159E6C883AEC6DC3BE4A66_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013 * V_0 = NULL;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_5;
	memset((&V_5), 0, sizeof(V_5));
	{
		// if (mShowingHelpWindow)
		bool L_0 = __this->get_mShowingHelpWindow_6();
		if (!L_0)
		{
			goto IL_00c5;
		}
	}
	{
		U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013 * L_1 = (U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass4_0__ctor_m0B4D84D4DCE8F38EF3771D4A27665027FB4BC4EC(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013 * L_2 = V_0;
		NullCheck(L_2);
		L_2->set_U3CU3E4__this_1(__this);
		// Vector2 size = GUI.skin.label.CalcSize(new GUIContent(m_Description));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3E5CBC6B113E392EBBE1453DEF2B7CD020F345AA_il2cpp_TypeInfo_var);
		GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * L_3 = GUI_get_skin_mF6804BE33CFD74FF0665C1185AC7B5C1687AE866(/*hidden argument*/NULL);
		NullCheck(L_3);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_4 = GUISkin_get_label_mC5C9D4CD377D7F5BB970B01E665EE077565AFF72(L_3, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_m_Description_5();
		GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * L_6 = (GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 *)il2cpp_codegen_object_new(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0_il2cpp_TypeInfo_var);
		GUIContent__ctor_m8EE36BB9048C2A279E7EAD258D63F2A1BDEA8A81(L_6, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = GUIStyle_CalcSize_m122C915B2050F60D120BDDDBD84433F26EC21E9F(L_4, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		// Vector2 halfSize = size * 0.5f;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_8, (0.5f), /*hidden argument*/NULL);
		V_2 = L_9;
		// float maxWidth = Mathf.Min(Screen.width - kPadding, size.x);
		U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013 * L_10 = V_0;
		int32_t L_11 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = V_1;
		float L_13 = L_12.get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_14 = Mathf_Min_mCF9BE0E9CAC9F18D207692BB2DAC7F3E1D4E1CB7(((float)il2cpp_codegen_subtract((float)(((float)((float)L_11))), (float)(40.0f))), L_13, /*hidden argument*/NULL);
		NullCheck(L_10);
		L_10->set_maxWidth_0(L_14);
		// float left = Screen.width * 0.5f - maxWidth * 0.5f;
		int32_t L_15 = Screen_get_width_m8ECCEF7FF17395D1237BC0193D7A6640A3FEEAD3(/*hidden argument*/NULL);
		U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013 * L_16 = V_0;
		NullCheck(L_16);
		float L_17 = L_16->get_maxWidth_0();
		V_3 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_15))), (float)(0.5f))), (float)((float)il2cpp_codegen_multiply((float)L_17, (float)(0.5f)))));
		// float top = Screen.height * 0.4f - halfSize.y;
		int32_t L_18 = Screen_get_height_mF5B64EBC4CDE0EAAA5713C1452ED2CE475F25150(/*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_19 = V_2;
		float L_20 = L_19.get_y_1();
		V_4 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_18))), (float)(0.4f))), (float)L_20));
		// Rect windowRect = new Rect(left, top, maxWidth, size.y);
		float L_21 = V_3;
		float L_22 = V_4;
		U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013 * L_23 = V_0;
		NullCheck(L_23);
		float L_24 = L_23->get_maxWidth_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_25 = V_1;
		float L_26 = L_25.get_y_1();
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_5), L_21, L_22, L_24, L_26, /*hidden argument*/NULL);
		// GUILayout.Window(400, windowRect, (id) => DrawWindow(id, maxWidth), m_Title);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_27 = V_5;
		U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013 * L_28 = V_0;
		WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100 * L_29 = (WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100 *)il2cpp_codegen_object_new(WindowFunction_t9AF05117863D95AA9F85D497A3B9B53216708100_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m216C357C45DF9A8ABE74056B8BDB1B7F94EE2D81(L_29, L_28, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass4_0_U3COnGUIU3Eb__0_m84E30E3962AC9F5BA3EB82DEF5ABDA2DDE85D6EE_RuntimeMethod_var), /*hidden argument*/NULL);
		String_t* L_30 = __this->get_m_Title_4();
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_31 = Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66_inline(/*hidden argument*/Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66_RuntimeMethod_var);
		GUILayout_Window_m25EBB2AE5252AE09E50F1E3DB3B1073A4CEA3205(((int32_t)400), L_27, L_29, L_30, L_31, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.ExampleHelpWindow::DrawWindow(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExampleHelpWindow_DrawWindow_m42568AC283611EA80B03CAAEE7FDD2C7025ED016 (ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378 * __this, int32_t ___id0, float ___maxWidth1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExampleHelpWindow_DrawWindow_m42568AC283611EA80B03CAAEE7FDD2C7025ED016_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GUILayout.BeginVertical(GUI.skin.box);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3E5CBC6B113E392EBBE1453DEF2B7CD020F345AA_il2cpp_TypeInfo_var);
		GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * L_0 = GUI_get_skin_mF6804BE33CFD74FF0665C1185AC7B5C1687AE866(/*hidden argument*/NULL);
		NullCheck(L_0);
		GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * L_1 = GUISkin_get_box_m7305FA20CDC5A31518AE753AEF7B9075F6242B11(L_0, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_2 = Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66_inline(/*hidden argument*/Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66_RuntimeMethod_var);
		GUILayout_BeginVertical_mEB1D15C4BE7F3FF88E50D55A89C44825CE81DFE7(L_1, L_2, /*hidden argument*/NULL);
		// GUILayout.Label(m_Description);
		String_t* L_3 = __this->get_m_Description_5();
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_4 = Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66_inline(/*hidden argument*/Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66_RuntimeMethod_var);
		GUILayout_Label_m0DD571F45BDDDCB45E9D195AB77F3D7050A2A030(L_3, L_4, /*hidden argument*/NULL);
		// GUILayout.EndVertical();
		GUILayout_EndVertical_m7AAD929902547940A6076D23277B7255B96584FE(/*hidden argument*/NULL);
		// if (GUILayout.Button("Got it!"))
		GUILayoutOptionU5BU5D_tAB2AD0A365DBD2277A04E397AE8E1430A022AF1B* L_5 = Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66_inline(/*hidden argument*/Array_Empty_TisGUILayoutOption_t27A0221AC2F6F53E7B89310FD19F51C565D835A6_m88E55351140AB39BE4B8A54049DBD85D467A8C66_RuntimeMethod_var);
		bool L_6 = GUILayout_Button_mACAF3D25298F91F12A312DB687F53258DB0B9918(_stringLiteral8297BB17C6AA1537D802E8AB86DCD018FFB7B6D0, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		// mShowingHelpWindow = false;
		__this->set_mShowingHelpWindow_6((bool)0);
	}

IL_0041:
	{
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.ExampleHelpWindow::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExampleHelpWindow__ctor_mC9FD7C32341606F660C2046D479AC4DB3A030857 (ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378 * __this, const RuntimeMethod* method)
{
	{
		// private bool mShowingHelpWindow = true;
		__this->set_mShowingHelpWindow_6((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cinemachine.Examples.ExampleHelpWindow_<>c__DisplayClass4_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0__ctor_m0B4D84D4DCE8F38EF3771D4A27665027FB4BC4EC (U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cinemachine.Examples.ExampleHelpWindow_<>c__DisplayClass4_0::<OnGUI>b__0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0_U3COnGUIU3Eb__0_m84E30E3962AC9F5BA3EB82DEF5ABDA2DDE85D6EE (U3CU3Ec__DisplayClass4_0_tBE9A66873605788750C12E6854F01E1A70364013 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	{
		// GUILayout.Window(400, windowRect, (id) => DrawWindow(id, maxWidth), m_Title);
		ExampleHelpWindow_t5CF61E67AC07A728256E0EF7170016AE35285378 * L_0 = __this->get_U3CU3E4__this_1();
		int32_t L_1 = ___id0;
		float L_2 = __this->get_maxWidth_0();
		NullCheck(L_0);
		ExampleHelpWindow_DrawWindow_m42568AC283611EA80B03CAAEE7FDD2C7025ED016(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cinemachine.Examples.GenericTrigger::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GenericTrigger_Start_m9795906FC841CC4B2236CF8888A8306E1C94BBA4 (GenericTrigger_t45676BF61EE6D8259E0E288AC627092B9E962CA7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericTrigger_Start_m9795906FC841CC4B2236CF8888A8306E1C94BBA4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// timeline = GetComponent<PlayableDirector>();
		PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * L_0 = Component_GetComponent_TisPlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2_m8EC8AA73D38CD6B2F7B6282C65E9C51946E3EC26(__this, /*hidden argument*/Component_GetComponent_TisPlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2_m8EC8AA73D38CD6B2F7B6282C65E9C51946E3EC26_RuntimeMethod_var);
		__this->set_timeline_4(L_0);
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.GenericTrigger::OnTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GenericTrigger_OnTriggerExit_m9B5A0ECBA53C54E0C12DDFA32144509226B0E54B (GenericTrigger_t45676BF61EE6D8259E0E288AC627092B9E962CA7 * __this, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___c0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericTrigger_OnTriggerExit_m9B5A0ECBA53C54E0C12DDFA32144509226B0E54B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (c.gameObject.CompareTag("Player"))
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_0 = ___c0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = GameObject_CompareTag_mF66519C9DAE4CC8873C36A04C3CAF7DDEC3C7EFE(L_1, _stringLiteralE53407CFE1A5156B9F0D1EED3BAB5EF3AE75CFD8, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		// timeline.time = 27;
		PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * L_3 = __this->get_timeline_4();
		NullCheck(L_3);
		PlayableDirector_set_time_mB9484F170C9751EF11842AB232AE04497DD47E96(L_3, (27.0), /*hidden argument*/NULL);
	}

IL_0026:
	{
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.GenericTrigger::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GenericTrigger_OnTriggerEnter_mFD4689405DEA14F65E2F8554BF3D571CD73C8E2C (GenericTrigger_t45676BF61EE6D8259E0E288AC627092B9E962CA7 * __this, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___c0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericTrigger_OnTriggerEnter_mFD4689405DEA14F65E2F8554BF3D571CD73C8E2C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (c.gameObject.CompareTag("Player"))
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_0 = ___c0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = GameObject_CompareTag_mF66519C9DAE4CC8873C36A04C3CAF7DDEC3C7EFE(L_1, _stringLiteralE53407CFE1A5156B9F0D1EED3BAB5EF3AE75CFD8, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		// timeline.Stop(); // Make sure the timeline is stopped before starting it
		PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * L_3 = __this->get_timeline_4();
		NullCheck(L_3);
		PlayableDirector_Stop_m7F0EE5192D66EBBE4A530BE98CF5E788A65D9776(L_3, /*hidden argument*/NULL);
		// timeline.Play();
		PlayableDirector_tAE05A3F910B71FA6BC7EED28C26C35945308D2B2 * L_4 = __this->get_timeline_4();
		NullCheck(L_4);
		PlayableDirector_Play_m10B6B7392DBAFF0C77513FDE4A805AD0B54DF919(L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.GenericTrigger::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GenericTrigger__ctor_mC9F645021E384E3112CEA2992E787D4CE5F12E88 (GenericTrigger_t45676BF61EE6D8259E0E288AC627092B9E962CA7 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cinemachine.Examples.MixingCameraBlend::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MixingCameraBlend_Start_mE6154055F56E65873E236AB81E1C7A53780F90D6 (MixingCameraBlend_tCB7BBB9AC892ECC6DE8154D1F397E7F7B359D8E7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MixingCameraBlend_Start_mE6154055F56E65873E236AB81E1C7A53780F90D6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (followTarget)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = __this->get_followTarget_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		// vcam = GetComponent<CinemachineMixingCamera>();
		CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE * L_2 = Component_GetComponent_TisCinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE_m42F414DD742DED90FDEB557D0B6DB4DD06E22328(__this, /*hidden argument*/Component_GetComponent_TisCinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE_m42F414DD742DED90FDEB557D0B6DB4DD06E22328_RuntimeMethod_var);
		__this->set_vcam_7(L_2);
		// vcam.m_Weight0 = initialBottomWeight;
		CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE * L_3 = __this->get_vcam_7();
		float L_4 = __this->get_initialBottomWeight_5();
		NullCheck(L_3);
		L_3->set_m_Weight0_17(L_4);
	}

IL_002a:
	{
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.MixingCameraBlend::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MixingCameraBlend_Update_m42804BA540A1712167B1EE7E8AEE3AEF359FAF90 (MixingCameraBlend_tCB7BBB9AC892ECC6DE8154D1F397E7F7B359D8E7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MixingCameraBlend_Update_m42804BA540A1712167B1EE7E8AEE3AEF359FAF90_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (followTarget)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = __this->get_followTarget_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bb;
		}
	}
	{
		// switch (axisToTrack)
		int32_t L_2 = __this->get_axisToTrack_6();
		V_0 = L_2;
		int32_t L_3 = V_0;
		switch (L_3)
		{
			case 0:
			{
				goto IL_002a;
			}
			case 1:
			{
				goto IL_0050;
			}
			case 2:
			{
				goto IL_0076;
			}
		}
	}
	{
		return;
	}

IL_002a:
	{
		// vcam.m_Weight1 = Mathf.Abs(followTarget.transform.position.x);
		CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE * L_4 = __this->get_vcam_7();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = __this->get_followTarget_4();
		NullCheck(L_5);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_9 = fabsf(L_8);
		NullCheck(L_4);
		L_4->set_m_Weight1_18(L_9);
		// break;
		return;
	}

IL_0050:
	{
		// vcam.m_Weight1 = Mathf.Abs(followTarget.transform.position.z);
		CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE * L_10 = __this->get_vcam_7();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = __this->get_followTarget_4();
		NullCheck(L_11);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_12, /*hidden argument*/NULL);
		float L_14 = L_13.get_z_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_15 = fabsf(L_14);
		NullCheck(L_10);
		L_10->set_m_Weight1_18(L_15);
		// break;
		return;
	}

IL_0076:
	{
		// vcam.m_Weight1 =
		//     Mathf.Abs(Mathf.Abs(followTarget.transform.position.x) +
		//               Mathf.Abs(followTarget.transform.position.z));
		CinemachineMixingCamera_t5DD935499FE2B68BA6BE23B09F36EB01F3F58FCE * L_16 = __this->get_vcam_7();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_17 = __this->get_followTarget_4();
		NullCheck(L_17);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_18, /*hidden argument*/NULL);
		float L_20 = L_19.get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_21 = fabsf(L_20);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_22 = __this->get_followTarget_4();
		NullCheck(L_22);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_23 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_23, /*hidden argument*/NULL);
		float L_25 = L_24.get_z_4();
		float L_26 = fabsf(L_25);
		float L_27 = fabsf(((float)il2cpp_codegen_add((float)L_21, (float)L_26)));
		NullCheck(L_16);
		L_16->set_m_Weight1_18(L_27);
	}

IL_00bb:
	{
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.MixingCameraBlend::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MixingCameraBlend__ctor_m5E506BC4C5275485E50A222DF896993983D3440F (MixingCameraBlend_tCB7BBB9AC892ECC6DE8154D1F397E7F7B359D8E7 * __this, const RuntimeMethod* method)
{
	{
		// public float initialBottomWeight = 20f;
		__this->set_initialBottomWeight_5((20.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Cinemachine.Examples.ScriptingExample::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptingExample_Start_m79B8DBE9F2711279277D13BF1308C3516C19F90A (ScriptingExample_t43DC6E8DA30C3BFFB9C51A0C22E54188F22A33E4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScriptingExample_Start_m79B8DBE9F2711279277D13BF1308C3516C19F90A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * V_0 = NULL;
	CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * V_1 = NULL;
	{
		// var brain = GameObject.Find("Main Camera").AddComponent<CinemachineBrain>();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(_stringLiteral1994557C9850CA12CEDF370B8F7E1A2E35748A9C, /*hidden argument*/NULL);
		NullCheck(L_0);
		CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB * L_1 = GameObject_AddComponent_TisCinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB_m8C98E6FD74E6A35274E48DD803B5900F1F291263(L_0, /*hidden argument*/GameObject_AddComponent_TisCinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB_m8C98E6FD74E6A35274E48DD803B5900F1F291263_RuntimeMethod_var);
		// brain.m_ShowDebugText = true;
		CinemachineBrain_t5996EACC2FD39294A21090F0FDF2D87257BE54EB * L_2 = L_1;
		NullCheck(L_2);
		L_2->set_m_ShowDebugText_4((bool)1);
		// brain.m_DefaultBlend.m_Time = 1;
		NullCheck(L_2);
		CinemachineBlendDefinition_t2AC0334955F853560C7F95D4B43BE701F099EF04 * L_3 = L_2->get_address_of_m_DefaultBlend_10();
		L_3->set_m_Time_1((1.0f));
		// vcam = new GameObject("VirtualCamera").AddComponent<CinemachineVirtualCamera>();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_4, _stringLiteral016337FC26A9B9DA6C098BECFA0937F0BE66B39A, /*hidden argument*/NULL);
		NullCheck(L_4);
		CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * L_5 = GameObject_AddComponent_TisCinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A_m7CF035809E82878DBC2A8ED4C21104BED3421143(L_4, /*hidden argument*/GameObject_AddComponent_TisCinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A_m7CF035809E82878DBC2A8ED4C21104BED3421143_RuntimeMethod_var);
		__this->set_vcam_4(L_5);
		// vcam.m_LookAt = GameObject.Find("Cube").transform;
		CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * L_6 = __this->get_vcam_4();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(_stringLiteral90CCC9AA84EF50B2769F7D99C20C539D78F3915C, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_m_LookAt_16(L_8);
		// vcam.m_Priority = 10;
		CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * L_9 = __this->get_vcam_4();
		NullCheck(L_9);
		((CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD *)L_9)->set_m_Priority_9(((int32_t)10));
		// vcam.gameObject.transform.position = new Vector3(0, 1, 0);
		CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * L_10 = __this->get_vcam_4();
		NullCheck(L_10);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_11, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_13), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_12, L_13, /*hidden argument*/NULL);
		// var composer = vcam.AddCinemachineComponent<CinemachineComposer>();
		CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * L_14 = __this->get_vcam_4();
		NullCheck(L_14);
		CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8 * L_15 = CinemachineVirtualCamera_AddCinemachineComponent_TisCinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8_m345831406FEA1F9C352CFC582909DC8CF50E2FED(L_14, /*hidden argument*/CinemachineVirtualCamera_AddCinemachineComponent_TisCinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8_m345831406FEA1F9C352CFC582909DC8CF50E2FED_RuntimeMethod_var);
		// composer.m_ScreenX = 0.30f;
		CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8 * L_16 = L_15;
		NullCheck(L_16);
		L_16->set_m_ScreenX_18((0.3f));
		// composer.m_ScreenY = 0.35f;
		NullCheck(L_16);
		L_16->set_m_ScreenY_19((0.35f));
		// freelook = new GameObject("FreeLook").AddComponent<CinemachineFreeLook>();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_17 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_17, _stringLiteralB9ACF0BA2F7AFB72061E78F24B2813707BC0174A, /*hidden argument*/NULL);
		NullCheck(L_17);
		CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * L_18 = GameObject_AddComponent_TisCinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152_mC55F5EE96B665C6498ED43145950B14496235AD3(L_17, /*hidden argument*/GameObject_AddComponent_TisCinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152_mC55F5EE96B665C6498ED43145950B14496235AD3_RuntimeMethod_var);
		__this->set_freelook_5(L_18);
		// freelook.m_LookAt = GameObject.Find("Cylinder/Sphere").transform;
		CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * L_19 = __this->get_freelook_5();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_20 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(_stringLiteralA7DE103B35721CCBF53276B75061293F51C3272B, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		L_19->set_m_LookAt_16(L_21);
		// freelook.m_Follow = GameObject.Find("Cylinder").transform;
		CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * L_22 = __this->get_freelook_5();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_23 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(_stringLiteral817B354DC01A34895EAB0C37E21AF38DC0850D08, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_24 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		L_22->set_m_Follow_17(L_24);
		// freelook.m_Priority = 11;
		CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * L_25 = __this->get_freelook_5();
		NullCheck(L_25);
		((CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD *)L_25)->set_m_Priority_9(((int32_t)11));
		// CinemachineVirtualCamera topRig = freelook.GetRig(0);
		CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * L_26 = __this->get_freelook_5();
		NullCheck(L_26);
		CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * L_27 = CinemachineFreeLook_GetRig_m27DEABB380ACF8A98E8C289F0EAE9E123315A470(L_26, 0, /*hidden argument*/NULL);
		V_0 = L_27;
		// CinemachineVirtualCamera middleRig = freelook.GetRig(1);
		CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * L_28 = __this->get_freelook_5();
		NullCheck(L_28);
		CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * L_29 = CinemachineFreeLook_GetRig_m27DEABB380ACF8A98E8C289F0EAE9E123315A470(L_28, 1, /*hidden argument*/NULL);
		V_1 = L_29;
		// CinemachineVirtualCamera bottomRig = freelook.GetRig(2);
		CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * L_30 = __this->get_freelook_5();
		NullCheck(L_30);
		CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * L_31 = CinemachineFreeLook_GetRig_m27DEABB380ACF8A98E8C289F0EAE9E123315A470(L_30, 2, /*hidden argument*/NULL);
		// topRig.GetCinemachineComponent<CinemachineComposer>().m_ScreenY = 0.35f;
		CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * L_32 = V_0;
		NullCheck(L_32);
		CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8 * L_33 = CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8_mA61ACEA9FC833694511132FC13E74FB7D7E14FD7(L_32, /*hidden argument*/CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8_mA61ACEA9FC833694511132FC13E74FB7D7E14FD7_RuntimeMethod_var);
		NullCheck(L_33);
		L_33->set_m_ScreenY_19((0.35f));
		// middleRig.GetCinemachineComponent<CinemachineComposer>().m_ScreenY = 0.25f;
		CinemachineVirtualCamera_tC5DB4815A85B3728BE50E27D81EBD3DCF54E908A * L_34 = V_1;
		NullCheck(L_34);
		CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8 * L_35 = CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8_mA61ACEA9FC833694511132FC13E74FB7D7E14FD7(L_34, /*hidden argument*/CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8_mA61ACEA9FC833694511132FC13E74FB7D7E14FD7_RuntimeMethod_var);
		NullCheck(L_35);
		L_35->set_m_ScreenY_19((0.25f));
		// bottomRig.GetCinemachineComponent<CinemachineComposer>().m_ScreenY = 0.15f;
		NullCheck(L_31);
		CinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8 * L_36 = CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8_mA61ACEA9FC833694511132FC13E74FB7D7E14FD7(L_31, /*hidden argument*/CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineComposer_tDB4047226BD5AA1FAB69B12D530DF4A70A89CAF8_mA61ACEA9FC833694511132FC13E74FB7D7E14FD7_RuntimeMethod_var);
		NullCheck(L_36);
		L_36->set_m_ScreenY_19((0.15f));
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.ScriptingExample::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptingExample_Update_mA1A4F8E6EC542DD873BD3DC42A24FAED1499AB8C (ScriptingExample_t43DC6E8DA30C3BFFB9C51A0C22E54188F22A33E4 * __this, const RuntimeMethod* method)
{
	{
		// if (Time.realtimeSinceStartup - lastSwapTime > 5)
		float L_0 = Time_get_realtimeSinceStartup_mCA1086EC9DFCF135F77BC46D3B7127711EA3DE03(/*hidden argument*/NULL);
		float L_1 = __this->get_lastSwapTime_6();
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_0, (float)L_1))) > ((float)(5.0f)))))
		{
			goto IL_0037;
		}
	}
	{
		// freelook.enabled = !freelook.enabled;
		CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * L_2 = __this->get_freelook_5();
		CinemachineFreeLook_t0827F1BA5B40CCA0FC29E097D107E5CD6831C152 * L_3 = __this->get_freelook_5();
		NullCheck(L_3);
		bool L_4 = Behaviour_get_enabled_mAA0C9ED5A3D1589C1C8AA22636543528DB353CFB(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_2, (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// lastSwapTime = Time.realtimeSinceStartup;
		float L_5 = Time_get_realtimeSinceStartup_mCA1086EC9DFCF135F77BC46D3B7127711EA3DE03(/*hidden argument*/NULL);
		__this->set_lastSwapTime_6(L_5);
	}

IL_0037:
	{
		// }
		return;
	}
}
// System.Void Cinemachine.Examples.ScriptingExample::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptingExample__ctor_mD5B229275ACF8576FA6EB85B97EAA7591F56DC01 (ScriptingExample_t43DC6E8DA30C3BFFB9C51A0C22E54188F22A33E4 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyPatrol::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyPatrol_Awake_mB4793D74956B3910848A4B4DF141FE6A7F826A25 (EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyPatrol_Awake_mB4793D74956B3910848A4B4DF141FE6A7F826A25_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _animator = GetComponent<Animator>();
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719(__this, /*hidden argument*/Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719_RuntimeMethod_var);
		__this->set__animator_9(L_0);
		// _weapon = GetComponentInChildren<Weapon>();
		Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * L_1 = Component_GetComponentInChildren_TisWeapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9_m0B9B6D77DDFC65C7C8FD16BD86CAD38A1B44861C(__this, /*hidden argument*/Component_GetComponentInChildren_TisWeapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9_m0B9B6D77DDFC65C7C8FD16BD86CAD38A1B44861C_RuntimeMethod_var);
		__this->set__weapon_10(L_1);
		// }
		return;
	}
}
// System.Void EnemyPatrol::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyPatrol_Start_mF69D7343C30458904BF576DC87C4EE0D94D5E913 (EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyPatrol_Start_mF69D7343C30458904BF576DC87C4EE0D94D5E913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// UpdateTarget();
		EnemyPatrol_UpdateTarget_mBF1D57F2C57B1B19634CFBEE098102D0F2C58819(__this, /*hidden argument*/NULL);
		// StartCoroutine("PatrolToTarget");
		MonoBehaviour_StartCoroutine_m590A0A7F161D579C18E678B4C5ACCE77B1B318DD(__this, _stringLiteral69C81CA8C8D9D9FD760315C6D4E17A09327BA88D, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyPatrol::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyPatrol_Update_m9C9A815AFD2925201FC71D4D080ACA2A77307182 (EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void EnemyPatrol::UpdateTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyPatrol_UpdateTarget_mBF1D57F2C57B1B19634CFBEE098102D0F2C58819 (EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyPatrol_UpdateTarget_mBF1D57F2C57B1B19634CFBEE098102D0F2C58819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_target == null)
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get__target_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006e;
		}
	}
	{
		// _target = new GameObject("Target");
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_2, _stringLiteral61AD50A9B9189CC3CF1874568E35E7901FF4C982, /*hidden argument*/NULL);
		__this->set__target_8(L_2);
		// _target.transform.position = new Vector2(minX, transform.position.y);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = __this->get__target_8();
		NullCheck(L_3);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_3, /*hidden argument*/NULL);
		float L_5 = __this->get_minX_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_y_3();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_9), L_5, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_9, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_4, L_10, /*hidden argument*/NULL);
		// transform.localScale = new Vector3(-1, 1, 1);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_12), (-1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_11, L_12, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_006e:
	{
		// if (_target.transform.position.x == minX)
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_13 = __this->get__target_8();
		NullCheck(L_13);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_14, /*hidden argument*/NULL);
		float L_16 = L_15.get_x_2();
		float L_17 = __this->get_minX_5();
		if ((!(((float)L_16) == ((float)L_17))))
		{
			goto IL_00db;
		}
	}
	{
		// _target.transform.position = new Vector2(maxX, transform.position.y);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_18 = __this->get__target_8();
		NullCheck(L_18);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_18, /*hidden argument*/NULL);
		float L_20 = __this->get_maxX_6();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_21, /*hidden argument*/NULL);
		float L_23 = L_22.get_y_3();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_24;
		memset((&L_24), 0, sizeof(L_24));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_24), L_20, L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_24, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_19, L_25, /*hidden argument*/NULL);
		// transform.localScale = new Vector3(1, 1, 1);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_26 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27;
		memset((&L_27), 0, sizeof(L_27));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_27), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_26, L_27, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00db:
	{
		// else if (_target.transform.position.x == maxX)
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_28 = __this->get__target_8();
		NullCheck(L_28);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_29 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_30 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_29, /*hidden argument*/NULL);
		float L_31 = L_30.get_x_2();
		float L_32 = __this->get_maxX_6();
		if ((!(((float)L_31) == ((float)L_32))))
		{
			goto IL_0147;
		}
	}
	{
		// _target.transform.position = new Vector2(minX, transform.position.y);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_33 = __this->get__target_8();
		NullCheck(L_33);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_34 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_33, /*hidden argument*/NULL);
		float L_35 = __this->get_minX_5();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_36 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_36, /*hidden argument*/NULL);
		float L_38 = L_37.get_y_3();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_39;
		memset((&L_39), 0, sizeof(L_39));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_39), L_35, L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_40 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_39, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_34, L_40, /*hidden argument*/NULL);
		// transform.localScale = new Vector3(-1, 1, 1);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_41 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42;
		memset((&L_42), 0, sizeof(L_42));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_42), (-1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_41, L_42, /*hidden argument*/NULL);
	}

IL_0147:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator EnemyPatrol::PatrolToTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* EnemyPatrol_PatrolToTarget_m675B9C77F20C755079DFE6B4AFFA4E1798C092ED (EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyPatrol_PatrolToTarget_m675B9C77F20C755079DFE6B4AFFA4E1798C092ED_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED * L_0 = (U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED *)il2cpp_codegen_object_new(U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED_il2cpp_TypeInfo_var);
		U3CPatrolToTargetU3Ed__11__ctor_m55E022613EF7239E767DC35B5D591AFF3361E960(L_0, 0, /*hidden argument*/NULL);
		U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void EnemyPatrol::CanShoot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyPatrol_CanShoot_mFBD932DF4BC7E42F3CD1ED71F77C9646FB51C7CE (EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyPatrol_CanShoot_mFBD932DF4BC7E42F3CD1ED71F77C9646FB51C7CE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_weapon != null)
		Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * L_0 = __this->get__weapon_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// _weapon.Shoot();
		Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * L_2 = __this->get__weapon_10();
		NullCheck(L_2);
		Weapon_Shoot_mF5B36E6C4428C1E8858F42526F5EEC7C7AF9D4E3(L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// }
		return;
	}
}
// System.Void EnemyPatrol::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyPatrol__ctor_mA0C5ED3222E96136C9E8930F5548D7CD32FE2AE2 (EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * __this, const RuntimeMethod* method)
{
	{
		// private float speed = 1f;
		__this->set_speed_4((1.0f));
		// public float waitingTime = 2f;
		__this->set_waitingTime_7((2.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyPatrol_<PatrolToTarget>d__11::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPatrolToTargetU3Ed__11__ctor_m55E022613EF7239E767DC35B5D591AFF3361E960 (U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void EnemyPatrol_<PatrolToTarget>d__11::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPatrolToTargetU3Ed__11_System_IDisposable_Dispose_m0EE6A302193BE3E6AFB381DED643FAC78FC8DFD5 (U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean EnemyPatrol_<PatrolToTarget>d__11::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CPatrolToTargetU3Ed__11_MoveNext_m03BFEC4A71DDD0D46580989D40E65F070AD55DD3 (U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPatrolToTargetU3Ed__11_MoveNext_m03BFEC4A71DDD0D46580989D40E65F070AD55DD3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * V_1 = NULL;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_009e;
			}
			case 2:
			{
				goto IL_017d;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_0((-1));
		// _animator.SetBool("Idle", false);
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_3 = V_1;
		NullCheck(L_3);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_4 = L_3->get__animator_9();
		NullCheck(L_4);
		Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E(L_4, _stringLiteralCC1EBDD04E76A4B8BAFFB29A22FA00446D382B18, (bool)0, /*hidden argument*/NULL);
		goto IL_00a5;
	}

IL_003c:
	{
		// Vector2 direction = _target.transform.position - transform.position;
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_5 = V_1;
		NullCheck(L_5);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = L_5->get__target_8();
		NullCheck(L_6);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_7, /*hidden argument*/NULL);
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_9 = V_1;
		NullCheck(L_9);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_8, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_13 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		// transform.Translate(direction.normalized * speed * Time.deltaTime);
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_14 = V_1;
		NullCheck(L_14);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_14, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_16 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_2), /*hidden argument*/NULL);
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_17 = V_1;
		NullCheck(L_17);
		float L_18 = L_17->get_speed_4();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_19 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_16, L_18, /*hidden argument*/NULL);
		float L_20 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_21 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_19, L_20, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_21, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_Translate_m0F354939D5084DDFF6B9902E62D3DCA7A3B53EA3(L_15, L_22, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_009e:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_00a5:
	{
		// while (Vector2.Distance(transform.position, _target.transform.position) > 0.05f)
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_23 = V_1;
		NullCheck(L_23);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_24 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_26 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_25, /*hidden argument*/NULL);
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_27 = V_1;
		NullCheck(L_27);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_28 = L_27->get__target_8();
		NullCheck(L_28);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_29 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_30 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_29, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_31 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_30, /*hidden argument*/NULL);
		float L_32 = Vector2_Distance_mB07492BC42EC582754AD11554BE5B7F8D0E93CF4(L_26, L_31, /*hidden argument*/NULL);
		if ((((float)L_32) > ((float)(0.05f))))
		{
			goto IL_003c;
		}
	}
	{
		// Debug.Log("Target reached");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteralDC06D8A75872E885632CFBEFAA0BC18E6C22F90F, /*hidden argument*/NULL);
		// transform.position = new Vector2(_target.transform.position.x, transform.position.y);
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_33 = V_1;
		NullCheck(L_33);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_34 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_33, /*hidden argument*/NULL);
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_35 = V_1;
		NullCheck(L_35);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_36 = L_35->get__target_8();
		NullCheck(L_36);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_37 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_38 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_37, /*hidden argument*/NULL);
		float L_39 = L_38.get_x_2();
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_40 = V_1;
		NullCheck(L_40);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_41 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_41, /*hidden argument*/NULL);
		float L_43 = L_42.get_y_3();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_44;
		memset((&L_44), 0, sizeof(L_44));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_44), L_39, L_43, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_45 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_44, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_34, L_45, /*hidden argument*/NULL);
		// UpdateTarget();
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_46 = V_1;
		NullCheck(L_46);
		EnemyPatrol_UpdateTarget_mBF1D57F2C57B1B19634CFBEE098102D0F2C58819(L_46, /*hidden argument*/NULL);
		// _animator.SetBool("Idle", true);
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_47 = V_1;
		NullCheck(L_47);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_48 = L_47->get__animator_9();
		NullCheck(L_48);
		Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E(L_48, _stringLiteralCC1EBDD04E76A4B8BAFFB29A22FA00446D382B18, (bool)1, /*hidden argument*/NULL);
		// _animator.SetTrigger("Shoot");
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_49 = V_1;
		NullCheck(L_49);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_50 = L_49->get__animator_9();
		NullCheck(L_50);
		Animator_SetTrigger_m68D29B7FA54C2F230F5AD780D462612B18E74245(L_50, _stringLiteralB301C10B3D0A20C4D34F04D383741C386814B593, /*hidden argument*/NULL);
		// Debug.Log("Waiting for " + waitingTime + " seconds");
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_51 = V_1;
		NullCheck(L_51);
		float L_52 = L_51->get_waitingTime_7();
		float L_53 = L_52;
		RuntimeObject * L_54 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_53);
		String_t* L_55 = String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC(_stringLiteralF914411ACA4F8D2182F5CD69FE08BE5A0733A5B7, L_54, _stringLiteral3E441FC593FEA48A7F0A70675200FE569B0A781F, /*hidden argument*/NULL);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_55, /*hidden argument*/NULL);
		// yield return new WaitForSeconds(waitingTime);
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_56 = V_1;
		NullCheck(L_56);
		float L_57 = L_56->get_waitingTime_7();
		WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * L_58 = (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 *)il2cpp_codegen_object_new(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559(L_58, L_57, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_58);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_017d:
	{
		__this->set_U3CU3E1__state_0((-1));
		// Debug.Log("Waited enough, lets update the target and move again");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteralE952365FDE19D5E2968E1F673DF7C02ED2CC2C11, /*hidden argument*/NULL);
		// StartCoroutine("PatrolToTarget");
		EnemyPatrol_t167719791858AB61B9226022483B788F2241BDBD * L_59 = V_1;
		NullCheck(L_59);
		MonoBehaviour_StartCoroutine_m590A0A7F161D579C18E678B4C5ACCE77B1B318DD(L_59, _stringLiteral69C81CA8C8D9D9FD760315C6D4E17A09327BA88D, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object EnemyPatrol_<PatrolToTarget>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CPatrolToTargetU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E023F09D9AF079421AD9CBC82AD92E13A25C3B0 (U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void EnemyPatrol_<PatrolToTarget>d__11::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPatrolToTargetU3Ed__11_System_Collections_IEnumerator_Reset_m4A790E2666FDFBE6B63FCC94B70F2522443C9B9B (U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPatrolToTargetU3Ed__11_System_Collections_IEnumerator_Reset_m4A790E2666FDFBE6B63FCC94B70F2522443C9B9B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, U3CPatrolToTargetU3Ed__11_System_Collections_IEnumerator_Reset_m4A790E2666FDFBE6B63FCC94B70F2522443C9B9B_RuntimeMethod_var);
	}
}
// System.Object EnemyPatrol_<PatrolToTarget>d__11::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CPatrolToTargetU3Ed__11_System_Collections_IEnumerator_get_Current_m1DCC9C5C0BF5244C7AB62C8E4CC715BDC076CBFE (U3CPatrolToTargetU3Ed__11_t679A7B8019A3EDA593D72720C3FADD3E1C47F4ED * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InstantiatePrefab::Instantiate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InstantiatePrefab_Instantiate_m31E43D0401E2D3691190E3780516A2EE9A881B23 (InstantiatePrefab_t36976A57AFF7271A16E6F3932ACF6823CA3CA292 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstantiatePrefab_Instantiate_m31E43D0401E2D3691190E3780516A2EE9A881B23_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_0 = NULL;
	{
		// GameObject instantiateObject = Instantiate(prefab, point.position, Quaternion.identity) as GameObject;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_prefab_4();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = __this->get_point_5();
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_3 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m4F397BCC6697902B40033E61129D4EA6FE93570F(L_0, L_2, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m4F397BCC6697902B40033E61129D4EA6FE93570F_RuntimeMethod_var);
		V_0 = L_4;
		// if (livingTime > 0f)
		float L_5 = __this->get_livingTime_6();
		if ((!(((float)L_5) > ((float)(0.0f)))))
		{
			goto IL_0035;
		}
	}
	{
		// Destroy(instantiateObject, livingTime);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = V_0;
		float L_7 = __this->get_livingTime_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m09F51D8BDECFD2E8C618498EF7377029B669030D(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0035:
	{
		// }
		return;
	}
}
// System.Void InstantiatePrefab::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InstantiatePrefab__ctor_mE32F9508E28E5A9AA2D84CA8CB9CB2E0FC7868E6 (InstantiatePrefab_t36976A57AFF7271A16E6F3932ACF6823CA3CA292 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LoadScene::OnMouseDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadScene_OnMouseDown_mCD182BA2B1429157BDCC1BCC4E31AB9AA0B47EA5 (LoadScene_t674EFD288A318DA4B83B5427402567479C0B493D * __this, const RuntimeMethod* method)
{
	{
		// SceneManager.LoadScene(scene);
		String_t* L_0 = __this->get_scene_4();
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LoadScene::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadScene__ctor_m52B221B2B1C02A68F125B164DDE573DF0353A336 (LoadScene_t674EFD288A318DA4B83B5427402567479C0B493D * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_Awake_m118C1D205A374B627E1EC963E5837E23CF554573 (PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Awake_m118C1D205A374B627E1EC963E5837E23CF554573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _rigidbody = GetComponent<Rigidbody2D>();
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_0 = Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625_RuntimeMethod_var);
		__this->set__rigidbody_11(L_0);
		// _animator = GetComponent<Animator>();
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_1 = Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719(__this, /*hidden argument*/Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719_RuntimeMethod_var);
		__this->set__animator_12(L_1);
		// }
		return;
	}
}
// System.Void PlayerController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_Start_mC0C9B9461D0BDAC48EC43715818A4BA63C4F45EF (PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void PlayerController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_Update_m38903EF1C8F12B9388303741F8040EE26C33DC33 (PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Update_m38903EF1C8F12B9388303741F8040EE26C33DC33_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// if (_isAttacking == false)
		bool L_0 = __this->get__isAttacking_16();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		// float horizontalInput = Input.GetAxisRaw("Horizontal");
		float L_1 = Input_GetAxisRaw_mC68301A9D93702F0C393E45C6337348062EACE21(_stringLiteral4F57A1CE99E68A7B05C42D0A7EA0070EAFABD31C, /*hidden argument*/NULL);
		V_0 = L_1;
		// _movement = new Vector2(horizontalInput, 0f);
		float L_2 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_3), L_2, (0.0f), /*hidden argument*/NULL);
		__this->set__movement_13(L_3);
		// if (horizontalInput < 0f && _facingRight == true)
		float L_4 = V_0;
		if ((!(((float)L_4) < ((float)(0.0f)))))
		{
			goto IL_003c;
		}
	}
	{
		bool L_5 = __this->get__facingRight_14();
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		// Flip();
		PlayerController_Flip_m829B4A4842ABD034CB56BCB1966D3C462A9D6AD4(__this, /*hidden argument*/NULL);
		// }
		goto IL_0052;
	}

IL_003c:
	{
		// else if (horizontalInput > 0f && _facingRight == false)
		float L_6 = V_0;
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_0052;
		}
	}
	{
		bool L_7 = __this->get__facingRight_14();
		if (L_7)
		{
			goto IL_0052;
		}
	}
	{
		// Flip();
		PlayerController_Flip_m829B4A4842ABD034CB56BCB1966D3C462A9D6AD4(__this, /*hidden argument*/NULL);
	}

IL_0052:
	{
		// _isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = __this->get_groundCheck_8();
		NullCheck(L_8);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_9, /*hidden argument*/NULL);
		float L_11 = __this->get_groundCheckRadius_10();
		LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  L_12 = __this->get_groundLayer_9();
		int32_t L_13 = LayerMask_op_Implicit_m2AFFC7F931005437E8F356C953F439829AF4CFA5(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_tB21970F986016656D66D2922594F336E1EE7D5C7_il2cpp_TypeInfo_var);
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_14 = Physics2D_OverlapCircle_m627FB9EE641A74B942877F57DD2FED656FDA5DC9(L_10, L_11, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_14, /*hidden argument*/NULL);
		__this->set__isGrounded_15(L_15);
		// if (Input.GetButtonDown("Jump") && _isGrounded == true && _isAttacking == false)
		bool L_16 = Input_GetButtonDown_m1E80BAC5CCBE9E0151491B8F8F5FFD6AB050BBF0(_stringLiteral1EBA140FDD9C6860A1730C408E3064AA417CA2A3, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00bb;
		}
	}
	{
		bool L_17 = __this->get__isGrounded_15();
		if (!L_17)
		{
			goto IL_00bb;
		}
	}
	{
		bool L_18 = __this->get__isAttacking_16();
		if (L_18)
		{
			goto IL_00bb;
		}
	}
	{
		// _rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_19 = __this->get__rigidbody_11();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_20 = Vector2_get_up_mC4548731D5E7C71164D18C390A1AC32501DAE441(/*hidden argument*/NULL);
		float L_21 = __this->get_jumpForce_7();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_22 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_19);
		Rigidbody2D_AddForce_m09E1A2E24DABA5BBC613E35772AE2C1C35C6E40C(L_19, L_22, 1, /*hidden argument*/NULL);
	}

IL_00bb:
	{
		// if (Input.GetButtonDown("Fire1") && _isGrounded == true && _isAttacking == false)
		bool L_23 = Input_GetButtonDown_m1E80BAC5CCBE9E0151491B8F8F5FFD6AB050BBF0(_stringLiteral0CB715D89D6589E699639FF0716A2BE52C44EEEE, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0102;
		}
	}
	{
		bool L_24 = __this->get__isGrounded_15();
		if (!L_24)
		{
			goto IL_0102;
		}
	}
	{
		bool L_25 = __this->get__isAttacking_16();
		if (L_25)
		{
			goto IL_0102;
		}
	}
	{
		// _movement = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_26 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		__this->set__movement_13(L_26);
		// _rigidbody.velocity = Vector2.zero;
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_27 = __this->get__rigidbody_11();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_28 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		NullCheck(L_27);
		Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83(L_27, L_28, /*hidden argument*/NULL);
		// _animator.SetTrigger("Attack");
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_29 = __this->get__animator_12();
		NullCheck(L_29);
		Animator_SetTrigger_m68D29B7FA54C2F230F5AD780D462612B18E74245(L_29, _stringLiteral1B81F28B5AFBFCAAF5253D26B7DBA4028A92A860, /*hidden argument*/NULL);
	}

IL_0102:
	{
		// }
		return;
	}
}
// System.Void PlayerController::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_FixedUpdate_m914EA3E3CCE4DF6AEB2E78317FFC1D507DACEBDE (PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// if (_isAttacking == false)
		bool L_0 = __this->get__isAttacking_16();
		if (L_0)
		{
			goto IL_0041;
		}
	}
	{
		// float horizontalVelocity = _movement.normalized.x * speed;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_1 = __this->get_address_of__movement_13();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_x_0();
		float L_4 = __this->get_speed_6();
		V_0 = ((float)il2cpp_codegen_multiply((float)L_3, (float)L_4));
		// _rigidbody.velocity = new Vector2(horizontalVelocity, _rigidbody.velocity.y);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_5 = __this->get__rigidbody_11();
		float L_6 = V_0;
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_7 = __this->get__rigidbody_11();
		NullCheck(L_7);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = Rigidbody2D_get_velocity_m5ABF36BDF90FD7308BE608667B9E8F3DA5A207F1(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_10), L_6, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83(L_5, L_10, /*hidden argument*/NULL);
	}

IL_0041:
	{
		// }
		return;
	}
}
// System.Void PlayerController::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_LateUpdate_mAF83D0B2354FDB3E9EEC98CB72E9D77EC004B881 (PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_LateUpdate_mAF83D0B2354FDB3E9EEC98CB72E9D77EC004B881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// _animator.SetBool("Idle", _movement == Vector2.zero);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = __this->get__animator_12();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = __this->get__movement_13();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		bool L_3 = Vector2_op_Equality_m0E86E1B1038DDB8554A8A0D58729A7788D989588(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E(L_0, _stringLiteralCC1EBDD04E76A4B8BAFFB29A22FA00446D382B18, L_3, /*hidden argument*/NULL);
		// _animator.SetBool("IsGrounded", _isGrounded);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_4 = __this->get__animator_12();
		bool L_5 = __this->get__isGrounded_15();
		NullCheck(L_4);
		Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E(L_4, _stringLiteralCC8D7979F8F6B6CAC74FEF89F920AA835E515A9E, L_5, /*hidden argument*/NULL);
		// _animator.SetFloat("VerticalVelocity", _rigidbody.velocity.y);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_6 = __this->get__animator_12();
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_7 = __this->get__rigidbody_11();
		NullCheck(L_7);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = Rigidbody2D_get_velocity_m5ABF36BDF90FD7308BE608667B9E8F3DA5A207F1(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_y_1();
		NullCheck(L_6);
		Animator_SetFloat_mE4C29F6980EBBBD954637721E6E13A0BE2B13C43(L_6, _stringLiteral42BC272E41ECB68FE544AA26C6F3AC14C8E21034, L_9, /*hidden argument*/NULL);
		// if (_animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_10 = __this->get__animator_12();
		NullCheck(L_10);
		AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  L_11 = Animator_GetCurrentAnimatorStateInfo_mBE5ED0D60A6F5CD0EDD40AF1494098D4E7BF84F2(L_10, 0, /*hidden argument*/NULL);
		V_0 = L_11;
		bool L_12 = AnimatorStateInfo_IsTag_m4E330F31FDC7A13E1B93F3ED09A7AEBF3F43A2D6((AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 *)(&V_0), _stringLiteral1B81F28B5AFBFCAAF5253D26B7DBA4028A92A860, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007a;
		}
	}
	{
		// _isAttacking = true;
		__this->set__isAttacking_16((bool)1);
		// } else
		goto IL_0081;
	}

IL_007a:
	{
		// _isAttacking = false;
		__this->set__isAttacking_16((bool)0);
	}

IL_0081:
	{
		// if (_animator.GetCurrentAnimatorStateInfo(0).IsTag("Idle"))
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_13 = __this->get__animator_12();
		NullCheck(L_13);
		AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  L_14 = Animator_GetCurrentAnimatorStateInfo_mBE5ED0D60A6F5CD0EDD40AF1494098D4E7BF84F2(L_13, 0, /*hidden argument*/NULL);
		V_0 = L_14;
		bool L_15 = AnimatorStateInfo_IsTag_m4E330F31FDC7A13E1B93F3ED09A7AEBF3F43A2D6((AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 *)(&V_0), _stringLiteralCC1EBDD04E76A4B8BAFFB29A22FA00446D382B18, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00cd;
		}
	}
	{
		// _longIdleTimer += Time.deltaTime;
		float L_16 = __this->get__longIdleTimer_5();
		float L_17 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		__this->set__longIdleTimer_5(((float)il2cpp_codegen_add((float)L_16, (float)L_17)));
		// if (_longIdleTimer >= longIdleTime)
		float L_18 = __this->get__longIdleTimer_5();
		float L_19 = __this->get_longIdleTime_4();
		if ((!(((float)L_18) >= ((float)L_19))))
		{
			goto IL_00d8;
		}
	}
	{
		// _animator.SetTrigger("LongIdle");
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_20 = __this->get__animator_12();
		NullCheck(L_20);
		Animator_SetTrigger_m68D29B7FA54C2F230F5AD780D462612B18E74245(L_20, _stringLiteral1A12A77F72140F5565E3C5D869CCFF5749CC4266, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00cd:
	{
		// _longIdleTimer = 0f;
		__this->set__longIdleTimer_5((0.0f));
	}

IL_00d8:
	{
		// }
		return;
	}
}
// System.Void PlayerController::Flip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_Flip_m829B4A4842ABD034CB56BCB1966D3C462A9D6AD4 (PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// _facingRight = !_facingRight;
		bool L_0 = __this->get__facingRight_14();
		__this->set__facingRight_14((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		// float localScaleX = transform.localScale.x;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_x_2();
		V_0 = L_3;
		// localScaleX = localScaleX * -1f;
		float L_4 = V_0;
		V_0 = ((float)il2cpp_codegen_multiply((float)L_4, (float)(-1.0f)));
		// transform.localScale = new Vector3(localScaleX, transform.localScale.y, transform.localScale.z);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		float L_6 = V_0;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_y_3();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_10, /*hidden argument*/NULL);
		float L_12 = L_11.get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_13), L_6, L_9, L_12, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_5, L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController__ctor_m648E40092E37395F4307411E855445886113CD60 (PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * __this, const RuntimeMethod* method)
{
	{
		// public float longIdleTime = 5f;
		__this->set_longIdleTime_4((5.0f));
		// public float speed = 2.5f;
		__this->set_speed_6((2.5f));
		// public float jumpForce = 2.5f;
		__this->set_jumpForce_7((2.5f));
		// private bool _facingRight = true;
		__this->set__facingRight_14((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerMove::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMove_Update_m816082939684D509B053B2848EE5BB2A4B655F45 (PlayerMove_tF4D209EF20A6DE6765B2D125AEA112228D5BD388 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMove_Update_m816082939684D509B053B2848EE5BB2A4B655F45_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B4_0;
	memset((&G_B4_0), 0, sizeof(G_B4_0));
	{
		// Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		float L_0 = Input_GetAxis_m6454498C755B9A2C964875927FB557CA9E75D387(_stringLiteral4F57A1CE99E68A7B05C42D0A7EA0070EAFABD31C, /*hidden argument*/NULL);
		float L_1 = Input_GetAxis_m6454498C755B9A2C964875927FB557CA9E75D387(_stringLiteral4B937CC841D82F8936CEF1EFB88708AB5B0F1EE5, /*hidden argument*/NULL);
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), L_0, (0.0f), L_1, /*hidden argument*/NULL);
		// if (input.magnitude > 0)
		float L_2 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_2) > ((float)(0.0f)))))
		{
			goto IL_011a;
		}
	}
	{
		// Vector3 fwd = worldDirection
		//     ? Vector3.forward : transform.position - Camera.main.transform.position;
		bool L_3 = __this->get_worldDirection_5();
		if (L_3)
		{
			goto IL_005a;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_4, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_6 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_5, L_8, /*hidden argument*/NULL);
		G_B4_0 = L_9;
		goto IL_005f;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D(/*hidden argument*/NULL);
		G_B4_0 = L_10;
	}

IL_005f:
	{
		V_1 = G_B4_0;
		// fwd.y = 0;
		(&V_1)->set_y_3((0.0f));
		// fwd = fwd.normalized;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_1), /*hidden argument*/NULL);
		V_1 = L_11;
		// if (fwd.magnitude > 0.001f)
		float L_12 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_12) > ((float)(0.001f)))))
		{
			goto IL_011a;
		}
	}
	{
		// Quaternion inputFrame = Quaternion.LookRotation(fwd, Vector3.up);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_15 = Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672(L_13, L_14, /*hidden argument*/NULL);
		// input = inputFrame * input;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C(L_15, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		// if (input.magnitude > 0.001f)
		float L_18 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_18) > ((float)(0.001f)))))
		{
			goto IL_011a;
		}
	}
	{
		// transform.position += input * (speed * Time.deltaTime);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = L_19;
		NullCheck(L_20);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_20, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = V_0;
		float L_23 = __this->get_speed_4();
		float L_24 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_22, ((float)il2cpp_codegen_multiply((float)L_23, (float)L_24)), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_21, L_25, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_20, L_26, /*hidden argument*/NULL);
		// if (rotatePlayer)
		bool L_27 = __this->get_rotatePlayer_6();
		if (!L_27)
		{
			goto IL_011a;
		}
	}
	{
		// float t = Cinemachine.Utility.Damper.Damp(1, rotationDamping, Time.deltaTime);
		float L_28 = __this->get_rotationDamping_7();
		float L_29 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		float L_30 = Damper_Damp_mA4664317F3E046AEA0F530941E50442262BD5F3B((1.0f), L_28, L_29, /*hidden argument*/NULL);
		V_2 = L_30;
		// Quaternion newRotation = Quaternion.LookRotation(input.normalized, Vector3.up);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_33 = Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672(L_31, L_32, /*hidden argument*/NULL);
		V_3 = L_33;
		// transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, t);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_34 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_35 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_36 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_35, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_37 = V_3;
		float L_38 = V_2;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_39 = Quaternion_Slerp_m56DE173C3520C83DF3F1C6EDFA82FF88A2C9E756(L_36, L_37, L_38, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_34, L_39, /*hidden argument*/NULL);
	}

IL_011a:
	{
		// if (Input.GetKeyDown(KeyCode.Space) && spaceAction != null)
		bool L_40 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(((int32_t)32), /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0136;
		}
	}
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_41 = __this->get_spaceAction_8();
		if (!L_41)
		{
			goto IL_0136;
		}
	}
	{
		// spaceAction();
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_42 = __this->get_spaceAction_8();
		NullCheck(L_42);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_42, /*hidden argument*/NULL);
	}

IL_0136:
	{
		// if (Input.GetKeyDown(KeyCode.Return) && enterAction != null)
		bool L_43 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(((int32_t)13), /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0152;
		}
	}
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_44 = __this->get_enterAction_9();
		if (!L_44)
		{
			goto IL_0152;
		}
	}
	{
		// enterAction();
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_45 = __this->get_enterAction_9();
		NullCheck(L_45);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_45, /*hidden argument*/NULL);
	}

IL_0152:
	{
		// }
		return;
	}
}
// System.Void PlayerMove::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMove__ctor_m95039BC415B767C2F57784CC458BB65C1188BFD7 (PlayerMove_tF4D209EF20A6DE6765B2D125AEA112228D5BD388 * __this, const RuntimeMethod* method)
{
	{
		// public float speed = 5;
		__this->set_speed_4((5.0f));
		// public bool rotatePlayer = true;
		__this->set_rotatePlayer_6((bool)1);
		// public float rotationDamping = 0.5f;
		__this->set_rotationDamping_7((0.5f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerMoveOnSphere::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMoveOnSphere_Update_m8E045C01FD8CCCCFC85B46DDD68978730E97C9B5 (PlayerMoveOnSphere_t82BC101F65B2488923B0D504BBA3BD830070009A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMoveOnSphere_Update_m8E045C01FD8CCCCFC85B46DDD68978730E97C9B5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		float L_0 = Input_GetAxis_m6454498C755B9A2C964875927FB557CA9E75D387(_stringLiteral4F57A1CE99E68A7B05C42D0A7EA0070EAFABD31C, /*hidden argument*/NULL);
		float L_1 = Input_GetAxis_m6454498C755B9A2C964875927FB557CA9E75D387(_stringLiteral4B937CC841D82F8936CEF1EFB88708AB5B0F1EE5, /*hidden argument*/NULL);
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), L_0, (0.0f), L_1, /*hidden argument*/NULL);
		// if (input.magnitude > 0)
		float L_2 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_2) > ((float)(0.0f)))))
		{
			goto IL_00d0;
		}
	}
	{
		// input = Camera.main.transform.rotation * input;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_3 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_5 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_4, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C(L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		// if (input.magnitude > 0.001f)
		float L_8 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_8) > ((float)(0.001f)))))
		{
			goto IL_00d0;
		}
	}
	{
		// transform.position += input * (speed * Time.deltaTime);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = L_9;
		NullCheck(L_10);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_10, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = V_0;
		float L_13 = __this->get_speed_5();
		float L_14 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_12, ((float)il2cpp_codegen_multiply((float)L_13, (float)L_14)), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_11, L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_10, L_16, /*hidden argument*/NULL);
		// if (rotatePlayer)
		bool L_17 = __this->get_rotatePlayer_6();
		if (!L_17)
		{
			goto IL_00d0;
		}
	}
	{
		// float t = Cinemachine.Utility.Damper.Damp(1, rotationDamping, Time.deltaTime);
		float L_18 = __this->get_rotationDamping_7();
		float L_19 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		float L_20 = Damper_Damp_mA4664317F3E046AEA0F530941E50442262BD5F3B((1.0f), L_18, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		// Quaternion newRotation = Quaternion.LookRotation(input.normalized, transform.up);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_22 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = Transform_get_up_m3E443F6EB278D547946E80D77065A871BEEEE282(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_24 = Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672(L_21, L_23, /*hidden argument*/NULL);
		V_2 = L_24;
		// transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, t);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_25 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_26 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_27 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_26, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_28 = V_2;
		float L_29 = V_1;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_30 = Quaternion_Slerp_m56DE173C3520C83DF3F1C6EDFA82FF88A2C9E756(L_27, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_25, L_30, /*hidden argument*/NULL);
	}

IL_00d0:
	{
		// if (Sphere != null)
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_31 = __this->get_Sphere_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_32 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_31, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0178;
		}
	}
	{
		// var up = transform.position - Sphere.transform.position;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_33 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_34 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_33, /*hidden argument*/NULL);
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_35 = __this->get_Sphere_4();
		NullCheck(L_35);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_36 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_38 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_34, L_37, /*hidden argument*/NULL);
		V_3 = L_38;
		// up = up.normalized;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_39 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_3), /*hidden argument*/NULL);
		V_3 = L_39;
		// var fwd = transform.forward.ProjectOntoPlane(up);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_40 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_41 = Transform_get_forward_m0BE1E88B86049ADA39391C3ACED2314A624BC67F(L_40, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_43 = UnityVectorExtensions_ProjectOntoPlane_m4011502AB317965F1142F74D9B5F17C1EBB639EA(L_41, L_42, /*hidden argument*/NULL);
		V_4 = L_43;
		// transform.position = Sphere.transform.position + up * (Sphere.radius + transform.localScale.y / 2);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_44 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_45 = __this->get_Sphere_4();
		NullCheck(L_45);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_46 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_47 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_46, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_48 = V_3;
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_49 = __this->get_Sphere_4();
		NullCheck(L_49);
		float L_50 = SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094(L_49, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_51 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_51);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_52 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_51, /*hidden argument*/NULL);
		float L_53 = L_52.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_54 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_48, ((float)il2cpp_codegen_add((float)L_50, (float)((float)((float)L_53/(float)(2.0f))))), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_55 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_47, L_54, /*hidden argument*/NULL);
		NullCheck(L_44);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_44, L_55, /*hidden argument*/NULL);
		// transform.rotation = Quaternion.LookRotation(fwd, up);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_56 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_57 = V_4;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_58 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_59 = Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672(L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_56, L_59, /*hidden argument*/NULL);
	}

IL_0178:
	{
		// }
		return;
	}
}
// System.Void PlayerMoveOnSphere::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMoveOnSphere__ctor_m04885E872445EB4B0ADAB9967C4A3F88D2F8A26D (PlayerMoveOnSphere_t82BC101F65B2488923B0D504BBA3BD830070009A * __this, const RuntimeMethod* method)
{
	{
		// public float speed = 5;
		__this->set_speed_5((5.0f));
		// public bool rotatePlayer = true;
		__this->set_rotatePlayer_6((bool)1);
		// public float rotationDamping = 0.5f;
		__this->set_rotationDamping_7((0.5f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerMovePhysics::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovePhysics_Start_m68C6BC74B80E0258B34E06A348E6C32F8FB48154 (PlayerMovePhysics_t4A6B0D15F96182589F0583094C84138989DDC1A8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMovePhysics_Start_m68C6BC74B80E0258B34E06A348E6C32F8FB48154_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// rb = GetComponent<Rigidbody> ();
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_0 = Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C_RuntimeMethod_var);
		__this->set_rb_9(L_0);
		// }
		return;
	}
}
// System.Void PlayerMovePhysics::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovePhysics_OnEnable_m43E1851523FAECBAEDD18FF7072145D99CE6CEDF (PlayerMovePhysics_t4A6B0D15F96182589F0583094C84138989DDC1A8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMovePhysics_OnEnable_m43E1851523FAECBAEDD18FF7072145D99CE6CEDF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// transform.position += new Vector3(10, 0, 0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = L_0;
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_1, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_3), (10.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_1, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerMovePhysics::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovePhysics_FixedUpdate_m41FE39EEE213101002AA90DB208FF4611B71B42E (PlayerMovePhysics_t4A6B0D15F96182589F0583094C84138989DDC1A8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMovePhysics_FixedUpdate_m41FE39EEE213101002AA90DB208FF4611B71B42E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B4_0;
	memset((&G_B4_0), 0, sizeof(G_B4_0));
	{
		// Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		float L_0 = Input_GetAxis_m6454498C755B9A2C964875927FB557CA9E75D387(_stringLiteral4F57A1CE99E68A7B05C42D0A7EA0070EAFABD31C, /*hidden argument*/NULL);
		float L_1 = Input_GetAxis_m6454498C755B9A2C964875927FB557CA9E75D387(_stringLiteral4B937CC841D82F8936CEF1EFB88708AB5B0F1EE5, /*hidden argument*/NULL);
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), L_0, (0.0f), L_1, /*hidden argument*/NULL);
		// if (input.magnitude > 0)
		float L_2 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_2) > ((float)(0.0f)))))
		{
			goto IL_00dd;
		}
	}
	{
		// Vector3 fwd = worldDirection
		//     ? Vector3.forward : transform.position - Camera.main.transform.position;
		bool L_3 = __this->get_worldDirection_5();
		if (L_3)
		{
			goto IL_005a;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_4, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_6 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_5, L_8, /*hidden argument*/NULL);
		G_B4_0 = L_9;
		goto IL_005f;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D(/*hidden argument*/NULL);
		G_B4_0 = L_10;
	}

IL_005f:
	{
		V_1 = G_B4_0;
		// fwd.y = 0;
		(&V_1)->set_y_3((0.0f));
		// fwd = fwd.normalized;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_1), /*hidden argument*/NULL);
		V_1 = L_11;
		// if (fwd.magnitude > 0.001f)
		float L_12 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_12) > ((float)(0.001f)))))
		{
			goto IL_00dd;
		}
	}
	{
		// Quaternion inputFrame = Quaternion.LookRotation(fwd, Vector3.up);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_15 = Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672(L_13, L_14, /*hidden argument*/NULL);
		// input = inputFrame * input;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C(L_15, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		// if (input.magnitude > 0.001f)
		float L_18 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_18) > ((float)(0.001f)))))
		{
			goto IL_00dd;
		}
	}
	{
		// rb.AddForce(speed * input);
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_19 = __this->get_rb_9();
		float L_20 = __this->get_speed_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839(L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_19);
		Rigidbody_AddForce_mC8140D90B806634A733624F671C45AD7CDBEDB38(L_19, L_22, /*hidden argument*/NULL);
		// if (rotatePlayer)
		bool L_23 = __this->get_rotatePlayer_6();
		if (!L_23)
		{
			goto IL_00dd;
		}
	}
	{
		// transform.rotation = Quaternion.LookRotation(input.normalized, Vector3.up);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_24 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_27 = Quaternion_LookRotation_m7BED8FBB457FF073F183AC7962264E5110794672(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_24, L_27, /*hidden argument*/NULL);
	}

IL_00dd:
	{
		// if (Input.GetKeyDown(KeyCode.Space) && spaceAction != null)
		bool L_28 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(((int32_t)32), /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00f9;
		}
	}
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_29 = __this->get_spaceAction_7();
		if (!L_29)
		{
			goto IL_00f9;
		}
	}
	{
		// spaceAction();
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_30 = __this->get_spaceAction_7();
		NullCheck(L_30);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_30, /*hidden argument*/NULL);
	}

IL_00f9:
	{
		// if (Input.GetKeyDown(KeyCode.Return) && enterAction != null)
		bool L_31 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(((int32_t)13), /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0115;
		}
	}
	{
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_32 = __this->get_enterAction_8();
		if (!L_32)
		{
			goto IL_0115;
		}
	}
	{
		// enterAction();
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_33 = __this->get_enterAction_8();
		NullCheck(L_33);
		Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_33, /*hidden argument*/NULL);
	}

IL_0115:
	{
		// }
		return;
	}
}
// System.Void PlayerMovePhysics::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovePhysics__ctor_m4018D7D2888F136029F447C52A9DB1538F5D5064 (PlayerMovePhysics_t4A6B0D15F96182589F0583094C84138989DDC1A8 * __this, const RuntimeMethod* method)
{
	{
		// public float speed = 5;
		__this->set_speed_4((5.0f));
		// public bool worldDirection = true;
		__this->set_worldDirection_5((bool)1);
		// public bool rotatePlayer = true;
		__this->set_rotatePlayer_6((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Weapon::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Weapon_Awake_m2AC14C75327F3526C722CACC9F4CCD7D95B21F98 (Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Weapon_Awake_m2AC14C75327F3526C722CACC9F4CCD7D95B21F98_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _firePoint = transform.Find("FirePoint");
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Transform_Find_m673797B6329C2669A543904532ABA1680DA4EAD1(L_0, _stringLiteral53DFE6C47A184FEE0A0DFEE574C2C6B5DD53D52F, /*hidden argument*/NULL);
		__this->set__firePoint_6(L_1);
		// }
		return;
	}
}
// System.Void Weapon::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Weapon_Start_m4FAB905F9926EEADEEF49D8B77218D1A14F459AC (Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Weapon::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Weapon_Update_mD65E9FD544FEE0974CED54FA39BB05A89FB6D99E (Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Weapon::Shoot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Weapon_Shoot_mF5B36E6C4428C1E8858F42526F5EEC7C7AF9D4E3 (Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Weapon_Shoot_mF5B36E6C4428C1E8858F42526F5EEC7C7AF9D4E3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059 * V_0 = NULL;
	{
		// if(bulletPrefab != null && _firePoint != null && shooter != null)
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_bulletPrefab_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007e;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = __this->get__firePoint_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_2, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_007e;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = __this->get_shooter_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_4, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_007e;
		}
	}
	{
		// GameObject myBullet = Instantiate(bulletPrefab, _firePoint.position, Quaternion.identity) as GameObject;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = __this->get_bulletPrefab_4();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = __this->get__firePoint_6();
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_9 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m4F397BCC6697902B40033E61129D4EA6FE93570F(L_6, L_8, L_9, /*hidden argument*/Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m4F397BCC6697902B40033E61129D4EA6FE93570F_RuntimeMethod_var);
		// Bullet bulletComponent = myBullet.GetComponent<Bullet>();
		NullCheck(L_10);
		Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059 * L_11 = GameObject_GetComponent_TisBullet_t1B228DBA3982FDB21DE04329BDC49915421B9059_mC44AB386177196D8110F3E6D8F0E814D44438271(L_10, /*hidden argument*/GameObject_GetComponent_TisBullet_t1B228DBA3982FDB21DE04329BDC49915421B9059_mC44AB386177196D8110F3E6D8F0E814D44438271_RuntimeMethod_var);
		V_0 = L_11;
		// if (shooter.transform.localScale.x < 0f)
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = __this->get_shooter_5();
		NullCheck(L_12);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_13, /*hidden argument*/NULL);
		float L_15 = L_14.get_x_2();
		if ((!(((float)L_15) < ((float)(0.0f)))))
		{
			goto IL_0073;
		}
	}
	{
		// bulletComponent.direction = Vector2.left;
		Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059 * L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_17 = Vector2_get_left_mDD2A2AFDC4B0C1C876093F3E9C405579287FF4F8(/*hidden argument*/NULL);
		NullCheck(L_16);
		L_16->set_direction_5(L_17);
		// } else {
		return;
	}

IL_0073:
	{
		// bulletComponent.direction = Vector2.right;
		Bullet_t1B228DBA3982FDB21DE04329BDC49915421B9059 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_19 = Vector2_get_right_mB4BD67462D579461853F297C0DE85D81E07E911E(/*hidden argument*/NULL);
		NullCheck(L_18);
		L_18->set_direction_5(L_19);
	}

IL_007e:
	{
		// }
		return;
	}
}
// System.Void Weapon::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Weapon__ctor_mF7C215ECC1D7032E6DE76DE606A9159F840B62FB (Weapon_t48BBA717651DD6DFBEA2C0F35AB8A9DF972D94C9 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZuroStreet::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZuroStreet_Awake_mF39B194AFCADDAB34B04D3D49D262BC9EE9B7336 (ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZuroStreet_Awake_mF39B194AFCADDAB34B04D3D49D262BC9EE9B7336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _rigidbody = GetComponent<Rigidbody2D>();
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_0 = Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625_RuntimeMethod_var);
		__this->set__rigidbody_9(L_0);
		// _animator = GetComponent<Animator>();
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_1 = Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719(__this, /*hidden argument*/Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719_RuntimeMethod_var);
		__this->set__animator_10(L_1);
		// }
		return;
	}
}
// System.Void ZuroStreet::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZuroStreet_Start_m36B79E7C1DA92EFFEDD8920CC13F24C189FBB39C (ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ZuroStreet::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZuroStreet_Update_mF55F06C2957CFD22027E9DC239991E181ADB7228 (ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZuroStreet_Update_mF55F06C2957CFD22027E9DC239991E181ADB7228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = __this->get_groundCheck_12();
		NullCheck(L_0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_groundCheckRadius_14();
		LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  L_4 = __this->get_groundLayer_13();
		int32_t L_5 = LayerMask_op_Implicit_m2AFFC7F931005437E8F356C953F439829AF4CFA5(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_tB21970F986016656D66D2922594F336E1EE7D5C7_il2cpp_TypeInfo_var);
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_6 = Physics2D_OverlapCircle_m627FB9EE641A74B942877F57DD2FED656FDA5DC9(L_2, L_3, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_6, /*hidden argument*/NULL);
		__this->set_isGrounded_7(L_7);
		// }
		return;
	}
}
// System.Void ZuroStreet::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZuroStreet_LateUpdate_m0866F695A7D62507AF67715D8439F810D8155A73 (ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZuroStreet_LateUpdate_m0866F695A7D62507AF67715D8439F810D8155A73_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _animator.SetBool("IsGrounded", isGrounded);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = __this->get__animator_10();
		bool L_1 = __this->get_isGrounded_7();
		NullCheck(L_0);
		Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E(L_0, _stringLiteralCC8D7979F8F6B6CAC74FEF89F920AA835E515A9E, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ZuroStreet::OnTriggerStay2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZuroStreet_OnTriggerStay2D_m09FD5E717BF2CBB97B122544254BB7521866D51D (ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * __this, Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZuroStreet_OnTriggerStay2D_m09FD5E717BF2CBB97B122544254BB7521866D51D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (collision.CompareTag("Player")){
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_0 = ___collision0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_mD074608D7FEC96A53C57A456BA613EE01C31D4B7(L_0, _stringLiteralE53407CFE1A5156B9F0D1EED3BAB5EF3AE75CFD8, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// StartCoroutine("Attack");
		MonoBehaviour_StartCoroutine_m590A0A7F161D579C18E678B4C5ACCE77B1B318DD(__this, _stringLiteral1B81F28B5AFBFCAAF5253D26B7DBA4028A92A860, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// }
		return;
	}
}
// System.Void ZuroStreet::AttackMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZuroStreet_AttackMovement_mFC008314CD71D1B0D3D15FD3ED1C5F3F778F78AA (ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZuroStreet_AttackMovement_mFC008314CD71D1B0D3D15FD3ED1C5F3F778F78AA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (numberOfJumps != 0)
		int32_t L_0 = __this->get_numberOfJumps_6();
		if (!L_0)
		{
			goto IL_0058;
		}
	}
	{
		// numberOfJumps = numberOfJumps - 1;
		int32_t L_1 = __this->get_numberOfJumps_6();
		__this->set_numberOfJumps_6(((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1)));
		// _rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_2 = __this->get__rigidbody_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = Vector2_get_up_mC4548731D5E7C71164D18C390A1AC32501DAE441(/*hidden argument*/NULL);
		float L_4 = __this->get_jumpForce_5();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_AddForce_m09E1A2E24DABA5BBC613E35772AE2C1C35C6E40C(L_2, L_5, 1, /*hidden argument*/NULL);
		// _rigidbody.velocity = new Vector2(horizontalVelocity, _rigidbody.velocity.y);
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_6 = __this->get__rigidbody_9();
		float L_7 = __this->get_horizontalVelocity_4();
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_8 = __this->get__rigidbody_9();
		NullCheck(L_8);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = Rigidbody2D_get_velocity_m5ABF36BDF90FD7308BE608667B9E8F3DA5A207F1(L_8, /*hidden argument*/NULL);
		float L_10 = L_9.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_11;
		memset((&L_11), 0, sizeof(L_11));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_11), L_7, L_10, /*hidden argument*/NULL);
		NullCheck(L_6);
		Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83(L_6, L_11, /*hidden argument*/NULL);
	}

IL_0058:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator ZuroStreet::Attack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZuroStreet_Attack_m586D7DE76436AE4116587A2A0A68809FF4D20347 (ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZuroStreet_Attack_m586D7DE76436AE4116587A2A0A68809FF4D20347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE * L_0 = (U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE *)il2cpp_codegen_object_new(U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE_il2cpp_TypeInfo_var);
		U3CAttackU3Ed__17__ctor_mBEBE2F6358D4DA86E68B52BB5F59AB1E5CFC7318(L_0, 0, /*hidden argument*/NULL);
		U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void ZuroStreet::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZuroStreet_OnDisable_mB0DACFD3640047DD56EF34506911FED3EB1F0583 (ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZuroStreet_OnDisable_mB0DACFD3640047DD56EF34506911FED3EB1F0583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// StopCoroutine("Attack");
		MonoBehaviour_StopCoroutine_mC2C29B39556BFC68657F27343602BCC57AA6604F(__this, _stringLiteral1B81F28B5AFBFCAAF5253D26B7DBA4028A92A860, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ZuroStreet::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZuroStreet__ctor_mD0E3CE2D0B16F0EDB9D56CDAFCFA45236C0B79B9 (ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * __this, const RuntimeMethod* method)
{
	{
		// public float jumpForce = 2.5f;
		__this->set_jumpForce_5((2.5f));
		// public int numberOfJumps = 1;
		__this->set_numberOfJumps_6(1);
		// public float aimingTime = 0.5f;
		__this->set_aimingTime_11((0.5f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZuroStreet_<Attack>d__17::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAttackU3Ed__17__ctor_mBEBE2F6358D4DA86E68B52BB5F59AB1E5CFC7318 (U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void ZuroStreet_<Attack>d__17::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAttackU3Ed__17_System_IDisposable_Dispose_m3660D96D37E3C0E1C0426C19F1091F73A456B932 (U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean ZuroStreet_<Attack>d__17::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CAttackU3Ed__17_MoveNext_mD7EA6147D3EE57A0195060599A0A3A6217BB1B49 (U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAttackU3Ed__17_MoveNext_mD7EA6147D3EE57A0195060599A0A3A6217BB1B49_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0057;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (isGrounded == true)
		ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = L_4->get_isGrounded_7();
		if (!L_5)
		{
			goto IL_0070;
		}
	}
	{
		// isAttacking = true;
		ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * L_6 = V_1;
		NullCheck(L_6);
		L_6->set_isAttacking_8((bool)1);
		// horizontalVelocity = 12f * -1f;
		ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * L_7 = V_1;
		NullCheck(L_7);
		L_7->set_horizontalVelocity_4((-12.0f));
		// AttackMovement();
		ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * L_8 = V_1;
		NullCheck(L_8);
		ZuroStreet_AttackMovement_mFC008314CD71D1B0D3D15FD3ED1C5F3F778F78AA(L_8, /*hidden argument*/NULL);
		// yield return new WaitForSeconds(0f);
		WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * L_9 = (WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 *)il2cpp_codegen_object_new(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m8E4BA3E27AEFFE5B74A815F26FF8AAB99743F559(L_9, (0.0f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_9);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0057:
	{
		__this->set_U3CU3E1__state_0((-1));
		// horizontalVelocity = 0f;
		ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * L_10 = V_1;
		NullCheck(L_10);
		L_10->set_horizontalVelocity_4((0.0f));
		// isAttacking = false;
		ZuroStreet_tD859900AFCB3C80F3D7CD4985D56968DD5504256 * L_11 = V_1;
		NullCheck(L_11);
		L_11->set_isAttacking_8((bool)0);
	}

IL_0070:
	{
		// }
		return (bool)0;
	}
}
// System.Object ZuroStreet_<Attack>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CAttackU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18001455E2553F8F415D869737739B8F954AE866 (U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void ZuroStreet_<Attack>d__17::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAttackU3Ed__17_System_Collections_IEnumerator_Reset_m44994CB80C794CD5F3C627870E3B2AC5AC6EAE5F (U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAttackU3Ed__17_System_Collections_IEnumerator_Reset_m44994CB80C794CD5F3C627870E3B2AC5AC6EAE5F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, U3CAttackU3Ed__17_System_Collections_IEnumerator_Reset_m44994CB80C794CD5F3C627870E3B2AC5AC6EAE5F_RuntimeMethod_var);
	}
}
// System.Object ZuroStreet_<Attack>d__17::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CAttackU3Ed__17_System_Collections_IEnumerator_get_Current_m9ECB9C3719CF235DD11691172A1E83EAE151F299 (U3CAttackU3Ed__17_t31710345E684367C5396DF270CDF5FE2818E12FE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t CinemachineVirtualCameraBase_get_Priority_m720C0CC7978B5E520E08B21B8F1E639A17243683_inline (CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * __this, const RuntimeMethod* method)
{
	{
		// public int Priority { get { return m_Priority; } set { m_Priority = value; } }
		int32_t L_0 = __this->get_m_Priority_9();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void CinemachineVirtualCameraBase_set_Priority_m0B66572EE643EE753EFCA3F5D22E34D6AF77A98F_inline (CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int Priority { get { return m_Priority; } set { m_Priority = value; } }
		int32_t L_0 = ___value0;
		__this->set_m_Priority_9(L_0);
		// public int Priority { get { return m_Priority; } set { m_Priority = value; } }
		return;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared_inline (const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = ((EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Value_0();
		return L_0;
	}
}
