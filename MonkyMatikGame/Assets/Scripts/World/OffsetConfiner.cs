﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetConfiner : MonoBehaviour
{
    public GameObject cameraPrefab;
    public int offset = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {
        transform.position = new Vector3(cameraPrefab.transform.position.x - offset, cameraPrefab.transform.position.y, transform.position.z);
    }
}
