﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    private const float PLAYER_DISTANCE_SPAWN_LEVEL = 200f;

    [SerializeField] private Transform levelPart_Start;
    [SerializeField] private List<Transform> levelPartList;
    private int indexDeleteWorld = 0;
    private List<GameObject> levelCreated;
    public GameObject player;

    private Vector3 lastEndPosition;
    private void Awake()
    {
        //InstanciateAllWords();
        levelCreated = new List<GameObject>();
        lastEndPosition = levelPart_Start.Find("EndPosition").position;
    
        int startingSpawnLevelParts = 2;

        for (int i = 0; i < startingSpawnLevelParts; i++)
        {
            SpawnLevelPart();
        }

      
    }


    private void SpawnLevelPart()
    {
        Transform choosenLevelPart = levelPartList[Random.Range(0, levelPartList.Count)];
        Transform lastLevelPartTransform = SpawnLevelPart(choosenLevelPart, lastEndPosition);
        lastEndPosition = lastLevelPartTransform.Find("EndPosition").position;
    
    }
    private Transform SpawnLevelPart(Transform levelPart, Vector3 spawnPosition)
    {
        Transform levelPartTransform = Instantiate(levelPart, spawnPosition, Quaternion.identity);
        
        levelCreated.Add(levelPartTransform.gameObject);

        if (levelCreated.Count > 5)
        {
            Debug.Log("Destruir mundo" +indexDeleteWorld);
            GameObject firstLevelCreated = levelCreated[indexDeleteWorld];
            indexDeleteWorld += 1;
            Destroy(firstLevelCreated);
        }
        
        else
        {
            Debug.Log("No destruimos nada");
        }
        return levelPartTransform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(player.transform.position, lastEndPosition) < PLAYER_DISTANCE_SPAWN_LEVEL)
        {
            Debug.Log("LevelCreated");
            SpawnLevelPart();
        }
    }
}
