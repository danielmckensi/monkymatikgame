﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayManager : MonoBehaviour
{
    private int currentScore;
    private bool isGamePaused;
    private bool isStartedGame = false;
    private bool isPlayerDead = false;
    private float maxCameraZoom = 15;
    private float normalCameraZoom = 10;
    private bool isFarCamera;

    public void updateScore(int score)
    {
        currentScore = score;
    }

    public void updateGameState(bool state)
    {
        isGamePaused = state;
    }

    public void updateGameIsStarted(bool state)
    {
        isStartedGame = state;
    }

    public void updatePlayerDeadState(bool state)
    {
        isPlayerDead = state;
    }

    public int getScore()
    {
        return currentScore;
    }

    public bool getStateGame()
    {
        return isGamePaused;
    }

    public bool getIsStartedGame()
    {
        return isStartedGame;
    }

    public bool getIsPlayerDead()
    {
        return isPlayerDead;
    }

    public void updateCameraZoom(bool isFarCameraByMap)
    {
        isFarCamera = isFarCameraByMap;
    }

    public float getCameraZoom()
    {
        if(isFarCamera == true)
        {
            return maxCameraZoom;
        }
        else
        {
            return normalCameraZoom;
        }
    }

    public bool getIsFarCamera()
    {
        return isFarCamera;
    }
}
