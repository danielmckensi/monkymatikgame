﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreMenu : MonoBehaviour
{
    private GamePlayManager gamePlayManager;
    public static bool gameIsPaused = false;
    public GameObject resumeMenu;

    private void Awake()
    {
        gamePlayManager = GameObject.FindObjectOfType<GamePlayManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void Resume()
    {
        resumeMenu.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
        gamePlayManager.updateGameState(gameIsPaused);
    }

    public void Pause()
    {
        resumeMenu.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
        gamePlayManager.updateGameState(gameIsPaused);
    }
}
