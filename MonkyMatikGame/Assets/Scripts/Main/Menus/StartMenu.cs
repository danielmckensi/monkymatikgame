﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenu : MonoBehaviour
{
    public GameObject startMenu;
    private GamePlayManager gamePlayManager;

    private void Awake()
    {
        gamePlayManager = GameObject.FindObjectOfType<GamePlayManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        startMenu.SetActive(false);
        gamePlayManager.updateGameIsStarted(true);

    }
}
