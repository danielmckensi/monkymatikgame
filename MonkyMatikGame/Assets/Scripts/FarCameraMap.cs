﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarCameraMap : MonoBehaviour
{
    // Start is called before the first frame update
    private GamePlayManager gamePlayManager;

    private void Awake()
    {
        gamePlayManager = GameObject.FindObjectOfType<GamePlayManager>();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Pidiendo que se aleje la camara");
            gamePlayManager.updateCameraZoom(true);
        }
    }
}
