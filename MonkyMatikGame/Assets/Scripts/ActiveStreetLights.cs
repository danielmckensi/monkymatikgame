﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveStreetLights : MonoBehaviour
{
    private Vector2 _movement;
    private Rigidbody2D _rigidbody;
    public bool isToRightLight = false;
    public float speed = 2.5f;
    public List<GameObject> lights;
    private SpriteRenderer renderer;

    // Start is called before the first frame update
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        renderer = GetComponent<SpriteRenderer>();
        renderer.enabled = false;
        lightsOff();
    }
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void lightsOff()
    {
        for (int i = 0; i < lights.Count; i ++)
        {
            lights[i].gameObject.SetActive(false);
        }
    }

    private void lightsOn()
    {
        for (int i = 0; i < lights.Count; i++)
        {
            lights[i].gameObject.SetActive(true);
        }
    }



    private void FixedUpdate()
    {
        if (_movement != null)
        {
            float horizontalVelocity = _movement.normalized.x * speed;
            _rigidbody.velocity = new Vector2(horizontalVelocity, _rigidbody.velocity.y);
        }
      
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
       


        if (collision.CompareTag("Player"))
        {
            renderer.enabled = true;
            lightsOn();
            if (isToRightLight)
            {
     
                moveLightRight();
            }
            else
            {
                moveLightLeft();
            }
        }
    }

    void moveLightRight()
    {
        _movement = new Vector2(1f, 0f);
    }

    void moveLightLeft()
    {
        _movement = new Vector2(-1f, 0f);
    }

    private void OnDisable()
    {

    }
}
