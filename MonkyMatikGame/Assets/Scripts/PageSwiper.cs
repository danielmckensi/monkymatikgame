﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class PageSwiper : MonoBehaviour, IDragHandler, IEndDragHandler
{

    private Vector3 panelLocation;
    public float percentThreshold = 0.2f;
    public float easing = 0.5f;
    private int numberOfPages = 2;
    private int currentPage = 1;

    // Start is called before the first frame update
    void Start()
    {
        panelLocation = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("Haciendo drag");
        float difference = eventData.pressPosition.x - eventData.position.x;
        transform.position = panelLocation - new Vector3(difference, 0, 0);

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        

        float percentaje = (eventData.pressPosition.x - eventData.position.x) / Screen.width;

        if(Mathf.Abs(percentaje) >= percentThreshold)
        {
            Vector3 newLocation = panelLocation;
            if(percentaje > 0 && currentPage < numberOfPages)
            {
                currentPage++;
                newLocation += new Vector3(-Screen.width, 0, 0);
            }else if(percentaje < 0 && currentPage > 1)
            {
                currentPage--;
                newLocation += new Vector3(Screen.width, 0, 0);
            }
      
            StartCoroutine(SmoothMove(transform.position, newLocation, easing));
            panelLocation = newLocation;
        }
        else
        {
            StartCoroutine(SmoothMove(transform.position, panelLocation, easing));
        }
    }

    IEnumerator SmoothMove(Vector3 startpos, Vector3 endPos, float seconds)
    {
        float t = 0f;

        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            transform.position = Vector3.Lerp(startpos, endPos, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

}
