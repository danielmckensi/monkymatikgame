﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BejucoHealth : MonoBehaviour
{
    private SpriteRenderer _renderer;
    public int totalHealth = 1;
    private int health;
    public GameObject retryMenu;
    private GamePlayManager gamePlayManager;
    // Start is called before the first frame update
    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
        gamePlayManager = GameObject.FindObjectOfType<GamePlayManager>();
    }
    void Start()
    {
        health = totalHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void addDamage(int amount)
    {
        health = health - amount;
        StartCoroutine("VisualFeedBack");

        //GameOver

        if (health <= 0)
        {
            health = 0;
           
            gamePlayManager.updateGameIsStarted(false);
            gamePlayManager.updateGameState(true);
            gamePlayManager.updatePlayerDeadState(true);
        }

        Debug.Log("Player got damaged. His current heath is " + health);
    }

    public void AddHeath(int amount)
    {
        health = health + amount;

        if (health > totalHealth)
        {
            health = totalHealth;
        }

        Debug.Log("Player got some life. His current heath is " + health);
    }

    private IEnumerator VisualFeedBack()
    {
        _renderer.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        _renderer.color = Color.white;

    }

    public void showRetryMenu()
    {
        retryMenu.SetActive(true);
    }


}
