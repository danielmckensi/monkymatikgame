﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitPlayer : MonoBehaviour
{

    public GameObject playerPrefab;
    // Start is called before the first frame update
    void Start()
    {
        reStartPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void reStartPlayer()
    {
        GameObject player = Instantiate(playerPrefab, this.transform.position, Quaternion.identity, this.transform);
    }
}
