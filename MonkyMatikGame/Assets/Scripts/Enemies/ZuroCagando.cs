﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZuroCagando : MonoBehaviour
{

    public GameObject boyoPrefab;
    private Transform firePoint;
    private Animator animator;
    private GamePlayManager gamePlayManager;
    public int numberBoyos = 3;
    private int boyoIndex = 1;
    // Start is called before the first frame update
    void Start()
    {
    
    }

    private void Awake()
    {
        firePoint = transform.Find("BoyoPoint");
        animator = GetComponent<Animator>();
        gamePlayManager = GameObject.FindObjectOfType<GamePlayManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Cagar()
    {
        if (boyoIndex <= numberBoyos)
        {
            bool stateGame = gamePlayManager.getIsStartedGame();
            if (boyoPrefab != null && firePoint != null && stateGame != false)
            {
                GameObject boyo = Instantiate(boyoPrefab, firePoint.position, Quaternion.identity) as GameObject;
            }
            boyoIndex += 1;
            //Debug.Log("Cagando");
        }
      
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            animator.SetBool("IsCagando", true);
        }
    }
}
