﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZuriskyIntro : MonoBehaviour
{
    private Rigidbody2D rigidbody;
    private Vector2 movement;
    private Transform firePoint;
    public int speed = 1;
    public GameObject zuroPrefab;
    public int numberOfZuros = 1;
    private int zuros = 0;

    private void Awake()
    {
        firePoint = transform.Find("FirePoint");
        movement = new Vector2(1f, 0f);
        rigidbody = GetComponent<Rigidbody2D>();

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        moveZurisky();
    }

    void moveZurisky()
    {
        float horizontalVelocity = movement.normalized.x * speed;
        rigidbody.velocity = new Vector2(horizontalVelocity, rigidbody.velocity.y);
    }

    public void LanzarZuro()
    {

        if (zuroPrefab != null && firePoint != null)
        {
            GameObject zuro = Instantiate(zuroPrefab, firePoint.position, Quaternion.identity) as GameObject;
        }

        //Debug.Log("Cagando");
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(zuros != numberOfZuros)
        {
            if (collision.CompareTag("Menu"))
            {
                LanzarZuro();
                zuros += 1;
            }
        }
        
    }
}
