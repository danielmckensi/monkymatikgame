﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZuroStreet : MonoBehaviour
{

    public int damage = 1;
    public float horizontalVelocity;
    public float jumpForce = 2.5f;
    public int numberOfJumps = 1;
    private bool isGrounded;
    private Rigidbody2D _rigidbody;
    private Animator _animator;
    public float aimingTime = 0.5f;

    public Transform groundCheck;
    public LayerMask groundLayer;
    public float groundCheckRadius;

    // Start is called before the first frame update

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
    }

    private void LateUpdate()
    {
        _animator.SetBool("IsGrounded", isGrounded);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")){
            StartCoroutine("Attack");
        }
    }

    private void AttackMovement ()
    {
       
        if (numberOfJumps != 0)
        {
            numberOfJumps = numberOfJumps - 1;
            _rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            _rigidbody.velocity = new Vector2(horizontalVelocity, _rigidbody.velocity.y);
        }
        
    }

    private IEnumerator Attack()
    {
        if (isGrounded == true)
        {
        
            horizontalVelocity = 12f * -1f;
            AttackMovement();
            yield return new WaitForSeconds(0f);
            horizontalVelocity = 0f;
    
        }
        
    }

    private void OnDisable()
    {
        StopCoroutine("Attack");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
       
            if (collision.CompareTag("PlayerHurt"))
            {
                collision.SendMessageUpwards("addDamage", damage);
                //Debug.Log("Encontre a Bejuco");
            }
      
       
    }
}
