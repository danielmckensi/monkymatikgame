﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZuriskyZombie : MonoBehaviour
{
    public int damage = 1;
    public float horizontalVelocity;
    public float jumpForce = 2.5f;
    public int numberOfJumps = 1;
    private bool isGrounded;
    private Rigidbody2D _rigidbody;
    private Animator _animator;
    public float aimingTime = 0.5f;
    private bool isRuning;
    private Vector2 _movement;

    public Transform groundCheck;
    public LayerMask groundLayer;
    public float groundCheckRadius;
    // Start is called before the first frame update

    private void Awake()
    {
        isRuning = false;
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
        AttackMovement();
    }

    private void LateUpdate()
    {
        //_animator.SetBool("IsGrounded", isGrounded);
   
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            isRuning = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("PlayerHurt"))
        {
            collision.SendMessageUpwards("addDamage", damage);
            //Debug.Log("Encontre a Bejuco");
        }


    }

    private void AttackMovement()
    {

        if (isRuning == true)
        {
            _animator.SetTrigger("Attack");
            _movement = new Vector2(1f, 0f);
            float horizontalVelocity = _movement.normalized.x * 10;
            _rigidbody.velocity = new Vector2(horizontalVelocity, _rigidbody.velocity.y);
        }

    }
}
