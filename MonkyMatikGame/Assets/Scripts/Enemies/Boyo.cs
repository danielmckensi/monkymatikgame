﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boyo : MonoBehaviour
{
    private bool isGrounded;

    public int damage = 1;
    public float speed;

    public Transform groundCheck;
    public LayerMask groundLayer;
    public float groundCheckRadius;
    private Animator animator;

    private Rigidbody2D rigidbody;
    private Vector2 movement;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody2D>();

        float localScaleX = transform.localScale.x;
        localScaleX = localScaleX * -1f;
        transform.localScale = new Vector3(localScaleX, transform.localScale.y, transform.localScale.z);
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
        walk();
    }

    private void FixedUpdate()
    {
        if (isGrounded == true)
        {
            float horizontalVelocity = movement.normalized.x * speed;
            rigidbody.velocity = new Vector2(horizontalVelocity, rigidbody.velocity.y);
        }
    }

    private void LateUpdate()
    {
        animator.SetBool("IsGrounded", isGrounded);
    }

    private void walk()
    {
        movement = new Vector2(-1f, 0f);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("PlayerHurt"))
        {
            collision.SendMessageUpwards("addDamage", damage);
            Debug.Log("Encontre a Bejuco");
        }


    }
}
